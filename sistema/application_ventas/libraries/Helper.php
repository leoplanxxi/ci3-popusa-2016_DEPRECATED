<?php

Class Helper {
    public function __construct() {
        parent::__construct();
    }

    public function enviarCorreoConfirmacion($correo) {

        $cc = "leoplanxxi@gmail.com"; // Asignar a correos reales

        $message = '<table style="width: 500px; margin: 0 auto; display: block; font-family: arial; font-size: 14px; line-height: 1.5;"><tr><td style="text-align: justify">&nbsp;</td></tr><tr><td>&nbsp;</td></tr><tr><td style="text-align: justify">Estimada/o cliente:</td></tr><tr><td>&nbsp;</td></tr><tr><td style="text-align: justify">Su cotización ha sido enviada y a la brevedad un vendedor se comunicará con usted.</td></tr><tr><td>&nbsp;</td></tr><tr><td style="text-align: justify">Grupo Industrial Popusa le agradece su preferencia.</td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr></table>';

        $this->CI->load->library('email');
        $config['protocol']    = 'mail';
        $config['charset']    = 'utf-8';

        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not

        $this->CI->email->initialize($config);
        $this->CI->email->set_crlf("\r\n");

        $this->CI->email->clear();
        $this->CI->email->from('no-reply@popusa.com.mx');
        $this->CI->email->to($correo);
		$this->CI->email->cc($cc);
        $this->CI->email->subject('Grupo Industrial Popusa le agradece su preferencia');
        $this->CI->email->message($message);

        if ($this->CI->email->send() === TRUE) {
			return true;
        } else {
            return false;
        }
    }
}
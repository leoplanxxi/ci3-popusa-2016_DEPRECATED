<!-- Modal -->
<div class="modal fade" id="cotizador" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel"><b>Cotización Realizada</b></h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="center">
        <?php if($this->session->userdata('popusa')[0]['rol_idrol'] == 2): ?>
          <div class="col-md-12">
            Se ha creado la cotización.
          </div>
          <br>
          <div class="col-md-12">
            <a href="<?php echo base_url('admin/index.php/cotizaciones/cotizacion/'.$this->session->userdata('folio')) ?>" target="_blank" class="btn btn-primary">Ver Cotización</a>
          </div>
          &#160;<br>
        <?php elseif($this->session->userdata('popusa')[0]['rol_idrol'] == 6): ?>

          <div class="col-md-12">
            Se ha creado la orden de pedido.
          </div>
          <div class="col-md-12">
            <a href="<?php echo base_url('admin/index.php/ordenes') ?>" target="_blank" class="btn btn-primary">Ver Orden</a>
          </div>
          &#160;

        <?php else: ?>
          Se ha realizado la cotización, un asesor de ventas se comunicara con usted.
          <?php $this->session->unset_userdata('folio'); ?>          
        <?php endif; ?>
      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
      </div>
    </div>
  </div>
</div>

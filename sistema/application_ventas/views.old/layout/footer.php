
    <div class="container-fluid" style="padding:0px;">
    <div class='navbar navbar-popusa-footer navbar-static-bottom footer'>

      <div class="col-md-12 footer-popusa">

        <div class="col-md-2 col-xs-6 footer-popusa-alianza">
          <img class="img-responsive" src="<?php echo base_url('public/imagenes/logo-alianza.png');?>" height="150px;">
        </div>

        <div class="col-md-2 col-xs-6 info">
            <div class="url-popusa">
              <p><b><a href="#">Aviso de Privacidad</a></b></p>
              <p><b><a href="#">Ubicación</a></b></p>
              <p><b><a href="#">Mapa del sitio</a></b></p>
            </div>
        </div>

        <div class="col-md-4 col-xs-12 rsp-lg logo">
            <div class="semi-circulo" align="center">
              <img class="img-responsive" src="<?php echo base_url('public/imagenes/logo-popusa.png');?>" width="80%">
            </div>
			<div class="info-popusa">
              <p>Grupo Industrial POPUSA</p>
              <div style="font-size:10px;color:#606062;">
              23 poniente 918<br>
              Colonia Chulavista<br>
              Tel.(222)2409999<br>
              Puebla, Puebla, México<br>
              </div>
              <p>telemarketing@popusa.com.mx</p>
            </div>
        </div>

        <div class="col-md-2 col-xs-6 redes">
          <div class="redes-popusa" align="center">
            <div class="col-md-4 col-xs-4">
            <img class="img-responsive" src="<?php echo base_url('public/imagenes/facebook.png');?>">
            </div>
            <div class="col-md-4 col-xs-4">
            <img class="img-responsive" style="padding-top: 8px" src="<?php echo base_url('public/imagenes/twitter.png');?>">
          </div>
          <div class="col-md-4 col-xs-4">
            <img class="img-responsive" style="padding-top: 8px" src="<?php echo base_url('public/imagenes/youtube.png');?>">
          </div>
          </div>
        </div>

        <div class="col-md-2 col-xs-6  footer-popusa-paket" align="center">
          <img class="img-responsive" src="<?php echo base_url('public/imagenes/logo-paket.png');?>" height="150px;">
        </div>

      </div>
</div>
</div>

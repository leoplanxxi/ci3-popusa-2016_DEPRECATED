<style>
.error{
  color:red;
}
</style>
<div class="col-md-4 col-md-offset-4" style="margin-top:60px;">
  <div align="center">
    <img src="http://132.148.18.129/sistema-popusa/public/imagenes/logo-popusa.png" class="img-responsive" />
  </div>
  &#160;

  <div class="panel panel-default">
    <?= form_open('index.php/registro');?>
    <div class="panel-body">
      <div class="">
        <h3><b>Crear Cuenta</b></h3>
      </div>
      <div class="fomr-input">
        <?php $data = array(
          'name' => 'nombre',
          'type' => 'text',
          'class' => 'form-control',
          'value' => $this->input->post('nombre'),
          'placeholder' => 'Nombre Completo',
          'required' => true
        ); ?>
        <?= form_input($data)?>
        <?php echo form_error('nombre', '<div class="error">', '</div>'); ?>
      </div>
      &#160;
      <div class="fomr-input">
        <?php $data = array(
          'name' => 'rfc',
          'type' => 'text',
          'class' => 'form-control',
          'value' => $this->input->post('rfc'),
          'placeholder' => 'RFC',
          'required' => true
        ); ?>
        <?= form_input($data)?>
        <?php echo form_error('rfc', '<div class="error">', '</div>'); ?>
      </div>
      &#160;
      <div class="fomr-input">
        <?php $data = array(
          'name' => 'telefono',
          'type' => 'number',
          'class' => 'form-control',
          'value' => $this->input->post('telefono'),
          'placeholder' => 'Teléfono',
          'required' => true
        ); ?>
        <?= form_input($data)?>
        <?php echo form_error('telefono', '<div class="error">', '</div>'); ?>
      </div>
      &#160;
      <div class="fomr-input">
        <?php $data = array(
          'name' => 'email',
          'type' => 'email',
          'class' => 'form-control',
          'value' => $this->input->post('email'),
          'placeholder' => 'E-mail',
          'required' => true
        ); ?>
        <?= form_input($data)?>
        <?php echo form_error('email', '<div class="error">', '</div>'); ?>
     </div>
    &#160;
    <div class="fomr-input">
      <?php $data = array(
        'name' => 'direccion',
        'type' => 'text',
        'class' => 'form-control',
        'value' => $this->input->post('direccion'),
        'placeholder' => 'Dirección (Calle,Número,Colonia, Ciudad, Estado)',
        'required' => true,
        'rows' => 4
      ); ?>
      <?= form_textarea($data)?>
      <?php echo form_error('direccion', '<div class="error">', '</div>'); ?>
   </div>
   &#160;
   <div class="fomr-input">
     <?php $data = array(
       'name' => 'pass1',
       'type' => 'password',
       'class' => 'form-control',
       'value' => '',
       'placeholder' => 'Contraseña',
       'required' => true,
     ); ?>
     <?= form_password($data)?>
     <?php echo form_error('pass1', '<div class="error">', '</div>'); ?>
    </div>
    &#160;
    <div class="fomr-input">
      <?php $data = array(
        'name' => 'pass2',
        'type' => 'password',
        'class' => 'form-control',
        'value' => '',
        'placeholder' => 'Vuelve a escribir la contraseña',
        'required' => true,
      ); ?>
      <?= form_password($data)?>
      <?php echo form_error('pass2', '<div class="error">', '</div>'); ?>
   </div>
   &#160;
   <div class="form-group">
      <button class="btn btn-block btn-warning">Crea cuenta Popusa</button>
   </div>
  </div>
  <?= form_close();?>
  <hr>
</div>

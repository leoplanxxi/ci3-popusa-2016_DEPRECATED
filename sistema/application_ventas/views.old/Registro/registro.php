<style>
.error{
  color:red;
}
.alert {
    padding: 2px;
    margin-bottom: 10px;
    text-align: center;

}
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
<div class="col-md-8 col-md-offset-2" style="margin-top:60px;">
  <div align="center">
    <img src="http://132.148.18.129/sistema-popusa/public/imagenes/logo-popusa.png" class="img-responsive" />
  </div>
  &#160;

  <div class="panel panel-default">
    <?= form_open('index.php/registro','id=""');?>
    <div class="panel-body">
      <div class="">
        <h3><b>Crear Cuenta</b></h3>
        <?php echo validation_errors(); ?>
      </div>
      <hr>
      <div class="col-md-12">
        <!--Datos Personales-->
        <div class="form-group col-md-4">
          <?php echo form_label('Rázon Social','class="control-label"')  ?>
          <?php $data = array(
              'name' => 'razon',
              'type' => 'text',
              'class' => 'form-control',
              'value' => $this->input->post('razon'),
              'placeholder' => 'Rázon Social',
              'required' => true
            );
            echo form_input($data);
            echo form_error('nombre','<div class="error">','</div');
            ?>
            <div class="help-block with-errors"></div>
          </div>

          <div class="form-group col-md-4">
            <?php echo form_label('RFC','class="control-label"')  ?>
            <?php $data = array(
              'name' => 'rfc',
              'type' => 'text',
              'class' => 'form-control',
              'id' => 'rfc_mask',
              'value' => $this->input->post('rfc'),
              'placeholder' => 'RFC',
              'required' => true,
              'minlength' => 10,
              'maxlength' => 13,
              'pattern'   => "^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))([A-Z\d]{3})?$"
            ); ?>
            <?= form_input($data)?>
            <?php echo form_error('rfc', '<div class="error">', '</div>'); ?>
            <div class="help-block with-errors"></div>
          </div>

          <div class="form-group col-md-4">
            <?php echo form_label('Nombre Completo','class="control-label"')  ?>
            <?php $data = array(
              'name' => 'nombre',
              'type' => 'text',
              'class' => 'form-control',
              'value' => $this->input->post('nombre'),
              'placeholder' => 'Nombre Completo',
              'required' => true
            ); ?>
            <?= form_input($data)?>
            <?php echo form_error('nombre', '<div class="error">', '</div>'); ?>
            <div class="help-block with-errors"></div>
          </div>
          <!--Datos Personales-->

          <div class="form-group col-md-4">
            <?php echo form_label('E-mail','class="control-label"')  ?>
            <?php $data = array(
              'name' => 'email',
              'type' => 'email',
              'class' => 'form-control',
              'value' => $this->input->post('email'),
              'placeholder' => 'E-mail',
              'required' => true
            ); ?>
            <?= form_input($data)?>
            <?php echo form_error('email', '<div class="error">', '</div>'); ?>
            <div class="help-block with-errors"></div>
         </div>

          <div class="form-group col-md-4">
            <?php echo form_label('Teléfono','class="control-label"')  ?>
            <?php $data = array(
              'name' => 'telefono',
              'type' => 'text',
              'class' => 'form-control',
              'value' => $this->input->post('telefono'),
              'placeholder' => 'Teléfono',
              'required' => true,
              'id' => 'phone-mask'
            ); ?>
            <?= form_input($data)?>
            <?php echo form_error('telefono', '<div class="error">', '</div>'); ?>
            <div class="help-block with-errors"></div>
          </div>

          <div class="form-group col-md-4">
            <?php echo form_label('Celular','class="control-label"')  ?>
            <?php
            $data = array(
              'name' => 'celular',
              'type' => 'text',
              'class' => 'form-control',
              'value' => $this->input->post('celular'),
              'placeholder' => 'Celular',
              'required' => true,
              'id' => 'cel-mask'
            );
            echo form_input($data);
            echo form_error('celular','<div class="error">','</div>');
            ?>
            <div class="help-block with-errors"></div>
          </div>
          <!---->
          <div class="form-group col-md-4">
            <?php echo form_label('País','class="control-label"')  ?>
            <?php
              foreach ($pais as $ps) {
                $mpais[$ps['id'].":".$ps['paisnombre']] = $ps['paisnombre'];
              }
            ?>
            <?php
            if($this->input->post('pais',true)){
              $spais = $this->input->post('pais',true);
            }else{
              $spais = "42:México";
            }
            echo  form_dropdown('pais',$mpais,$spais,'class="form-control" placeholder="País" required="true" id="pais" onChange="estados()"')?>
            <?php echo form_error('pais', '<div class="error">', '</div>'); ?>
            <div class="help-block with-errors"></div>
          </div>

          <div class="form-group col-md-4" id="estado">

          </div>

          <div class="form-group col-md-4">
          <?php echo form_label('Código Postal','class="control-label"')  ?>
          <?php $data = array(
            'name' => 'cp',
            'type' => 'text',
            'class' => 'form-control',
            'value' => $this->input->post('cp'),
            'placeholder' => 'Código Postal',
            'id' => 'cp',
            'minlength' => 4,
            'maxlength' => 5,
            'onChange' => 'info_cp()',
            'required' => true,
          ); ?>
          <?= form_input($data)?>
          <?php echo form_error('cp', '<div class="error">', '</div>'); ?>
          <div class="help-block with-errors"></div>
          </div>

          <div class="form-group col-md-6">
           <?php echo form_label('Ciudad/Municipio','class="control-label"')  ?>
           <?php $data = array(
             'name' => 'ciudad',
             'type' => 'text',
             'class' => 'form-control',
             'value' => $this->input->post('ciudad'),
             'placeholder' => 'Ciudad',
             'required' => true,
             'id' => 'ciudad'
           ); ?>
           <?= form_input($data)?>
           <?php echo form_error('ciudad', '<div class="error">', '</div>'); ?>
           <div class="help-block with-errors"></div>
         </div>


       <div class="form-group col-md-6" id="colonia">
         <?php echo form_label('Colonia','class="control-label"')  ?>
         <?php $data = array(
           'name' => 'colonia',
           'type' => 'text',
           'class' => 'form-control',
           'value' => $this->input->post('colonia'),
           'placeholder' => 'Colonia',
           'required' => true
         ); ?>
         <?= form_input($data)?>
         <?php echo form_error('colonia', '<div class="error">', '</div>'); ?>
         <div class="help-block with-errors"></div>

        </div>


      <div class="form-group col-md-6">
        <?php echo form_label('Calle','class="control-label"')  ?>
        <?php $data = array(
          'name' => 'calle',
          'type' => 'text',
          'class' => 'form-control',
          'value' => $this->input->post('calle'),
          'placeholder' => 'Calle',
          'required' => true
        ); ?>
        <?= form_input($data)?>
        <?php echo form_error('calle', '<div class="error">', '</div>'); ?>
        <div class="help-block with-errors"></div>

     </div>

      <div class="form-group col-md-3">
        <?php echo form_label('Número Exterior','class="control-label"')  ?>
        <?php $data = array(
          'name' => 'numeroe',
          'type' => 'text',
          'class' => 'form-control',
          'value' => $this->input->post('numeroe'),
          'placeholder' => 'Número Exterior',
          'required' => true
        ); ?>
        <?= form_input($data)?>
        <?php echo form_error('numeroe', '<div class="error">', '</div>'); ?>
        <div class="help-block with-errors"></div>

     </div>

     <div class="form-group col-md-3">
       <?php echo form_label('Número Interior','class="control-label"')  ?>
       <?php $data = array(
         'name' => 'numeroi',
         'type' => 'text',
         'class' => 'form-control',
         'value' => $this->input->post('numeroi'),
         'placeholder' => 'Número Interior'
       ); ?>
       <?= form_input($data)?>
       <?php echo form_error('numeroi', '<div class="error">', '</div>'); ?>
       <div class="help-block with-errors"></div>
     </div>

  <div class="form-group col-md-6">
    <?php echo form_label('Contraseña','class="control-label"')  ?>
    <?php $data = array(
      'name' => 'pass1',
      'type' => 'password',
      'class' => 'form-control',
      'value' => '',
      'placeholder' => 'Contraseña',
      'required' => true,
      'pattern' => "^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$",
      'minlength' => 8,
      'maxlength' => 8
    ); ?>
    <?= form_password($data)?>
    <?php echo form_error('pass1', '<div class="error">', '</div>'); ?>
    <div class="help-block with-errors"></div>
    <div class="alert alert-info">
    <small>La contraseña debe de ser de 8 caracteres y por lo menos una letra y un número.</small>
    </div>
   </div>

    <div class="form-group col-md-6">
      <?php echo form_label('Vuelve a escribir la contraseña','class="control-label"')  ?>
      <?php $data = array(
        'name' => 'pass2',
        'type' => 'password',
        'class' => 'form-control',
        'value' => '',
        'placeholder' => 'Vuelve a escribir la contraseña',
        'required' => true,
        'pattern' => "^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$",
        'minlength' => 8,
        'maxlength' => 8
      ); ?>
      <?= form_password($data)?>
      <?php echo form_error('pass2', '<div class="error">', '</div>'); ?>
      <div class="help-block with-errors"></div>
      <div class="alert alert-info">
      <small>La contraseña debe de ser identica a la anterior</small>
      </div>
     </div>


        <div class="form-group col-md-4">
          <?php echo form_label('Método de pago','class="control-label"')  ?>
          <?php
           $metodo = array(
             '0' => 'Seleccione',
             '01 Efectivo' => 'Efectivo',
             '02 Cheque Nominativo' => 'Cheque Nominativo',
             '03 Transferencia electrónica' => 'Transferencia electrónica',
             '04 Tarjeta de Crédito' => 'Tarjeta de Crédito',
             '28 Tarjeta de Débito' => 'Tarjeta de Débito',
             '99 Por Definir' => 'Por Definir'
           );

           echo form_dropdown('metodo',$metodo,$this->input->post('metodo'),'class="form-control"');
           echo form_error('metodo','<div class="error">','</div>');
           ?>
           <div class="help-block with-errors"></div>

        </div>

     <div class="form-group col-md-4">
       <?php echo form_label('Uso de CFDI','class="control-label"')  ?>
       <?php $cfdi = array(
         '0' => 'Seleccione',
         'G01 Adquisición de Mercancías' => 'Adquisición de Mercancías',
         'G02 Devoluciones, Descuentos o Bonificaciones' => 'Devoluciones, Descuentos o Bonificaciones',
         'G03 Gastos en General' => 'Gastos en General'
       );
       echo form_dropdown('cfdi',$cfdi,$this->input->post('cfdi'),'class="form-control"');
       echo form_error('cfdi','<div class="error">','</div>');
       ?>
       <div class="help-block with-errors"></div>

     </div>

     <div class="form-group col-md-4">
       <?php echo form_label('Forma de pago','class="control-label"')  ?>
       <?php
       $forma = array(
         '0' => 'Seleccione',
         'PUE- En una sola Exhibición' => 'En una sola Exhibición',
         'PPD- En parcialidades o Diferido' => 'En parcialidades o Diferido'
       );
       echo form_dropdown('forma',$forma,$this->input->post('forma'),'class="form-control"');
       echo form_error('forma',$forma,$this->input->post('forma'),'class="form-control"');
       ?>
       <div class="help-block with-errors"></div>
     </div>

</div>

  </div>
  <div class="panel-footer">
    <div class="form-group">
       <button type="submit" class="btn btn-block btn-warning">Crea cuenta Popusa</button>
    </div>
  </div>
  <?= form_close();?>
</div>
<script src="<?php echo base_url('public/js/imask.js') ?>"></script>
<script>
$('#registro').validator().on('submit', function (e) {
  console.log(e);
  if (e.isDefaultPrevented()) {
    // handle the invalid form...
  } else {
    // everything looks good!
  }
})
  $(document).ready(function(){
    var id = document.getElementById('pais').value;
    $.post('<?php echo base_url("index.php/registro/estado");?>',{'mpais':id,'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(item){
      $('#estado').html(item);
    });
    info_cp();
  });
  <?php if($this->input->post('colonia',true)): ?>
    document.getElementById('colonias').value = <?php echo $this->input->post('colonia',true);  ?>
  <?php endif; ?>
function estados(){
  var id = document.getElementById('pais').value;
  $.post('<?php echo base_url("index.php/registro/estado");?>',{'mpais':id,'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(item){
    $('#estado').html(item);
  });

}

function info_cp(){
  $.post('<?php echo base_url('index.php/registro/get_cp_info'); ?>',{cp:document.getElementById('cp').value,'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(item){
    if(JSON.parse(item)!=null){
      var val = JSON.parse(item);
      $('#ciudad').val(val[0].Municipio_cp);
      $('#mestado').val(val[0].Estado_cp);
      $.post('<?php echo base_url('index.php/registro/get_colonias')?>',{c:val[0].Colonia_cp,'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(item){
        $('#colonia').html(item);
      });
    }else{
      $('#ciudad').val('');
      $('#estado').val('');
      $.post('<?php echo base_url('index.php/registro/get_colonias/1')?>',{'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(item){
        $('#colonia').html(item);
      });
    }
  });
}

var phoneMask = new IMask(
  document.getElementById('phone-mask'), {
    mask: '(000)000-00-00'
  });
  var celMask = new IMask(
    document.getElementById('cel-mask'), {
      mask: '0000-00-00-00'
    });
</script>

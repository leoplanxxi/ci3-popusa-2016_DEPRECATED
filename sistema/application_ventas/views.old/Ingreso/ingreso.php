<!-- Modal -->
<div class="modal fade" id="ingresar">
  <div class="modal-dialog modal-sm" role="document">

    <div class="modal-content">
      <?php if($this->session->flashdata('error')): ?>
      <div class="alert alert-danger alert-dismissible" role="alert" align="center">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong><?php echo $this->session->flashdata('error') ?></strong>
      </div>
    <?php endif; ?>
    <?php if($this->session->flashdata('ok')): ?>
    <div class="alert alert-success alert-dismissible" role="alert" align="center">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <strong><?php echo $this->session->flashdata('ok') ?></strong>
    </div>
  <?php endif; ?>
      <div class="modal-header" align="center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Ingreso de Usuarios</h4>
      </div>

      <?php echo form_open('index.php/login/ingresar'); ?>
      <div class="modal-body">
        <div class="input-group">
          <label class="control-label">Correo Electrónico</label>
        </div>
        <div class="input-group">
          <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
          <?php $data = array(
            'name' => 'usuario',
            'type' => 'text',
            'class' => 'form-control',
            'id' => 'usuario',
            'placeholder' => 'Correo Electrónico',
            'required' => true
          );?>
          <?= form_input($data);?>
        </div>
        &#160;
        <div class="input-group">
          <label class="control-label">Contraseña</label>
        </div>
        <div class="input-group">
          <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
          <?php $data=array(
            'name' => 'contrasena',
            'type' => 'password',
            'class' => 'form-control',
            'id' => 'contrasena',
            'placeholder' => 'Contraseña',
            'required' => true
          );?>
          <?= form_input($data);?>
          <span class="input-group-btn">
        <button class="btn btn-default" type="button" onclick="viewpass()"><span class="fa fa-eye"></span></button>
      </span>
        </div>
        &#160;
        <?php

        if($this->session->userdata('intentos')>=3): ?>
        <div class="form-group">
                <div class="input-group">
                 <span class="imag"><?= $captcha['image']?></span>
                 <span><a class="refresh" onclick="image()" href="javascript:;"><img src="<?php echo base_url('public/img/refresh.png')?>"/></a></span>
                </div>
            </div>
        <div class="form-group">
          <div class="input-group">
            <input type="text" id="captcha" name="captcha" class="form-control" placeholder="captcha" required/>
            <input type="hidden" value="<?=$captcha['word']?>" name="string_captcha" />
          </div>
        </div>
      <?php endif; ?>
      <div class="form-group" align="center">
        <h4>
          <a href="<?php echo base_url('index.php/registro') ?>">Registrarse</a>
        </h4>
        <small>
          <a href="<?php echo base_url('index.php/recuperar') ?>">Olvidé mi contraseña</a>
        </small>
      </div>
      </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Entrar</button>
      </div>
    <?php echo form_close();?>
    </div>
  </div>
</div>

<script>
function ingresar(){

  $.ajax({
          url: '<?php echo base_url('index.php/login/ingresar') ?>',
          type: 'post',
          data: $('#ingreso').serializeArray(),
          success: function(response){
            $('#ingresar').modal('hide');
          }
      });

}

function viewpass(){
  if($('#contrasena').attr('type') == "password"){
    $('#contrasena').attr('type','text');
  }else{
    $('#contrasena').attr('type','password');

  }
}
</script>
<script type="text/javascript">
function image(){
 $.post('<?php echo base_url('index.php/login/refresh_captcha');?>',
   {'<?php echo $this->security->get_csrf_token_name();?>':'<?php echo $this->security->get_csrf_hash(); ?>'},function(resp){
    $('.imag').html(resp);
  });
  }
</script>

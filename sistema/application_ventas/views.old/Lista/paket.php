
  <div class="container-fluid" style="margin-top:40px;">
    <div class="row">
      <?php //var_dump($familiasp); ?>
      <?php foreach($familiasp as $fam):?>
        <div class="col-md-12">
          <div class="col-md-4" style="float:left;background-color:<?php echo $fam->color;?>; margin-bottom:10px; color:white;" align="center"><b><em><?php echo $fam->familia."<br>"; ?></em></b></div>
        </div>
        <div class="col-md-12">

          <?php
           $subfamilia = $this->productos_model->get_subfxfamp($fam->familia);
           //var_dump($subfamilia);
          ?>
          <?php  foreach($subfamilia as $subf):?>
            <div class="col-md-4" style="float:left;background-color:<?php echo $fam->color;?>; color:white;" align="center"><small><b><em><?php echo $subf['subfamilia']."<br>"; ?></em></b></small></div>
            <?php
            $paket=$this->productos_model->get_list_paket($fam->familia,$subf['subfamilia']);
            ?>
            <table class="table table-bordered" >
              <tr>
              <thead style="background-color:#efefef;">
                  <th width="5"></th>
                  <th style="background-color:#efefef;" width="25">CATALOGO</th>
                  <th style="background-color:#efefef;" width="100">DESCRIPCIÓN</th>
                  <th style="background-color:#efefef;" width="50">
                  <?php if($subf['var1']){
                  echo $subf['var1'].' '.$subf['unidad1'];
                  }else{
                    echo "valor";
                  }
                  ?>
                  </th>
                  <th style="background-color:#efefef;" width="50"><?php if($subf['var2']){
                    echo $subf['var2'].' '.$subf['unidad2'];
                  }else{
                    echo "valor";
                  }
                  ?></th>
                  <th style="background-color:#efefef;" width="50"><?php if($subf['var3']){
                    echo $subf['var3'].' '.$subf['unidad3'];
                  }else{
                    echo "valor";
                  }
                  ?></th>
                  <th style="background-color:#efefef;" width="600">GENERAL</th>
                  <th style="background-color:#efefef;" width="100">FOTOGRAFÍA</th>
                  <th style="background-color:#efefef;" width="250">DESCRIPCIÓN DE USO</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($paket as $a): ?>

                  <tr>
                    <th><?php echo $a['rotacion'];?></th>
                    <th><?php echo $a['codigo'];?></th>
                    <th ><?php echo $a['descorta']."<br>".$a['desespecial']."<br><a target='_blank' href='".base_url('public/pdfs/').$a['pdf']."'>Ficha Técnica</a>";?></th>
                    <th><?php echo $a['cant1'];?></th>
                    <th><?php echo $a['cant2'];?></th>
                    <th><?php echo $a['cant3'];?></th>
                    <th style="padding:0px;">
                      <?php
                      $cvpp = $a['cvpp'];
                      $array = str_split($cvpp);
                      ?>
                      <table class="table table-bordered" style="margin-bottom:0px;">
                        <thead>
                          <tr>
                            <?php if($tipo=='bonasa'): ?>
                            <th style="color:red;">BONASA</th>
                            <th style="color:red;">PRECIO PUBLICO SUGERIDO</th>
                          <?php elseif($tipo=='distribuidor'): ?>
                            <th style="color:red;">PRECIO DISTRIBUIDOR</th>
                            <th style="color:red;">PRECIO MINIMO</th>
                            <th style="color:red;">PRECIO PUBLICO SUGERIDO</th>
                          <?php else: ?>
                            <th style="color:red;">P0</th>
                            <th style="color:red;">P1</th>
                            <th style="color:red;">P2</th>
                            <th style="color:red;">PL</th>
                          <?php endif; ?>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <?php if($tipo!='distribuidor' && $tipo!='bonasa'): ?>
                            <th style="padding:0px;">
                              <table class="table table-bordered" style="margin-bottom:0px;">
                                <?php

                                $p0 = (1*$f0p*$a['precio']);
                                $aux_cant = (1*$f0p*$a['precio']);

                                $p0ax = (1*$f0p*$a['pre_aux']);
                                $aux_cantax = (1*$f0p*$a['pre_aux']);
                                ?>
                                <thead>
                                  <tr>
                                    <th style="background-color:#efefef;"><?php echo $a['u_base2'];?></th>
                                    <th style="background-color:yellow;"><?php echo $a['u_aux2'];?></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>
                                    <?php
                                    if($array[0]==0){
                                      echo "No aplica";
                                    }else{
                                      echo money_format("%n", $aux_cant);
                                    }
                                    ?>
                                    </td>

                                    <td style="background-color:yellow;">
                                      <?php
                                    if($array[1]==0){
                                      echo "No aplica";
                                    }else{
                                      echo money_format("%n", $p0ax);
                                    }
                                     ?>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                           </th>
                         <?php endif; ?>
                         <?php if($tipo!='bonasa'): ?>
                            <th style="padding:0px;">
                              <table class="table table-bordered" style="margin-bottom:0px;">
                                <?php
                                $p1 = (1*$f0p*$f1p*$a['precio']);
                                $aux = (1*$f0p*$f1p*$a['precio']);

                                $p1ax = (1*$f0p*$f1p*$a['pre_aux']);
                                $aux_cantax = (1*$f0p*$f1p*$a['pre_aux']);
                                 ?>
                                <thead>
                                  <tr>
                                    <th style="background-color:#efefef;"><?php echo $a['u_base2'];?></th>
                                    <th style="background-color:yellow;"><?php echo $a['u_aux2'];?></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>
                                      <?php
                                      if($array[2]==0){
                                        echo "No aplica";
                                      }else{
                                        echo money_format("%n", $aux);

                                      }
                                      ?>
                                    </td>
                                    <td style="background-color:yellow;">
                                      <?php
                                    if($array[3]==0){
                                      echo "No aplica";
                                    }else{
                                      echo money_format("%n", $p1ax);
                                    }
                                     ?>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </th>
                          <?php endif; ?>
                            <th style="padding:0px;">
                              <table class="table table-bordered" style="margin-bottom:0px;">
                                <?php
                                  $p2 = (1*$f0p*$f1p*$f2p*$a['precio']);
                                  $aux_cant = (1*$f0p*$f1p*$f2p*$a['precio']);

                                  $p2ax = (1*$f0p*$f1p*$f2p*$a['pre_aux']);
                                  $aux_cantax = (1*$f0p*$f1p*$f2p*$a['pre_aux']);
                                 ?>
                                <thead>
                                  <tr>
                                    <th style="background-color:#efefef;"><?php echo $a['u_base2'];?></th>
                                    <th style="background-color:yellow;"><?php echo $a['u_aux2'];?></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>
                                      <?php
                                      if($array[4]==0){
                                        echo "No aplica";
                                      }else{
                                        echo money_format("%n", $aux_cant);
                                      }
                                      ?>
                                    </td>
                                    <td style="background-color:yellow;">
                                      <?php
                                    if($array[5]==0){
                                      echo "No aplica";
                                    }else{
                                      if(($a['cant2']*1)==0){
                                        echo money_format("%n", $aux_cantax);
                                      }else{
                                        echo money_format("%n", $p2ax);
                                      }
                                    }
                                     ?>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </th>
                            <th style="padding:0px;">
                              <table class="table table-bordered" style="margin-bottom:0px;">
                                <?php
                                $p3 = (1*$f0p*$f1p*$f2p*$f3p*$a['precio']);
                                $aux_cant = (1*$f0p*$f1p*$f2p*$f3p*$a['precio']);
                                $p3ax = (1*$f0p*$f1p*$f2p*$f3p*$a['pre_aux']);
                                $aux_cantax = (1*$f0p*$f1p*$f2p*$f3p*$a['pre_aux']);
                                ?>
                                <thead>
                                  <tr>
                                    <th style="background-color:#efefef;"><?php echo $a['u_base2'];?></th>
                                    <th style="background-color:yellow;"><?php echo $a['u_aux2'];?></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>
                                      <?php
                                      if($array[6]==0){
                                        echo "No aplica";
                                      }else{
                                        echo money_format("%n", $aux_cant);

                                      }
                                      ?>
                                    </td>
                                    <td style="background-color:yellow">
                                      <?php
                                    if($array[7]==0){
                                      echo "No aplica";
                                    }else{
                                      if(($a['cant2']*1)==0){
                                        echo money_format("%n", ceil($aux_cantax));
                                      }else{
                                        echo money_format("%n", ceil($p3ax));
                                      }
                                    }
                                     ?>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </th>
                          </tr>
                        </tbody>
                      </table>
                      <table class="table" style="margin-bottom:0px;">
                        <tbody>
                          <tr>
                            <td>
                              <small>
                                <b>
                                  <?php echo $a['iva']." causa IVA" ?>
                                </b>
                              </small>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </th>
                    <th><img class="img img-responsive" src="<?php echo base_url('public/imagenes/'.$a['img']); ?>" width="100" height="100"/></th>
                    <th style="font-size:8px;"><?php echo $a['deslarga'] ?></th>
                  </tr>

                <?php endforeach;?>
              </tbody>
            </table>
          <?php  endforeach;?>

        </div>
      <?php endforeach;?>

    </div>
  </div>

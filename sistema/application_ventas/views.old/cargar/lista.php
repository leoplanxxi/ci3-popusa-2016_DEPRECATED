<link href="<?php echo base_url('public/Loading/src/css/incipit.css') ?>" rel="stylesheet" type="text/css">

<div class="container cuerpo" style="padding-top:25px;">
  <div class="row">
    <div class="col-md-12">

      <div class="panel panel-default">
        <div class="panel-heading">
          <label>CARGAR ARCHIVO</label>
        </div>
        <?php
          echo form_open_multipart("index.php/cargar/guardar");
        ?>
        <div class="panel-body">
          <div class="form-group">
            <input type="file" class="form-control" name="productos" required></input>
          </div>
          <div class="pull-right">
            <?php echo form_submit('mysubmit', 'Enviar','class="btn btn-primary"'); ?>
          </div>
        </div>

        <?php

          echo form_close();
        ?>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading">
          <label>LISTAS VIAJERAS</label>
        </div>
        <div class="panel-body">
          <div class="table table-responsive">
            <table class="table table-bordered" style="font-size:12px;">
              <thead>
                <tr>
                  <th>Tipo</th>
                  <th>Lugar</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Tienda</td>
                  <td>Principal / Zona / Gerencia</td>
                  <td>
                    <div class="btn-group">
                    <a href="<?php echo base_url('index.php/list_v/lista'); ?>" onclick="crear()" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-file"></span>Crear</a>
                  <a href="<?php echo base_url('index.php/list_v/descargar');?>" target="_blank" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-download-alt"></span> Descargar</a>

                  </div>
                </td>
                </tr>
                <tr>
                  <td>Tienda</td>
                  <td>23 PTE</td>
                  <td>
                    <div class="btn-group">
                    <a href="<?php echo base_url('index.php/list_v/lista/T23P'); ?>" onclick="crear()" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-file"></span>Crear</a>
                  <a href="<?php echo base_url('index.php/list_v/descargar/T23P');?>" target="_blank" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-download-alt"></span> Descargar</a>
                  </div>
                </td>
                </tr>
                <tr>
                  <td>Tienda</td>
                  <td>AQUIXTLA</td>
                  <td>
                    <div class="btn-group">
                    <a href="<?php echo base_url('index.php/list_v/lista/AQUIX'); ?>" onclick="crear()" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-file"></span>Crear</a>
                  <a href="<?php echo base_url('index.php/list_v/descargar/AQUIX');?>" target="_blank" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-download-alt"></span> Descargar</a>
                  </div>
                </td>
                </tr>
                <tr>
                  <td>Tienda</td>
                  <td>C. ABASTOS</td>
                  <td>
                    <div class="btn-group">
                    <a href="<?php echo base_url('index.php/list_v/lista/TCA'); ?>" onclick="crear()" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-file"></span>Crear</a>
                  <a href="<?php echo base_url('index.php/list_v/descargar/TCA');?>" target="_blank" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-download-alt"></span> Descargar</a>
                  </div>
                </td>
                </tr>
                <tr>
                  <td>Tienda</td>
                  <td>HUIXCOLOTLA</td>
                  <td>
                    <div class="btn-group">
                    <a href="<?php echo base_url('index.php/list_v/lista/THUIX'); ?>" onclick="crear()" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-file"></span>Crear</a>
                  <a href="<?php echo base_url('index.php/list_v/descargar/THUIX');?>" target="_blank" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-download-alt"></span> Descargar</a>
                  </div>
                </td>
                </tr>
                <tr>
                  <td>Vendedor</td>
                  <td>23 PTE</td>
                  <td>
                    <div class="btn-group">
                    <a href="<?php echo base_url('index.php/list_v/lista/V23P'); ?>" onclick="crear()" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-file"></span>Crear</a>
                <a href="<?php echo base_url('index.php/list_v/descargar/V23P');?>" target="_blank" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-download-alt"></span> Descargar</a>
                  </div>
                </td>
                </tr>
                <tr>
                  <td>Vendedor</td>
                  <td>C. ABASTOS</td>
                  <td>
                    <div class="btn-group">
                    <a href="<?php echo base_url('index.php/list_v/lista/VCA'); ?>" onclick="crear()" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-file"></span>Crear</a>
                  <a href="<?php echo base_url('index.php/list_v/descargar/VCA');?>" target="_blank" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-download-alt"></span> Descargar</a>
                  </div>
                </td>
                </tr>
                <tr>
                  <td>Vendedor</td>
                  <td>HUIXCOLOTLA</td>
                  <td>
                    <div class="btn-group">
                    <a href="<?php echo base_url('index.php/list_v/lista/VHUIX'); ?>" onclick="crear()" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-file"></span>Crear</a>
                  <a href="<?php echo base_url('index.php/list_v/descargar/VHUIX');?>" target="_blank" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-download-alt"></span> Descargar</a>
                  </div>
                </td>
                </tr>
                <tr>
                  <td>Bonasa</td>
                  <td>BONASA</td>
                  <td>
                    <div class="btn-group">
                    <a href="<?php echo base_url('index.php/list_v/lista/bonasa'); ?>" onclick="crear()" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-file"></span>Crear</a>
                  <a href="<?php echo base_url('index.php/list_v/descargar/bonasa');?>" target="_blank" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-download-alt"></span> Descargar</a>
                  </div>
                </td>
                </tr>
                <tr>
                  <td>Distribuidor</td>
                  <td>DISTRIBUIDOR</td>
                  <td>
                    <div class="btn-group">
                  <a href="<?php echo base_url('index.php/list_v/lista/distribuidor'); ?>" onclick="crear()" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-file"></span>Crear</a>
                  <a href="<?php echo base_url('index.php/list_v/descargar/distribuidor');?>" target="_blank" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-download-alt"></span> Descargar</a>
                  </div>
                </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
<script src="<?php echo base_url('public/Loading/src/js/incipit.js') ?>"></script>
<script>

$('body').IncipitInit({
	backgroundColor : "#ffd222",
  icon : "solid-snake",
  note : true,
  noteCustom: "La información se esta cargando, espere por favor.",
  logo : true,
  logoSrc : '<?php echo base_url('public/imagenes/logo-popusa.png') ?>'
});

function crear(){
  $.Incipit('show');
  setTimeout(function(){
    $.Incipit('hide');
  }, 300000);
}
</script>

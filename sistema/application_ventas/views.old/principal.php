<script>
window.addEventListener("resize", cambio_orientacion);

function mostrar() {
	var ventana_ancho = $(window).width();
        if (ventana_ancho < 768)
		{
			document.getElementById("menu_productos").style.marginLeft = "-30px";
			document.getElementById("mostrar").style.display = "none";
			document.getElementById("ocultar").style.display = "inline-block";
		}
		else if (ventana_ancho > 767)
		{
			document.getElementById("menu_productos").style.marginLeft = "0";
			document.getElementById("menu_productos").style.left = "0px";
			document.getElementById("mostrar").style.display = "none";
			document.getElementById("ocultar").style.display = "inline-block";
			document.getElementById("ocultar").style.marginLeft = "0";
			document.getElementById("ocultar").style.left = "250px";
		}
}

function ocultar() {
	var ventana_ancho = $(window).width();
        if (ventana_ancho < 992)
        {
            document.getElementById("menu_productos").style.marginLeft = "-290px";
			document.getElementById("mostrar").style.display = "inline-block";
			document.getElementById("ocultar").style.display = "none";
			window.scroll({top: 0, left: 0, behavior: 'smooth' });
        }
}

function cambio_orientacion() {
	
	var ancho = $(window).width();

	
	if (ancho > 991) {
		document.getElementById("menu_productos").style.marginLeft = "initial";
		document.getElementById("mostrar").style.display = "none";
		document.getElementById("ocultar").style.display = "none";
	}
	
	else if (ancho < 992) {
		document.getElementById("menu_productos").style.marginLeft = "-290px";
		document.getElementById("mostrar").style.display = "inline-block";
		document.getElementById("ocultar").style.display = "none";
	}

}

</script>

<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<body onload="fproductos(null)">
	<div class="principal" style="padding-top:90px;" id="top">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
					</div>
					<div class="col-md-9" align="center" style="color:white;">
						<h2 class="titulo-producto"><b id="nomprod"></b></h2>
					</div>
					<div class="col-md-3 menu_productos" id="menu_productos">
						<div class="etiqueta_lateral_mostrar" id="mostrar" onclick="mostrar()">
							<p><br>P<br>R<br>O<br>D<br>U<br>C<br>T<br>O<br>S<br></p>
						</div>
						<div class="etiqueta_lateral_ocultar" id="ocultar" onclick="ocultar()">
							<p><br>P<br>R<br>O<br>D<br>U<br>C<br>T<br>O<br>S<br></p>
						</div>
						<div class="navbar-popusa sidebar" role="navigation">
							<div class="sidebar-nav popnavbar-collapse">
								<ul class="nav" id="side-menu">
									<ul class="nav nav-second-level">
									<?php $i=0;
												foreach ($alianzaf as $af) {
													if($alianzasf[$i]->familia){
														?>
												<li>
											<a href="#<?php echo $af->familia;?>" onclick='fproductos("<?php echo $af->familia;?>")'>
												<div style="background-color:<?php echo $af->color;?>; width:4px; height:21px; float:left; margin-right:5px;">
												</div>
													<b style="color:<?php echo $af->color;?>;"><?php echo strtoupper($af->familia);?></b>
													<?php if($af->subfamilia!="nosubfam"){ ?>
														<span class="fa arrow" style="color:<?php echo $af->color;?>;"></span>
																	<?php }?>
												 </a>
												 <ul class="nav nav-third-level">
														<?php foreach ($alianzasf as $asf) {//subfamilia
																		if($af->familia == $asf->familia){
																		?>
												 <li>
														<a href="#<?php echo $asf->subfamilia;?>"   style="color:<?php echo $af->color;?>; padding-left:20px;" onclick='sfproductos("<?php echo $asf->subfamilia;?>"), ocultar()'><?php echo strtoupper($asf->subfamilia);?></a>
												 </li>
															<?php }
																			}?>
													</ul>
												</li>
														<?php }else{?>
												<li>
													<a href='#<?php echo $af->familia;?>' onclick='fproductos("<?php echo $af->familia;?>")'>
														<b><?php echo $af->familia;?></b>
													 </a>
												 </li>
														<?php
															}
															$i++;
														//}
																}?>
																<?php
																$i=0;
																foreach ($paketf as $pf) {
																	//for($i=0; $i<count($alianzasf);$i++){
																		if($paketf[$i]->familia){
																	?>
													<li>
														<a href="#<?php echo $pf->familia;?>" onclick='fproductos("<?php echo $pf->familia;?>")'>
															<div style="background-color:<?php echo $pf->color;?>; width:4px; height:21px; float:left; margin-right:5px;"></div>
																<b style="color:<?php echo $pf->color;?>"><?php echo strtoupper($pf->familia);?></b>
																	<?php if($pf->subfamilia!="nosubfam"){ ?>
																				<!--span class="fa arrow"></span-->
																		<?php }?>
															 </a>
															 		<ul class="nav nav-third-level">
																		<?php foreach ($paketsf as $psf) {//subfamilia
																						if($pf->familia == $psf->familia){
																					?>
																		<li>
																		<a href="#<?php echo $psf->subfamilia;?>, #nomprod" onclick='sfproductos("<?php echo $psf->subfamilia;?>"), ocultar()' style="color:<?php echo $psf->color;?>; padding-left:20px;">
																								<?php echo $psf->subfamilia;?>
																		</a>
																		</li>
																		<?php }
																									}?>
																	</ul>
														 </li>
															<?php }else{?>
														 <li>
														 		<a href='#<?php echo $pf->familia;?>' onclick='fproductos("<?php echo $pf->familia;?>")'><?php echo $pf->familia;?></a>
															</li>
																	<?php
																		}

																		$i++;
																	//}
																			}?>
															<!--/ul>
													</li-->
											</ul>
								</ul>
						</div>
				</div>
				<hr>
			</div>

			<div class="col-md-9" id="top">
				<div id="listprod">
				</div>
			</div>

	</div>
</div>
</div>
</div>
</body>

<script type="text/javascript">
  <?php if($this->input->post('buscar',true)): ?>
  $("#nomprod").html('Búsqueda: '+'<?php echo $this->input->post('buscar'); ?>');
  <?php endif; ?>
</script>
<style>
.view .mask .back-popusa{
  <?php
    if($productos[0]->color=="#F5DB06"){
      echo "color:black;";
    }else{
      echo "color:white;";
    }
  ?>
  background-color: <?php echo $productos[0]->color;?>;;
}
.masinfo{
background-color: <?php echo $productos[0]->color;?>;;/*Color para la familia de productos*/
}
.addshop{
background-color: <?php echo $productos[0]->color;?>;/*Color para la familia de productos*/
}
.addshop a i{
background-color: <?php echo $productos[0]->color;?>;/*Color para la familia de productos*/
}
.panel-heading{
<?php
  if($productos[0]->color=="#F5DB06"){
    echo "color:black;";
  }else{
    echo "color:white;";
  }
?>
}
.panel-popusa {
  border-color: <?php echo $productos[0]->color;?>;/*Color para la familia de productos*/
}
.titulo-producto{
  color:<?php echo $productos[0]->color;?>;
}
.panel{
  background-color: <?php echo $productos[0]->color;?>;
}
</style>

<script>
var carrito_aux;
var carrito_ext;

function valores(id_cliente){
  if(document.getElementById("val_1")==null){

    var carrito ={
      id_cliente:id_cliente,
      descorta:document.getElementById("descorta").innerText,
      //unidad:document.getElementById("escala").value,//pendiente
      cantidad:document.getElementById("cantidad").value,
      cant_confeccion:document.getElementById("vs1").value,
      cant_costura:document.getElementById("vs2").value,
      cant_ojillos:document.getElementById("vs3").value
    };

  }else{
    if(document.getElementById("val_2")==null){
      var carrito ={
        id_cliente:id_cliente,
        descorta:document.getElementById("descorta").innerText,
        variable1:document.getElementById("val_1").value,
        //unidad:document.getElementById("escala").value,//pendiente
        cantidad:document.getElementById("cantidad").value,
        cant_confeccion:document.getElementById("vs1").value,
        cant_costura:document.getElementById("vs2").value,
        cant_ojillos:document.getElementById("vs3").value
      };
    }else {
      if(document.getElementById("val_3")==null){
      var carrito ={
          id_cliente:id_cliente,
          descorta:document.getElementById("descorta").innerText,
          variable1:document.getElementById("val_1").value,
          variable2:document.getElementById("val_2").value,
          //unidad:document.getElementById("escala").value,//pendiente
          cantidad:document.getElementById("cantidad").value,
          cant_confeccion:document.getElementById("vs1").value,
          cant_costura:document.getElementById("vs2").value,
          cant_ojillos:document.getElementById("vs3").value
        };
      }else {
        if(document.getElementById("val_4")==null){
           var carrito ={
            id_cliente:id_cliente,
            descorta:document.getElementById("descorta").innerText,
            variable1:document.getElementById("val_1").value,
            variable2:document.getElementById("val_2").value,
            variable3:document.getElementById("val_3").value,
            //unidad:document.getElementById("escala").value,//pendiente
            cantidad:document.getElementById("cantidad").value,
            cant_confeccion:document.getElementById("vs1").value,
            cant_costura:document.getElementById("vs2").value,
            cant_ojillos:document.getElementById("vs3").value
          };
        }else{
          if(document.getElementById("val_5")==null){
             var carrito ={
              id_cliente:id_cliente,
              descorta:document.getElementById("descorta").innerText,
              variable1:document.getElementById("val_1").value,
              variable2:document.getElementById("val_2").value,
              variable3:document.getElementById("val_3").value,
              variable4:document.getElementById("val_4").value,
              //unidad:document.getElementById("escala").value,//pendiente
              cantidad:document.getElementById("cantidad").value,
              cant_confeccion:document.getElementById("vs1").value,
              cant_costura:document.getElementById("vs2").value,
              cant_ojillos:document.getElementById("vs3").value
            };
          }else{
            if(document.getElementById("val_6")==null){
               var carrito ={
                id_cliente:id_cliente,
                descorta:document.getElementById("descorta").innerText,
                variable1:document.getElementById("val_1").value,
                variable2:document.getElementById("val_2").value,
                variable3:document.getElementById("val_3").value,
                variable4:document.getElementById("val_4").value,
                variable5:document.getElementById("val_5").value,
                //unidad:document.getElementById("escala").value,//pendiente
                cantidad:document.getElementById("cantidad").value,
                cant_confeccion:document.getElementById("vs1").value,
                cant_costura:document.getElementById("vs2").value,
                cant_ojillos:document.getElementById("vs3").value
              };
            }else {
               var carrito ={
                id_cliente:id_cliente,
                descorta:document.getElementById("descorta").innerText,
                variable1:document.getElementById("val_1").value,
                variable2:document.getElementById("val_2").value,
                variable3:document.getElementById("val_3").value,
                variable4:document.getElementById("val_4").value,
                variable5:document.getElementById("val_5").value,
                variable6:document.getElementById("val_6").value,
                //unidad:document.getElementById("escala").value,//pendiente
                cantidad:document.getElementById("cantidad").value,
                cant_confeccion:document.getElementById("vs1").value,
                cant_costura:document.getElementById("vs2").value,
                cant_ojillos:document.getElementById("vs3").value
              };
            }
          }
        }
      }
    }
  }
  if(id_cliente!=null){

    carrito.unidad=document.getElementById("escala").value;
  }

    return carrito;
}

function carrito(id_cliente){

  var carrito = valores(id_cliente);
  console.log(carrito);
  carrito_aux = carrito;

  $("[class*='modal-backdrop fade']").remove();
  $("#addshop").modal('hide');

  $.post("<?= base_url('index.php/productos/get_info_extra')?>",carrito,function(data){
    console.log(JSON.parse(data));
    if(JSON.parse(data)[0].confeccion!=null){
        $.post("<?= base_url('index.php/productos/get_desc')?>",{cod:JSON.parse(data)[0].confeccion},function(des){
          var get_desc = JSON.parse(des)[0];
          $('#inf_conf').html(get_desc.deslarga);
          $('#inf_s1').html(get_desc.descorta);
          if(get_desc.unidad1!=null){
            $('#lb_s1').html(get_desc.var1+" "+get_desc.unidad1);
          }else{
            $('#lb_s1').html(JSON.parse(des)[0].var1);
          }
        });

        $.post("<?= base_url('index.php/productos/get_desc')?>",{cod:JSON.parse(data)[0].costura},function(des){

          $('#inf_confs2').html(JSON.parse(des)[0].deslarga);
          $('#inf_s2').html(JSON.parse(des)[0].descorta);
          if(JSON.parse(des)[0].unidad1!=null){
            $('#lb_s2').html(JSON.parse(des)[0].var1+" "+JSON.parse(des)[0].unidad1);
          }else{
            $('#lb_s2').html(JSON.parse(des)[0].var1);
          }
        })

        $.post("<?= base_url('index.php/productos/get_desc')?>",{cod:JSON.parse(data)[0].ojillos},function(des){

          $('#inf_confs3').html(JSON.parse(des)[0].deslarga);
          $('#inf_s3').html(JSON.parse(des)[0].descorta);
          if(JSON.parse(des)[0].unidad1!=null){
            $('#lb_s3').html(JSON.parse(des)[0].var1+" "+JSON.parse(des)[0].unidad1);
          }else{
            $('#lb_s3').html(JSON.parse(des)[0].var1);
          }
        });
          console.log(document.getElementById("vs1").value);
        if(document.getElementById("vs1").value==0 && document.getElementById("vs2").value==0  && document.getElementById("vs3").value==0){
          $("#inf_extra").modal('show');

        }else{
          $("#inf_extra").modal('hide');
          document.getElementById("vs1").value=0;
          document.getElementById("vs2").value=0;
          document.getElementById("vs3").value=0;
        }

    }else{
      $.post("<?= base_url('index.php/productos/add_carrito');?>",carrito,function(data){
        var shop = JSON.parse(data);
        $("#carcant").html(shop.length);
        if(shop.length>0){
          <?php if($this->session->userdata('popusa')[0]["rol_idrol"]==6): ?>
          var prodcar='<ol style="padding:0px"><div id="totalcarrito" style="height:320px; overflow:auto"></div></ol><ol style="padding:0px"><button class="btn btn-primary btn-block btn-sm" onclick="cotizar()">Crear Orden</button></ol>';
          <?php else: ?>
          var prodcar='<ol style="padding:0px"><div id="totalcarrito" style="height:320px; overflow:auto"></div></ol><ol style="padding:0px"><button class="btn btn-primary btn-block btn-sm" onclick="cotizar()">Cotizar</button></ol>';
          <?php endif; ?>

          $("#prodcar").html(prodcar);
        var totalcarrito="";
        var carrito="";
        for(var i=0;i<shop.length;i++){
          carrito = '<div class="col-md-12"><div style="padding:5px" class="col-md-4"><img  src="'+"<?php echo base_url('public/imagenes/')?>"+shop[i].imagen+'" class="img img-responsive thumbnail"></div>'+'<div class="col-md-6"><b>'+shop[i].descorta+'</b><br>Cantidad: '+shop[i].cantidad+'</div>'+'<div class="col-md-2">'+'<form>'+'<button type="button" class="close" aria-label="Close" onclick="del_carrito('+shop[i].id_cotizacion+')">'+'<span aria-hidden="true">&times;</span>'+'</button>'+'</form>'+'</div></div><ol role="separator" class="divider"></ol>';
          totalcarrito=totalcarrito+carrito;
        }
        $('#totalcarrito').html(totalcarrito);
      }else{
        var prodcar = '<ol style="padding:0px"><div style="text-align: center!important;font-weight: bold;">No hay productos agregados</div></ol>';
        $("#prodcar").html(prodcar);
      }
      });
    }
  });
carrito_ext = carrito;
document.getElementById('cantidad').value = 1;
}

function info_extra(id){
  $("#inf_extra").modal('hide');
  //$("[class*='modal-backdrop fade']").remove();
  carrito(id);
  $.post("<?= base_url('index.php/productos/add_carrito');?>",carrito_ext,function(data){
    var shop = JSON.parse(data);
    $("#carcant").html(shop.length);
    if(shop.length>0){
      <?php if($this->session->userdata('popusa')[0]["rol_idrol"]==6): ?>
      var prodcar='<ol style="padding:0px"><div id="totalcarrito" style="height:320px; overflow:auto"></div></ol><ol style="padding:0px"><button class="btn btn-primary btn-block btn-sm" onclick="cotizar()">Crear Orden</button></ol>';
      <?php else: ?>
      var prodcar='<ol style="padding:0px"><div id="totalcarrito" style="height:320px; overflow:auto"></div></ol><ol style="padding:0px"><button class="btn btn-primary btn-block btn-sm" onclick="cotizar()">Cotizar</button></ol>';
      <?php endif; ?>
      $("#prodcar").html(prodcar);
    var totalcarrito="";
    var carrito="";
    for(var i=0;i<shop.length;i++){
      carrito = '<div class="col-md-12"><div style="padding:5px" class="col-md-4"><img  src="'+"<?php echo base_url('public/imagenes/')?>"+shop[i].imagen+'" class="img img-responsive thumbnail"></div>'+'<div class="col-md-6"><b>'+shop[i].descorta+'</b><br>Cantidad: '+shop[i].cantidad+'</div>'+'<div class="col-md-2">'+'<form>'+'<button type="button" class="close" aria-label="Close" onclick="del_carrito('+shop[i].id_cotizacion+')">'+'<span aria-hidden="true">&times;</span>'+'</button>'+'</form>'+'</div></div><ol role="separator" class="divider"></ol>';
      totalcarrito=totalcarrito+carrito;
    }
    $('#totalcarrito').html(totalcarrito);

  }else{

    var prodcar = '<ol style="padding:0px"><div style="text-align: center!important;font-weight: bold;">No hay productos agregados</div></ol>';
    $("#prodcar").html(prodcar);

  }
  });

}

function pdfs(nombre){
  document.getElementById('pdf').src = "http://docs.google.com/gview?url=<?php echo base_url('public/pdfs/')?>"+nombre+"&embedded=true";

}

/*--------------------------------------------------------------------------------------------------------------------------*/
function medidas(descorta){

  $("#infoprod2").html("");
  $("#val2").html("");
  $("#infoprod3").html("");
  $("#val3").html("");
  $("#infoprod4").html("");
  $("#val4").html("");
  $("#infoprod5").html("");
  $("#val5").html("");
  $("#infoprod6").html("");
  $("#val6").html("");

  $.post("<?php echo base_url('index.php/productos/get_medidas');?>",{"descorta":descorta },function(data){
    var json = JSON.parse(data);

    var label;
    var h = descorta;

    if(data!=0){
      if(json[0].cant1!=null){
        var select = $("<select id='val_1' name='val1' class='form-control'></select>");
      }

    var tam = json.length;
    var aux_unidad ="";

    if(json[0].var1!=null){
      if(json[0].unidad1!=null){
        aux_unidad=" en "+json[0].unidad1;
      }
      label = '<label class="control-label col-md-6">'+json[0].var1+aux_unidad+'</label>'
              +'<input type="hidden" name="udm1" value="ancho">';
    }

    for( var i = 0; i < json.length; i++ ) {
      if(json[i].cant1!=null){
        var option = $("<option value='"+json[i].cant1+"'></option>");
        option.append(json[i].cant1);
      }
      if(json[0].cant1!=null){
      select.append(option);
    }
  }

  $("#infoprod").html(select);
  var opc;
  var auxdata=JSON.parse(data);

  if(auxdata[0]['var1']!=null){
    opc = document.getElementById('val_1').value;
    medidas2(descorta,opc);
    var json_esc = {'descorta':descorta,'opc1':opc};
    escala(json_esc);

    $('#val_1').on('change', function() {
     opc = document.getElementById('val_1').value;
     medidas2(descorta,opc);
     var json_esc = {'descorta':descorta,'opc1':opc};
     escala(json_esc);
  });
  }else{

    var json_esc = {'descorta':descorta};
    escala(json_esc);
    //ajax para obtener los precios

  }

}else{

  $("#infoprod").html("");
  label = '<label class="control-label col-md-12">El producto no tiene información que mostrar</label>&#160;';
}
$("#descorta").html(h);

$("#val1").html(label);
  document.getElementById('val_desc').value=h;
  })
  <?php if($this->session->userdata('popusa')[0]["rol_idrol"]==6): ?>
    var valor = valores(null);
    valor.descorta = descorta;
    $.post('<?php echo base_url('index.php/productos/get_preciolist'); ?>',valor,function(item){

      $('#distp').html(item);
    });
    <?php endif; ?>

}

function medidas2(descorta2,opc1){
  $("#infoprod3").html("");
  $("#val3").html("");
  $("#infoprod4").html("");
  $("#val4").html("");
  $("#infoprod5").html("");
  $("#val5").html("");
  $("#infoprod6").html("");
  $("#val6").html("");
  $.post("<?php echo base_url('index.php/productos/get_medidas2');?>",{"descorta2":descorta2,"opc1":opc1},function(data2){
    var json2 = JSON.parse(data2);
    var label2;
    if(json2!=null){
      if(json2[0].cant2!=null){
      var select2 = $("<select id='val_2' name='val2' class='form-control'></select>");
    }else{
        document.getElementById("infoprod2").style.display = "none";
        document.getElementById("val2").style.display = "none";
    }
      var aux_unidad2="";

      if(json2[0].var2!=null){
        if(json2[0].unidad2!=null){
          aux_unidad2=" en "+json2[0].unidad2;
        }

        label2 = '<label class="control-label col-md-6">'+json2[0].var2+aux_unidad2+'</label>'
        +'<input type="hidden" name="udm2" value="var2">';
        medidas3(descorta2);
      }

      for( var i = 0; i < json2.length; i++ ) {
        if(json2[i].cant2!=null){
          var option2 = $("<option value='"+json2[i].cant2+"'></option>");
          option2.append(json2[i].cant2);
        }
        if(json2[0].cant2!=null){
        select2.append(option2);
        }
    }
    $("#infoprod2").html(select2);
    $("#val2").html(label2);
    var opc2;
    opc2 = document.getElementById('val_2').value;
    medidas3(descorta2,opc1,opc2);
    var json_esc = {'descorta':descorta2,'opc1':opc1,'opc2':opc2};
    escala(json_esc);

    $('#val_2').on('change', function() {
     opc2 = document.getElementById('val_2').value;

     medidas3(descorta2,opc1,opc2);
     var json_esc = {'descorta':descorta2,'opc1':opc1,'opc2':opc2};
     escala(json_esc);
  })

  }else {
    $("#infoprod2").html("");
    $("#val2").html("");
    //document.getElementById("val_2").value="";
    //document.getElementById("infoprod2").style.display = "none";
    //document.getElementById("val2").style.display = "none";

  }

  //ajax para obtener los precios

  });

  <?php if($this->session->userdata('popusa')[0]["rol_idrol"]==6): ?>
    var valor = valores(null);

    valor.descorta = descorta2;
    $.post('<?php echo base_url('index.php/productos/get_preciolist'); ?>',valor,function(item){

      $('#distp').html(item);
    });
    <?php endif; ?>
}

function medidas3(descorta3,opc1,opc2){
  $("#infoprod4").html("");
  $("#val4").html("");
  $("#infoprod5").html("");
  $("#val5").html("");
  $("#infoprod6").html("");
  $("#val6").html("");
  $.post("<?php echo base_url('index.php/productos/get_medidas3');?>",{"descorta3":descorta3,"opc1":opc1,"opc2":opc2},function(data3){
    var json3 = JSON.parse(data3);

    var label3;
    if(data3!=undefined){
      if(json3!=undefined){
        console.log(json3[0]);
        console.log(json3[0].var3);
      //json3[0].var3!=null
      if(json3[0].var3!=null || json3[0].var3!=undefined){
        var select3 = $("<select id='val_3' name='val3' class='form-control'></select>");
        var aux_unidad3="";
        //json3[0].unidad3!=null
        if(json3[0].unidad3!=null){
          aux_unidad3=" en "+json3[0].unidad3;
        }
        label3 = '<label class="control-label col-md-6">'+json3[0].var3+aux_unidad3+'</label>'
                +'<input type="hidden" name="udm3" value="var3">';
      }

      for( var i = 0; i < json3.length; i++ ) {
        if(json3[i].cant3!=null){
          var option3 = $("<option value='"+json3[i].cant3+"'></option>");
          option3.append(json3[i].cant3);
        }
        if(json3[0].cant3!=null){
        select3.append(option3);
      }
    }
    $("#infoprod3").html(select3);
    $("#val3").html(label3);
    var opc3;
    if(document.getElementById('val_3') != null){
      opc3 = document.getElementById('val_3').value;
      medidas4(descorta3,opc1,opc2,opc3);
    }

    var json_esc = {'descorta':descorta3,'opc1':opc1,'opc2':opc2,'opc3':opc3};
    escala(json_esc);
    $('#val_3').on('change', function() {
     opc3 = document.getElementById('val_3').value;
     medidas4(descorta3,opc1,opc2,opc3);
     var json_esc = {'descorta':descorta3,'opc1':opc1,'opc2':opc2,'opc3':opc3};
     escala(json_esc);
   })
      }else{
        //document.getElementById("infoprod3").style.display = "none";
        //document.getElementById("val3").style.display = "none";
      }
  }else {
    $("#infoprod3").html("");
    $("#val3").html("");
    document.getElementById("val_3").value="";
    document.getElementById("infoprod3").style.display = "none";
    document.getElementById("val3").style.display = "none";

    //ajax para obtener los precios
  }
  });
}

function medidas4(descorta4,opc1,opc2,opc3){

  $("#infoprod5").html("");
  $("#val5").html("");
  $("#infoprod6").html("");
  $("#val6").html("");
  $.post("<?php echo base_url('index.php/productos/get_medidas4');?>",{"descorta4":descorta4,"opc1":opc1,"opc2":opc2,"opc3":opc3},function(data4){
    var json4 = JSON.parse(data4);
    var label4;
    if(data4!=undefined){
      if(json4[0].cant4!=null){
        var select4 = $("<select id='val_4' name='val4' class='form-control'></select>");
        var aux_unidad4="";
        if(json4[0].var4!=null){
          if(json4[0].unidad4!=null){
            aux_unidad4=" en "+json4[0].unidad4;
          }
          label4 = '<label class="control-label col-md-6">'+json4[0].var4+aux_unidad4+'</label>'
                  +'<input type="hidden" name="udm4" value="var4">';
        }

        for( var i = 0; i < json4.length; i++ ) {
          if(json4[i].cant4!=null){

            var option4 = $("<option value='"+json4[i].cant4+"'></option>");
              option4.append(json4[i].cant4);
          }
          if(json4[0].cant4!=null){
          select4.append(option4);
        }
      }

      $("#infoprod4").html(select4);
      $("#val4").html(label4);
      var opc4;
      opc4 = document.getElementById('val_4').value;
      medidas5(descorta4,opc1,opc2,opc3,opc4);
      var json_esc = {'descorta':descorta4,'opc1':opc1,'opc2':opc2,'opc3':opc3};
      escala(json_esc);
      $('#val_4').on('change', function() {
       opc4 = document.getElementById('val_4').value;
       medidas5(descorta4,opc1,opc2,opc3,opc4);
       var json_esc = {'descorta':descorta4,'opc1':opc1,'opc2':opc2,'opc3':opc3,'opc4':opc4};
       escala(json_esc);
     })

      }

  }else {

    $("#infoprod4").html("");
    $("#val4").html("");

  }
  });

  <?php if($this->session->userdata('popusa')[0]["rol_idrol"]==6): ?>
    var valor = valores(null);
    $.post('<?php echo base_url('index.php/productos/get_preciolist'); ?>',valor,function(item){

      $('#distp').html(item);
    });
    <?php endif; ?>

}

function medidas5(descorta5,opc1,opc2,opc3,opc4){

  $.post("<?php echo base_url('index.php/productos/get_medidas5')?>",{"descorta5":descorta5,"opc1":opc1,"opc2":opc2,"opc3":opc3,"opc4":opc4},function(data5){

    var json5 = JSON.parse(data5);
    var label5;
    if(data5!=undefined){
      if(json5[0].cant5!=null){
        var select5 = $("<select id='val_5' name='val5' class='form-control'></select>");
        var aux_unidad5="";
        if(json5[0].var5!=null){
          if(json5[0].unidad5!=null){
            aux_unidad5=" en "+json5[0].unidad5;
          }
          label5 = '<label class="control-label col-md-6">'+json5[0].var5+aux_unidad5+'</label>'
                  +'<input type="hidden" name="udm5" value="var5">';
          //medidas5(descorta4);
        }

        for( var i = 0; i < json5.length; i++ ) {
          if(json5[i].cant5!=null){

            var option5 = $("<option value='"+json5[i].cant5+"'></option>");
              option5.append(json5[i].cant5);
          }
          if(json5[0].cant5!=null){
          select5.append(option5);
        }
      }
      $("#infoprod5").html(select5);
      $("#val5").html(label5);
      }else{
        document.getElementById("infoprod6").style.display = "none";
        document.getElementById("val6").style.display = "none";
        //medidas5(descorta4);
      }
  }else {

    $("#infoprod5").html("");
    $("#val5").html("");

  }

});
}

function escala(descorta){
  $("#vale").html("");
  $("#infoes").html("");
  $.post("<?= base_url('index.php/productos/get_escala');?>",descorta,function(data){
    var escala = JSON.parse(data);
    //var escala = data;
    var aux_escala="";
    if(escala.length!=0){
      label = '<label class="control-label col-md-6">Unidad</label>'
      /*if(escala.length==1){
        if(escala[0].u_base!=null){
          var select = $("<input id='escala' class='form-control' value='"+escala[0].u_base+"'>");
        }else{
          var select = $("<input id='escala' class='form-control' value='"+escala[0].u_aux+"'>");
        }
      }else{*/
        var select = $("<select id='escala' name='escala' class='form-control'></select>");
        if(escala[0].u_base2!=null){
          var option1 = $("<option value='"+escala[0].u_base2+"'></option>");
          option1.append(escala[0].u_base2);
          select.append(option1);
        }

        if(escala[0].u_aux2!=null){
          var option2 = $("<option value='"+escala[0].u_aux2+"'></option>");
          option2.append(escala[0].u_aux2);
          select.append(option2);
        }

      //}
      $("#vale").html(label);
      $("#infoes").html(select);
    }else{
      $("#vale").html("");
      $("#infoes").html("");
      document.getElementById("vale").style.diplay="none";
      document.getElementById("infoes").style.diplay="none";
    }
  });

}
</script>

<script>
(function($) {
$(document).ready(function(){
  var x = <?php echo count($productos);  ?>;

  window.addEventListener("orientationchange", ()=> {
    if(window.screen.orientation.type == "landscape-primary"){
      for(var i =0; i<x; i++){
      document.getElementById("or_pr"+i).classList.remove('col-sm-6');
      document.getElementById("or_pr"+i).classList.add('col-xs-6');
    }
    }else{
      for(var i =0; i<x; i++){
        document.getElementById("or_pr"+i).classList.remove('col-xs-6');
        document.getElementById("or_pr"+i).classList.add('col-sm-6');
      }
    }
});
  if (window.matchMedia("(orientation: portrait)").matches) {
      /// portrait codigo
      for(var i =0; i<x; i++){
        document.getElementById("or_pr"+i).classList.remove('col-xs-6');
        document.getElementById("or_pr"+i).classList.add('col-sm-6');
      }

  }

  if (window.matchMedia("(orientation: landscape)").matches) {
     /// landscape codigo
     for(var i =0; i<x; i++){
     document.getElementById("or_pr"+i).classList.remove('col-sm-6');
     document.getElementById("or_pr"+i).classList.add('col-xs-6');
   }
  }
  $("select[name=val1]").change(function(){
            alert($('select[name=val1] option:selected').text());
        });
})
})(jQuery);
</script>

<div class="col-md-12" >
  <div class="row" >
  <?php
  $i = 0;
  foreach($productos as $pr):?>
    <div id="or_pr<?php echo $i; ?>" class="col-md-4 col-sm-6 productos">
      <div class="panel panel-popusa view">
        <div class="panel-heading">
          <span class="cortar">
            <?php echo $pr->descorta?>
          </span>
            <div class="clearfix"></div>
          </div>

          <div class="panel-body max-body">
            <div>
              <img class="img-responsive im-pop" src="<?php echo base_url('public/imagenes/').$pr->img;?>">
                <div class="mask">
                  <div class="back-popusa">
                    <b><?php echo $pr->descorta?></b>
                  </div>
                  <p class="info"><?php echo $pr->deslarga; ?></p>
                  </div>
                  <!--div class="comparar pull-left"><small>Comparar antes de comprar &rarr;</small></div-->
                  <div class="masinfo">
                    <a href="#" onclick="pdfs(<?php echo "'".$pr->pdf."'"; ?>)" data-toggle="modal" data-target="#masinfo">
                      <!--i class="fa fa-file-text-o" aria-hidden="true"></i-->
                      <img src="<?php echo base_url('public/img/ficha_tecnica.png') ?>" />
                     </a>
                   </div>

                   <div class="addshop">
                    <?php $val1 = str_replace('"',"|",$pr->descorta);?>
                      <a href="#" onclick="medidas(<?php echo "'".$val1."'";?>)" data-toggle="modal" data-target="#addshop">
                        <!--i class="fa fa-shopping-cart" aria-hidden="true"></i-->
                        <img src="<?php echo base_url('public/img/carrito_compra.png') ?>" />
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
      <?php
      $i++;
    endforeach;?>
    <?php if($i == 0): ?>
      <div id="or_pr<?php echo $i; ?>" class="col-md-12">
        <div class="alert alert-info" align="center">
          <h4>No hay resultados de la búsqueda: <?php echo $this->input->post('buscar',true); ?><br>Intente con otras palabras.</h4>
        </div>
      </div>
    <?php endif; ?>
  </div>
</div>

  <div class="modal fade" id="masinfo" tabindex="-1" role="dialog" aria-labelledby="myModal5Label" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <iframe frameborder="0" width="100%" height="550" marginheight="0" marginwidth="0" id="pdf"
          ></iframe>
        </div>
      </div>
    </div>
  </div>

 <div  class="modal fade" id="addshop" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-md">
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
         <h5 class="modal-title" id="descorta"></h5>
       </div>
       <div class="modal-body">
         <div>
         <div id="val1"></div>
         <div id="infoprod" class="col-md-6"></div>
         &#160;
         <div id="val2"></div>
         <div id="infoprod2" class="col-md-6"></div>
         &#160;
         <div id="val3"></div>
         <div id="infoprod3" class="col-md-6"></div>
         &#160;
         <div id="val4"></div>
         <div id="infoprod4" class="col-md-6"></div>
         &#160;
         <div id="val5"></div>
         <div id="infoprod5" class="col-md-6"></div>
         &#160;
         <div id="val6"></div>
         <div id="infoprod6" class="col-md-6"></div>
         &#160;
         <div id="vale"></div>
         <div id="infoes" class="col-md-6"></div>
         &#160;
         <div>
           <label class="control-label col-md-6">Cantidad</label>
        </div>
        <div class="col-md-6">
          <input type="number" id="cantidad" name="cantidad" class="form-control" value="1" min="1"/>
        </div>
         &#160;
         <div id="distp"></div>
        </div>
        <div>
          <input type="hidden" name="val_desc" id="val_desc" value="">
        </div>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
         <?php if(isset($this->session->userdata('popusa')[0]['idcliente'])): ?>
         <button type="submit" onclick="carrito('<?php echo $this->session->userdata('popusa')[0]['idcliente'];?>');" class="btn btn-primary">Agregar</button>
       <?php else: ?>
         <?php if(isset($this->session->userdata('popusa')[0]['rol_idrol'])): ?>
           <button type="submit" onclick="carrito('<?php echo $this->session->userdata('popusa')[0]['rol_idrol'].":".$this->session->userdata('popusa')[0]['idlogin'];?>');" class="btn btn-primary">Agregar</button>
         <?php else: ?>
           <button type="submit" onclick="carrito('<?php echo $this->session->userdata('tk');?>');" class="btn btn-primary">Agregar</button>
         <?php endif; ?>
       <?php endif;?>
       </div>
     </div>
   </div>
 </div>

<div class="modal fade" id="inf_extra">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <label>De ser necesario los siguientes servicios, haga favor de llenar los campos a continuación</label>
      </div>
      <div class="modal-body">
        <h4>Servicio 1 <em id="inf_s1"></em></h4>
        <div class="form-group">
          <div class="col-md-4">
          <?= form_label('','class="control-label" id="lb_s1"')?>
        </div>
        <div class="col-md-8">

          <input type="number" id="vs1" name="confeccion" class="form-control" value="0" min="0"/>
        </div>
        &#160;
        <div class="col-md-12">
        <small id="inf_conf" style="font-size: 65%;"></small>
        </div>
        </div>
        <hr>

        <h4>Servicio 2 <em id="inf_s2"></em></h4>
        <div class="form-group">
          <div class="col-md-4">
          <?= form_label('','class="control-label" id="lb_s2"')?>
        </div>
        <div class="col-md-8">
          <?= form_input(array('name'=>"uniones",'value' => 0,'class'=>"form-control",'type'=>'number','min'=>0,'id'=>'vs2'))?>
        </div>
        &#160;
        <div class="col-md-12">
        <small id="inf_confs2" style="font-size: 65%;"></small>
        </div>
        </div>
        <hr>

        <h4>Servicio 3 <em id="inf_s3"></em></h4>
        <div class="form-group">
          <div class="col-md-4">
          <?= form_label('','class="control-label" id="lb_s3"')?>
        </div>
        <div class="col-md-8">
          <?= form_input(array('name'=>"ojillos",'value' => 0,'class'=>"form-control",'type'=>'number','min'=>0,'id'=>'vs3'))?>
        </div>
        &#160;
        <div class="col-md-12">
        <small id="inf_confs3" style="font-size: 65%;"></small>
        </div>
        </div>
        &#160;
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <?php if($this->session->userdata('popusa')[0]['idcliente'] != null): ?>
        <button type="submit" onclick="info_extra('<?php echo $this->session->userdata('popusa')[0]['idcliente'];?>')" class="btn btn-primary">Agregar</button>
      <?php elseif($this->session->userdata('popusa')[0]['rol_idrol']):?>
        <button type="submit" onclick="info_extra('<?php echo $this->session->userdata('popusa')[0]['rol_idrol'].":".$this->session->userdata('popusa')[0]['idlogin']; ?>')" class="btn btn-primary">Agregar</button>
      <?php else: ?>
          <button type="submit" onclick="info_extra('<?php echo $this->session->userdata('tk');?>')" class="btn btn-primary">Agregar</button>
        <?php endif;?>
      </div>
    </div>
  </div>
</div>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE-edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="/sistema-popusa/admin/favicon.ico" type="image/ico">
  <link rel="stylesheet" href="<?php echo base_url('public/bootstrap/css/bootstrap.min.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('public/SbAdmin/css/sb-admin-2.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('public/metisMenu/metisMenu.min.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/Fonts/css/font-awesome.min.css');?>">
  <script src="<?php echo base_url('public/jquery/jquery.min.js');?>"></script>
  <link rel="stylesheet" href="<?= base_url('public/bootstrap-chosen/bootstrap-chosen.css');?>">
  <script src="<?php echo base_url('public/bootstrap/js/bootstrap.min.js');?>"></script>
  <script src="<?php echo base_url('public/metisMenu/metisMenu.min.js');?>"></script>
  <script src="<?php echo base_url('public/SbAdmin/js/sb-admin-2.min.js');?>"></script>
</head>

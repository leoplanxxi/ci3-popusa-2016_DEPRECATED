<style>
  .head{
    min-height: 20px;
    padding: 19px;
    margin-bottom: 20px;
    border: 1px solid #e3e3e3;
    border-radius: 4px;
    -webkit-box-shadow: 2px 2px 10px #999;
    box-shadow: 2px 2px 10px #999;
  }
</style>
<div class="col-md-8 col-md-offset-2 head" align="center">

  <img  src="<?php echo base_url($header[0]['imagen']);?>" class="img-responsive" />
  <br>
  <div align="justify">
    <?php echo nl2br($header[0]['texto']); ?>
  </div>
</div>

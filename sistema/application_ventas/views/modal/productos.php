<script src="<?php echo base_url('public/js/imask.js'); ?>"></script>
<script>
$(document).ready(function(){

  <?php if(strtoupper($this->input->post('familia')) == 'TEJIDOS'): ?>
  $.ajax({
    async:true,
    cache:false,
    dataType:"html",
    type: 'POST',
    url: '<?php echo base_url('index.php/productos/get_productos_extras') ?>',
    data: {'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
    success:  function(data){
        $('#mdinfoextra').html(data);
        //$("#inf_extra").modal();
    },
    beforeSend:function(){

    },
    error:function(objXMLHttpRequest){}
  });
  <?php else: ?>
  //$('#mdinfoextra').empty();
  <?php endif; ?>
  medidas('<?php echo $this->input->post('medidasmd',true); ?>');
});

function medidas(descorta){

  $("#infoprod2").html("").hide();
  $("#val2").html("").hide();
  $("#infoprod3").html("").hide();
  $("#val3").html("").hide();
  $("#infoprod4").html("").hide();
  $("#val4").html("").hide();
  $("#infoprod5").html("").hide();
  $("#val5").html("").hide();
  $("#infoprod6").html("").hide();
  $("#val6").html("").hide();

  $.post("<?php echo base_url('index.php/productos/get_medidas');?>",{"descorta":descorta },function(data){
    var json = JSON.parse(data);
    var label;
    var h = descorta;

    if(data){
      if(json[0].cant1){

        var select = $("<select id='val_1' name='val1' class='form-control'></select>");
      }

    var tam = json.length;
    var aux_unidad ="";

    if(json[0].var1){

      if(json[0].unidad1){

        aux_unidad=" en "+json[0].unidad1;
      }
      label = '<label class="control-label col-md-6">'+json[0].var1+aux_unidad+'</label>'
              +'<input type="hidden" name="udm1" value="ancho">';
    }

    for( var i = 0; i < json.length; i++ ) {
      if(json[i].cant1!=null){
        var option = $("<option value='"+json[i].cant1+"'></option>");
        option.append(json[i].cant1);
      }
      if(json[0].cant1){
      select.append(option);
    }
  }

  $("#infoprod").html(select);
  var opc;
  var auxdata=JSON.parse(data);

  if(auxdata[0]['var1']){
    opc = document.getElementById('val_1').value;

    if(opc!=""){

      medidas2(descorta,opc);
      var json_esc = {'descorta':descorta,'opc1':opc};
      escala(json_esc);
      $('#val_1').on('change', function() {
       opc = document.getElementById('val_1').value;

       medidas2(descorta,opc);
       var json_esc = {'descorta':descorta,'opc1':opc};
       escala(json_esc);
    });
    }



  }else{

    var json_esc = {'descorta':descorta};
    escala(json_esc);
    //ajax para obtener los precios

  }

}else{

  $("#infoprod").html("").show();
  label = '<label class="control-label col-md-12">El producto no tiene información que mostrar</label>&#160;';
}
$("#descorta").html(h);

$("#val1").html(label);
  document.getElementById('val_desc').value=h;
  })

  <?php if($this->session->userdata('popusa')[0]["rol_idrol"]==6): ?>
    var valor = valores(null);
    valor.descorta = descorta;
    valor.unidad = $('#escala').val();
    $.post('<?php echo base_url('index.php/productos/get_preciolist'); ?>',valor,function(item){

      $('#distp').html(item);
    });
    <?php endif; ?>

}

function medidas2(descorta2,opc1){

  $("#infoprod3").html("").hide();
  $("#val3").html("").hide();
  $("#infoprod4").html("").hide();
  $("#val4").html("").hide();
  $("#infoprod5").html("").hide();
  $("#val5").html("").hide();
  $("#infoprod6").html("").hide();
  $("#val6").html("").hide();

  $.post("<?php echo base_url('index.php/productos/get_medidas2');?>",{"descorta2":descorta2,"opc1":opc1},function(data2){
    var json2 = JSON.parse(data2);
    var label2;
    if(json2!=null){
      //json2[0].cant2!=null
      if(json2[0].cant2){
      var select2 = $("<select id='val_2' name='val2' class='form-control'></select>");
    }else{
        document.getElementById("infoprod2").style.display = "none";
        document.getElementById("val2").style.display = "none";
    }
      var aux_unidad2="";

      if(json2[0].var2!=null){
        if(json2[0].unidad2!=null){
          aux_unidad2=" en "+json2[0].unidad2;
        }

        label2 = '<label class="control-label col-md-6">'+json2[0].var2+aux_unidad2+'</label>'
        +'<input type="hidden" name="udm2" value="var2">';
        //medidas3(descorta2);
      }
      var option2 = null;
      for( var i = 0; i < json2.length; i++ ) {
        if(json2[i].cant2!=null || json2[0].cant2!=""){
          option2 = $("<option value='"+json2[i].cant2+"'></option>");
          option2.append(json2[i].cant2);
        }

        if(json2[0].cant2!=null || json2[0].cant2!=""){
        select2.append(option2);
        }
    }

    $("#infoprod2").html(select2).show();
    $("#val2").html(label2).show();

    if($('#val_2').val()!=null){
      var opc2 = document.getElementById('val_2').value;
      medidas3(descorta2,opc1,opc2);
      var json_esc = {'descorta':descorta2,'opc1':opc1,'opc2':opc2};
      escala(json_esc);

      $('#val_2').on('change', function() {
       var opc2 = document.getElementById('val_2').value;
       medidas3(descorta2,opc1,opc2);
       var json_esc = {'descorta':descorta2,'opc1':opc1,'opc2':opc2};
       escala(json_esc);
    })
    }


  }else {
    $("#infoprod2").html("").hide();
    $("#val2").html("").hide();
    //document.getElementById("val_2").value="";
    //document.getElementById("infoprod2").style.display = "none";
    //document.getElementById("val2").style.display = "none";

  }

  //ajax para obtener los precios

  });

  <?php if($this->session->userdata('popusa')[0]["rol_idrol"]==6): ?>
    var valor = valores(null);

    valor.descorta = descorta2;
    valor.unidad = $('#escala').val();
    $.post('<?php echo base_url('index.php/productos/get_preciolist'); ?>',valor,function(item){

      $('#distp').html(item);
    });
    <?php endif; ?>
}

function medidas3(descorta3,opc1,opc2){
  $("#infoprod4").html("").hide();
  $("#val4").html("").hide();
  $("#infoprod5").html("").hide();
  $("#val5").html("").hide();
  $("#infoprod6").html("").hide();
  $("#val6").html("").hide();
  $.post("<?php echo base_url('index.php/productos/get_medidas3');?>",{"descorta3":descorta3,"opc1":opc1,"opc2":opc2},function(data3){
    var json3 = JSON.parse(data3);

    var label3;
    if(data3!=undefined){
      if(json3!=undefined){
        // console.log(json3[0].var3);
      //json3[0].var3!=null
      //!=null || json3[0].var3!=undefined)
      if(json3[0].var3){
        var select3 = $("<select id='val_3' name='val3' class='form-control'></select>");
        var aux_unidad3="";
        //json3[0].unidad3!=null
        if(json3[0].unidad3!=null){
          aux_unidad3=" en "+json3[0].unidad3;
        }
        label3 = '<label class="control-label col-md-6">'+json3[0].var3+aux_unidad3+'</label>'
                +'<input type="hidden" name="udm3" value="var3">';
      }

      for( var i = 0; i < json3.length; i++ ) {
        if(json3[i].cant3!=null){
          var option3 = $("<option value='"+json3[i].cant3+"'></option>");
          option3.append(json3[i].cant3);
        }
        if(json3[0].cant3!=null){
        select3.append(option3);
      }
    }
    $("#infoprod3").html(select3).show();
    $("#val3").html(label3).show();
    var opc3;
    opc3 = document.getElementById('val_3').value;
    medidas4(descorta3,opc1,opc2,opc3);
    var json_esc = {'descorta':descorta3,'opc1':opc1,'opc2':opc2,'opc3':opc3};
    escala(json_esc);
    $('#val_3').on('change', function() {
     opc3 = document.getElementById('val_3').value;
     medidas4(descorta3,opc1,opc2,opc3);
     var json_esc = {'descorta':descorta3,'opc1':opc1,'opc2':opc2,'opc3':opc3};
     escala(json_esc);
   })
      }
      /*else{
        document.getElementById("infoprod3").style.display = "none";
        document.getElementById("val3").style.display = "none";
      }*/
  }else {
    $("#infoprod3").html("").hide();
    $("#val3").html("").hide();
    document.getElementById("val_3").value="";
    document.getElementById("infoprod3").style.display = "none";
    document.getElementById("val3").style.display = "none";

    //ajax para obtener los precios
  }
  });
  <?php if($this->session->userdata('popusa')[0]["rol_idrol"]==6): ?>
    var valor = valores(null);
    valor.descorta = descorta3;
    valor.unidad = $('#escala').val();
    $.post('<?php echo base_url('index.php/productos/get_preciolist'); ?>',valor,function(item){

      $('#distp').html(item);
    });
    <?php endif; ?>
}

function medidas4(descorta4,opc1,opc2,opc3){
  $("#infoprod5").html("").hide();
  $("#val5").html("").hide();
  $("#infoprod6").html("").hide();
  $("#val6").html("").hide();
  $.post("<?php echo base_url('index.php/productos/get_medidas4');?>",{"descorta4":descorta4,"opc1":opc1,"opc2":opc2,"opc3":opc3},function(data4){
    var json4 = JSON.parse(data4);

    var label4;
    //!=null || json4[0].var4!=undefined
    if(json4[0].var4){
      if(json4[0].cant4!=null){
        var select4 = $("<select id='val_4' name='val4' class='form-control'></select>");
        var aux_unidad4="";
        if(json4[0].var4!=null){
          if(json4[0].unidad4!=null){
            aux_unidad4=" en "+json4[0].unidad4;
          }
          label4 = '<label class="control-label col-md-6">'+json4[0].var4+aux_unidad4+'</label>'
                  +'<input type="hidden" name="udm4" value="var4">';
        }

        for( var i = 0; i < json4.length; i++ ) {
          if(json4[i].cant4!=null){

            var option4 = $("<option value='"+json4[i].cant4+"'></option>");
              option4.append(json4[i].cant4);
          }
          if(json4[0].cant4!=null){
          select4.append(option4);
        }
      }

      $("#infoprod4").html(select4).show();
      $("#val4").html(label4).show();
      var opc4;
      opc4 = document.getElementById('val_4').value;
      medidas5(descorta4,opc1,opc2,opc3,opc4);
      var json_esc = {'descorta':descorta4,'opc1':opc1,'opc2':opc2,'opc3':opc3};
      escala(json_esc);
      $('#val_4').on('change', function() {
       opc4 = document.getElementById('val_4').value;
       medidas5(descorta4,opc1,opc2,opc3,opc4);
       var json_esc = {'descorta':descorta4,'opc1':opc1,'opc2':opc2,'opc3':opc3,'opc4':opc4};
       escala(json_esc);
     })

      }

  }else {

    $("#infoprod4").html("").hide();
    $("#val4").html("").hide();

  }
  });

  <?php if($this->session->userdata('popusa')[0]["rol_idrol"]==6): ?>
    var valor = valores(null);
    valor.descorta = descorta4;
    valor.unidad = $('#escala').val();

    $.post('<?php echo base_url('index.php/productos/get_preciolist'); ?>',valor,function(item){

      $('#distp').html(item).show();
    });
    <?php endif; ?>

}

function medidas5(descorta5,opc1,opc2,opc3,opc4){

  $.post("<?php echo base_url('index.php/productos/get_medidas5')?>",{"descorta5":descorta5,"opc1":opc1,"opc2":opc2,"opc3":opc3,"opc4":opc4},function(data5){
    var json5 = JSON.parse(data5);
    var label5;
    if(data5!=undefined){
      //!=null
      if(json5[0].cant5){
        var select5 = $("<select id='val_5' name='val5' class='form-control'></select>");
        var aux_unidad5="";
        if(json5[0].var5!=null){
          if(json5[0].unidad5!=null){
            aux_unidad5=" en "+json5[0].unidad5;
          }
          label5 = '<label class="control-label col-md-6">'+json5[0].var5+aux_unidad5+'</label>'
                  +'<input type="hidden" name="udm5" value="var5">';
          //medidas5(descorta4);
        }

        for( var i = 0; i < json5.length; i++ ) {
          if(json5[i].cant5!=null){

            var option5 = $("<option value='"+json5[i].cant5+"'></option>");
              option5.append(json5[i].cant5);
          }
          if(json5[0].cant5!=null){
          select5.append(option5);
        }
      }
      $("#infoprod5").html(select5).show();
      $("#val5").html(label5).show();
      var opc5;
      opc5 = document.getElementById('val_5').value;
      medidas6(descorta5,opc1,opc2,opc3,opc4,opc5);
      var json_esc = {'descorta':descorta5,'opc1':opc1,'opc2':opc2,'opc3':opc3,'opc4':opc4,'opc5':opc5};
      escala(json_esc);
      $('#val_5').on('change', function() {
       opc5 = document.getElementById('val_3').value;
       medidas6(descorta5,opc1,opc2,opc3,opc4,opc5);
       var json_esc = {'descorta':descorta5,'opc1':opc1,'opc2':opc2,'opc3':opc3,'opc4':opc4,'opc5':opc5};
       escala(json_esc);
     });
      }else{
        document.getElementById("infoprod6").style.display = "none";
        document.getElementById("val6").style.display = "none";
        //medidas5(descorta4);
      }
  }else {

    $("#infoprod6").html("").hide();
    $("#val6").html("").hide();

  }

});
<?php if($this->session->userdata('popusa')[0]["rol_idrol"]==6): ?>
  var valor = valores(null);

  valor.descorta = descorta5;
  valor.unidad = $('#escala').val();
  $.post('<?php echo base_url('index.php/productos/get_preciolist'); ?>',valor,function(item){

    $('#distp').html(item);
  });
  <?php endif; ?>
}

function medidas6(descorta6,opc1,opc2,opc3,opc4,opc5){

  $.post("<?php echo base_url('index.php/productos/get_medidas6')?>",{"descorta6":descorta6,"opc1":opc1,"opc2":opc2,"opc3":opc3,"opc4":opc4,"opc5":opc5},function(data6){
    var json6 = JSON.parse(data6);
    var label6;
    if(data6!=undefined){
      //!=null
      if(json6[0].cant6){
        var select6 = $("<select id='val_6' name='val6' class='form-control'></select>");
        var aux_unidad6="";
        if(json6[0].var6!=null){
          if(json6[0].unidad6!=null){
            aux_unidad6=" en "+json6[0].unidad6;
          }
          label6 = '<label class="control-label col-md-6">'+json6[0].var6+aux_unidad6+'</label>'
                  +'<input type="hidden" name="udm6" value="var6">';
          //medidas5(descorta4);
        }

        for( var i = 0; i < json6.length; i++ ) {
          if(json6[i].cant6!=null){

            var option6 = $("<option value='"+json6[i].cant6+"'></option>");
              option6.append(json6[i].cant6);
          }
          if(json6[0].cant6!=null){
          select6.append(option6);
        }
      }
      $("#infoprod6").html(select6).show();
      $("#val6").html(label6).show();
      }else{
        document.getElementById("infoprod6").style.display = "none";
        document.getElementById("val6").style.display = "none";
        //medidas5(descorta4);
      }
  }else {

    $("#infoprod5").html("").hide();
    $("#val5").html("").hide();

  }

});
}
</script>
<div  class="modal fade" id="addshop">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h5 class="modal-title" id="descorta"></h5>
      </div>
      <div class="modal-body">
        <div>
        <div id="val1"></div>
        <div id="infoprod" class="col-md-6"></div>
        &#160;
        <div id="val2"></div>
        <div id="infoprod2" class="col-md-6"></div>
        &#160;
        <div id="val3"></div>
        <div id="infoprod3" class="col-md-6"></div>
        &#160;
        <div id="val4"></div>
        <div id="infoprod4" class="col-md-6"></div>
        &#160;
        <div id="val5"></div>
        <div id="infoprod5" class="col-md-6"></div>
        &#160;
        <div id="val6"></div>
        <div id="infoprod6" class="col-md-6"></div>
        &#160;
        <div id="vale"></div>
        <div id="infoes" class="col-md-6"></div>
        &#160;
        <div>
          <label class="control-label col-md-6">Cantidad</label>
       </div>
       <div class="col-md-6">
         <input type="text" id="cantidad" name="cantidad" class="form-control" value="1" min="1" />
       </div>
        &#160;
        <div id="distp"></div>
       </div>
       <div>
         <input type="hidden" name="val_desc" id="val_desc" value="">
       </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <?php if(isset($this->session->userdata('popusa')[0]['idcliente'])): ?>
          <?php if($this->session->userdata('popusa')[0]['idcliente']==0): ?>
            <button type="submit" onclick="carrito('<?php echo $this->session->userdata('popusa')[0]['rol_idrol'].":".$this->session->userdata('popusa')[0]['idlogin'];?>');" class="btn btn-primary">Agregar</button>
          <?php else: ?>
            <button type="submit" onclick="carrito('<?php echo $this->session->userdata('popusa')[0]['idcliente'];?>');" class="btn btn-primary">Agregar</button>
          <?php endif; ?>

      <?php else: ?>
      <?php if(isset($this->session->userdata('popusa')[0]['rol_idrol'])): ?>
          <button type="submit" onclick="carrito('<?php echo $this->session->userdata('popusa')[0]['rol_idrol'].":".$this->session->userdata('popusa')[0]['idlogin'];?>');" class="btn btn-primary">Agregar</button>
        <?php else: ?>
          <button type="submit" onclick="carrito('<?php echo $this->session->userdata('tk');?>');" class="btn btn-primary">Agregar</button>
        <?php endif; ?>
      <?php endif;?>
      </div>
    </div>
  </div>
</div>
<div id="mdinfoextra"></div>
<script>
var numberMask = new IMask(
document.getElementById('cantidad'),
{
  mask: Number,
  min: 1,
  max: 100000000000000000000000000000,
  thousandsSeparator: ' ',
  radix: '.',
  scale: 3,
});
</script>
<script>
var carrito_aux;
var carrito_ext;

function valores(id_cliente){

  if(document.getElementById("val_1")==null){

    var carrito ={
      id_cliente:id_cliente,
      descorta:document.getElementById("descorta").innerText,
      //unidad:document.getElementById("escala").value,//pendiente
      cantidad:document.getElementById("cantidad").value,
      <?php if(strtoupper($this->input->post('familia')) == 'TEJIDOS'): ?>
      cant_confeccion:document.getElementById("vs1").value,
      cant_costura:document.getElementById("vs2").value,
      cant_ojillos:document.getElementById("vs3").value
      <?php endif; ?>
    };

  }else{
    if(document.getElementById("val_2")==null){
      var carrito ={
        id_cliente:id_cliente,
        descorta:document.getElementById("descorta").innerText,
        variable1:document.getElementById("val_1").value,
        //unidad:document.getElementById("escala").value,//pendiente
        cantidad:document.getElementById("cantidad").value,
        <?php if(strtoupper($this->input->post('familia')) == 'TEJIDOS'): ?>
        cant_confeccion:document.getElementById("vs1").value,
        cant_costura:document.getElementById("vs2").value,
        cant_ojillos:document.getElementById("vs3").value
        <?php endif; ?>
      };
    }else {
      if(document.getElementById("val_3")==null){
      var carrito ={
          id_cliente:id_cliente,
          descorta:document.getElementById("descorta").innerText,
          variable1:document.getElementById("val_1").value,
          variable2:document.getElementById("val_2").value,
          //unidad:document.getElementById("escala").value,//pendiente
          cantidad:document.getElementById("cantidad").value,
          <?php if(strtoupper($this->input->post('familia')) == 'TEJIDOS'): ?>
          cant_confeccion:document.getElementById("vs1").value,
          cant_costura:document.getElementById("vs2").value,
          cant_ojillos:document.getElementById("vs3").value
          <?php endif; ?>
        };
      }else {
        if(document.getElementById("val_4")==null){
           var carrito ={
            id_cliente:id_cliente,
            descorta:document.getElementById("descorta").innerText,
            variable1:document.getElementById("val_1").value,
            variable2:document.getElementById("val_2").value,
            variable3:document.getElementById("val_3").value,
            //unidad:document.getElementById("escala").value,//pendiente
            cantidad:document.getElementById("cantidad").value,
            <?php if(strtoupper($this->input->post('familia')) == 'TEJIDOS'): ?>
            cant_confeccion:document.getElementById("vs1").value,
            cant_costura:document.getElementById("vs2").value,
            cant_ojillos:document.getElementById("vs3").value
            <?php endif; ?>
          };
        }else{
          if(document.getElementById("val_5")==null){
             var carrito ={
              id_cliente:id_cliente,
              descorta:document.getElementById("descorta").innerText,
              variable1:document.getElementById("val_1").value,
              variable2:document.getElementById("val_2").value,
              variable3:document.getElementById("val_3").value,
              variable4:document.getElementById("val_4").value,
              //unidad:document.getElementById("escala").value,//pendiente
              cantidad:document.getElementById("cantidad").value,
              <?php if(strtoupper($this->input->post('familia')) == 'TEJIDOS'): ?>
              cant_confeccion:document.getElementById("vs1").value,
              cant_costura:document.getElementById("vs2").value,
              cant_ojillos:document.getElementById("vs3").value
              <?php endif; ?>
            };
          }else{
            if(document.getElementById("val_6")==null){
               var carrito ={
                id_cliente:id_cliente,
                descorta:document.getElementById("descorta").innerText,
                variable1:document.getElementById("val_1").value,
                variable2:document.getElementById("val_2").value,
                variable3:document.getElementById("val_3").value,
                variable4:document.getElementById("val_4").value,
                variable5:document.getElementById("val_5").value,
                //unidad:document.getElementById("escala").value,//pendiente
                cantidad:document.getElementById("cantidad").value,
                <?php if(strtoupper($this->input->post('familia')) == 'TEJIDOS'): ?>
                cant_confeccion:document.getElementById("vs1").value,
                cant_costura:document.getElementById("vs2").value,
                cant_ojillos:document.getElementById("vs3").value
                <?php endif; ?>
              };
            }else {
               var carrito ={
                id_cliente:id_cliente,
                descorta:document.getElementById("descorta").innerText,
                variable1:document.getElementById("val_1").value,
                variable2:document.getElementById("val_2").value,
                variable3:document.getElementById("val_3").value,
                variable4:document.getElementById("val_4").value,
                variable5:document.getElementById("val_5").value,
                variable6:document.getElementById("val_6").value,
                //unidad:document.getElementById("escala").value,//pendiente
                cantidad:document.getElementById("cantidad").value,
                <?php if(strtoupper($this->input->post('familia')) == 'TEJIDOS'): ?>
                cant_confeccion:document.getElementById("vs1").value,
                cant_costura:document.getElementById("vs2").value,
                cant_ojillos:document.getElementById("vs3").value
                <?php endif; ?>
              };
            }
          }
        }
      }
    }
  }
  if(id_cliente!=null){

    carrito.unidad=document.getElementById("escala").value;
  }

    return carrito;
}

function carrito(id_cliente,opc = false){

  var carrito = valores(id_cliente);
  carrito_aux = carrito;

  $("#addshop").modal('hide');

  $.post("<?= base_url('index.php/productos/get_info_extra')?>",carrito,function(data){
    //console.log(JSON.parse(data));
    if(JSON.parse(data)[0].confeccion){
        $.post("<?= base_url('index.php/productos/get_desc')?>",{cod:JSON.parse(data)[0].confeccion},function(des){
          var get_desc = JSON.parse(des)[0];
          $('#inf_conf').html(get_desc.deslarga);
          $('#inf_s1').html(get_desc.descorta);
          if(get_desc.unidad1!=null){
            $('#lb_s1').html(get_desc.var1+" "+get_desc.unidad1);
          }else{
            $('#lb_s1').html(JSON.parse(des)[0].var1);
          }
        });

        $.post("<?= base_url('index.php/productos/get_desc')?>",{cod:JSON.parse(data)[0].costura},function(des){

          $('#inf_confs2').html(JSON.parse(des)[0].deslarga);
          $('#inf_s2').html(JSON.parse(des)[0].descorta);
          if(JSON.parse(des)[0].unidad1!=null){
            $('#lb_s2').html(JSON.parse(des)[0].var1+" "+JSON.parse(des)[0].unidad1);
          }else{
            $('#lb_s2').html(JSON.parse(des)[0].var1);
          }
        })

        $.post("<?= base_url('index.php/productos/get_desc')?>",{cod:JSON.parse(data)[0].ojillos},function(des){

          $('#inf_confs3').html(JSON.parse(des)[0].deslarga);
          $('#inf_s3').html(JSON.parse(des)[0].descorta);
          if(JSON.parse(des)[0].unidad1!=null){
            $('#lb_s3').html(JSON.parse(des)[0].var1+" "+JSON.parse(des)[0].unidad1);
          }else{
            $('#lb_s3').html(JSON.parse(des)[0].var1);
          }
        });

        /*if(document.getElementById("vs1").value==0 && document.getElementById("vs2").value==0  && document.getElementById("vs3").value==0){
          $("#inf_extra").modal('show');

        }else{
          $("#inf_extra").modal('hide');
          document.getElementById("vs1").value=0;
          document.getElementById("vs2").value=0;
          document.getElementById("vs3").value=0;
        }*/
        <?php if(strtoupper($this->input->post('familia')) == 'TEJIDOS'): ?>
        if($('#escala').val() != 'M2'){
          $('#vs1').prop( "disabled", true );
        }
        if(opc == false){
          $("#inf_extra").modal('show');
        }else{
          document.getElementById('cantidad').value = 1;
        }
        <?php else: ?>
        document.getElementById('cantidad').value = 1;
        <?php endif; ?>
    }else{
      $.post("<?= base_url('index.php/productos/add_carrito');?>",carrito,function(data){
        var shop = JSON.parse(data);
        $("#carcant").html(shop.length);
        if(shop.length>0){
          <?php if($this->session->userdata('popusa')[0]["rol_idrol"]==6): ?>
          var prodcar='<ol style="padding:0px"><div id="totalcarrito" style="height:320px; overflow:auto"></div></ol><ol style="padding:0px"><button class="btn btn-primary btn-block btn-sm" onclick="cotizar()">Crear Orden</button></ol>';
          <?php else: ?>
          var prodcar='<ol style="padding:0px"><div id="totalcarrito" style="height:320px; overflow:auto"></div></ol><ol style="padding:0px"><button class="btn btn-primary btn-block btn-sm" onclick="cotizar()">Cotizar</button></ol>';
          <?php endif; ?>

          $("#prodcar").html(prodcar);
        var totalcarrito="";
        var carrito="";
        for(var i=0;i<shop.length;i++){
          carrito = '<div class="col-md-12"><div style="padding:5px" class="col-md-4"><img  src="'+"<?php echo base_url('public/imagenes/')?>"+shop[i].imagen+'" class="img img-responsive thumbnail"></div>'+'<div class="col-md-6"><b>'+shop[i].descorta+'</b><br>Cantidad: '+shop[i].cantidad+'</div>'+'<div class="col-md-2">'+'<form>'+'<button type="button" class="close" aria-label="Close" onclick="del_carrito('+shop[i].id_cotizacion+')">'+'<span aria-hidden="true">&times;</span>'+'</button>'+'</form>'+'</div></div><ol role="separator" class="divider"></ol>';
          totalcarrito=totalcarrito+carrito;
        }
        $('#totalcarrito').html(totalcarrito);
      }else{
        var prodcar = '<ol style="padding:0px"><div style="text-align: center!important;font-weight: bold;">No hay productos agregados</div></ol>';
        $("#prodcar").html(prodcar);
      }
      });
    }
  });
carrito_ext = carrito;



}

function info_extra(id){
  $("#inf_extra").modal('hide');
  //$("[class*='modal-backdrop fade']").remove();
  carrito(id,true);
  $.post("<?= base_url('index.php/productos/add_carrito');?>",carrito_ext,function(data){
    var shop = JSON.parse(data);
    $("#carcant").html(shop.length);
    if(shop.length>0){
      <?php if($this->session->userdata('popusa')[0]["rol_idrol"]==6): ?>
      var prodcar='<ol style="padding:0px"><div id="totalcarrito" style="height:320px; overflow:auto"></div></ol><ol style="padding:0px"><button class="btn btn-primary btn-block btn-sm" onclick="cotizar()">Crear Orden</button></ol>';
      <?php else: ?>
      var prodcar='<ol style="padding:0px"><div id="totalcarrito" style="height:320px; overflow:auto"></div></ol><ol style="padding:0px"><button class="btn btn-primary btn-block btn-sm" onclick="cotizar()">Cotizar</button></ol>';
      <?php endif; ?>
      $("#prodcar").html(prodcar);
    var totalcarrito="";
    var carrito="";
    for(var i=0;i<shop.length;i++){
      carrito = '<div class="col-md-12"><div style="padding:5px" class="col-md-4"><img  src="'+"<?php echo base_url('public/imagenes/')?>"+shop[i].imagen+'" class="img img-responsive thumbnail"></div>'+'<div class="col-md-6"><b>'+shop[i].descorta+'</b><br>Cantidad: '+shop[i].cantidad+'</div>'+'<div class="col-md-2">'+'<form>'+'<button type="button" class="close" aria-label="Close" onclick="del_carrito('+shop[i].id_cotizacion+')">'+'<span aria-hidden="true">&times;</span>'+'</button>'+'</form>'+'</div></div><ol role="separator" class="divider"></ol>';
      totalcarrito=totalcarrito+carrito;
    }
    $('#totalcarrito').html(totalcarrito);

  }else{

    var prodcar = '<ol style="padding:0px"><div style="text-align: center!important;font-weight: bold;">No hay productos agregados</div></ol>';
    $("#prodcar").html(prodcar);

  }
  });

}



/*--------------------------------------------------------------------------------------------------------------------------*/

function escala(descorta){
  $("#vale").html("");
  $("#infoes").html("");
  $.post("<?= base_url('index.php/productos/get_escala');?>",descorta,function(data){
    var escala = JSON.parse(data);
    //var escala = data;
    var aux_escala="";
    if(escala.length!=0){
      label = '<label class="control-label col-md-6">Unidad</label>'
      /*if(escala.length==1){
        if(escala[0].u_base!=null){
          var select = $("<input id='escala' class='form-control' value='"+escala[0].u_base+"'>");
        }else{
          var select = $("<input id='escala' class='form-control' value='"+escala[0].u_aux+"'>");
        }
      }else{*/
      var mdescorta = descorta.descorta;
        var select = $('<select id="escala" name="escala" class="form-control" onchange="get_pdist(\''+mdescorta+'\')"></select>');
        if(escala[0].u_base2!=null){
          var option1 = $("<option value='"+escala[0].u_base2+"'></option>");
          option1.append(escala[0].u_base2);
          select.append(option1);
        }

        if(escala[0].u_aux2!=null){
          var option2 = $("<option value='"+escala[0].u_aux2+"'></option>");
          option2.append(escala[0].u_aux2);
          select.append(option2);
        }

      //}
      $("#vale").html(label);
      $("#infoes").html(select);
    }else{
      $("#vale").html("");
      $("#infoes").html("");
      document.getElementById("vale").style.diplay="none";
      document.getElementById("infoes").style.diplay="none";
    }
  });

}

function get_pdist(descorta){
  <?php if($this->session->userdata('popusa')[0]["rol_idrol"]==6): ?>
    var valor = valores(null);
    valor.descorta = descorta;
    valor.unidad = $('#escala').val();
    console.log(valor.unidad);
    $.post('<?php echo base_url('index.php/productos/get_preciolist'); ?>',valor,function(item){
      $('#distp').html(item);
    });
    <?php endif; ?>
}
</script>

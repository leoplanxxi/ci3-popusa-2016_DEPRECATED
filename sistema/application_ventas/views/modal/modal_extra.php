<div class="modal fade" id="inf_extra">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">

        <label>De ser necesario los siguientes servicios, haga favor de llenar los campos a continuación</label>
      </div>
      <div class="modal-body">
        <h4>Servicio 1 <em id="inf_s1"></em></h4>
        <div class="form-group">
          <div class="col-md-4">
          <?= form_label('','class="control-label" id="lb_s1"')?>
        </div>
        <div class="col-md-8">
          <?php
          $confecciones = array(
            'name' => 'confecion',
            'value' => 0,
            'type' => 'number',
            'min' => 0,
            'class' => 'form-control',
            'id' => 'vs1'
          );
          echo form_input($confecciones);
           ?>

        </div>
        &#160;
        <div class="col-md-12">
        <small id="inf_conf" style="font-size: 65%;"></small>
        </div>
        </div>
        <hr>

        <h4>Servicio 2 <em id="inf_s2"></em></h4>
        <div class="form-group">
          <div class="col-md-4">
          <?= form_label('','class="control-label" id="lb_s2"')?>
        </div>
        <div class="col-md-8">
          <?php
          $uniones = array(
            'name' => 'uniones',
            'value' => 0,
            'class' => 'form-control',
            'type' => 'number',
            'min' => 0,
            'id' => 'vs2',
            'onchange' => 'actojillos()'
          );
          echo  form_input($uniones)?>
        </div>
        &#160;
        <div class="col-md-12">
        <small id="inf_confs2" style="font-size: 65%;"></small>
        </div>
        </div>
        <hr>

        <h4>Servicio 3 <em id="inf_s3"></em></h4>
        <div class="form-group">
          <div class="col-md-4">
          <?= form_label('','class="control-label" id="lb_s3"')?>
        </div>
        <div class="col-md-8">
          <?php
          $ojillos = array(
            'name' => 'ojillos',
            'value' => 0,
            'class' => "form-control",
            'type' => "number",
            'min' => 0,
            'id' => 'vs3',
            'readonly' => true
          );
          echo form_input($ojillos)?>
        </div>
        &#160;
        <div class="col-md-12">
        <small id="inf_confs3" style="font-size: 65%;"></small>
        </div>
        </div>
        &#160;
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <?php

        if(isset($this->session->userdata('popusa')[0]['idcliente']) != null): ?>
        <button type="submit" onclick="info_extra('<?php echo $this->session->userdata('popusa')[0]['idcliente'];?>')" class="btn btn-primary">Agregar</button>
      <?php elseif($this->session->userdata('popusa')[0]['rol_idrol']):?>
        <button type="submit" onclick="info_extra('<?php echo $this->session->userdata('popusa')[0]['rol_idrol'].":".$this->session->userdata('popusa')[0]['idlogin']; ?>')" class="btn btn-primary">Agregar</button>
      <?php else: ?>
          <button type="submit" onclick="info_extra('<?php echo $this->session->userdata('tk');?>')" class="btn btn-primary">Agregar</button>
        <?php endif;?>
      </div>
    </div>
  </div>
</div>
</div>
<script>

    function actojillos(){
      if($('#vs2').val()!=0){
        $('#vs3').attr('readonly', false);
      }else{
        $('#vs3').val('0');
        $('#vs3').attr('readonly', true);
      }
    }

</script>

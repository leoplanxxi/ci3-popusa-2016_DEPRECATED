
<div class="container-fluid" style="margin-top:40px;">
    <div class="row">
      <page>
        <page-header>
        <div class="col-md-12">
          <table class="table" style="margin-bottom:0px;">
            <thead>
            <tr>
              <th>
                <img class="img-responsive img-mark" src="<?php echo  base_url('public/img/logo-paket.jpg');?>">
              </th>
              <th>
                <div style="font-size:12px;margin:0px;"><b>Catalogo de precios</b></div>
                <div style="font-size:10px;margin:0px;">VALIDA DESDE EL <?php echo strftime("%A, %d de %B de %Y",strtotime($validacion[0]['creacion']));?></div>
              </th>
            </tr>
              </thead>
          </table>
          </div>
        </page-header>
      <?php foreach($familiasp as $fam):?>
        <div class="col-md-12">
          <div class="col-md-4" style="float:left;background-color:<?php echo $fam->color;?>; margin-bottom:10px; color:white;" align="center"><b><em><?php echo $fam->familia."<br>"; ?></em></b></div>
        </div>
        <div class="col-md-12">

          <?php
           $subfamilia = $this->productos_model->get_subfxfamp($fam->familia);
           //var_dump($subfamilia);

          ?>
          <?php  foreach($subfamilia as $subf):?>
            <div class="col-md-4" style="float:left;background-color:<?php echo $fam->color;?>; color:white;" align="center"><small><b><em><?php echo $subf['subfamilia']."<br>"; ?></em></b></small></div>
            <?php
            $descripcion = $this->productos_model->get_descv($subf['subfamilia']);


             ?>

            <?php foreach($descripcion as $d): ?>

            <div class="col-md-4" style="float:left;background-color:<?php echo $fam->color;?>; color:white;" align="center"><small><b><em><?php echo $d["descorta"]."<br>"; ?></em></b></small></div>

            <table class="table table-bordered" >
              <tr>
              <thead style="background-color:#efefef;">
                  <th width="5"></th>
                  <th style="background-color:#efefef;" width="25">CATALOGO</th>
                  <th style="background-color:#efefef;" width="100">DESCRIPCIÓN</th>
                  <th style="background-color:#efefef;" width="50">
                  <?php if($d['var1']){
                  echo $d['var1'].' '.$d['unidad1'];
                  }else{
                    echo "valor";
                  }
                  ?>
                  </th>
                  <th style="background-color:#efefef;" width="50"><?php if($d['var2']){
                    echo $d['var2'].' '.$d['unidad2'];
                  }else{
                    echo "valor";
                  }
                  ?></th>
                  <th style="background-color:#efefef;" width="50"><?php if($d['var3']){
                    echo $d['var3'].' '.$d['unidad3'];
                  }else{
                    echo "valor";
                  }
                  ?></th>
                  <th style="background-color:#efefef;" width="600">GENERAL</th>
                  <th style="background-color:#efefef;" width="100">FOTOGRAFÍA</th>
                  <th style="background-color:#efefef;" width="250">DESCRIPCIÓN DE USO</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $paket=$this->productos_model->get_list_paket($fam->familia,$subf['subfamilia'],$d["descorta"]);
                ?>
                <?php foreach($paket as $a): ?>

                  <tr>
                    <th><?php echo $a['rotacion'];?></th>
                    <th><?php echo $a['codigo'];?></th>
                    <th ><?php echo $a['descorta']."<br>".$a['desespecial']."<br><a target='_blank' href='".base_url('public/pdfs/').$a['pdf']."'>Ficha Técnica</a>";?></th>
                    <th><?php echo $a['cant1'];?></th>
                    <th><?php echo $a['cant2'];?></th>
                    <th><?php echo $a['cant3'];?></th>
                    <th style="padding:0px;">
                      <?php
                      $cvpp = $a['cvpp'];
                      $array = str_split($cvpp);
                      ?>
                      <table class="table table-bordered" style="margin-bottom:0px;">
                        <thead>
                          <tr>
                            <?php if($tipo=='bonasa'): ?>
                            <th style="color:red;">BONASA</th>
                            <th style="color:red;">PRECIO PUBLICO SUGERIDO</th>
                          <?php elseif($tipo=='distribuidor'): ?>
                            <th style="color:red;">PRECIO DISTRIBUIDOR</th>
                            <th style="color:red;">PRECIO MINIMO</th>
                            <th style="color:red;">PRECIO PUBLICO SUGERIDO</th>
                          <?php else: ?>
                            <th style="color:red;">P0</th>
                            <th style="color:red;">P1</th>
                            <th style="color:red;">P2</th>
                            <th style="color:red;">PL</th>
                          <?php endif; ?>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <?php if($tipo!='distribuidor' && $tipo!='bonasa'): ?>
                            <th style="padding:0px;">
                              <table class="table table-bordered" style="margin-bottom:0px;">
                                <?php

                                $p0 = (1*$f0p*$a['precio']);

                                $aux_cantax = (1*$f0p*$a['pre_aux'])*$iva[0]['Valor'];

                                if($a['iva'] == 'Si'){

                                  $p0ax = (1*$f0p*$a['pxua'])*$iva[0]['Valor'];
                                  $aux_cant = (1*$f0p*$a['pxub'])*$iva[0]['Valor'];

                                }else{

                                  $p0ax = (1*$f0p*$a['pxua'])*1;
                                  $aux_cant = (1*$f0p*$a['pxub']);

                                }




                                ?>
                                <thead>
                                  <tr>
                                    <th style="background-color:#efefef;"><?php echo $a['u_base2'];?></th>
                                    <th style="background-color:yellow;"><?php echo $a['u_aux2'];?></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>
                                    <?php
                                    if($array[0]==0){
                                      echo "No aplica";
                                    }else{
                                      echo money_format("%n", $aux_cant);
                                    }
                                    ?>
                                    </td>

                                    <td style="background-color:yellow;">
                                      <?php
                                    if($array[1]==0){
                                      echo "No aplica";
                                    }else{
                                      echo money_format("%n", $p0ax);
                                    }
                                     ?>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                           </th>
                         <?php endif; ?>
                         <?php if($tipo!='bonasa'): ?>
                            <th style="padding:0px;">
                              <table class="table table-bordered" style="margin-bottom:0px;">
                                <?php
                                $p1 = (1*$f0p*$f1p*$a['precio']);
                                $aux_cantax = (1*$f0p*$f1p*$a['pre_aux']);
                                //$aux = (1*$f0p*$f1p*$a['precio']);
                                //$p1ax = (1*$f0p*$f1p*$a['pre_aux']);
                                if($a['iva'] == 'Si'){
                                  $aux = (1*$f0p*$f1p*$a['pxub'])*$iva[0]['Valor'];
                                  $p1ax = (1*$f0p*$f1p*$a['pxua'])*$iva[0]['Valor'];
                                }else{
                                  $aux = (1*$f0p*$f1p*$a['pxub']);
                                  $p1ax = (1*$f0p*$f1p*$a['pxua'])*1;
                                }

                                 ?>
                                <thead>
                                  <tr>
                                    <th style="background-color:#efefef;"><?php echo $a['u_base2'];?></th>
                                    <th style="background-color:yellow;"><?php echo $a['u_aux2'];?></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>
                                      <?php
                                      if($array[2]==0){
                                        echo "No aplica";
                                      }else{
                                        echo money_format("%n", $aux);

                                      }
                                      ?>
                                    </td>
                                    <td style="background-color:yellow;">
                                      <?php
                                    if($array[3]==0){
                                      echo "No aplica";
                                    }else{
                                      echo money_format("%n", $p1ax);
                                    }
                                     ?>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </th>
                          <?php endif; ?>
                            <th style="padding:0px;">
                              <table class="table table-bordered" style="margin-bottom:0px;">
                                <?php
                                  $p2 = (1*$f0p*$f1p*$f2p*$a['precio']);
                                  $aux_cantax = (1*$f0p*$f1p*$f2p*$a['pre_aux']);
                                  //$aux_cant = (1*$f0p*$f1p*$f2p*$a['precio']);
                                  //$p2ax = (1*$f0p*$f1p*$f2p*$a['pre_aux']);
                                  if($a['iva']=='Si'){
                                    $p2ax = (1*$f0p*$f1p*$f2p*$a['pxua'])*$iva[0]['Valor'];
                                    $aux_cant = (1*$f0p*$f1p*$f2p*$a['pxub'])*$iva[0]['Valor'];

                                  }else{
                                    $p2ax = (1*$f0p*$f1p*$f2p*$a['pxua'])*1;
                                    $aux_cant = (1*$f0p*$f1p*$f2p*$a['pxub']);
                                  }

                                 ?>
                                <thead>
                                  <tr>
                                    <th style="background-color:#efefef;"><?php echo $a['u_base2'];?></th>
                                    <th style="background-color:yellow;"><?php echo $a['u_aux2'];?></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>
                                      <?php
                                      if($array[4]==0){
                                        echo "No aplica";
                                      }else{
                                        echo money_format("%n", $aux_cant);
                                      }
                                      ?>
                                    </td>
                                    <td style="background-color:yellow;">
                                      <?php
                                    if($array[5]==0){
                                      echo "No aplica";
                                    }else{
                                      if(($a['cant2']*1)==0){
                                        echo money_format("%n", $aux_cantax);
                                      }else{
                                        echo money_format("%n", $p2ax);
                                      }
                                    }
                                     ?>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </th>
                            <th style="padding:0px;">
                              <table class="table table-bordered" style="margin-bottom:0px;">
                                <?php
                                $p3 = (1*$f0p*$f1p*$f2p*$f3p*$a['precio']);
                                $aux_cantax = (1*$f0p*$f1p*$f2p*$f3p*$a['pre_aux']);
                                //$aux_cant = (1*$f0p*$f1p*$f2p*$f3p*$a['precio']);
                                //$p3ax = (1*$f0p*$f1p*$f2p*$f3p*$a['pre_aux']);
                                if($a['iva'] == 'Si'){

                                  $aux_cant = (1*$f0p*$f1p*$f2p*$f3p*$a['pxub'])*$iva[0]['Valor'];
                                  $p3ax = (1*$f0p*$f1p*$f2p*$f3p*$a['pxua'])*$iva[0]['Valor'];

                                }else{

                                  $aux_cant = (1*$f0p*$f1p*$f2p*$f3p*$a['pxub']);
                                  $p3ax = (1*$f0p*$f1p*$f2p*$f3p*$a['pxua'])*1;

                                }

                                ?>
                                <thead>
                                  <tr>
                                    <th style="background-color:#efefef;"><?php echo $a['u_base2'];?></th>
                                    <th style="background-color:yellow;"><?php echo $a['u_aux2'];?></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>
                                      <?php
                                      if($array[6]==0){
                                        echo "No aplica";
                                      }else{
                                        echo money_format("%n", $aux_cant);

                                      }
                                      ?>
                                    </td>
                                    <td style="background-color:yellow">
                                      <?php
                                    if($array[7]==0){
                                      echo "No aplica";
                                    }else{
                                      if(($a['cant2']*1)==0){
                                        echo money_format("%n", ceil($aux_cantax));
                                      }else{
                                        echo money_format("%n", ceil($p3ax));
                                      }
                                    }
                                     ?>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </th>
                          </tr>
                        </tbody>
                      </table>
                      <table class="table" style="margin-bottom:0px;">
                        <tbody>
                          <tr>
                            <td>
                              <small>
                                <b>
                                  <?php echo $a['iva']." causa IVA" ?>
                                </b>
                              </small>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </th>
                    <th><img class="img img-responsive" src="<?php echo base_url('public/imagenes/'.$a['img']); ?>" width="100" height="100"/></th>
                    <th style="font-size:8px;"><?php echo $a['deslarga'] ?></th>
                  </tr>

                <?php endforeach;?>
              </tbody>
            </table>
          <?php endforeach; ?>
          <?php  endforeach;?>

        </div>
      <?php endforeach;?>
    </page>
    </div>
  </div>
</div>
<script src="<?php echo base_url('public/html2pdf.js/dist/html2pdf.bundle.js') ?>"></script>
<script>
function myCallback(pdf) {
console.log(pdf);
}
var element = document.getElementById('pdf_paket');
var opt = {
  margin:       [0.5, 0, 0.5, 0],
  filename:     '<?php echo date('dmyhi')."_lista_viajera_".$tipo;?>.pdf',
  image:        { type: 'jpeg', quality: 1 },
  pagebreak:    {mode: ['all','css', 'legacy']},
  html2canvas:  { dpi: 600, letterRendering: true,useCORS: true},
  jsPDF:        { unit: 'mm', format: 'legal', orientation: 'l',compress:true, },
  pdfCallback: myCallback
};

// html2pdf().set(opt).from(element).save();
html2pdf().set(opt).from(element).outputPdf('blob').then(function (myBlob) {
	var formData = new FormData();
    formData.append('pdf', myBlob);
    $.ajax({
        type: 'POST',
        url: '<?php echo base_url('index.php/list_v/savepdf/'.$tipo); ?>',
        data: formData,
        contentType: true, // set accordingly
        processData: false
    }).done(function(data) {
           //console.log(data);
           window.location.replace('<?php echo base_url('index.php/cargar/lista'); ?>')
    });
});

</script>

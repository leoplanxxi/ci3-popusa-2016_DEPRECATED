
  <style>
  .img-mark{

    width: 15%;
    display: inline-table;
    float: left;
    padding: 10px;
}
.table>thead>tr>th {
  padding: 1px 4px;
}
.table>tbody>tr>td, .table>tbody>tr>th,.table>thead>tr>td, .table>thead>tr>th{
  vertical-align: middle;
  padding: 1px 4px;
}
table th{
  text-align: center;
  font-size: 8px;
}
table td{
  text-align: center;
  font-size: 8px;
}
table { page-break-inside:auto }
tr    { page-break-inside:avoid; page-break-after:auto }
  </style>
<div id="pdf_paket">
  <div class="container-fluid rows-print-as-pages" style="margin-top:40px;" >
    <div class="row">
    <page>
      <page-header>
      <div class="col-md-12">
        <table class="table" style="margin-bottom:0px;">
          <thead>
          <tr>
            <th>
              <img class="img-responsive img-mark" src="<?php echo  base_url('public/img/logo-alianza.jpg');?>">
            </th>
            <th>
              <div style="font-size:12px;margin:0px;"><b>Catalogo de precios</b></div>
              <div style="font-size:10px;margin:0px;">VALIDA DESDE EL <?php echo strftime("%A, %d de %B de %Y",strtotime($validacion[0]['creacion']));?></div>
            </th>
          </tr>
            </thead>
        </table>
        </div>
      </page-header>

      <?php foreach($familias as $fam):?>
        <div class="col-md-12">
          <div class="col-md-4 " style="float:left;background-color:<?php echo $fam->color;?>; margin-bottom:10px; color:white;" align="center"><b><em><?php echo $fam->familia."<br>"; ?></em></b></div>
        </div>
        <div class="col-md-12">

          <?php
           $subfamilia = $this->productos_model->get_subfxfam($fam->familia);
          ?>
          <?php  foreach($subfamilia as $subf):?>
          <div class="col-md-4" style="float:left;background-color:<?php echo $fam->color;?>;  color:white;" align="center"><small><b><em><?php echo $subf['subfamilia']."<br>"; ?></em></b></small></div>
          <?php $descripcion = $this->productos_model->get_descv($subf['subfamilia']); ?>
          <?php  foreach($descripcion as $d): ?>
            <br>
          <div class="col-md-4" style="float:left;background-color:<?php echo $fam->color;?>;  color:white;" align="center"><small><b><em><?php echo $d["descorta"]."<br>"; ?></em></b></small></div>

            <table class="table table-bordered" >
              <tr>
              <thead >
                <th style="background-color:#efefef;" width="5"></th>
                  <th style="background-color:#efefef;" width="25">CATALOGO</th>
                  <th style="background-color:#efefef;" width="100">DESCRIPCIÓN</th>
                  <th style="background-color:#efefef;" width="50">
                  <?php
                    if($d['var1']){
                  echo $d['var1'].' '.$d['unidad1'];
                  }else{
                    echo "valor";
                  }
                  ?>
                  </th>
                  <th style="background-color:#efefef;" width="50"><?php if($d['var2']){
                    echo $d['var2'].' '.$d['unidad2'];
                  }else{
                    echo "valor";
                  }
                  ?></th>
                  <th style="background-color:#efefef;" width="50"><?php if($d['var3']){
                    echo $d['var3'].' '.$d['unidad3'];
                  }else{
                    echo "valor";
                  }
                  ?></th>
                  <th style="background-color:#efefef;" width="600">GENERAL</th>
                  <?php switch (strtoupper($tipo)) {

                    case 'T23P':
                      echo '<th style="background-color:#efefef;" width="200" > TIENDA 23 PTE </th>';
                      break;

                    case 'V23P':
                      echo '<th style="background-color:#efefef;" width="200" > VENDEDOR 23 PTE </th>';
                      break;

                    case 'AQUIX':
                      echo '<th style="background-color:#efefef;" width="200" > AQUIXTLA </th>';
                      break;

                    case 'THUIX':
                      echo '<th style="background-color:#efefef;" width="200" > TIENDA HUIXCOLOTLA </th>';
                      break;

                    case 'VHUIX':
                      echo '<th style="background-color:#efefef;" width="200" > VENDEDOR HUIXCOLOTLA </th>';
                      break;

                    case 'TCA':
                      echo '<th style="background-color:#efefef;" width="200" > C. ABASTOS TIENDA </th>';
                      break;

                    case 'VCA':
                      echo '<th style="background-color:#efefef;" width="200" > C. ABASTOS VENDEDOR </th>';
                      break;
                    case 'BONANZA':
                    break;



                  } ?>
                  <th style="background-color:#efefef;" width="100">FOTOGRAFÍA</th>
                  <th style="background-color:#efefef;" width="250">
                    DESCRIPCIÓN DE USO
                  </th>

                </tr>
              </thead>

              <tbody>
                <?php
                $alianza=$this->productos_model->get_list_alianza($fam->familia,$subf['subfamilia'],$d["descorta"]);
                //,$d["descorta"]

                ?>
                <?php foreach($alianza as $a): ?>

                  <tr>
                    <th><?php echo $a['rotacion'];?></th>
                    <th><?php echo $a['codigo'];?></th>
                    <th ><?php echo $a['descorta']."<br>".$a['desespecial']."<br><a target='_blank' href='".base_url('public/pdfs/').$a['pdf']."'>Ficha Técnica</a>";?></th>
                    <th><?php echo $a['cant1'];?></th>
                    <th><?php echo $a['cant2'];?></th>
                    <th><?php echo $a['cant3'];?></th>
                    <th style="padding:0px;">
                      <?php
                      $cvpp = $a['cvpp'];
                      $array = str_split($cvpp);//código de precio base y precio auxiliar
                      ?>
                      <table class="table table-bordered" style="margin-bottom:0px;">
                        <thead>
                          <tr>
                            <?php if($tipo=='bonasa'): ?>
                            <th style="color:red;">BONASA</th>
                            <th style="color:red;">PRECIO PUBLICO SUGERIDO</th>
                          <?php elseif($tipo=='distribuidor'): ?>
                            <th style="color:red;">PRECIO DISTRIBUIDOR</th>
                            <th style="color:red;">PRECIO MINIMO</th>
                            <th style="color:red;">PRECIO PUBLICO SUGERIDO</th>
                          <?php else: ?>
                            <th style="color:red;">P0</th>
                            <th style="color:red;">P1</th>
                            <th style="color:red;">P2</th>
                            <th style="color:red;">PL</th>
                          <?php endif; ?>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <?php if($tipo!='distribuidor' && $tipo!='bonasa'): ?>

                            <th style="padding:0px;" id="p0">
                              <table class="table table-bordered" style="margin-bottom:0px;" id="tablep0">
                                <?php
                                //$a['cant1']
                                //$p0 = (1*$f0a*$a['precio'])*1;
                                //$aux_cantax = (1*$f0a*$a['pre_aux']);

                                //echo $a['iva'];

                                if($a['iva'] == 'Si'){

                                  $aux_cant = (1*$f0a*$a['pxub'])*$iva[0]['Valor'];
                                  $p0ax = (1*$f0a*$a['pxua'])*$iva[0]['Valor'];

                                }else{

                                  $aux_cant = (1*$f0a*$a['pxub']);
                                  $p0ax = (1*$f0a*$a['pxua'])*1;

                                }


                                //$aux = $a['pxub'];
                                //$p1ax = $a['pxua'];
                                ?>
                                <thead>
                                  <tr>
                                    <th style="background-color:#efefef;">
                                      <?php echo $a['u_base2'];?>
                                    </th>
                                    <th style="background-color:yellow;">
                                      <?php echo $a['u_aux2'];?>
                                    </th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>

                                    <?php
                                    if($array[0]==0){
                                      echo "No aplica";
                                    }else{
                                      echo money_format("%n", $aux_cant);
                                    }
                                    ?>

                                    </td>

                                    <td style="background-color:yellow;">
                                      <?php
                                    if($array[1]==0){
                                      echo "No aplica";
                                    }else{
                                      echo money_format("%n", $p0ax);
                                    }
                                     ?>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>

                           </th>
                         <?php endif; ?>

                         <?php if($tipo!='bonasa'): ?>
                            <th style="padding:0px;" id="p1">
                              <table class="table table-bordered" style="margin-bottom:0px;">

                                <?php

                                //echo $a['cant1'];
                                //$p1 = (1*$f0a*$f1a*$a['precio'])*1;
                                //$aux_cantax = (1*$f0a*$f1a*$a['pre_aux']);

                                //echo $a['iva'];

                                if($a['iva'] == 'Si'){

                                  $aux = (1*$f0a*$f1a*$a['pxub'])*$iva[0]['Valor'];
                                  $p1ax = (1*$f0a*$f1a*$a['pxua'])*$iva[0]['Valor'];

                                }else{
                                  $aux = (1*$f0a*$f1a*$a['pxub']);
                                  $p1ax = (1*$f0a*$f1a*$a['pxua'])*1;
                                }


                                //$aux = $a['pxub'];
                                //$p1ax = $a['pxua'];


                                 ?>
                                <thead>
                                  <tr>
                                    <th style="background-color:#efefef;"><?php echo $a['u_base2'];?></th>
                                    <th style="background-color:yellow;"><?php echo $a['u_aux2'];?></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>

                                    <td>
                                      <?php

                                      if($array[2]==0){
                                        echo "No aplica";
                                      }else{
                                        echo money_format("%n", $aux);
                                      }

                                      ?>
                                    </td>

                                    <td style="background-color:yellow;">
                                      <?php

                                    if($array[3]==0){
                                      echo "No aplica";
                                    }else{
                                      echo money_format("%n", $p1ax);
                                    }

                                     ?>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </th>
                          <?php endif;  ?>

                            <th style="padding:0px;" id="p2">
                              <table class="table table-bordered" style="margin-bottom:0px;">

                                <?php
                                //echo $a['cant1'];
                                  //$p2 = (1*$f0a*$f1a*$f2a*$a['precio'])*1;
                                  //echo $p2."<br>";
                                  //echo $p2ax."<br>";

                                  $aux_cantax = (1*$f0a*$f1a*$f2a*$a['pre_aux']);
                                  //echo $aux_cantax."<br>";
                                  //$aux = $a['pxub'];
                                  //$p1ax = $a['pxua'];

                                  //echo $a['iva'];

                                  if($a['iva'] == 'Si'){

                                    $aux_cant = (1*$f0a*$f1a*$f2a*$a['pxub'])*$iva[0]['Valor'];
                                    $p2ax = (1*$f0a*$f1a*$f2a*$a['pxua'])*$iva[0]['Valor'];

                                  }else{

                                    $aux_cant = (1*$f0a*$f1a*$f2a*$a['pxub']);
                                    $p2ax = (1*$f0a*$f1a*$f2a*$a['pxua'])*1;

                                  }
                                 ?>

                                <thead>
                                  <tr>
                                    <th style="background-color:#efefef;">
                                      <?php echo $a['u_base2'];?>
                                    </th>
                                    <th style="background-color:yellow;">
                                      <?php echo $a['u_aux2'];?>
                                    </th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>
                                      <?php

                                      if($array[4]==0){
                                        echo "No aplica";
                                      }else{
                                        echo money_format("%n", $aux_cant);
                                      }

                                      ?>
                                    </td>
                                    <td style="background-color:yellow;">
                                      <?php
                                      if($array[5]==0){
                                        echo "No aplica";
                                      }else{

                                        if(($a['cant2']*1)==0){
                                          echo money_format("%n", $aux_cantax);
                                        }else{
                                          echo money_format("%n", $p2ax);
                                          //echo money_format("%n", $aux_cantax);

                                        }
                                      }
                                     ?>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </th>

                            <th style="padding:0px;" id="p3">
                              <table class="table table-bordered" style="margin-bottom:0px;">
                                <?php

                                $p3 = (1*$f0a*$f1a*$f2a*$f3a*$a['precio'])*1;
                                $aux_cantax = (1*$f0a*$f1a*$f2a*$f3a*$a['pre_aux']);


                                //echo $a['iva'];

                                if($a['iva'] == 'Si'){

                                  $aux_cant = (1*$f0a*$f1a*$f2a*$f3a*$a['pxub'])*$iva[0]['Valor'];
                                  $p3ax = (1*$f0a*$f1a*$f2a*$f3a*$a['pxua'])*$iva[0]['Valor'];

                                }else{

                                  $aux_cant = (1*$f0a*$f1a*$f2a*$f3a*$a['pxub']);
                                  $p3ax = (1*$f0a*$f1a*$f2a*$f3a*$a['pxua'])*1;


                                }
                                //$aux = $a['pxub'];
                                //$p1ax = $a['pxua'];
                                ?>
                                <thead>
                                  <tr>
                                    <th style="background-color:#efefef;"><?php echo $a['u_base2'];?></th>
                                    <th style="background-color:yellow;"><?php echo $a['u_aux2'];?></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>
                                      <?php
                                      if($array[6]==0){
                                        echo "No aplica";
                                      }else{
                                        echo money_format("%n", $aux_cant);

                                      }
                                      ?>
                                    </td>
                                    <td style="background-color:yellow">
                                      <?php
                                    if($array[7]==0){
                                      echo "No aplica";
                                    }else{
                                      if(($a['cant2']*1)==0){
                                        echo money_format("%n", ceil($aux_cantax));
                                      }else{
                                        echo money_format("%n", ceil($p3ax));
                                        //echo money_format("%n", ceil($aux_cantax));

                                      }
                                    }
                                     ?>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </th>
                          </tr>
                        </tbody>
                      </table>
                      <table class="table" style="margin-bottom:0px;">
                        <tbody>
                          <tr>
                            <td>
                              <small>
                                <b>
                                  <?php echo $a['iva']." causa IVA" ?>
                                </b>
                              </small>
                              <br>
                                <b style="color:red;">
                                  <?php echo $a['deslivi'] ?>
                                </b>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </th>
                    <?php

                    switch (strtoupper($tipo)) {

                      case 'T23P':
                      case 'V23P':
                      case 'AQUIX':
                      case 'THUIX':
                      case 'VHUIX':
                      case 'TCA':
                      case 'VCA':
                      $p2 = (1*$f0a*$f1a*$f2a*$a['precio'])*1;
                      $aux_cant = (1*$f0a*$f1a*$f2a*$a['precio']);

                      $p2ax = (1*$f0a*$f1a*$f2a*$a['pre_aux'])*1;

                      $aux_cantax = (1*$f0a*$f1a*$f2a*$a['pre_aux']);
                        echo '<th style="padding:0px;">
                                <table class="table" style="margin-bottom:0px;">
                                  <thead>
                                  <tr>
                                    <th>
                                      PRECIO UNICO
                                    </th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                    <td style="padding:0px;">
                                      <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                        <th style="background-color:#efefef;" width="100" >'.$a['u_base2'].
                                        '</th>
                                        <th style="background-color:yellow;" width="100" >'.$a['u_aux2'].'</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                          <tr>
                                          <td>';
                                          if($array[4]==0){
                                            echo "No aplica";
                                          }else{
                                            echo money_format("%n", $aux);
                                          }
                                          echo '
                                            </td>
                                            <td>';
                                            if($array[5]==0){
                                              echo "No aplica";
                                            }else{
                                              if(($a['cant2']*1)==0){
                                                echo money_format("%n", $aux_cantax);
                                              }else{
                                                echo money_format("%n", $p2ax);
                                              }
                                            }
                                          echo  '</td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </th>';
                        break;


                    } ?>
                    <th><img class="img img-responsive" src="<?php echo base_url('public/imagenes/'.$a['img']); ?>" width="100" height="100"/></th>
                    <th style="font-size:8px;"><?php echo $a['deslarga'] ?></th>
                  </tr>

                <?php endforeach;?>
                <?php //var_dump($subf['subfamilia']);
                //die(); ?>
              </tbody>
            </table>
          <?php endforeach; ?>
          <?php  endforeach;?>

        </div>
      <?php endforeach;?>

    </div>
  </page>
  </div>

<style>
  .chosen-container{
    width: 100% !important;
  }
</style>
<div class="modal fade" id="clientes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel"><b>Lista de Clientes</b></h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php if(isset($clientes)): ?>
      <?php echo form_open('index.php/carrito/set_cliente'); ?>
      <div class="modal-body">
        <?php echo form_label('Seleccione el Cliente','class="control-label"'); ?>
        <?php echo form_dropdown('mcliente',$clientes,$this->input->post('mcliente'),'class="form-control chosen-select"'); ?>
      </div>
      <div class="modal-footer">

        <button type="submit" class="btn btn-primary">Aceptar</button>
      </div>
      <?php echo form_close(); ?>
    <?php else: ?>
      <div class="modal-body">
        <div class="alert alert-danger">
          <b>Usted no cuenta con clientes</b>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
      </div>
    <?php endif; ?>
    </div>
  </div>
</div>



<script>

      $(function() {
        $('.chosen-select').chosen();
        $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
      });
</script>

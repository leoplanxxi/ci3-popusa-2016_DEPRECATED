<?php setlocale(LC_MONETARY, 'es_MX');  ?>
<div class="alert alert-info">
  <label>Precio por unidad sin IVA</label><br>
  <b>Codigo :</b> <?php echo $codigo; ?><br>
  <b>Precio distribuidor:</b> <?php echo money_format("%n MXN",$p1); ?><br>
  <b>Precio Mínimo:</b> <?php echo money_format("%n MXN",$p2); ?><br>
  <b>Precio público sugerido:</b> <?php echo money_format("%n MXN",$p3); ?><br>
</div>


<script type="text/javascript">
  <?php if($this->input->post('buscar',true)): ?>
  $("#nomprod").html('Búsqueda: '+'<?php echo $this->input->post('buscar'); ?>');
  <?php endif; ?>
</script>
<style>
.view .mask .back-popusa{
  <?php
    if($productos[0]->color=="#F5DB06"){
      echo "color:black;";
    }else{
      echo "color:white;";
    }
  ?>
  background-color: <?php echo $productos[0]->color;?>;;
}
.masinfo{
background-color: <?php echo $productos[0]->color;?>;;/*Color para la familia de productos*/
}
.addshop{
background-color: <?php echo $productos[0]->color;?>;/*Color para la familia de productos*/
}
.addshop a i{
background-color: <?php echo $productos[0]->color;?>;/*Color para la familia de productos*/
}
.panel-heading{
<?php
  if($productos[0]->color=="#F5DB06"){
    echo "color:black;";
  }else{
    echo "color:white;";
  }
?>
}
.panel-popusa {
  border-color: <?php echo $productos[0]->color;?>;/*Color para la familia de productos*/
}
.titulo-producto{
  color:<?php echo $productos[0]->color;?>;
}
.panel{
  background-color: <?php echo $productos[0]->color;?>;
}
</style>


<script>
(function($) {
$(document).ready(function(){
  var x = <?php echo count($productos);  ?>;

  window.addEventListener("orientationchange", ()=> {
    if(window.screen.orientation.type == "landscape-primary"){
      for(var i =0; i<x; i++){
      document.getElementById("or_pr"+i).classList.remove('col-sm-6');
      document.getElementById("or_pr"+i).classList.add('col-xs-6');
    }
    }else{
      for(var i =0; i<x; i++){
        document.getElementById("or_pr"+i).classList.remove('col-xs-6');
        document.getElementById("or_pr"+i).classList.add('col-sm-6');
      }
    }
});
  if (window.matchMedia("(orientation: portrait)").matches) {
      /// portrait codigo
      for(var i =0; i<x; i++){
        document.getElementById("or_pr"+i).classList.remove('col-xs-6');
        document.getElementById("or_pr"+i).classList.add('col-sm-6');
      }

  }

  if (window.matchMedia("(orientation: landscape)").matches) {
     /// landscape codigo
     for(var i =0; i<x; i++){
     document.getElementById("or_pr"+i).classList.remove('col-sm-6');
     document.getElementById("or_pr"+i).classList.add('col-xs-6');
   }
  }
  $("select[name=val1]").change(function(){
            alert($('select[name=val1] option:selected').text());
        });
})
})(jQuery);
</script>

<div class="col-md-12" >
  <div class="row" >

  <?php
  $i = 0;
  foreach($productos as $pr):?>
    <div id="or_pr<?php echo $i; ?>" class="col-md-4 col-sm-6 productos">
      <div class="panel panel-popusa view">
        <div class="panel-heading">
          <span class="cortar">
            <?php echo $pr->descorta?>
          </span>
            <div class="clearfix"></div>
          </div>

          <div class="panel-body max-body">
            <div>
              <img class="img-responsive im-pop" src="<?php echo base_url('public/imagenes/').$pr->img;?>">
                <div class="mask">
                  <div class="back-popusa">
                    <b><?php echo $pr->descorta?></b>
                  </div>
                  <p class="info"><?php echo $pr->deslarga; ?></p>
                  </div>
                  <!--div class="comparar pull-left"><small>Comparar antes de comprar &rarr;</small></div-->
                  <div class="masinfo">
                    <a href="#" onclick="pdfs(<?php echo "'".$pr->pdf."'"; ?>)" data-toggle="modal" data-target="#masinfo">
                      <!--i class="fa fa-file-text-o" aria-hidden="true"></i-->
                      <img src="<?php echo base_url('public/img/ficha_tecnica.png') ?>" />
                     </a>
                   </div>

                   <div class="addshop">
                    <?php $val1 = str_replace('"',"|",$pr->descorta);?>

                      <a href="#or_pr<?php echo $i; ?>" onclick="mdmedidas('<?php echo trim($val1);?>','<?php echo $productos[0]->familia; ?>')">

                        <img src="<?php echo base_url('public/img/carrito_compra.png') ?>" />
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
      <?php
      $i++;
    endforeach;?>
    <?php if($i == 0): ?>
      <div id="or_pr<?php echo $i; ?>" class="col-md-12">
        <div class="alert alert-info" align="center">
          <h4>No hay resultados de la búsqueda: <?php echo $this->input->post('buscar',true); ?><br>Intente con otras palabras.</h4>
        </div>
      </div>
    <?php endif; ?>
  </div>
</div>

  <div class="modal fade" id="masinfo" tabindex="-1" role="dialog" aria-labelledby="myModal5Label" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <iframe frameborder="0" width="100%" height="550" marginheight="0" marginwidth="0" id="pdf"
          ></iframe>
        </div>
      </div>
    </div>
  </div>
<div id="mdaddshop"></div>

<script>
function mdmedidas(medida,familia){
  $.ajax({
    async:true,
    cache:false,
    dataType:"html",
    type: 'POST',
    url: '<?php echo base_url('index.php/productos/get_productos') ?>',
    data: {medidasmd:medida,familia:familia,'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
    success:  function(data){
        $('#mdaddshop').html(data);
        $("#addshop").modal();
    },
    beforeSend:function(){
      $('#mdaddshop').modal('hide');
    },
    error:function(objXMLHttpRequest){}
  });

}
function pdfs(nombre){
  document.getElementById('pdf').src = "http://docs.google.com/gview?url=<?php echo base_url('public/pdfs/')?>"+nombre+"&embedded=true";

}
</script>

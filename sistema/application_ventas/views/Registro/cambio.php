<style>
.error{
  color:red;
}
</style>
<div class="col-md-4 col-md-offset-4" style="margin-top:60px;">
  <div align="center">
    <img src="<?php echo base_url('public/imagenes/logo-popusa.png'); ?>" class="img-responsive" />
  </div>
  &#160;
  <?php if($this->session->flashdata('error')): ?>
  <div class="alert alert-success alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong><?php echo $this->session->flashdata('error'); ?></strong>
  </div>
<?php endif; ?>

  <div class="panel panel-default">
    <?= form_open('index.php/cambiarcon/'.$token);?>
    <div class="panel-body">
      <div class="">
        <h3><b>Recuperar Contraseña</b></h3>
      </div>
      <hr>
      <div class="col-md-12">
        <div class="input-group">
          <label class="control-label">Nueva Contraseña</label>
        </div>
        <div class="input-group">
          <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
          <?php $data = array(
            'name' => 'pass1',
            'type' => 'password',
            'class' => 'form-control',
            'id' => 'usuario',
            'placeholder' => 'Nueva Contraseña',
            'required' => true
          );?>
          <?= form_input($data);?>
        </div>
          <?php echo form_error('pass1', '<div class="error">', '</div>'); ?>
       </div>
       &#160;
       <div class="col-md-12">
         <div class="input-group">
           <label class="control-label">Repita Nueva Contraseña</label>
         </div>
         <div class="input-group">
           <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
           <?php $data = array(
             'name' => 'pass2',
             'type' => 'password',
             'class' => 'form-control',
             'id' => 'usuario',
             'placeholder' => 'Repita Nueva Contraseña',
             'required' => true
           );?>
           <?= form_input($data);?>
         </div>
           <?php echo form_error('pass2', '<div class="error">', '</div>'); ?>
           <?php echo form_hidden('nekot',$nekot,'');
                 echo form_error('nekot', '<div class="error">', '</div>');
            ?>
        </div>
        &#160;
    </div>
   <div class="form-group">
      <button class="btn btn-block btn-warning">Enviar</button>
   </div>
  </div>
  <?= form_close();?>
  <hr>
</div>

<div class="form-group">
  <?php echo form_label('Colonia','class="control-label"')  ?>
  <?php
  $options = array(
    'class' => 'form-control',
    'id' => 'colonias'
  );

  echo form_dropdown('colonia',$colonias,$this->input->post('colonia',true),$options);
   ?>
</div>

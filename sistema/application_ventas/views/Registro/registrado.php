<div class="container">
<div class="col-md-4 col-md-offset-4" style="margin-top:60px;">
  <div align="center">
    <img src="<?php echo base_url('public/imagenes/logo-popusa.png'); ?>" class="img-responsive" />
  </div>
  &#160;

  <div class="panel panel-default">
    <div class="panel-body" align="left">
      <h4><b>Gracias por registrarse, ahora ya puede iniciar sesión.</b></h4>
      <hr>
      <?= form_open('index.php/login/ingresar'); ?>
      <div class="col-md-12">
      <?= form_label('Correo Electrónico','class="control-label"')?>
      </div>
      <div class="col-md-12 input-group">
        <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
        <?php $data = array(
          'name' => 'usuario',
          'type' => 'email',
          'class' => 'form-control',
          'id' => 'usuario',
          'placeholder' => 'Correo Electrónico',
          'required' => true
        );?>
        <?= form_input($data);?>
      </div>
      &#160;
      <div class="col-md-12">
      <?= form_label('Contraseña','class="control-label"')?>
      </div>
      <div class="col-md-12 input-group">
        <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
        <?php $data=array(
          'name' => 'contrasena',
          'type' => 'password',
          'class' => 'form-control',
          'id' => 'contrasena',
          'placeholder' => 'Contraseña',
          'required' => true
        );?>
        <?= form_input($data);?>
        <span class="input-group-btn">
      <button class="btn btn-default" type="button" onclick="viewpass()"><span class="fa fa-eye"></span></button>
    </span>
      </div>
      &#160;
      <?php if($this->session->userdata('intentos')>=3): ?>
      <div class="form-group">
              <div class="input-group">
               <span class="imag"><?= $captcha['image']?></span>
               <span><a class="refresh" onclick="image()" href="javascript:;"><img src="<?php echo base_url('public/img/refresh.png')?>"/></a></span>
              </div>
          </div>
      <div class="form-group">
              <div class="input-group">
                <input type="text" id="captcha" name="captcha" class="form-control" placeholder="captcha" required/>
                <input type="hidden" value="<?=$captcha['word']?>" name="string_captcha" />
              </div>
            </div>
      &#160;
    <?php endif; ?>
      <div class="col-md-12 input-group">
        <button class="btn btn-block btn-primary">Entrar</buttton>
      </div>
      &#160;
      <?= form_close();?>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
function image(){
 $.post('<?php echo base_url('index.php/login/refresh_captcha');?>',
   {'<?php echo $this->security->get_csrf_token_name();?>':'<?php echo $this->security->get_csrf_hash(); ?>'},function(resp){
    $('.imag').html(resp);
  });
  }

  function viewpass(){
    if($('#contrasena').attr('type') == "password"){
      $('#contrasena').attr('type','text');
    }else{
      $('#contrasena').attr('type','password');

    }
  }
</script>

<?php echo form_label('Colonia','class="control-label"')  ?>
  <?php $data = array(
    'name' => 'colonia',
    'type' => 'text',
    'class' => 'form-control',
    'value' => $this->input->post('colonia'),
    'placeholder' => 'Colonia',
    'required' => true,
    'id' => 'colonias'
  ); ?>
  <?= form_input($data)?>
  <?php echo form_error('colonia', '<div class="error">', '</div>'); ?>
  <div class="help-block with-errors"></div>

<style>
.error{
  color:red;
}
</style>
<div class="col-md-4 col-md-offset-4" style="margin-top:60px;">
  <div align="center">
    <img src="<?php echo base_url('public/imagenes/logo-popusa.png'); ?>" class="img-responsive" />
  </div>
  &#160;
  <?php if($this->session->flashdata('error')): ?>
  <div class="alert alert-danger alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong><?php echo $this->session->flashdata('error'); ?></strong>
  </div>
<?php endif; ?>
<?php if($this->session->flashdata('ok')): ?>
<div class="alert alert-success alert-dismissable">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<strong><?php echo $this->session->flashdata('ok'); ?></strong>
</div>
<?php endif; ?>
  <div class="panel panel-default">
    <?= form_open('index.php/recuperar');?>
    <div class="panel-body">
      <div class="">
        <h3><b>Recuperar Contraseña</b></h3>
      </div>
      <hr>
      <div class="col-md-12">
        <div class="input-group">
          <label class="control-label">Correo Electrónico</label>
        </div>
        <div class="input-group">
          <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
          <?php $data = array(
            'name' => 'usr_email',
            'type' => 'email',
            'class' => 'form-control',
            'id' => 'usuario',
            'placeholder' => 'Correo Electrónico',
            'required' => true
          );?>
          <?= form_input($data);?>
        </div>
          <?php echo form_error('correo', '<div class="error">', '</div>'); ?>
       </div>
       &#160;
       <div class="form-group">
               <div class="input-group">
                <span class="imag"><?= $captcha['image']?></span>
                <span><a class="refresh" onclick="image()" href="javascript:;"><img src="<?php echo base_url('public/img/refresh.png')?>"/></a></span>
               </div>
           </div>
       <div class="form-group">
         <div class="input-group">
           <input type="text" id="captcha" name="captcha" class="form-control" placeholder="captcha" required/>
           <input type="hidden" value="<?=$captcha['word']?>" name="string_captcha" />
         </div>
       </div>
       &#160;
    </div>
   <div class="form-group">
      <button class="btn btn-block btn-warning">Enviar</button>
   </div>
  </div>
  <?= form_close();?>
  <hr>
</div>

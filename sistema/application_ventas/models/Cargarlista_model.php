<?php
Class Cargarlista_model extends CI_Model
{
    public function __construct() {
        //parent::__construct();
        $this->load->helper('url');
    }

    function elimina_acentos($text){

        $text = htmlentities($text, ENT_QUOTES, 'UTF-8');
        $text = strtolower($text);
        //echo $text."<br>";
        $patron = array (
            // Espacios, puntos y comas por guion
            //'/[\., ]+/' => ' ',

            // Vocales
            '/\+/' => '',
            '/&agrave;/' => 'a',
            '/&egrave;/' => 'e',
            '/&igrave;/' => 'i',
            '/&ograve;/' => 'o',
            '/&ugrave;/' => 'u',

            '/&aacute;/' => 'a',
            '/&eacute;/' => 'e',
            '/&iacute;/' => 'i',
            '/&oacute;/' => 'o',
            '/&uacute;/' => 'u',

            '/&acirc;/' => 'a',
            '/&ecirc;/' => 'e',
            '/&icirc;/' => 'i',
            '/&ocirc;/' => 'o',
            '/&ucirc;/' => 'u',

            '/&atilde;/' => 'a',
            '/&etilde;/' => 'e',
            '/&itilde;/' => 'i',
            '/&otilde;/' => 'o',
            '/&utilde;/' => 'u',

            '/&auml;/' => 'a',
            '/&euml;/' => 'e',
            '/&iuml;/' => 'i',
            '/&ouml;/' => 'o',
            '/&uuml;/' => 'u',

            '/&auml;/' => 'a',
            '/&euml;/' => 'e',
            '/&iuml;/' => 'i',
            '/&ouml;/' => 'o',
            '/&uuml;/' => 'u',

            // Otras letras y caracteres especiales
            '/&aring;/' => 'a',
            '/&ntilde;/' => 'n',

            // Agregar aqui mas caracteres si es necesario

        );

        $text = preg_replace(array_keys($patron),array_values($patron),$text);
        return $text;
    }


    public function delete(){
      $this->db->truncate('productos');
    }

    public function agregar($datos) {

      $img="";
      if($datos[73]!=""){
        $img=$datos[73];
      }else{
        $img="1.png";
      }

      if($datos[2]!=null){
      $data = array(
        //'visible' => $datos[0][0],
        'msistema' =>$datos[0],//Mostrar u ocultar sistema
        'mlista' =>$datos[1],//Mostrar u ocultar Lista viajera
        'marca' => trim($datos[2]),//Marca
        'familia' => strtoupper($this->elimina_acentos(trim($datos[3]))),//Familia
        //'familia' => strtoupper(trim($datos[3])),//Familia
        'subfamilia' => strtoupper($this->elimina_acentos(trim($datos[4]))),//SubFamilia
        //'subfamilia' => strtoupper(trim($datos[4])),//SubFamilia
        'descorta' => $datos[5],//Descripcion Corta
        'deslarga' => trim($datos[6]),//Descripcion del Producto (En Sistema Herramienta de ventas y lista viajera)
        'descotizador' => trim($datos[7]),//Descripcion del producto para Cotizador
        'desespecial' => trim($datos[8]),//Descripcion del producto caracteristicas especiales  (cotizador)
        'tipo' => $datos[9],//Tipo Producto
        'rotacion' => $datos[10],//Rotacion
        'codigo' => $datos[11],//codigo
        'desc_cod' => $datos[12],//Descripcion codigo
        'var1' => $datos[13],//Variable 1
        'cant1' => $datos[14],//cantidad 1
        'unidad1'=> $datos[15],//unidad 1
        'var2' => $datos[16],//Variable 2
        'cant2' => $datos[17],//cantidad 2
        'unidad2' => $datos[18],//unidad 2
        'var3' => $datos[19],//Variable 3
        'cant3' => $datos[20],//Cantidad 3
        'unidad3' => $datos[21],//unidad 3
        'var4' => $datos[22],//Variable 4
        'cant4' => $datos[23],//Cantidad 4
        'unidad4' => $datos[24],//unidad 4
        'var5' => $datos[25],//Variable 5
        'cant5' => $datos[26],//Cantidad 5
        'unidad5' => $datos[27],//unidad 5
        'var6' => $datos[28],//Variable 6
        'cant6' => $datos[29],//Cantidad 6
        'unidad6' => $datos[30],//unidad 6
        'escala' => $datos[31],//Escala
        'u_base' => $datos[32],//Unidad Base
        'precio_siva' => $datos[33],// Precio S/IVA
        'precio' => $datos[34],//Precio
        'auxiliar' => $datos[35],//Escala Auxiliar
        'u_aux' => $datos[36],//Unidad Aux.
        'aux_iva' => $datos[37],// Pre. Aux. S/Iva
        'pre_aux' => $datos[38],//Pre. Aux.
        'moneda' => $datos[39],//Moneda
        'u_base2' => $datos[40],//UNIDAD BASE
        'pxub' => $datos[41],//PRECIO BASE
        'u_aux2' => $datos[42],//UNIDAD AUXILIAR
        'pxua' => $datos[43],//PRECIO AUXILIAR
        'iva' => $datos[44],// IVA
        'tient' => $datos[45],//tiempo de entrega
        'cvuni' => $datos[46],//clave unidades
        'cvpp' => $datos[47],//clave precios principal
        //'cusistema' => $datos[0][40],
        'pbasp' => $datos[48],//PRECIO BASE LV PRINCIPAL
        'pauxp' => $datos[49],//PRECIO AUXILIAR LV PRINCIPAL
        'pbasd' => $datos[50],//PRECIO BASE LV DISTRIBUIDOR
        'pauxd' => $datos[51],//PRECIO AUXILIAR LV DISTRIBUIDOR
        'pbase23' => $datos[52],//PRECIO BASE LV TIENDA 23 PTE
        'paux23' => $datos[53],//PRECIO AUXILIAR LV TIENDA 23 PTE
        'pbasev23' => $datos[54],//PRECIO BASE LV VENDEDOR 23 PTE
        'pauxv23' => $datos[55],//PRECIO AUXILIAR LV VENDEDOR 23 PTE
        'pbaseaquix' => $datos[56],//PRECIO BASE LV AQUIXTLA
        'pauxaquix' => $datos[57],//PRECIO AUXILIAR LV AQUIXTLA
        'pbasethuix' => $datos[58],//PRECIO BASE LV TIENDA HUIXCOLOTLA
        'pauxthuix' => $datos[59],//PRECIO AUXILIAR LV TIENDA HUIXCOLOTLA
        'pbasevhuix' => $datos[60],//PRECIO BASE LV VENDEDOR HUIXCOLOTLA
        'pauxvhuix' => $datos[61],//PRECIO AUXILIAR LV VENDEDOR HUIXCOLOTLA
        'pbasecent' => $datos[62],//PRECIO BASE LV CENTRAL DE ABASTOS
        'pauxcent' => $datos[63],//PRECIO AUXILIAR LV CENTRAL DE ABASTOS
        'pbasevcent' => $datos[64],//PRECIO BASE LV CENTRAL DE ABASTOS VENDEDOR
        'pauxvcent' => $datos[65],//PRECIO AUXILIAR LV CENTRAL DE ABASTOS VENDEDOR
        'pbasepak' => $datos[66],//PRECIO BASE LV PLANTA PAKET
        'pauxpak' => $datos[67],//PRECIO AUXILIAR LV PLANTA PAKET
        'pbaseplant' => $datos[68],//PRECIO BASE LV PLANTA
        'pauxplant' => $datos[69],//PRECIO AUXILIAR LV PLANTA
        'pbasebon' => $datos[70],//PRECIO BASE LV BONASA
        'pauxbon' => $datos[71],//PRECIO AUXILIAR LV BONASA
        'pdf' => $datos[72],//pdf
        'img' => $img,//img
        'color' => "#".$datos[74],//color
        'mos_ocu' => $datos[75],//Mostrar u oculatar
        'confeccion' => $datos[76],//Confección
        'costura' => $datos[77],//Cosutua
        'ojillos' => $datos[78],//Ojillos
        'deslivi' => $datos[79],//Descripción
        'sat' => $datos[80],//SAT
      );

      $this->db->insert('productos',$data);


      //die();
    }

    }

}

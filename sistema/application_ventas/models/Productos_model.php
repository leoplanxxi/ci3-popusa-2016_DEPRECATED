<?php
Class Productos_model extends CI_Model
{
    public function __construct() {
        //parent::__construct();
        $this->load->helper('url');
    }

    public function get_familias($marca){
      $this->db->select('marca,familia,subfamilia,color');
      $this->db->from('productos');
      $this->db->where('marca',$marca);
      $this->db->where('msistema!="O"');
      $this->db->group_by('familia');
      $this->db->order_by('id_productos','asc');
      $query = $this->db->get();
      return $query->result();
    }

    public function get_subfamilias($marca){
      $this->db->select('marca,familia,subfamilia,color');
      $this->db->from('productos');
      $this->db->where('marca',$marca);
      $this->db->where('msistema!="O"');
      $this->db->where('subfamilia!="nosubfam"');
      $this->db->group_by('subfamilia');
      $this->db->order_by('id_productos','asc');
      $query = $this->db->get();

      return $query->result();
    }

    public function get_listprodf($familia){
      $this->db->select("*");
      $this->db->from("productos");
      $this->db->where("familia",$familia);
      $this->db->where('msistema!="O"');
      $this->db->group_by("descorta");
      $this->db->order_by("id_productos asc");
      $query = $this->db->get();
      return $query->result();
    }

    public function get_listprodsf($subfamilia){
      //select * from productos where subfamilia = "CUBIERTAS" group by descorta
      $this->db->select("*");
      $this->db->from("productos");
      $this->db->where("subfamilia",$subfamilia);
      $this->db->where('msistema!="O"');
      $this->db->group_by("descorta");
      $this->db->order_by("id_productos asc");
      $query = $this->db->get();
      return $query->result();
    }

	/***********************************************************/
	public function buscar($param)
	{
		$this->db->select("*");
		$this->db->from("productos");
    //$this->db->where('msistema','M');
		$this->db->like("descorta",$param,'both');
		$this->db->or_like("deslarga",$param,'both');
		$this->db->or_like("codigo",$param,'both');
    $this->db->or_like("familia",$param,'both');
		$this->db->or_like("subfamilia",$param,'both');
		$this->db->group_by("descorta");
		$this->db->order_by("id_productos asc");
		$query = $this->db->get();
    $i=0;
    $x=0;
    foreach($query->result() as $q){
      if($q->msistema=="M"){
        $result[$x] = $query->result()[$i];
        $x++;
      }
      $i++;
    }

    return $result;
	}
	/**********************************************************/

    public function get_medidas(){
      $data =array("var1","cant1","unidad1");
      $descorta = str_replace('|','"',$this->input->post("descorta"));
        $this->db->select($data);
        $this->db->distinct();
        $this->db->from("productos");
        $this->db->where("descorta",$descorta);
        $this->db->where('msistema!="O"');
        $this->db->group_by("cant1");
        $this->db->order_by("id_productos ASC");
        $query = $this->db->get();
        $val = $query->result();
      return $val;
    }

    public function get_medidas2(){

      $data =array("var2","cant2","unidad2");
      $descorta = str_replace('|','"',$this->input->post("descorta2"));
      $medidas2 = $this->input->post("medida2");
      $opc1 = $this->input->post("opc1");
        $this->db->select($data);
        //$this->db->distinct();
        $this->db->from("productos");
        $this->db->where("descorta",$descorta);
        $this->db->where("cant1",$opc1);
        $this->db->where('msistema!="O"');
        //$this->db->where($data[0],$medidas2);
        $this->db->group_by("cant2");
        $this->db->order_by("id_productos ASC");
        $query = $this->db->get();
        $val = $query->result();

      return $val;
    }

    public function get_medidas3(){
      $data =array("var3","cant3","unidad3");
      $descorta = str_replace('|','"',$this->input->post("descorta3"));
      $medidas3 = $this->input->post("medida3");
      $opc1 = $this->input->post("opc1");
      $opc2 = $this->input->post("opc2");

        $this->db->select($data);
        //$this->db->distinct();
        $this->db->from("productos");
        $this->db->where("descorta",$descorta);
        $this->db->where("cant1",$opc1);
        $this->db->where("cant2",$opc2);
        $this->db->where('msistema!="O"');
        //$this->db->where($data[0],$medidas3);
        $this->db->group_by("cant3");
        $this->db->order_by("id_productos ASC");
        $query = $this->db->get();
        $val = $query->result();

      return $val;
    }

    public function get_medidas4(){
      $data =array("var4","cant4","unidad4");
      $descorta = str_replace('|','"',$this->input->post("descorta4"));
      $medidas4 = $this->input->post("medida4");
      $opc1 = $this->input->post("opc1");
      $opc2 = $this->input->post("opc2");
      $opc3 = $this->input->post("opc3");

        $this->db->select($data);
        //$this->db->distinct();
        $this->db->from("productos");
        $this->db->where("descorta",$descorta);
        $this->db->where("cant1",$opc1);
        $this->db->where("cant2",$opc2);
        $this->db->where("cant3",$opc3);
        $this->db->where('msistema!="O"');
        //$this->db->where($data[0],$medidas4);
        $this->db->group_by("cant4");
        $this->db->order_by("id_productos ASC");
        $query = $this->db->get();
        $val = $query->result();

      return $val;
    }
    public function get_medidas5(){
      $data =array("var5","cant5","unidad5");
      $descorta = str_replace('|','"',$this->input->post("descorta5"));

      $opc1 = $this->input->post("opc1");
      $opc2 = $this->input->post("opc2");
      $opc3 = $this->input->post("opc3");
      $opc4 = $this->input->post("opc4");

        $this->db->select($data);
        //$this->db->distinct();
        $this->db->from("productos");
        $this->db->where("descorta",$descorta);
        $this->db->where("cant1",$opc1);
        $this->db->where("cant2",$opc2);
        $this->db->where("cant3",$opc3);
        $this->db->where("cant4",$opc4);
        $this->db->where('msistema!="O"');
        $this->db->group_by("cant5");
        $this->db->order_by("id_productos ASC");
        $query = $this->db->get();
        $val = $query->result();

      return $val;
    }
    public function get_medidas6(){
      $data =array("var6","cant6","unidad6");
      $descorta = str_replace('|','"',$this->input->post("descorta6"));
      $medidas6 = $this->input->post("medida6");

        $this->db->select($data);
        $this->db->distinct();
        $this->db->from("productos");
        $this->db->where("descorta",$descorta);
        $this->db->where('msistema!="O"');
        //$this->db->where($data[0],$medidas6);
        $this->db->group_by("cant6");
        $this->db->order_by("id_productos ASC");
        $query = $this->db->get();
        $val = $query->result();

      return $val;
    }

    public function get_escala(){
      //$escala=0;
      $this->db->select('cvuni');
      $this->db->from('productos');
      $this->db->where('descorta',$this->input->post('descorta'));
      $this->db->where('msistema!="O"');
      //$this->db->where('descorta',$descorta);
      $this->db->group_by('cvuni');
      $this->db->order_by('cvuni desc');
      $query=$this->db->get();
      $aux=$query->result_array();

      switch ($aux[0]['cvuni']) {
        case '00':
            $escala = 0;
          break;
        case '01':
          $escala ="u_aux2";
          break;
        case '10':
          $escala ="u_base2";
          break;
        case '11':
          $escala = "u_base2,u_aux2";
          break;
        default:
          $escala=0;
          break;
      }
        //var_dump($escala);
        //if($escala!=0){

        $this->db->select($escala);
        $this->db->from('productos');
        $this->db->where('descorta',$this->input->post("descorta"));
        if($this->input->post("opc1")){
          $this->db->where('cant1',$this->input->post("opc1"));
        }
        if($this->input->post("opc2")){
          $this->db->where('cant2',$this->input->post("opc2"));
        }
        if($this->input->post("opc3")){
          $this->db->where('cant3',$this->input->post("opc3"));
        }
        if($this->input->post("opc4")){
          $this->db->where('cant4',$this->input->post("opc4"));
        }
        $this->db->where('msistema!="O"');
        $this->db->group_by('descorta');
        $query=$this->db->get();

        return $query->result_array();

      /*}else{
        return $escala;
      }*/


    }

    public function get_codigo(){
      $this->db->select("*");
      $this->db->from("productos");
      $this->db->where("descorta",$this->input->post('descorta'));
      if($this->input->post('variable1')!=null){
        $this->db->where("cant1",$this->input->post('variable1'));
      }

      if($this->input->post('variable2')!=null){
        $this->db->where("cant2",$this->input->post('variable2'));
      }

      if($this->input->post('variable3')!=null){
        $this->db->where("cant3",$this->input->post('variable3'));
      }

      if($this->input->post('variable4')!=null){
        $this->db->where("cant4",$this->input->post('variable4'));
      }

      if($this->input->post('variable5')!=null){
        $this->db->where("cant5",$this->input->post('variable5'));
      }

      if($this->input->post('variable6')!=null){
        $this->db->where("cant6",$this->input->post('variable6'));
      }

      $query=$this->db->get();
      //var_dump($this->input->post());
      return $query->result_array();
    }

    public function carrito_i(){
      $aux=$this->get_codigo();


      $data=array(
        'usuario'=>$this->input->post('id_cliente'),
        'codigo_producto'=>$aux[0]['codigo'],
        'imagen'=>$aux[0]['img'],
        'unidad'=>$this->input->post('unidad'),
        'descorta'=>$this->input->post('descorta'),
        'cantidad'=> str_replace(" ","",$this->input->post('cantidad')),
        'cant_confeccion' => $this->input->post('cant_confeccion'),
        'cant_costura' => $this->input->post('cant_costura'),
        'cant_ojillos' => $this->input->post('cant_ojillos'),
        'json_carrito' => json_encode($aux),

      );

      // if($this->get_last_puser()){
      //   $data['id_cotizado'] = $this->get_last_puser()[0]['id_cotizado'];
      // }
      if(isset($this->session->userdata('popusa')[0]['id_cotizado'])){
        $data['id_cotizado'] = $this->session->userdata('popusa')[0]['id_cotizado'];
      }
      // var_dump($data);
      // die();
      $this->db->insert('carrito',$data);

      return $this->db->insert_id();
    }
    public function get_last_puser(){
      $this->db->select('id_cotizado');
      $this->db->from('carrito');
      $this->db->where('eliminado',0);
      $this->db->where('estado',0);
      $this->db->where('usuario',$this->input->post('id_cliente'));
      $this->db->limit(1);
      $this->db->order_by('id_cotizacion','desc');
      return $this->db->get()->result_array();
    }
    public function veri_prod(){
      $aux=$this->get_codigo();
      $this->db->select("*");
      $this->db->from('carrito');
      $this->db->where('usuario',$this->input->post('id_cliente'));
      $this->db->where('codigo_producto',$aux[0]['codigo']);
      $this->db->where('eliminado',0);
      $this->db->where('estado',0);
      $query=$this->db->get();
      return $query->result_array();
    }

    public function upd_carrito(){
      $aux=$this->veri_prod();
      $data['cantidad']=$this->input->post('cantidad')+$aux[0]['cantidad'];
      $this->db->where('codigo_producto',$aux[0]['codigo_producto']);
      $this->db->where('usuario',$this->input->post('id_cliente'));
      $this->db->where('eliminado',0);
      // if (isset($this->session->userdata('popusa')[0]['id_cotizado'])) {
      //   $this->db->where('id_cotizado',$this->session->userdata('popusa')[0]['id_cotizado']);
      // }
      $this->db->where('estado',0);
      $this->db->update('carrito',$data);
    }

    public function add_carrito(){
      $aux=$this->veri_prod();

      if($aux!=null && $aux[0]['cant_confeccion'] == 0 && $aux[0]['cant_costura'] == 0 && $aux[0]['cant_ojillos'] == 0){
        $this->upd_carrito();
      }else{
        $this->carrito_i();
      }

      $this->db->select("*");
      $this->db->from('carrito');
      $this->db->where('usuario',$this->input->post('id_cliente'));
      $this->db->where('estado',0);
      if (isset($this->session->userdata('popusa')[0]['id_cotizado'])) {
        $this->db->where('id_cotizado',$this->session->userdata('popusa')[0]['id_cotizado']);
      }
      $this->db->where('eliminado',0);
      $query=$this->db->get();
      return $query->result_array();
    }

    public function ver_carrito(){
      if($this->input->post('id_cliente',true)==0){
        $id_cliente = $this->session->userdata('popusa')[0]['rol_idrol'].":".$this->session->userdata('popusa')[0]['idlogin'];
      }else{
        $id_cliente = $this->input->post('id_cliente');
      }
      $this->db->select("*");
      $this->db->from('carrito');
      $this->db->where('usuario', $id_cliente);
      $this->db->where('estado',0);
      //$this->db->or_where('estado',2);
      if($this->input->post('id_cliente',true) == 0){
        $this->db->where('id_cotizado',$this->session->userdata('popusa')[0]['id_cotizado']);
      }
      // if(isset($this->session->userdata('popusa')[0]['id_cotizado'])){
      //   $this->db->where('id_cotizado',$this->session->userdata('popusa')[0]['id_cotizado']);
      // }
      $this->db->where('eliminado',0);
      $this->db->order_by('creacion desc');
      $query=$this->db->get();
      return $query->result_array();
    }

    public function del_carrito(){
      $data['eliminado']=1;
      $this->db->where('id_cotizacion',$this->input->post('id_cotizacion'));
      $this->db->update('carrito',$data);
      return $this->ver_carrito();
    }


    public function get_info_extra(){
      return $this->get_codigo();
    }

    public function get_desc(){
      $this->db->select('descorta,deslarga,var1,cant1,unidad1');
      $this->db->from('productos');
      $this->db->where('codigo',$this->input->post('cod',true));
      return $this->db->get()->result_array();
    }
    public function get_descv($subfamilia){
      $this->db->select('subfamilia,color,descorta,deslarga,desespecial,var1,var2,var3,var4,var5,var6,cant1,cant2,cant3,cant4,cant5,cant6,unidad1,unidad2,unidad3,unidad4,unidad5,unidad6,sat');
      $this->db->from('productos');
      $this->db->where('subfamilia',$subfamilia);
      $this->db->where('mlista','M');
      $this->db->group_by('descorta');
      $this->db->order_by('id_productos asc');
      return $this->db->get()->result_array();
    }

    public function get_list_alianza($familia,$subfamilia,$descorta){
//descorta
      $this->db->select('rotacion,codigo,descorta,var1,var2,var3,var4,var5,var6,cant1,cant2,cant3,cant4,cant5,cant6,unidad1,unidad2,unidad3,unidad4,unidad5,unidad6,img,iva,pxub,pxua,desespecial,pre_aux,cvpp,u_base2,u_aux2,precio,iva,marca,familia,subfamilia,deslarga,pdf,deslivi,sat');
      $this->db->from('productos');
      $this->db->where('marca','Alianza');
      $this->db->where('familia',$familia);
      $this->db->where('subfamilia',$subfamilia);
      $this->db->where('descorta',$descorta);
      $this->db->where('mlista','M');
      $this->db->order_by('id_productos asc');
      return $this->db->get()->result_array();

    }
    public function get_subfxfam($familia){
      $this->db->select('subfamilia,color,descorta,deslarga,desespecial,var1,var2,var3,var4,var5,var6,cant1,cant2,cant3,cant4,cant5,cant6,unidad1,unidad2,unidad3,unidad4,unidad5,unidad6,sat');
      $this->db->from('productos');
      $this->db->where('marca','Alianza');
      $this->db->where('familia',$familia);
      $this->db->where('mlista','M');
      $this->db->group_by('subfamilia');
      $this->db->order_by('id_productos asc');
      return $this->db->get()->result_array();
    }
    public function get_subfxfamp($familia){
      $this->db->select('subfamilia,color,descorta,deslarga,desespecial,var1,var2,var3,var4,var5,var6,cant1,cant2,cant3,cant4,cant5,cant6,unidad1,unidad2,unidad3,unidad4,unidad5,unidad6,sat');
      $this->db->from('productos');
      $this->db->where('marca','Paket');
      $this->db->where('familia',$familia);
      $this->db->where('mlista','M');
      $this->db->group_by('subfamilia');
      $this->db->order_by('id_productos asc');
      return $this->db->get()->result_array();
    }
    public function get_list_paket($familia,$subfamilia,$descorta){
      $this->db->select('rotacion,codigo,descorta,var1,var2,var3,var4,var5,var6,cant1,cant2,cant3,cant4,cant5,cant6,unidad1,unidad2,unidad3,unidad4,unidad5,unidad6,img,iva,pxub,pxua,desespecial,pre_aux,cvpp,u_base2,u_aux2,precio,iva,marca,familia,subfamilia,deslarga,pdf,deslivi,sat');
      $this->db->from('productos');
      $this->db->where('marca','Paket');
      $this->db->where('familia',$familia);
      $this->db->where('subfamilia',$subfamilia);
      $this->db->where('descorta',$descorta);
      $this->db->where('mlista','M');
      $this->db->order_by('id_productos asc');
      return $this->db->get()->result_array();
    }

    public function get_fec_validacion(){
      $this->db->select('creacion');
      $this->db->from('productos');
      $this->db->limit(1);
      $this->db->order_by('creacion desc');
      return $this->db->get()->result_array();
    }
    public function factores($factor){
      $admin = $this->load->database('db_h_ventas', TRUE);
      $admin->select('valor');
      $admin->from('meta');
      $admin->where('Nombre',$factor);
      return $admin->get()->result_array()[0]['valor'];

    }

    public function get_header(){
      $this->db->select('*');
      $this->db->from('encabezados');
      $this->db->where('familia',$this->input->post('fnombre',true));
      $this->db->where('estado',1);
      return $this->db->get()->result_array();
    }

  }

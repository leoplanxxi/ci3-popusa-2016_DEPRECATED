<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registro_model extends CI_Model {

  public function __construct() {
      //parent::__construct();
      $this->rand = random_string('alnum',7);
    }

  public function get_pais(){
    $this->db->select('*');
    $this->db->from('pais');
    $this->db->order_by('paisnombre');
    return $this->db->get()->result_array();
  }

  public function get_estado(){
      $id = explode(':',$this->input->post('mpais',true));
      $this->db->select('estadonombre');
      $this->db->from('estado');
      $this->db->where('ubicacionpaisid',$id[0]);
      return $this->db->get()->result_array();

  }

  public function registrar(){

    $admin = $this->load->database('db_h_ventas', TRUE);

    /*$direccion  =
    "Calle: ".$this->input->post('calle',true).
    ", Número Exterior: ".$this->input->post('numeroe',true).
    ", Número Interior: ".$this->input->post('numeroi',true).
    ", Ciudad: ".$this->input->post('ciudad',true).
    ", Estado: ".$this->input->post('estado',true).
    ", Código Postal: ".$this->input->post('cp',true);*/

    $direccion = "";
    $pais = explode(':',$this->input->post('pais',true));

    $data = array(
      'contacto' => $this->input->post('nombre',true),
      'razon' => $this->input->post('razon',true),
      'rfc' => $this->input->post('rfc',true),
      'tels' => $this->input->post('telefono',true),
      'celular' => $this->input->post('celular',true),
      'cfdi' => $this->input->post('cfdi',true),
      'metodo' => $this->input->post('metodo',true),
      'forma' => $this->input->post('forma',true),
      'email' => $this->input->post('email',true),
      'direccion' => $direccion,
      'calle' => $this->input->post('calle',true),
      'exterior' => $this->input->post('numeroe',true),
      'interior' => $this->input->post('numeroi',true),
      'ciudad' => $this->input->post('ciudad',true),
      'codigo_postal' => $this->input->post('cp',true),
      'pais' => $pais[1],
      'estado' => $this->input->post('estado',true),
      'colonia' => $this->input->post('colonia',true),
    );

    $admin->insert('cliente',$data);
    $this->gen_access();

  }

  public function token(){
  return sha1(uniqid(rand(),true));
  }

  public function gen_access(){

    $data = array(
      'usr' => $this->input->post('email',true),
      'passwd' => $this->bcrypt->hash_password(trim($this->input->post('pass1',true))),
      'rol' => 2,
      'token' => $this->token()
    );

    $this->db->insert('login',$data);

  }

  public function get_rfc($rfc){
    $admin = $this->load->database('db_h_ventas', TRUE);
    $admin->select('rfc');
    $admin->from('cliente');
    $admin->where('rfc',$rfc);
    return count($admin->get()->result_array());
  }

  public function get_email($email){
    $admin = $this->load->database('db_h_ventas', TRUE);
    $admin->select('email');
    $admin->from('cliente');
    $admin->where('email',$email);
    return count($admin->get()->result_array());
  }

  public function get_data($email){


    $this->db->select('*');
    $this->db->from('login');
    $this->db->where('usr',$email);
    return $this->db->get()->result_array();

  }

  public function verify_token($token){
    $current_stamp = date('Y-m-d H:i:s');
		$query = $this->db->select("idlogin")
				 ->from("login")
				 ->where("token",$token)
				 ->where("request_token >",$current_stamp)
				 ->get();

		if($query->num_rows() === 1){
			return $query->row();
		}else{
			return FALSE;
		}
  }

  public function startRecoveryPassword($user_id){

  //damos 5 minutos al usuario para recuperar su password

    $expire_stamp = date('Y-m-d H:i:s', strtotime("+5 min"));

    $data = array("request_token" => $expire_stamp);

    $this->db->where("idlogin", $user_id);

    if($this->db->update("login", $data)){
      return TRUE;
    }
 }
 public function get_reset(){
   $this->db->select('reset');
   $this->db->from('login');
   $this->db->where('idlogin',$this->input->post('nekot'));
   return $this->db->get()->result_array();
 }
 public function act_pass(){
   //var_dump($this->get_reset());
   //die();
   $data=array(
     'token'=> $this->token(),
     'passwd'=> $this->bcrypt->hash_password($this->input->post('pass1',true)),
     //'reset' => ($this->get_reset()[0]['reset'])+(1)
   );
   $this->db->where('idlogin',$this->input->post('nekot'));
   $this->db->update('login',$data);
 }
 public function get_cp_info(){
   $this->db->select('*');
   $this->db->from('codigospostales');
   $this->db->where('CodigoPostal',$this->input->post('cp',true));
   return $this->db->get()->result_array();
   }
  }

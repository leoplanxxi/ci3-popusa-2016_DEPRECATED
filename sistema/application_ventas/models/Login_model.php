<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

  public function __construct() {
      //parent::__construct();
    }

    public function ingresar(){

      $ingc = $this->cliente();
      var_dump($ingc);
      if($ingc){
        $this->session->unset_userdata('intentos');
        redirect('index.php/principal/productos');
      }else{
        $ingv = $this->vendedores();
        if($ingv){
          $this->session->unset_userdata('intentos');
          redirect('index.php/principal/productos');
        }else{
          // var_dump($this->input->post());
          // die();
          $this->session->set_flashdata('error','El nombre de usuario o contraseña no es el correcto');
          if($this->session->userdata('intentos')){
            $intentos = $this->session->userdata('intentos');
            $this->session->set_userdata('intentos',($intentos+1));
          }else{
            $this->session->set_userdata('intentos',1);
          }
          redirect('index.php/principal/productos');
        }
      }

    }

    public function cliente(){

      if($this->bcrypt->check_password($this->input->post('contrasena'),$this->get_pass())){

        $admin = $this->load->database('db_h_ventas', TRUE);
        $admin->select('*');
        $admin->from('cliente');
        $admin->where('email',$this->input->post('usuario',true));
        $user = $admin->get()->result_array();
        $user[0]['rol_idrol'] = 0;
        $this->session->set_userdata('popusa',$user);
        $this->cotizar();
        return true;

      }

      /*else{

        $this->session->set_flashdata('error','El nombre de usuario o contraseña no es el correcto');
        redirect('index.php/principal/productos');

      }*/
    }

    public function vendedores(){

      if($this->bcrypt->check_password($this->input->post('contrasena'),$this->get_passv())){

        $admin = $this->load->database('db_h_ventas', TRUE);
        $admin->select('*');
        $admin->from('login');
        $admin->where('nombre',$this->input->post('usuario',true));
        $user = $admin->get()->result_array();

        if(($user[0]['rol_idrol'] == 1 || $user[0]['rol_idrol'] == 2  || $user[0]['rol_idrol'] == 4 || $user[0]['rol_idrol'] == 6 ) && $user[0]['activo'] == 1 ){

          $sess_data = array(
              'idusuario' => $user[0]["idlogin"],
              'nombre' => $user[0]["nombre"],
              'rol_idrol' => $user[0]['rol_idrol']
          );
          $this->session->set_userdata("logged_in", $sess_data);
          $this->session->set_userdata('popusa',$user);

          return true;
        }
        /*elseif(($user[0]['rol_idrol'] != 2 || $user[0]['rol_idrol'] != 6) && $user[0]['activo'] == 1){
          $sess_data = array(
              'idusuario' => $user[0]["idlogin"],
              'nombre' => $user[0]["nombre"],
              'rol_idrol' => $user[0]['rol_idrol']
          );
          $this->session->set_userdata("logged_in", $sess_data);
          redirect('admin/index.php/admin');
        }*/
        else{
          $this->session->set_flashdata('error','Usted no tiene permisos de Ingreso');
          if($this->session->userdata('intentos')){
            $intentos = $this->session->userdata('intentos');
            $this->session->set_userdata('intentos',($intentos+1));
          }else{
            $this->session->set_userdata('intentos',1);
          }
          redirect('index.php/principal/productos');
        }
      }else{
        return false;
      }

    }



    public function get_pass(){
      $this->db->select('passwd');
      $this->db->from('login');
      $this->db->where('usr',$this->input->post('usuario',true));
      $pass = $this->db->get()->result_array();

      if($pass!=null){
        return $pass[0]['passwd'];
      }else{
        return false;
      }

    }

    public function get_passv(){

      $admin = $this->load->database('db_h_ventas', TRUE);
      $admin->select('passwd');
      $admin->from('login');
      $admin->where('nombre',$this->input->post('usuario',true));

      $pass = $admin->get()->result_array();

      if($pass!=null){
        return $pass[0]['passwd'];
      }else{
        return false;
      }

    }

    public function cotizar(){
      $cotizar = array(
        'usuario' => $this->session->userdata('popusa')[0]['idcliente']
      );
      $this->db->where('usuario',$this->session->userdata('tk'));
      $this->db->update('carrito',$cotizar);
    }

  }

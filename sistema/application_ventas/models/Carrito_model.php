<?php
defined('BASEPATH') OR exit('No direct script access allowed');

  class Carrito_model extends CI_Model
  {

		public function __construct() {
			//parent::__construct();
			$this->load->helper('url');
		}

    public function get_cotizacion(){
      $this->db->select('*');
      $this->db->from('carrito');
      $this->db->where('usuario',$this->input->post('usr',true));
      $this->db->where('estado',0);
      //$this->db->or_where('estado',2);
      $this->db->where('eliminado',0);

      return $this->db->get()->result_array();

    }

    public function est_cotizado(){
        $data=array(
          'estado' => 1
        );
        $this->db->where('eliminado',0);
        $this->db->where('usuario',$this->input->post('usr',true));
        $this->db->update('carrito',$data);

    }
    public function get_data_cotizacion($id){
      $admin = $this->load->database('db_h_ventas', TRUE);
      $admin ->select('cliente_idcliente,asesor_idasesor,id_carrito');
      $admin->from('cotizaciones');
      $admin->where('idcotizaciones',$id);
      return $admin->get()->result_array();

    }
    public function reset_data_carrito($id,$idc){
      $data=array(
        'estado' => 0,//este estado significa que es recotización
        'id_cotizado' => $idc
      );
      $this->db->where('id_cotizacion',$id);
      $this->db->update('carrito',$data);

    }

    public function reset_json_out($id){
      $data = array(
        'json_out' => "rec"
      );
      $admin = $this->load->database('db_h_ventas', TRUE);
      $admin->where('idcotizaciones',$id);
      $admin->update('cotizaciones',$data);
    }

    
    public function data_usr_cotizacion($id){
      $admin = $this->load->database('db_h_ventas', TRUE);
      $admin->select('cliente_idcliente,info_cliente');
      $admin->from('cotizaciones');
      $admin->where('idcotizaciones',$id);
      return $admin->get()->result_array();
    }
    public function est_recotizar(){
      $data=array(
        'estado' => 1
      );
      $this->db->where('eliminado',0);
      $this->db->where('usuario',$this->input->post('usr',true));
      $this->db->update('carrito',$data);
    }
    public function get_info_asesor($id_asesor = null){
      $admin = $this->load->database('db_h_ventas', TRUE);
      $admin->select('idasesor,login_idlogin,nombre,datos,login_idlogin');
      $admin->from('asesor');

      if($id_asesor == null){

        $admin->where('login_idlogin',explode(':',$this->input->post('usr',true))[1]);

      }else{
        $admin->where('idasesor',$id_asesor);
      }
      return $admin->get()->result_array();
    }

    public function crear_cotizacion($cotizacion,$idc=null){
      $admin = $this->load->database('db_h_ventas', TRUE);
      $admin->select('asesor_idasesor');
      $admin->from('cliente');
      $admin->where('idcliente',$this->input->post('usr',true));

      $idas = $admin->get()->result_array();

      if($idas!=null){
        $asesor = $idas[0]['asesor_idasesor'];
      }else{
        $asesor = null;
      }

      if(isset(explode(':',$this->input->post('usr',true))[1])){
        $asesor = $this->get_info_asesor();
        $data = array(
          'cliente_idcliente' =>  0,
          'asesor_idasesor' => $asesor[0]['idasesor'],
          'asesor_login' => $asesor[0]['login_idlogin'],
          'asesor' => $asesor[0]['nombre']." <br>".$asesor[0]['datos'],
          'json_in' => $cotizacion
        );
        if(explode(':',$this->input->post('usr',true))[0] == 6){
          $data['estado'] = 2;
        }
      }else{
        $asesordata = $this->get_info_asesor($asesor);
        $data = array(
          'cliente_idcliente' => $this->input->post('usr',true),
          'asesor_idasesor' => $asesor,
          'asesor_login' => $asesordata[0]["login_idlogin"],
          'json_in' => $cotizacion
        );
      }

      if($idc){
        $admin->where('idcotizaciones',$idc);
        $admin->update('cotizaciones',$data);
      }else{
        $admin->insert('cotizaciones',$data);
        return $admin->insert_id();
      }

    }
    public function salvar_ids($folio,$ids_c){
      $admin = $this->load->database('db_h_ventas', TRUE);

      $data = array(
        'id_carrito' => $ids_c
      );
      $admin->where('idcotizaciones',$folio);
      $admin->update('cotizaciones',$data);
    }
	public function datos_cliente($cotizacion, $cliente) {

		$admin = $this->load->database("db_h_ventas", TRUE);

		$datos_cliente = $admin->query("SELECT * FROM cliente WHERE idcliente = ?", array($cliente))->result_array()[0];

		$array_datos["nombre"] = $datos_cliente["razon"];
		$array_datos["tels"] = $datos_cliente["tels"];
		$array_datos["rfc"] = $datos_cliente["rfc"];
		$array_datos["email"] = $datos_cliente["email"];
		$array_datos["direccion"] = $datos_cliente["calle"] . " " . $datos_cliente["exterior"] . " " . $datos_cliente["interior"] . " Col. " . $datos_cliente["colonia"] . " CP. " . $datos_cliente["codigo_postal"] . ". " . $datos_cliente["ciudad"] . ", " . $datos_cliente["estado"] . ", " . $datos_cliente["pais"];

		$json_datos = json_encode($array_datos);

		$data = array(
			'info_cliente' => $json_datos,
		);

		$admin->where("idcotizaciones", $cotizacion);
		$admin->update("cotizaciones", $data);

	}

  public function datos_distribuidor_vendedor($cotizacion, $cliente){
    $admin = $this->load->database("db_h_ventas", TRUE);

    $info = $this->make_info_asesor($cliente);

    $data_info = array(
      'nombre' => $info[0]['nombre'],
      'tels' => str_replace('Tel.','',explode('/',$info[0]['datos'])[0]),
      'rfc' => null,
      'email' => str_replace('E-mail: ','',explode('/',$info[0]['datos'])[1]),
      'direccion' => null ,
    );

    $json_datos = json_encode($data_info);

    $data = array(
      'info_cliente' => $json_datos,
    );

    $admin->where("idcotizaciones", $cotizacion);
		$admin->update("cotizaciones", $data);
  }

  public function make_info_asesor($cliente){
    $admin = $this->load->database("db_h_ventas", TRUE);

    return $admin->select('*')
                    ->from('asesor')
                    ->where('login_idlogin',explode(':',$cliente)[1])
                    ->get()
                    ->result_array();
  }

	public function caducidadCotizacion($idCotizacion) {
		$admin = $this->load->database('db_h_ventas', TRUE);

		$dias_caducidad = $admin->query("SELECT Valor FROM meta WHERE Nombre = ?", array("Dias_caducidad_cotiz"));
		$dias_caducidad = $dias_caducidad->result_array()[0]["Valor"];

		var_dump($dias_caducidad);

		$fecha_cotizacion = $admin->query("SELECT fecha FROM cotizaciones WHERE idcotizaciones = ?", array($idCotizacion))->result_array()[0]["fecha"];

		var_dump($fecha_cotizacion);

        $fecha_nueva = date("Y-m-d", strtotime($fecha_cotizacion . ' + ' . $dias_caducidad . ' days'));

		var_dump($fecha_nueva);
		die();

		$admin->query("UPDATE cotizaciones SET caducidad = ? WHERE idcotizaciones = ?", array($fecha_nueva, $idCotizacion));

	}

		public function json_producto()
		{
			$val1 = $this->input->post('val1');
			$val2 = $this->input->post('val2');
			$udm1 = $this->input->post('udm1');
			$udm2 = $this->input->post('udm2');
			echo $val1."<br>";
			echo $val2."<br>";
			echo $udm1."<br>";
			echo $udm2."<br>";
			echo $this->input->post('val_desc');
			//die();
			if($val1!=null && $val2!=null)
			{
				echo "<br>entre al primer if";
				$this->db->select('*');
				$this->db->from('productos');
				$this->db->where('descorta',$this->input->post('val_desc'));
				$this->db->where($udm1,$val1);
				$this->db->where($udm2,$val2);
				$query=$this->db->get();
				return $query->result_array();
			}else
			{
				if($val1!=null && $val2==null )
				{
				  echo "<br>entre al segundo if<br>";
				  $where= $udm1." = ".$val1;
				  echo $where;
				  $this->db->select('*');
				  $this->db->from('productos');
				  $this->db->where('descorta',$this->input->post('val_desc'));
				  $this->db->where($where);
				  $query=$this->db->get();
				  return $query->result_array();
				}else
				{
					if($val2!=null && $val1==null)
					{
						echo "<br>entre al tercer if";
						$this->db->select('*');
						$this->db->from('productos');
						$this->db->where('descorta',$this->input->post('val_desc'));
						$this->db->where($udm1,$val2);
						$query=$this->db->get();
						return $query->result_array();
					}else
					{
						echo "<br>entre al cuarto if";
						$this->db->select('*');
						$this->db->from('productos');
						$this->db->where('descorta',$this->input->post('val_desc'));
						$query=$this->db->get();
						return $query->result_array();
					}
				}
			}
		}

    public function get_clientes(){
      $admin = $this->load->database('db_h_ventas', TRUE);
      $admin->select('idcliente,contacto,razon');
      $admin->from('cliente,asesor');
      $admin->where('cliente.asesor_idasesor = asesor.idasesor');
      $admin->where('asesor.login_idlogin',$this->session->userdata('popusa')[0]['idlogin']);
      $admin->order_by('razon ASC');
      return $admin->get()->result_array();
    }

	}

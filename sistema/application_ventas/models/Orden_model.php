<?php
	Class Orden_model extends CI_Model {
		public function __construct() {
			//parent::__construct();
		}

		public function addProducto($idproducto, $idformato, $cantidad, $p = "p0b", $m = "uma") {
			$query = $this->db->query("SELECT $p, $m FROM productos WHERE id_productos = ?", array($idproducto));

			$precio = $query->result_array()[0][$p];
			$umed = $query->result_array()[0][$m];

			$data = array(
				"formato_idformato" => $idformato,
				"producto_idproducto" => $idproducto,
				"cantidad" => $cantidad,
				"importe_neto" => $precio,
				"unidad_med" => $umed
			);

			$this->db->insert("ordenes", $data);
		}

		public function deleteProducto($idproducto, $idformato) {
			$query = $this->db->query("DELETE FROM ordenes WHERE formato_idformato = ? AND producto_idproducto = ?", array($idformato, $idproducto));
		}

		public function updateCantidadProducto($idproducto, $idformato, $cantidad) {
			$data = array(
				'cantidad' => $cantidad
			);

			$this->db->where("idproducto", $idproducto);
			$this->db->where("idformato", $idformato);
			$this->db->update("ordenes", $data);
		}

		public function updateImporte($idproducto, $idformato, $p = "p0b") {
			$query = $this->db->query("SELECT ? FROM productos WHERE id_productos = ?", array($p, $idproducto));

			$query = $query->result_array()[0][$p];

			$data = array(
				'importe_neto' => $query,
			);

			$this->db->where("formato_idformato", $idformato);
			$this->db->where("producto_idproducto", $idproducto);
			$this->db->update("ordenes", $data);
		}

		public function getAllProductosOrden($idformato) {
			$this->db->where("formato_idformato", $idformato);
			$query = $this->db->get("ordenes");

			return $query->result_array();
		}

	}
?>

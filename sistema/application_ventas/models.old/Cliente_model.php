<?php
	Class Cliente_model extends CI_Model {
		public function __construct() {
			parent::__construct();
		}
		
		public function createCliente($nombre, $email, $domicilio, $telefono, $rfc, $login) {
			$data = array(
				'nombre' => $nombre,
				'email' => $email,
				'domicilio' => $domicilio,
				'telefono' => $telefono,
				'rfc' => $rfc,
				'login_idlogin' => $login
			);
			
			$this->db->insert("cliente",$data);
			return $this->db->insert_id();

		}
		
		public function asignVendedor($idcliente, $idvendedor) {
			$data = array(
				'vendedor' => $idvendedor
			);
			
			$this->db->where('idcliente', $idcliente);
			$this->db->update('cliente', $data);
		}
		
		public function updateCliente($nombre, $email, $domicilio, $telefono, $rfc, $idcliente) {
			$data = array(
				'nombre' => $nombre,
				'email' => $email,
				'domicilio' => $domicilio,
				'telefono' => $telefono,
				'rfc' => $rfc,
			);
			
			$this->db->where('idcliente', $idcliente);
			$this->db->update('cliente', $data);
		}
		
		public function deleteCliente($idcliente) {
			$this->db->where('idcliente', $idcliente);
			$this->db->delete('cliente');
		}
		
		public function getCliente($idcliente) {
			$this->db->where('idcliente', $idcliente);
			$query = $this->db->get('cliente'); 
			return $query->result_array();
		}
	}
?>
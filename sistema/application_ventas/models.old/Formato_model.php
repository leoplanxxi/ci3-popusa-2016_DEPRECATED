<?php

	Class Formato_model extends CI_Model {
		public function __construct() {
			parent::__construct();
		}
		
		public function createFormato($elaborado, $fecha, $cliente) {
			$data = array(
				'elaborado' => $elaborado,
				'fecha' => $fecha,
				'cliente_idcliente' => $cliente
			);
			
			$this->db->insert("formato", $data);
			return $this->db->insert_id();

		}
		
		public function asignDirEnvio($dirEnvio, $idformato) {
			$data = array(
				'dir_envio' => $dirEnvio
			);
			
			$this->db->where("idformato", $idformato);
			$this->db->update("formato", $data);
		}
		
		public function asignFechaEntrega($fecha, $idformato) {
			$data = array(
				'fecha_entrega' => $fecha,
			);
			
			$this->db->where("idformato", $idformato);
			$this->db->update("formato", $data);
			
		}
		
		public function asignImporte($importe, $idformato) {
			$data = array(
				'importe' => $importe,
			);
			
			$this->db->where("idformato", $idformato);
			$this->db->update("formato", $data);
		}
		
		public function asignAnticipo($anticipo, $idformato) {
			$data = array(
				'anticipo' => $anticipo,
			);
			
			$this->db->where("idformato", $idformato);
			$this->db->update("formato", $data);
		}
		
		public function asignSaldo($importe, $anticipo, $idformato) 
		{		
			$saldo = $importe - $anticipo;
			
			$data = array(
				'saldo' => $saldo,
			);
			
			$this->db->where("idformato", $idformato);
			$this->db->update("formato", $data);
		}
		
		public function asignEstado($estado, $idformato) {
			// 1. Cotizacion --- 2. Pedido 
			$data = array(
				'estado' => $estado,
			);
			
			$this->db->where("idformato", $idformato);
			$this->db->update("formato", $data);
		}
		
		public function getFormato($idformato) {
			$this->db->where('idformato', $idformato);
			$query = $this->db->get("formato");
			
			return $query->result_array();
		}
		
		public function deleteFormato($idformato) {
			$this->db->where("formato", $idformato);
			$this->db->delete("formato");
		}
	}

?>
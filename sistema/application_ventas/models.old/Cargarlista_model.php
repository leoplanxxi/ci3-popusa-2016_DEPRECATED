<?php
Class Cargarlista_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
    }

    function elimina_acentos($text){

        $text = htmlentities($text, ENT_QUOTES, 'UTF-8');
        $text = strtolower($text);
        //echo $text."<br>";
        $patron = array (
            // Espacios, puntos y comas por guion
            //'/[\., ]+/' => ' ',

            // Vocales
            '/\+/' => '',
            '/&agrave;/' => 'a',
            '/&egrave;/' => 'e',
            '/&igrave;/' => 'i',
            '/&ograve;/' => 'o',
            '/&ugrave;/' => 'u',

            '/&aacute;/' => 'a',
            '/&eacute;/' => 'e',
            '/&iacute;/' => 'i',
            '/&oacute;/' => 'o',
            '/&uacute;/' => 'u',

            '/&acirc;/' => 'a',
            '/&ecirc;/' => 'e',
            '/&icirc;/' => 'i',
            '/&ocirc;/' => 'o',
            '/&ucirc;/' => 'u',

            '/&atilde;/' => 'a',
            '/&etilde;/' => 'e',
            '/&itilde;/' => 'i',
            '/&otilde;/' => 'o',
            '/&utilde;/' => 'u',

            '/&auml;/' => 'a',
            '/&euml;/' => 'e',
            '/&iuml;/' => 'i',
            '/&ouml;/' => 'o',
            '/&uuml;/' => 'u',

            '/&auml;/' => 'a',
            '/&euml;/' => 'e',
            '/&iuml;/' => 'i',
            '/&ouml;/' => 'o',
            '/&uuml;/' => 'u',

            // Otras letras y caracteres especiales
            '/&aring;/' => 'a',
            '/&ntilde;/' => 'n',

            // Agregar aqui mas caracteres si es necesario

        );

        $text = preg_replace(array_keys($patron),array_values($patron),$text);
        return $text;
    }


    public function delete(){
      $this->db->truncate('productos');
    }

    public function agregar($datos) {

      $img="";
      if($datos[0][73]!=null){
        $img=$datos[0][73];
      }else{
        $img="1.png";
      }

      if($datos[0][2]!=null){
      $data = array(
        //'visible' => $datos[0][0],
        'msistema' =>$datos[0][0],//Mostrar u ocultar sistema
        'mlista' =>$datos[0][1],//Mostrar u ocultar Lista viajera
        'marca' => trim($datos[0][2]),//Marca
        'familia' => strtoupper($this->elimina_acentos(trim($datos[0][3]))),//Familia
        'subfamilia' => strtoupper($this->elimina_acentos(trim($datos[0][4]))),//SubFamilia
        'descorta' => $datos[0][5],//Descripcion Corta
        'deslarga' => trim($datos[0][6]),//Descripcion del Producto (En Sistema Herramienta de ventas y lista viajera)
        'descotizador' => trim($datos[0][7]),//Descripcion del producto para Cotizador
        'desespecial' => trim($datos[0][8]),//Descripcion del producto caracteristicas especiales  (cotizador)
        'tipo' => $datos[0][9],//Tipo Producto
        'rotacion' => $datos[0][10],//Rotacion
        'codigo' => $datos[0][11],//codigo
        'desc_cod' => $datos[0][12],//Descripcion codigo
        'var1' => $datos[0][13],//Variable 1
        'cant1' => $datos[0][14],//cantidad 1
        'unidad1'=> $datos[0][15],//unidad 1
        'var2' => $datos[0][16],//Variable 2
        'cant2' => $datos[0][17],//cantidad 2
        'unidad2' => $datos[0][18],//unidad 2
        'var3' => $datos[0][19],//Variable 3
        'cant3' => $datos[0][20],//Cantidad 3
        'unidad3' => $datos[0][21],//unidad 3
        'var4' => $datos[0][22],//Variable 4
        'cant4' => $datos[0][23],//Cantidad 4
        'unidad4' => $datos[0][24],//unidad 4
        'var5' => $datos[0][25],//Variable 5
        'cant5' => $datos[0][26],//Cantidad 5
        'unidad5' => $datos[0][27],//unidad 5
        'var6' => $datos[0][28],//Variable 6
        'cant6' => $datos[0][29],//Cantidad 6
        'unidad6' => $datos[0][30],//unidad 6
        'escala' => $datos[0][31],//Escala
        'u_base' => $datos[0][32],//Unidad Base
        'precio_siva' => $datos[0][33],// Precio S/IVA
        'precio' => $datos[0][34],//Precio
        'auxiliar' => $datos[0][35],//Escala Auxiliar
        'u_aux' => $datos[0][36],//Unidad Aux.
        'aux_iva' => $datos[0][37],// Pre. Aux. S/Iva
        'pre_aux' => $datos[0][38],//Pre. Aux.
        'moneda' => $datos[0][39],//Moneda
        'u_base2' => $datos[0][40],//UNIDAD BASE
        'pxub' => $datos[0][41],//PRECIO BASE
        'u_aux2' => $datos[0][42],//UNIDAD AUXILIAR
        'pxua' => $datos[0][43],//PRECIO AUXILIAR
        'iva' => $datos[0][44],// IVA
        'tient' => $datos[0][45],//tiempo de entrega
        'cvuni' => $datos[0][46],//clave unidades
        'cvpp' => $datos[0][47],//clave precios principal
        //'cusistema' => $datos[0][40],
        'pbasp' => $datos[0][48],//PRECIO BASE LV PRINCIPAL
        'pauxp' => $datos[0][49],//PRECIO AUXILIAR LV PRINCIPAL
        'pbasd' => $datos[0][50],//PRECIO BASE LV DISTRIBUIDOR
        'pauxd' => $datos[0][51],//PRECIO AUXILIAR LV DISTRIBUIDOR
        'pbase23' => $datos[0][52],//PRECIO BASE LV TIENDA 23 PTE
        'paux23' => $datos[0][53],//PRECIO AUXILIAR LV TIENDA 23 PTE
        'pbasev23' => $datos[0][54],//PRECIO BASE LV VENDEDOR 23 PTE
        'pauxv23' => $datos[0][55],//PRECIO AUXILIAR LV VENDEDOR 23 PTE
        'pbaseaquix' => $datos[0][56],//PRECIO BASE LV AQUIXTLA
        'pauxaquix' => $datos[0][57],//PRECIO AUXILIAR LV AQUIXTLA
        'pbasethuix' => $datos[0][58],//PRECIO BASE LV TIENDA HUIXCOLOTLA
        'pauxthuix' => $datos[0][59],//PRECIO AUXILIAR LV TIENDA HUIXCOLOTLA
        'pbasevhuix' => $datos[0][60],//PRECIO BASE LV VENDEDOR HUIXCOLOTLA
        'pauxvhuix' => $datos[0][61],//PRECIO AUXILIAR LV VENDEDOR HUIXCOLOTLA
        'pbasecent' => $datos[0][62],//PRECIO BASE LV CENTRAL DE ABASTOS
        'pauxcent' => $datos[0][63],//PRECIO AUXILIAR LV CENTRAL DE ABASTOS
        'pbasevcent' => $datos[0][64],//PRECIO BASE LV CENTRAL DE ABASTOS VENDEDOR
        'pauxvcent' => $datos[0][65],//PRECIO AUXILIAR LV CENTRAL DE ABASTOS VENDEDOR
        'pbasepak' => $datos[0][66],//PRECIO BASE LV PLANTA PAKET
        'pauxpak' => $datos[0][67],//PRECIO AUXILIAR LV PLANTA PAKET
        'pbaseplant' => $datos[0][68],//PRECIO BASE LV PLANTA
        'pauxplant' => $datos[0][69],//PRECIO AUXILIAR LV PLANTA
        'pbasebon' => $datos[0][70],//PRECIO BASE LV BONASA
        'pauxbon' => $datos[0][71],//PRECIO AUXILIAR LV BONASA
        'pdf' => $datos[0][72],//pdf
        'img' => $img,//img
        'color' => "#".$datos[0][74],//color
        'mos_ocu' => $datos[0][75],//Mostrar u oculatar
        'confeccion' => $datos[0][76],//Confección
        'costura' => $datos[0][77],//Cosutua
        'ojillos' => $datos[0][78],//Ojillos
        'deslivi' => $datos[0][79],//Descripción
        'sat' => $datos[0][80],//SAT
      );
      $this->db->insert('productos',$data);
      //die();
    }

    }

}

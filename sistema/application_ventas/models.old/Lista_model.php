<?php
	Class Lista_model extends CI_Model {

    public function __construct() {
			parent::__construct();
		}

    public function save_lista($url,$tipo){
      $data = array(
        'url' => $url,
				'tipo' => strtoupper($tipo)
      );
      $this->db->insert('lista_viajera',$data);
    }

		public function get_lista($tipo){
			$this->db->select('*');
			$this->db->from('lista_viajera');
			$this->db->where('tipo',$tipo);
			$this->db->order_by('creacion desc');
			$this->db->limit(1);
			return $this->db->get()->result_array();
		}

		public function get_lista_usuario($idlogin){
			$admin = $this->load->database('db_h_ventas', TRUE);
			$admin->select('pdf');
			$admin->from('asesor');
			$admin->where('login_idlogin',$idlogin);
			return $admin->get()->result_array();
		}

  }

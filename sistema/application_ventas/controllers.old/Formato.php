<?php 
	Class Formato extends CI_Controller {
		public function __construct() {
			parent::__construct();
		}
		
		public function verFormato($id) {
			$this->load->model("formato_model");
			$this->load->model("orden_model");
			$this->load->model("cliente_model");
			$this->load->model("productos_model");
			
			$formato = $this->formato_model->getFormato($id);
			$productos = $this->orden_model->getAllProductosOrden($id);
			$cliente = $this->cliente_model->getCliente($formato[0]["cliente_idcliente"]);
			
			echo "Fecha: " . $formato[0]["fecha"] . "<br>";
			echo "Elaborado por: " . $formato[0]["elaborado"] . "<br>";
			echo "Nombre: " . $cliente[0]["nombre"] . "<br>";
			echo "Tel&eacute;fono: " . $cliente[0]["telefono"] . "<br>";
			echo "RFC: " . $cliente[0]["rfc"] . "<br>";
			echo "Domicilio: " . $cliente[0]["domicilio"] . "<br>";
			echo "Direcci&oacute;n de envio: " . $formato[0]["dir_envio"] . "<br>";
			echo "Fecha de entrega: " . $formato[0]["fecha_entrega"] . "<br>";
			
			echo "<table>";
			echo "<tr>";
			echo "<th>Codigo</th><th>Descripci&oacute;n</th><th>Cant</th><th>Unidad Med</th><th>Precio Unit Neto</th><th>Importe Neto</th>";
			echo "</tr>";
			
			foreach ($productos as $producto) {
				echo "<tr>";
					echo "<td>" . $this->productos_model->getProducto($producto["producto_idproducto"])[0]["codigo"] . "</td>";
					echo "<td>" . $this->productos_model->getProducto($producto["producto_idproducto"])[0]["descorta"] . "</td>";
					echo "<td>" . $producto["cantidad"] . "</td>";
					echo "<td>" . $producto["unidad_med"] . "</td>";
					echo "<td>" . $producto["importe_neto"] . "</td>";
					echo "<td>" . $producto["cantidad"]*$producto["importe_neto"] . "</td>";
				echo "</tr>";
				
			}
			echo "<pre>";
		
		}
	}
?>
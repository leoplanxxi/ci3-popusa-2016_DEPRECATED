    <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class List_v extends CI_Controller {

  public function __construct() {
      parent::__construct();
      $this->load->model('productos_model');
      $this->load->model('lista_model');
      //setlocale(LC_ALL,"es_MX");

  }

  public function index(){

  }

  public function iajera($marca = "lnz",$tipo="prin"){
    if($marca == "lnz"){
      switch ($tipo) {
        case 'prin':
        $this->lista();
          break;

        default:
          # code...
          break;
      }
    }else{
      if($marca == "pkr"){
        switch ($tipo) {
          case 'prin':
            # code...
            break;

          default:
            # code...
            break;
        }
      }

    }
  }



  public function lista($tipo = "Principal"){

    $this->load->library('M_pdf');
    ini_set('memory_limit', '512M');
    set_time_limit(0);

    $data =array(
      'f0a' => $this->productos_model->factores('Factor_P0_Alianza'),
      'f1a' => $this->productos_model->factores('Factor_P1_Alianza'),
      'f2a' => $this->productos_model->factores('Factor_P2_Alianza'),
      'f3a' => $this->productos_model->factores('Factor_P3_Alianza'),
      'familias' => $this->productos_model->get_familias('Alianza'),
      'tipo' => $tipo

    );

    $datap = array(
      'f0p' => $this->productos_model->factores('Factor_P0_Paket'),
      'f1p' => $this->productos_model->factores('Factor_P1_Paket'),
      'f2p' => $this->productos_model->factores('Factor_P2_Paket'),
      'f3p' => $this->productos_model->factores('Factor_P3_Paket'),
      'familiasp' => $this->productos_model->get_familias('Paket'),
      'tipo' => $tipo

    );
    $validacion = $this->productos_model->get_fec_validacion();

    $html = $this->load->view('layout/bootstrap','',true).$this->load->view('Lista/viajera',$data,true);

    $this->m_pdf->pdf->SetHTMLHeader('<div class="col-md-12">
    <table class="table" style="margin-bottom:0px;">
      <thead>
      <tr>
        <th>
          <img class="img-responsive img-mark" src="'.base_url('public/img/logo-alianza.jpg').'">
        </th>
        <th>
          <div style="font-size:12px;margin:0px;"><b>Catalogo de precios</b></div>
          <div style="font-size:10px;margin:0px;">VALIDA DESDE EL '.strftime("%A, %d de %B de %Y",strtotime($validacion[0]['creacion'])).'</div>
        </th>
      </tr>
        </thaed>
    </table>
    </div>');

    $html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');;
    $this->m_pdf->allow_charset_conversion = true;
    $this->m_pdf->charset_in = 'iso-8859-4';
    $this->m_pdf->pdf->AddPage('L','','','','',5,5,22,5,0,0);
  	$this->m_pdf->pdf->WriteHTML($html); // write the HTML into the PDF

    $html2 =$this->load->view('Lista/paket',$datap,true);

    $this->m_pdf->pdf->SetHTMLHeader('<div class="col-md-12">
    <table class="table" style="margin-bottom:0px;">
      <thead>
      <tr>
        <th>
          <img class="img-responsive img-mark" src="'.base_url('public/img/logo-paket.jpg').'">
        </th>
        <th>
          <div style="font-size:12px;margin:0px;"><b>Catalogo de precios</b></div>
          <div style="font-size:10px;margin:0px;">VALIDA DESDE EL '.strftime("%A, %d de %B de %Y",strtotime($validacion[0]['creacion'])).'</div>
        </th>
      </tr>
        </thaed>
    </table>
    </div>');

    $html2 = mb_convert_encoding($html2, 'UTF-8', 'UTF-8');
    $this->m_pdf->allow_charset_conversion = true;
    $this->m_pdf->charset_in = 'iso-8859-4';
    $this->m_pdf->pdf->AddPage('L','','','','',5,5,27,5,0,0);
    $this->m_pdf->pdf->setFooter('{PAGENO}');
  	$this->m_pdf->pdf->WriteHTML($html2); // write the HTML into the PDF
    $this->m_pdf->pdr->showImageErrors = true;
    $pdfFilePath = './public/listaviajera/'.date('dmyhi')."_lista_viajera.pdf";
  	$this->m_pdf->pdf->Output($pdfFilePath, 'F');
    $this->lista_model->save_lista($pdfFilePath, $tipo );
    $this->session->set_flashdata('ok','Se creo la lista viajera');
    redirect('index.php/cargar/lista');
  }

  public function bonanza(){

    $data = array(
      'f0p' => $this->productos_model->factores('Factor_P0_Paket'),
      'f1p' => $this->productos_model->factores('Factor_P1_Paket'),
      'f2p' => $this->productos_model->factores('Factor_P2_Paket'),
      'f3p' => $this->productos_model->factores('Factor_P3_Paket'),
      'familiasp' => $this->productos_model->get_familias('Paket'),
      'familias' => $this->productos_model->get_familias('Alianza'),
    );
    $validacion = $this->productos_model->get_fec_validacion();
    $this->load->view('layout/bootstrap');
    $this->load->view('Lista/bonanza',$data);
  }

  public function descargar($name = "Principal"){

    switch (strtoupper($name)) {
      case 'PRINCIPAL':
      case 'ZONA':
      case 'GERENCIA':
      case 'PLANTA':
        $name = "PRINCIPAL";
      break;
      default:
      $name = $name;
      break;
    }
    $url = $this->lista_model->get_lista($name)[0]['url'];
    //echo str_replace('./',base_url(),$url);
    redirect(str_replace('./',base_url(),$url));
  }

  public function demo($tipo = null){
    //error_reporting(-1);
    setlocale(LC_ALL,"es_MX");
    //var_dump($this->productos_model->factores('Factor_P3_Alianza'));
    $data =array(
      'validacion' => $this->productos_model->get_fec_validacion(),
      'f0a' => $this->productos_model->factores('Factor_P0_Alianza'),
      'f1a' => $this->productos_model->factores('Factor_P1_Alianza'),
      'f2a' => $this->productos_model->factores('Factor_P2_Alianza'),
      'f3a' => $this->productos_model->factores('Factor_P3_Alianza'),
      'familias' => $this->productos_model->get_familias('Alianza'),
      'tipo' => $tipo
    );

    $datap = array(
      'f0p' => $this->productos_model->factores('Factor_P0_Paket'),
      'f1p' => $this->productos_model->factores('Factor_P1_Paket'),
      'f2p' => $this->productos_model->factores('Factor_P2_Paket'),
      'f3p' => $this->productos_model->factores('Factor_P3_Paket'),
      'familiasp' => $this->productos_model->get_familias('Paket'),
      'tipo' => $tipo
    );
    $this->load->view('layout/bootstrap');
    $this->load->view('Lista/viajera',$data);
    $this->load->view('Lista/paket',$datap);
  }
}

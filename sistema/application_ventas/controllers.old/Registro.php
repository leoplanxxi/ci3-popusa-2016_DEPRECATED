<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registro extends CI_Controller {

  public function __construct() {

      parent::__construct();
      $this->load->library('bcrypt');
      $this->load->helper('string');
      $this->load->model('registro_model');
      $this->load->helper('string');
      $this->load->helper('captcha');
      $this->load->model('captcha_model');
      $this->rand = random_string('alnum',5);
    }

    public function index(){
      $this->form_validation->set_rules('nombre','Rázon Social','trim|xss_clean|required');
      $this->form_validation->set_rules('razon','Nombre','trim|xss_clean|required');
      $this->form_validation->set_rules('rfc','RFC','trim|xss_clean|required|callback_rfc_unique');
      $this->form_validation->set_rules('telefono','Teléfono','trim|xss_clean|required');
      $this->form_validation->set_rules('celular','Celular','trim|xss_clean|required');
      $this->form_validation->set_rules('email','E-mail','trim|xss_clean|valid_email|required|callback_email_unique');
      $this->form_validation->set_rules('calle','Calle','trim|xss_clean|required');
      $this->form_validation->set_rules('numeroe','Número Exterior','trim|xss_clean|required');
      $this->form_validation->set_rules('numeroi','Número Inerior','trim|xss_clean');
      $this->form_validation->set_rules('ciudad','Ciudad','trim|xss_clean|required');
      $this->form_validation->set_rules('colonia','Colonia','trim|xss_clean|required');
      $this->form_validation->set_rules('estado','Estado','trim|xss_clean|required');
      $this->form_validation->set_rules('pais','País','trim|xss_clean|required');
      $this->form_validation->set_rules('cp','Código Postal','trim|xss_clean|required');
      $this->form_validation->set_rules('cfdi','Uso de CFDI','trim|xss_clean|required');
      $this->form_validation->set_rules('metodo','Método de pago','trim|xss_clean|required');
      $this->form_validation->set_rules('forma','Forma de pago','trim|xss_clean|required');
      $this->form_validation->set_rules('pass1','Contraseña','trim|xss_clean|required');
      $this->form_validation->set_rules('pass2','Vuelve a escribir la contraseña','trim|xss_clean|required|matches[pass1]');

      if($this->form_validation->run() == false){
        $data = array(
          'pais' => $this->registro_model->get_pais()
        );

        $this->load->view('layout/bootstrap');
        $this->load->view('Registro/registro',$data);
        //$this->load->view('layout/footer');
      }else{
        $this->registro_model->registrar();
		shell_exec("curl http://132.148.18.129/sistema-popusa/admin/index.php/API/notificarClienteNuevoTelemarketing");

        redirect('index.php/registro/registrado');
      }

    }

    public function ingresar(){

      $this->form_validation->set_rules('usuario','Usuario','trim|xss_clean|required');
      $this->form_validation->set_rules('contrasena','Contraseña','trim|xss_clean|required');

      if($this->form_validation->run() == false){

      }else{

      }
    }

    public function estado(){
      $id = explode(':',$this->input->post('mpais',true));
      $data = array(
        'estado' => $this->registro_model->get_estado()
      );

      $this->load->view('Registro/estado',$data);

    }

    public function rfc_unique($rfc){

      $rfc_aux = $this->registro_model->get_rfc($rfc);

      if($rfc_aux>0 && $rfc != "XAXX010101000"){
        $this->form_validation->set_message('rfc_unique', 'El {field} ya existe, ingrese otro por favor.');
        return FALSE;
      }else{
        return TRUE;
      }

    }

    public function email_unique($email){

      $email_aux = $this->registro_model->get_email($email);

      if($email_aux>0){
        $this->form_validation->set_message('email_unique', 'El {field} ya existe, ingrese otro por favor.');
        return FALSE;
      }else{
        return TRUE;
      }

    }

    public function registrado(){
      $this->session->set_userdata('captcha', $this->rand);
      $data = array(
  			'captcha' => $this->captcha()
  		);
      $this->load->view('layout/bootstrap');
      $this->load->view('Registro/registrado',$data);
    }

    public function refresh_captcha(){

      //configuramos el captcha
        $this->rand = random_string('alnum',5);
        $this->session->set_userdata('captcha', strtoupper($this->rand));
      $conf_captcha = array(
        'word'   =>  strtoupper($this->rand),
        'img_path' => './public/captcha/',
        'img_url' =>  base_url().'public/captcha/',
        'font_path' => './public/tipografias/comic.ttf',
        'img_width' => '175',
        'img_height' => '60',
        'font_size' => 28,
        //decimos que pasados 10 minutos elimine todas las imágenes
        //que sobrepasen ese tiempo
        'expiration' => 600,
           'colors'        => array(
                  'background' => array(255, 255, 255),
                  'border' => array(255, 255, 255),
                  'text' => array(0, 0, 0),
                  'grid' => array(255, 40, 40)
          )
      );

      //guardamos la info del captcha en $cap
      $cap = create_captcha($conf_captcha);

      //pasamos la info del captcha al modelo para
      //insertarlo en la base de datos
      $this->captcha_model->nuevo_captcha($cap,explode(',',$this->getRealIP())[0]);

      //devolvemos el captcha para utilizarlo en la vista
      echo $cap['image'];
    }

    public function captcha(){
    $conf_captcha = array(
      'word'   => strtoupper($this->rand),
      'img_path' => './public/captcha/',
      'img_url' =>  base_url().'public/captcha/',
      'font_path' => './public/tipografias/comic.ttf',
      'img_width' => '200',
      'img_height' => '60',
      'font_size' => 28,
      'expiration' => 600,
         'colors'        => array(
                'background' => array(255, 255, 255),
                'border' => array(255, 255, 255),
                'text' => array(0, 0, 0),
                'grid' => array(255, 40, 40)
        )
    );

    $cap = create_captcha($conf_captcha);
    $this->captcha_model->nuevo_captcha($cap,$this->getRealIP());
    return $cap;
  }

  public function recuperar(){

    $this->form_validation->set_rules('usr_email','Correo Eléctronico','trim|xss_clean|required');
    $this->form_validation->set_rules('captcha','Captcha','trim|xss_clean|required');

    if($this->form_validation->run() == false){

      $this->session->set_userdata('captcha', strtoupper($this->rand));
      $data = array(
        'captcha' => $this->captcha()
      );

      $this->load->view('layout/bootstrap');
      $this->load->view('Registro/recuperar',$data);
    }else{

      if($this->validate_captcha()){

        if($this->validate_email()){

          $this->send_emailr();
          redirect('index.php/principal/productos');
        }else{
          redirect('index.php/recuperar');
        }

      }else{
        redirect('index.php/recuperar');
      }

    }
  }

  public function validate_captcha()
	{

	    if($this->input->post('captcha') != $this->session->userdata('captcha'))
	    {
	         $this->session->set_flashdata("error", "Captcha Incorrecto");
	        return false;
	    }else{
	        return true;
	    }

	}

  public function validate_email(){


    if($this->registro_model->get_email($this->input->post('usr_email'))){

      return true;
    }else{
      $this->session->set_flashdata('error','El correo eléctronico no esta registrado, intente de nuevo porfavor');
      return false;
    }
  }

  public function send_emailr(){
    $this->load->library('email');
    echo $this->input->post('usr_email');
    $userdata = $this->registro_model->get_data($this->input->post('usr_email'));

    $config['protocol'] = 'smtp';
    $config['smtp_host'] = 'smtp.office365.com';
    $config['smtp_port'] = '587';
    $config['smtp_crypto'] = 'tls';
    $config['smtp_timeout'] = '5';
    $config['newline'] = "\r\n";
    $config['smtp_user'] = 'luis.salasr@correo.buap.mx';
    $config['smtp_pass'] = 'akibakero@643';
    $config['charset'] = 'utf-8';
    $config['mailtype'] = 'html';
    $config['validation'] = TRUE;

    $this->email->initialize($config);
    $this->email->set_crlf( "\r\n" );
    $this->email->from('luis.salasr@correo.buap.mx','Popusa');
    $this->email->to($this->input->post('usr_email',true));
    $this->email->subject('Recuperar Contraseña');
    $ruta= base_url('index.php/cambiarcon');
    $html = '<h2>Pulsa el siguiente enlace para cambiar tu contraseña</h2><hr><br>';
    $html .= '<a href="'.$ruta.'/'.$userdata[0]['token'].'">';
    $html .= $ruta.'/'.$userdata[0]['token'].'</a>';

    $this->email->message($html);
    if($this->email->send() == true){
      $this->session->set_flashdata('ok','Se ha mandado un correo de restablecimiento de contraseña');
      $this->registro_model->startRecoveryPassword($userdata[0]['idlogin']);
    }else{
      $this->session->set_flashdata('error','No se pudo enviar correo de restauración, intente mas tarde');
    }
  }

  public function cambiar($token = null){
    $this->form_validation->set_rules('pass1','Contraseña','trim|xss_clean|required');
    $this->form_validation->set_rules('pass2','Repetir Contraseña','trim|xss_clean|required|matches[pass1]');

    //$this->form_validation->set_rules('nekot','Error cierre la ventana e intente de nuevo porfavor','trim|xss_clean|required');

    if($this->form_validation->run()== false){
      if($this->verify_token($token) === FALSE){

       $this->session->set_flashdata(
              "error", "Si necesita recuperar su password rellene el
              formulario con su email y le haremos llegar un correo con instrucciones"
              );
              redirect(base_url("index.php/recuperar"));
            }else{

              $data = array(
                'token' => $token,
                'nekot' => $this->verify_token($token)->idlogin
              );
               $this->load->view('layout/bootstrap');
               $this->load->view('Registro/cambio',$data);

            }
    }else{
      $this->registro_model->act_pass();
      $this->session->set_flashdata('ok','La contraseña ha sido actualizada');
      redirect('index.php/principal/productos');
    }





  }

  public function verify_token($token){
    return $this->registro_model->verify_token($token);
  }

  public function getRealIP(){

	   if (isset($_SERVER["HTTP_CLIENT_IP"])){
	       return $_SERVER["HTTP_CLIENT_IP"];
	   }elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
	       return $_SERVER["HTTP_X_FORWARDED_FOR"];
	   }elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
	       return $_SERVER["HTTP_X_FORWARDED"];
	   }elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
	       return $_SERVER["HTTP_FORWARDED_FOR"];
	   }elseif (isset($_SERVER["HTTP_FORWARDED"])){
	       return $_SERVER["HTTP_FORWARDED"];
	   }else{
	       return $_SERVER["REMOTE_ADDR"];
	   }

	}

  public function get_cp_info(){
  $cp_info = $this->registro_model->get_cp_info();
  if($cp_info!=null){
    $colonia = explode(';',$cp_info[0]['Colonia_cp']);
    $cp_info[0]['Colonia_cp'] = $colonia;
    echo json_encode($cp_info);
  }else{
    echo json_encode(null);
  }
}

public function get_colonias($id = null){
  if($id==null){
    $colonias = $this->input->post('c',true);

    foreach($colonias as $c){
    $aux_c[$c] = $c;
    }
    $data = array(
      'colonias' => $aux_c
    );
    $this->load->view('Registro/colonia',$data);
  }else{

    $this->load->view('Registro/colonia2','');
  }

}

  }

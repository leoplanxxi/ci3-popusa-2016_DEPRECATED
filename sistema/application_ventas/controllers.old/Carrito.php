<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Carrito extends CI_Controller {

  public function __construct() {
      parent::__construct();
      $this->load->database();
      $this->load->model("carrito_model");
      $this->load->helper('url');
  }

  public function guardar(){

    $carrito=$this->carrito_model->json_producto();
    $carrito = json_encode($carrito);
    $this->session->set_tempdata("carrito",$carrito, 900);
    echo "guardado el json una session temporal de 15 minutos<br>";
    //var_dump($this->session->tempdata("carrito"));
    echo $carrito;
  }

  public function cotizador(){
    $aux = $this->carrito_model->get_cotizacion();
    $ids_carrito = "";
    /*echo json_encode($aux);
    die();*/
    $usr = $this->input->post("usr", true);

    for($i=0;$i<sizeof($aux);$i++){
      $ids_carrito = $ids_carrito.$aux[$i]['id_cotizacion']."|";
      $aux_subf = json_decode($aux[$i]['json_carrito']);
      switch ($aux_subf[0]->familia) {
        case 'PLASTICOS':
        if($aux_subf[0]->subfamilia=="CUBIERTAS DE IMPORTACION"){
          $aux_ext=array(
            'unidad' => $aux[$i]['unidad'],
            'cantidad' => $aux[$i]['cantidad'],
            'parametrosExtra' => array(
              'ancho' => $aux_subf[0]->cant2,
              'largo' =>$aux_subf[0]->cant3,
              'rollos' =>$aux[$i]['cantidad']
            )
          );

          if(isset(explode(':',$usr)[1])){
            if(explode(':',$usr)[0] == 6) {
              $aux_ext['precio_cama'] =1;
            }else{
              $aux_ext['precio_cama'] =2;
            }
          }else{
            $aux_ext['precio_cama'] =2;
          }

        }else{
          $aux_ext=array(
            'unidad' => $aux[$i]['unidad'],
            'cantidad' => $aux[$i]['cantidad']
          );
          if(isset(explode(':',$usr)[1])){
            if(explode(':',$usr)[0] == 6) {
              $aux_ext['precio_cama'] =1;
            }else{
              $aux_ext['precio_cama'] =2;
            }
            }else{
              $aux_ext['precio_cama'] =2;
            }
        }
          break;
          case 'TEJIDOS':
          if($aux[$i]['cant_costura']>0){$opc_cos = "SI";}else{$opc_cos = "NO";}
          if($aux[$i]['cant_ojillos']>0){$opc_oji = "SI";}else{$opc_oji = "NO";}
          if($aux[$i]['cant_confeccion']>0){$opc_con = "SI";}else{$opc_con = "NO";}
          $aux_ext=array(
            'unidad' => $aux[$i]['unidad'],
            'cantidad' => $aux[$i]['cantidad'],
            'parametrosExtra' => array(
              'ancho' => $aux_subf[0]->cant2,
              'largo' =>$aux_subf[0]->cant3,
              'rollos' =>$aux[$i]['cantidad'],
              "costura" => $opc_cos,
          		"uniones" => $aux[$i]['cant_costura'],
          		"costura_p" => 3,
          		"ojillos" => $opc_oji,
          		"numeroOjillos" => $aux[$i]['cant_ojillos'],
          		"ojillos_p" => 3,
              "confeccion" => $opc_con,
          		"confeccion_p" => 3,
          		"confeccion_m2" => $aux[$i]['cant_confeccion'],
            )
          );

          if(isset(explode(':',$usr)[1])){
            if(explode(':',$usr)[0] == 6) {
              $aux_ext['precio_cama'] =1;
            }else{
              $aux_ext['precio_cama'] =2;
            }
            }else{
              $aux_ext['precio_cama'] =2;
            }


            break;
        default:
        $aux_ext=array(
          'unidad' => $aux[$i]['unidad'],
          'cantidad' => $aux[$i]['cantidad']
        );
        if(isset(explode(':',$usr)[1])){
          if(explode(':',$usr)[0] == 6) {
            $aux_ext['precio_cama'] =1;
          }else{
            $aux_ext['precio_cama'] =2;
          }
          }else{
            $aux_ext['precio_cama'] =2;
          }
          break;
      }

      $aux_cot[$aux[$i]['codigo_producto']][]=$aux_ext;
    }
    $cotizador = str_replace(']','',str_replace('[','',json_encode($aux_cot)));
    //echo $cotizador;

    //die();
    $folio = $this->carrito_model->crear_cotizacion($cotizador,$aux[0]['id_cotizado']);
    if($folio){
      $this->carrito_model->salvar_ids($folio,$ids_carrito);
      $this->session->set_userdata('folio',$folio);
    }else{

      $this->carrito_model->salvar_ids($aux[0]['id_cotizado'],$ids_carrito);
      $this->session->set_userdata('folio',$aux[0]['id_cotizado']);

    }
    //die();
	$usr = $this->input->post("usr", true);

  if(isset(explode(':',$usr)[1])){

    switch (explode(':',$usr)[0]) {

      case 6:
      $this->carrito_model->datos_distribuidor_vendedor($folio,$usr);
      shell_exec("curl http://132.148.18.129/sistema-popusa/admin/index.php/cotizador/index/" . $folio);
      break;

      case 4:
      $this->carrito_model->datos_distribuidor_vendedor($folio,$usr);
      shell_exec("curl http://132.148.18.129/sistema-popusa/admin/index.php/cotizador/index/" . $folio);
      break;

      case 2:
      $this->carrito_model->datos_distribuidor_vendedor($folio,$usr);
      shell_exec("curl http://132.148.18.129/sistema-popusa/admin/index.php/cotizador/index/" . $folio);
      break;

      case 1:
      $this->carrito_model->datos_distribuidor_vendedor($folio,$usr);
      shell_exec("curl http://132.148.18.129/sistema-popusa/admin/index.php/cotizador/index/" . $folio);
      break;

      default:

      break;
    }
  }else{
    $this->carrito_model->datos_cliente($folio, $usr);
    shell_exec("curl http://132.148.18.129/sistema-popusa/admin/index.php/API/notificarNuevaCotizacionCliente/" . $folio);//descomentar despues
    //Envio de correo a cliente, de que se está procesando su pedido.
    shell_exec("curl http://132.148.18.129/sistema-popusa/admin/index.php/API/notificarClienteCorreo/" . $folio);

  }


  $this->carrito_model->est_cotizado();

	//$this->carrito_model->cotizacionCaducidad($folio);

  }

  public function eliminar(){

  }
  public function actualizar(){

  }

  public function crearFormato() {
	$this->load->model("orden_model");
	$this->load->model("formato_model");

	$form_id = $this->formato_model->createFormato("Alvaro Lopez", date("Y-m-d"), 2);
	$this->formato_model->asignEstado("1", $form_id);

	$this->orden_model->addProducto(94, $form_id, 1);
	$this->orden_model->addProducto(91, $form_id, 3);
	$this->orden_model->addProducto(90, $form_id, 2);

  }

  public function get_clientes(){
    //var_dump($this->session->userdata('popusa'));
    $aclientes = $this->carrito_model->get_clientes();
    $clientes = null;
    $clientes['0'] = null;
    $clientes['1'] = $this->session->userdata('popusa')[0]['nombre'];
    foreach($aclientes  as $c){
      $clientes[$c['idcliente'].":".$c['contacto']] = $c['contacto'];
    }
    $data = array(
      'clientes' => $clientes
    );
    $this->load->view('cotizador/clientes',$data);
  }

  public function set_cliente(){
    $this->session->unset_userdata('folio');
    $popusa = $this->session->userdata('popusa');
    if(explode(':',$this->input->post('mcliente'))[0]!=0){
      $popusa[0]['idcliente'] = explode(':',$this->input->post('mcliente'))[0];
      $popusa[0]['emcliente'] = explode(':',$this->input->post('mcliente'))[1];
    }else{
      $popusa[0]['idcliente'] = null;
      $popusa[0]['emcliente'] = $this->session->userdata('popusa')[0]['nombre'];
    }

    $this->session->set_userdata('popusa',$popusa);
    /*echo "<pre>";
    var_dump($this->session->userdata('popusa'));
    echo "</pre>";*/
    redirect('index.php/principal/productos');

  }

}

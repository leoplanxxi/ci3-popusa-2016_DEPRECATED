<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cargar extends CI_Controller {

  public function __construct() {
      parent::__construct();
      $this->load->database();
      $this->load->model("cargarlista_model");
      $this->load->helper('url');
      //$this->load->library('ChunkReadFilter');
      ini_set('memory_limit', '512M');
      set_time_limit(0);
  }

  public function lista(){

		$this->load->view('layout/bootstrap');
		//$this->load->view('layout/navbar');
		$this->load->view('cargar/lista');
		//$this->load->view('layout/footer');
	}

  public function guardar(){

    $config['upload_path'] = config_item('archivos');
    $config['allowed_types'] = 'xls|xlsx|ods|csv';
    //$config['allowed_types']        = 'gif|jpg|png|webp';
    $config['max_size'] = '10240';
    $this->load->library('upload',$config);
    $this->upload->initialize($config);
    if(!$this->upload->do_upload('productos')){
      //redirect('cargar/lista');
      $this->session->set_flashdata('ok',$this->upload->display_errors());
      redirect('cargar/lista');
      //var_dump($this->upload->data());
      //die();
    }else{
    $this->cargarlista_model->delete();
    $dataa = $this->upload->data();
    echo "<pre>";
    var_dump($this->upload->data());
    echo "</pre>";
    $nombre = $this->upload->data()['file_name'];
    echo "nombre: ".$nombre."<br>";

    $this->load->library('excel');
		$archivo = config_item('archivos').$nombre;
		echo $archivo;
		try{
			//echo "despues de entrar al try";
			$tipoarchivo = PHPExcel_IOFactory::identify($archivo);
			echo $tipoarchivo;

			$leerobjeto = PHPExcel_IOFactory::createReader($tipoarchivo);
      //$spreadsheetInfo = $leerobjeto->listWorksheetInfo($archivo);
			$objetoexcel = $leerobjeto->load($archivo);
		}catch(Exception $e){
			die('<br>Hay un error leyendo el archivo "'.pathinfo($archivo,PATHINFO_BASENAME).'":<br>'.$e->getMessage());
		}

    /*$chunkFilter = new ChunkReadFilter();
    $leerobjeto->setReadFilter($chunkFilter);
    $leerobjeto->setReadDataOnly(true);
    $chunkFilter->setRows(0, 1);
    echo("Reading file " . $archivo . PHP_EOL . "<br>");
    $totalRows = $spreadsheetInfo[0]['totalRows'];
    echo("Total rows in file " . $totalRows . " " . PHP_EOL . "<br>");
    $chunkSize=500;
    for ($startRow = 2; $startRow <= $totalRows; $startRow += $chunkSize) {
           echo("Loading WorkSheet for rows " . $startRow . " to " . ($startRow + $chunkSize - 1) . PHP_EOL . "<br>");
           $i = 0;

           $chunkFilter->setRows($startRow, $chunkSize);

           $objPHPExcel = $leerobjeto->load($archivo);
           $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, false);

           $startIndex = ($startRow == 1) ? $startRow : $startRow - 1;
           //dumping in database
           if (!empty($sheetData) && $startRow < $totalRows) {

                $this->cargarlista_model->agregar(array_slice($sheetData, $startIndex, $chunkSize));
                echo "<table border='1'>";
              foreach ($sheetData as $key => $value) {
                  $i++;
                  if ($value[0] != null) {
                      echo "<tr><td>id:$i</td><td>{$value[0]} </td><td>{$value[1]} </td><td>{$value[2]} </td><td>{$value[3]} </td></tr>";
                  }
              }
              echo "</table><br/><br/>";
           }
           $objPHPExcel->disconnectWorksheets();
           unset($objPHPExcel, $sheetData);
           //die();
       }
       */
		$hoja = $objetoexcel->getSheet(0);//obtenemos la hoja 1.
		$filamaxima = $hoja->getHighestRow();//Obtenemos las filas
		$columnamaxima = $hoja->getHighestColumn() ;//Obtenemos las columnas.

    echo "<br>filamaxima ".$filamaxima."<br>";
    echo "columnamaxima ".$columnamaxima."<br>";
    //$filamaxima
    echo "<pre>";
		for($fila = 2; $fila <= $filamaxima; $fila++){
      //

			$datosfila = $hoja->rangeToArray('A'.$fila.':'.$columnamaxima.$fila,NULL,FALSE,FALSE);
      $this->cargarlista_model->agregar($datosfila);
		}
    echo "</pre>";

    echo "se cargaron ".$fila." productos";

    }

  }


}

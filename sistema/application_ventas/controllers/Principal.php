<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Principal extends CI_Controller {

	 public function __construct() {
			 parent::__construct();
			 date_default_timezone_set('America/Mexico_City');
			 $this->load->database();
			 $this->load->helper('url');
			 $this->load->helper('string');
			 $this->load->helper('captcha');
			 $this->load->model("productos_model");
			 $this->load->model("lista_model");

			 $this->load->model('captcha_model');
       $this->rand = random_string('alnum',5);

	 }

	public function index(){
		redirect("index.php/principal/productos");
	}


	public function productos($id = null){
		// var_dump($this->session->userdata('popusa'));
		//echo dirname(__DIR__,2);
		//
		$this->load->model('carrito_model');
		if($id!=null){
			if($this->session->userdata('popusa')){

				$cotizacion = $this->carrito_model->get_data_cotizacion($id);

				if($cotizacion){
					$id_C = explode('|',$cotizacion[0]['id_carrito']);

					foreach($id_C as $idc){
						if($idc!=""){
							$this->carrito_model->reset_data_carrito($idc,$id);

						}
					}

					$this->carrito_model->reset_json_out($id);
					$info = $this->carrito_model->data_usr_cotizacion($id);

					$infoc = (array)json_decode($info[0]['info_cliente']);
					$popusa = $this->session->userdata('popusa');
					$popusa[0]['idcliente'] = $info[0]["cliente_idcliente"];;
		      $popusa[0]['emcliente'] = $infoc['nombre'];
					$popusa[0]['id_cotizado'] = $id;

		    	$this->session->set_userdata('popusa',$popusa);
				}

			}


			redirect('index.php/principal/productos');

		}
		$this->session->set_userdata('captcha', strtoupper($this->rand));
		if($this->session->userdata('tk')==null){
			$tk = md5($_SERVER['REMOTE_ADDR'].date('mYd_Hms'));
			$this->session->set_userdata('tk',$tk);
		}else{

		}

		$data = array(
			'alianzaf' => $this->productos_model->get_familias('ALIANZA'),
			'paketf' => $this->productos_model->get_familias('PAKET'),
			'alianzasf' => $this->productos_model->get_subfamilias('ALIANZA'),
			'paketsf' => $this->productos_model->get_subfamilias('PAKET'),

		);
		$data2 = array(
			'captcha' => $this->captcha()
		);
		$this->load->view("layout/bootstrap");
		$this->load->view("layout/navbar",$data);
		$this->load->view("principal",$data);
		$this->load->view("cotizador/modal");
		$this->load->view("Ingreso/ingreso",$data2);
		$this->load->view("layout/footer");

	}

	public function captcha(){

  $conf_captcha = array(
    'word'   => strtoupper($this->rand),
    'img_path' => './public/captcha/',
    'img_url' =>  base_url().'/public/captcha/',
    'font_path' => dirname(__DIR__,2).'/public/tipografias/comic.ttf',
    'img_width' => '200',
    'img_height' => '60',
    'font_size' => 40,
    'expiration' => 600,
       'colors'        => array(
              'background' => array(255, 255, 255),
              'border' => array(255, 255, 255),
              'text' => array(0, 0, 0),
              'grid' => array(255, 40, 40)
      )
  );

  $cap = create_captcha($conf_captcha);
  $this->captcha_model->nuevo_captcha($cap,$this->getRealIP());
  return $cap;
}

	public function getRealIP(){

	   if (isset($_SERVER["HTTP_CLIENT_IP"])){
	       return $_SERVER["HTTP_CLIENT_IP"];
	   }elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
	       return $_SERVER["HTTP_X_FORWARDED_FOR"];
	   }elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
	       return $_SERVER["HTTP_X_FORWARDED"];
	   }elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
	       return $_SERVER["HTTP_FORWARDED_FOR"];
	   }elseif (isset($_SERVER["HTTP_FORWARDED"])){
	       return $_SERVER["HTTP_FORWARDED"];
	   }else{
	       return $_SERVER["REMOTE_ADDR"];
	   }

	}

	public function lista() {

	}
}

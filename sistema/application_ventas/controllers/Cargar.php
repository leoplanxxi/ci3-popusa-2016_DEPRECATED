<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cargar extends CI_Controller {

  public function __construct() {
      parent::__construct();
      header('Content-Type: text/html; charset=utf-8');
      $this->load->database();
      $this->load->model("cargarlista_model");
      $this->load->helper('url');

  }

  public function lista(){

		$this->load->view('layout/bootstrap');
		//$this->load->view('layout/navbar');
		$this->load->view('cargar/lista');
		//$this->load->view('layout/footer');
	}
  public function prueba($archivo){
    //$this->load->library('excel_spreed');
    require_once APPPATH."/third_party/spreadsheet-reader/php-excel-reader/excel_reader2.php";
    require_once APPPATH."/third_party/spreadsheet-reader/SpreadsheetReader.php";
    error_reporting(0);
    $Reader = new SpreadsheetReader($archivo);
    $Sheets = $Reader -> Sheets();

  	//foreach ($Sheets as $Index => $Name){
      //echo 'Sheet #'.$Index.': '.$Name;

      $Reader -> ChangeSheet(0);

      $i = 0;

  		foreach ($Reader as $Row){
        if($i!=0){
          

          $this->cargarlista_model->agregar($Row);
          //var_dump($Row);
        }
        $i++;
      }
      
      echo "Se cargaron ".$i." productos a la herramienta de venta";
  	//}
  }

  public function guardar(){
    // error_reporting(0);
    // $this->load->library('excel');
    echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
    ini_set('memory_limit', '1024M');
    set_time_limit(0);
    error_reporting(0);
    echo 'Memoria inicial: ' . memory_get_usage() . ' ('. memory_get_usage()/1024 .'M) <br>';
    echo 'Memory limit: ' . ini_get('memory_limit') . '<br>';
    $config['upload_path'] = config_item('archivos');
    //$config['allowed_types'] = 'xls|xlsx|ods|csv';
    //$config['allowed_types']        = 'gif|jpg|png|webp';
    $config['allowed_types'] = '*';
    $config['max_size'] = '10240';
    $this->load->library('upload',$config);
    $this->upload->initialize($config);
    if(!$this->upload->do_upload('productos')){
      //redirect('cargar/lista');
      $this->session->set_flashdata('ok',$this->upload->display_errors());
      redirect('index.php/cargar/lista');
      //var_dump($this->upload->data());
      //die();
    }else{
    $this->cargarlista_model->delete();
    $dataa = $this->upload->data();
    $nombre = $this->upload->data()['file_name'];

    //$this->load->library('excel');
		$archivo = config_item('archivos').$nombre;
    $this->prueba($archivo);

		// try{
		// 	//echo "despues de entrar al try";
		// 	$tipoarchivo = PHPExcel_IOFactory::identify($archivo);
		// 	echo "<br>".$tipoarchivo."<br>";
    //
		// 	$leerobjeto = PHPExcel_IOFactory::createReader($tipoarchivo);
    //   //$leerobjeto->setReadDataOnly(true);
    //   //$spreadsheetInfo = $leerobjeto->listWorksheetInfo($archivo);
		// 	$objetoexcel = $leerobjeto->load($archivo);
    //   //die();
		// }catch(Exception $e){
		// 	die('<br>Hay un error leyendo el archivo "'.pathinfo($archivo,PATHINFO_BASENAME).'":<br>'.$e->getMessage());
		// }
    //
		// $hoja = $objetoexcel->getSheet(0);//obtenemos la hoja 1.
		// $filamaxima = $hoja->getHighestRow();//Obtenemos las filas
		// $columnamaxima = $hoja->getHighestColumn() ;//Obtenemos las columnas.
    //
    // //echo "<br>filamaxima ".$filamaxima."<br>";
    // //echo "columnamaxima ".$columnamaxima."<br>";
    // //$filamaxima
    // //echo "<pre>";
		// for($fila = 2; $fila <= $filamaxima; $fila++){
		// 	$datosfila = $hoja->rangeToArray('A'.$fila.':'.$columnamaxima.$fila,NULL,FALSE,FALSE);
    //   //var_dump($datosfila);
    //   $this->cargarlista_model->agregar($datosfila);
		// }
    // echo "</pre>";
    //
    // echo "se cargaron ".$fila." productos";

    echo '<br>Memoria final: ' . memory_get_usage() . ' ('. memory_get_usage()/1024 .'M) <br>';
    }

  }


}

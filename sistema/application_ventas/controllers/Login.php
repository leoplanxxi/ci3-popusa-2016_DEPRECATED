<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

  public function __construct() {
      parent::__construct();
      date_default_timezone_set('America/Mexico_City');
      $this->load->library('bcrypt');
      $this->load->helper('string');
      $this->load->helper('captcha');
      $this->load->model('captcha_model');
      $this->rand = random_string('alnum',5);
      $this->load->model('login_model');

    }

  public function ingresar(){

    //|valid_email
    $this->form_validation->set_rules('usuario','Usuario','trim|xss_clean|required');
    $this->form_validation->set_rules('contrasena','Contraseña','trim|xss_clean|required');
    
    if($this->session->userdata('intentos')>=3){
      $rel = $this->form_validation->set_rules('captcha', 'Captcha','callback_validate_captcha|required|trim|xss_clean');
    }


    if($this->form_validation->run() == false){
      $this->session->set_userdata('captcha',strtoupper($this->rand));
      $this->session->set_flashdata('error','El nombre de usuario o contraseña no es el correcto');

      redirect('index.php/principal/productos');
    }else{

      $this->login_model->ingresar();
    }

  }

  public function error(){

    $this->load->view('Ingreso/error_login');

  }

  public function validate_captcha(){

	   if($this->input->post('captcha') != $this->session->userdata('captcha')){
	      $this->session->set_flashdata("error", "Captcha Incorrecto");
        echo "incorrecto";
	       return false;
	    }else{
        echo "correcto";
	       return true;
	    }

	}

  public function salir(){
    $this->session->unset_userdata('popusa');
    session_destroy();
    redirect('index.php/principal/productos');
  }

  public function refresh_captcha(){
    $this->session->set_userdata('captcha', strtoupper($this->rand));
    $conf_captcha = array(
    'word'   => strtoupper($this->rand),
    'img_path' => './public/captcha/',
    'img_url' =>  base_url().'public/captcha/',
    'font_path' => dirname(__DIR__,2).'/public/tipografias/comic.ttf',
    'img_width' => '200',
    'img_height' => '60',
    'font_size' => 28,
    //decimos que pasados 10 minutos elimine todas las imágenes
    //que sobrepasen ese tiempo
    'expiration' => 600,
       'colors'        => array(
              'background' => array(255, 255, 255),
              'border' => array(255, 255, 255),
              'text' => array(0, 0, 0),
              'grid' => array(255, 40, 40)
      )
  );

  //guardamos la info del captcha en $cap
  $cap = create_captcha($conf_captcha);

  //pasamos la info del captcha al modelo para
  //insertarlo en la base de datos
  $this->captcha_model->nuevo_captcha($cap,$this->getRealIP());

  //devolvemos el captcha para utilizarlo en la vista
  echo $cap['image'];
}
public function getRealIP(){

   if (isset($_SERVER["HTTP_CLIENT_IP"])){
       return $_SERVER["HTTP_CLIENT_IP"];
   }elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
       return $_SERVER["HTTP_X_FORWARDED_FOR"];
   }elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
       return $_SERVER["HTTP_X_FORWARDED"];
   }elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
       return $_SERVER["HTTP_FORWARDED_FOR"];
   }elseif (isset($_SERVER["HTTP_FORWARDED"])){
       return $_SERVER["HTTP_FORWARDED"];
   }else{
       return $_SERVER["REMOTE_ADDR"];
   }

}
  }

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends CI_Controller {

	 public function __construct() {
			 parent::__construct();
			 $this->load->database();
			 $this->load->model("productos_model");
			 $this->load->model("lista_model");
			 $this->load->helper('url');
			 $this->load->library('session');
	 }

   public function lista(){

     //echo $this->input->post("nombre")."<br> Aqui tengo mas contenido papu";
		 $data = array();
		 if($this->input->post("fnombre")){
			 $data = array(
				 "productos" => $this->productos_model->get_listprodf($this->input->post("fnombre")),
			 );
		 }else{
			 $data = array(
				 "productos" => $this->productos_model->get_listprodsf($this->input->post("sfnombre")),
			 );
		 }

		 $this->load->view("productos",$data);

   }

	 public function get_medidas(){
	 	echo json_encode($this->productos_model->get_medidas());
	 }

	 public function get_medidas2(){
	 	 	echo json_encode($this->productos_model->get_medidas2());
	 }
	 public function get_medidas3(){
	 	echo json_encode($this->productos_model->get_medidas3());
	 }

	 public function get_medidas4(){
	 	 	echo json_encode($this->productos_model->get_medidas4());
	 }
	 public function get_medidas5(){
	 	 	echo json_encode($this->productos_model->get_medidas5());
	 }
	 public function get_medidas6(){
	 	 	echo json_encode($this->productos_model->get_medidas6());
	 }

	 public function get_escala(){
		 echo json_encode($this->productos_model->get_escala());
	 }

	 public function add_carrito(){
		 echo json_encode($this->productos_model->add_carrito());
	 }

	 public function ver_carrito(){
		 echo json_encode($this->productos_model->ver_carrito());
		 // echo $this->db->last_query();
	 }
	 public function del_carrito(){
		 echo json_encode($this->productos_model->del_carrito());
	 }

	public function buscar()
	{
		$data = array(
				 "productos" => $this->productos_model->buscar($this->input->post('buscar', true)),
			 );

		if(sizeof($data)>0)
		{
			$this->load->view("productos",$data);
		}
		else
		{
			$data = array(
				 "productos" => "NO SE ENCONTRARON RESULTADOS",
			 );
			 $this->load->view("productos",$data);
		}

	}

	public function get_info_extra(){
		echo json_encode($this->productos_model->get_info_extra());
		//echo "hola";
	}

	public function get_desc(){
		echo json_encode($this->productos_model->get_desc());
	}

	public function get_preciolist(){
		//Obtener el codigo del productos[0]['codigo']
		$codigo = $this->productos_model->get_codigo();
		//var_dump($codigo);
		//Obtenemos los factores de conversión
		if($codigo[0]['marca'] == "Alianza"){
			$f0 = $this->productos_model->factores('Factor_P0_Alianza');
			$f1 = $this->productos_model->factores('Factor_P1_Alianza');
			$f2 = $this->productos_model->factores('Factor_P2_Alianza');
			$f3 = $this->productos_model->factores('Factor_P3_Alianza');
		}else{
			$f0 = $this->productos_model->factores('Factor_P0_Paket');
			$f1 = $this->productos_model->factores('Factor_P1_Paket');
      $f2 = $this->productos_model->factores('Factor_P2_Paket');
      $f3 = $this->productos_model->factores('Factor_P3_Paket');
		}

		// $data = array(
		// 	'p1' => round(1 * $f1 * $codigo[0]['precio'],2),
		// 	'p2' => round(1 * $f1 * $f2 *$codigo[0]['precio'],2),
		// 	'p3' => round(1 * $f1 * $f2 * $f3 * $codigo[0]['precio'],2),
		// 	'codigo' => $codigo[0]['codigo']
		// );

		$cvuni = $codigo[0]['cvuni'];


		$precio = array(
			$codigo[0]['u_base2'],$codigo[0]['u_aux2']
		);

		switch(trim($this->input->post('unidad',true))){
			case trim($codigo[0]['u_base2']):

			$data = array(
				'p1' => round(1*$f0*$f1*$codigo[0]['pxub'],2),
				'p2' => round(1 * $f0 * $f1* $f2 *$codigo[0]['pxub'],2),
				'p3' => round(1 * $f0 * $f1 * $f2 * $f3 * $codigo[0]['pxub'],2),
				'codigo' => $codigo[0]['codigo']
			);
			break;
			case trim($codigo[0]['u_aux2']):
			$data = array(
				'p1' => round(1 * $f0 * $f1 * $codigo[0]['pxua'],2),
				'p2' => round(1 * $f0 * $f1 * $f2 *$codigo[0]['pxua'],2),
				'p3' => round(1 * $f0 * $f1 * $f2 * $f3 * $codigo[0]['pxua'],2),
				'codigo' => $codigo[0]['codigo']
			);
			break;
			default:
			$data = array(
				'p1' => round(1*$f0*$f1*$codigo[0]['pxub'],2),
				'p2' => round(1 * $f0 * $f1* $f2 *$codigo[0]['pxub'],2),
				'p3' => round(1 * $f0 * $f1 * $f2 * $f3 * $codigo[0]['pxub'],2),
				'codigo' => $codigo[0]['codigo']
			);
			break;
		}

		$this->load->view('cotizador/preciosdist',$data);
		//echo $codigo[0]['precio'];
		//echo json_encode($codigo);
		//$codigo[0]['']
		//una vez obtenido el codigo se calcula el precio con respecto al P1,P2,P3
		//se manda la informacion obtenido a la vista y se muestra
	}

	public function prod_header(){
		$data = array(
			'header' => $this->productos_model->get_header()
		);
		$this->load->view('layout/prod_header',$data);
	}

	public function get_productos(){
		$this->load->view('modal/productos');
	}

	public function get_productos_extras(){
		//var_dump($this->input->post());
		$this->load->view('modal/modal_extra');
	}

 }

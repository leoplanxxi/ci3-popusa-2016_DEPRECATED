<?php

    Class API extends CI_Controller {
        public function __construct() {
            parent::__construct();
        }

        public function getListadoProds($marca, $arg1 = NULL, $arg2 = NULL) {
            $this->load->database();
            if ($arg2 == NULL) {
                $sql = "SELECT * FROM productos WHERE marca = ? AND familia = ? GROUP BY descorta";
                $resultado = $this->db->query($sql, array($marca, urldecode($arg1)));
                $resultado = $resultado->result_array();
            }
            else {
                $sql = "SELECT * FROM productos WHERE marca = ? AND familia = ? AND subfamilia = ? GROUP BY descorta";
                $resultado = $this->db->query($sql, array($marca, urldecode($arg1), urldecode($arg2)));
                $resultado = $resultado->result_array();
            }

            header('Content-Type: application/json');
            echo json_encode($resultado);
        }

        public function getDetalleProd($codigo) {
            $this->load->database();

            $sql = "SELECT * FROM productos WHERE codigo = ?";
            $resultado = $this->db->query($sql, array($codigo));
            $resultado = $resultado->result_array();
            
            header("Content-Type: application/json");
            echo json_encode($resultado);
        }
    }
?>
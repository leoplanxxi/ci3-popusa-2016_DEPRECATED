<script>
  $(document).ready(function() {
    $('.input-daterange input').each(function() {
			$(this).datepicker('clearDates');
		});
        
    table = $('#LogsTable').DataTable({
        "ajax": {
            url : "<?php echo site_url("log/ajax") ?>",
            type : 'GET'
        },
        initComplete: function () {
        this.api().columns().every( function () {
            var column = this;
            var select = $('<select><option value=""></option></select>')
                .appendTo( $(column.footer()).empty() )
                .on( 'change', function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );

                    column
                        .search( val ? '^'+val+'$' : '', true, false )
                        .draw();
                } );

            column.data().unique().sort().each( function ( d, j ) {
                select.append( '<option value="'+d+'">'+d+'</option>' )
            } );
        } );
    }
    });

    $.fn.dataTable.ext.search.push(
			function(settings, data, dataIndex) {
				var min = $('#min-date').val();
				var max = $('#max-date').val();
				var createdAt = data[1] || 0; // Our date column in the table

				if (
				(min == "" || max == "") ||
				(moment(createdAt).isSameOrAfter(min) && moment(createdAt).isSameOrBefore(max))
				) {
				return true;
				}
				return false;
			}
		);

			// Re-draw the table when the a date range filter changes
		$('.date-range-filter').change(function() {
			table.draw();
		});

		$('#my-table_filter').hide();
});
</script>
<div class="container">
<div class="col-md-12">
<h1 class="align-center">Bitácora de eventos</h1>

<div class="row">
    <div class="col-md-4 pull-right">
    Buscar por rango de fechas:
        <div class="input-group input-daterange">

        <input type="text" id="min-date" class="form-control date-range-filter" data-date-format="yyyy-mm-dd" placeholder="De:">

        <div class="input-group-addon">a</div>

        <input type="text" id="max-date" class="form-control date-range-filter" data-date-format="yyyy-mm-dd" placeholder="A:">

        </div>
    </div>
</div>
<table id="LogsTable">
  <thead>
    <tr>
      <th>ID</th>
      <th>Fecha</th>
      <th>Severidad</th>
      <th>Tipo</th>
      <th>Usuario</th>
      <th>Mensaje</th>
    </tr>
</thead>
<tbody>
 
</tbody>
<tfoot>
					<tr>
          <th class="no-display">ID</th>
      <th class="no-display">Fecha</th>
      <th>Severidad</th>
      <th>Tipo</th>
      <th>Usuario</th>
      <th class="no-display">Mensaje</th> 
					</tr>
				</tfoot>
</table>
</div>
</div>
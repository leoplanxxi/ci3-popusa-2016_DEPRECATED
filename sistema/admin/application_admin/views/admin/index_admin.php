<style>
    .icon-users {
        position: absolute;
        top: 5px !important;
    }
</style>
<section class="content">
    <div class="row">
      <div class="col-md-6">
          <div class="small-box bg-yellow">
          <div class="inner">
            <h3>&nbsp;</h3>
          <p>Clientes</p>
          </div>
          <div class="icon icon-users">
          <i class="fa fa-user"></i>
          </div>
          <a href="<?php echo base_url("index.php/clientes/admin"); ?>" class="small-box-footer">
          Ver más <i class="fa fa-arrow-circle-right"></i>
          </a>

      </div>
      </div>
      <div class="col-md-6">
          <div class="small-box bg-red">
          <div class="inner">
            <h3>&nbsp;</h3>
          <p>Cotizaciones</p>
          </div>
          <div class="icon icon-users">
          <i class="fa fa-usd"></i>
          </div>
          <a href="<?php echo base_url("index.php/cotizaciones/admin"); ?>" class="small-box-footer">
          Ver más <i class="fa fa-arrow-circle-right"></i>
          </a>

      </div>
      </div>
        <div class="col-md-6">
            <div class="small-box bg-aqua">
            <div class="inner">
              <h3>&nbsp;</h3>
            <p>Órdenes</p>
            </div>
            <div class="icon icon-users">
            <i class="fa fa-archive"></i>
            </div>
            <a href="<?php echo base_url("index.php/ordenes/admin"); ?>" class="small-box-footer">
            Ver más <i class="fa fa-arrow-circle-right"></i>
            </a>

        </div>
        </div>
        <div class="col-md-6">
            <div class="small-box bg-green">
            <div class="inner">
              <h3>&nbsp;</h3>
            <p>Configuración</p>
            </div>
            <div class="icon icon-users">
            <i class="fa fa-cogs"></i>
            </div>
            <a href="<?php echo base_url("index.php/configuracion"); ?>" class="small-box-footer">
            Ver más <i class="fa fa-arrow-circle-right"></i>
            </a>

        </div>

        </div>
        <div class="col-md-6">
            <div class="small-box bg-red">
            <div class="inner">
              <h3>&nbsp;</h3>
            <p><a href="<?php echo base_url("index.php/login/logout"); ?>" style="width: 100%; display: block; color: white;">Salir</a></p>
            </div>
            <div class="icon icon-users">
            <i class="fa fa-close"></i>
            </div>
            <a href="<?php echo base_url("index.php/login/logout"); ?>" class="small-box-footer">
            Cerrar sesión <i class="fa fa-arrow-circle-right"></i>
            </a>

        </div>

        </div>

</section>

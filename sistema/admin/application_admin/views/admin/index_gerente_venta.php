<style>
    .icon-users {
        position: absolute;
        top: 5px !important;
    }
</style>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="small-box bg-aqua">
            <div class="inner">
            <h3><?php echo count($asesores); ?></h3>

            <p>Vendedores registrados</p>
            </div>
            <div class="icon icon-users">
            <i class="fa fa-users"></i>
            </div>
            <a href="<?php echo base_url("index.php/vendedores"); ?>" class="small-box-footer">
            Ver más <i class="fa fa-arrow-circle-right"></i>
            </a>
            
        </div>
        </div>
        <div class="col-md-6">
            <div class="small-box bg-green">
            <div class="inner">
            <h3><?php echo count($cotizaciones_nuevas); ?></h3>

            <p>Cotizaciones nuevas</p>
            </div>
            <div class="icon icon-users">
            <i class="fa fa-file-text-o"></i>
            </div>
            <a href="<?php echo base_url("index.php/cotizaciones"); ?>" class="small-box-footer">
            Ver más <i class="fa fa-arrow-circle-right"></i>
            </a>
            
        </div>
        </div>
        <div class="col-md-6">
            <div class="small-box bg-red">
            <div class="inner">
              <h3>&nbsp;</h3>
            <p><a href="<?php echo base_url("index.php/login/logout"); ?>" style="width: 100%; display: block; color: white;">Salir</a></p>
            </div>
            <div class="icon icon-users">
            <i class="fa fa-close"></i>
            </div>
            <a href="<?php echo base_url("index.php/login/logout"); ?>" class="small-box-footer">
            Cerrar sesión <i class="fa fa-arrow-circle-right"></i>
            </a>

        </div>
        </div>
    </div>
</div>
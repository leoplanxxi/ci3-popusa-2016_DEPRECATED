<?php if ($this->session->userdata("logged_in")["rol_idrol"] == 0): ?>
    <li class="header">MENU</li>
    <!-- Optionally, you can add icons to the links -->
    <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Link</span></a></li>
    <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>
    <li class="treeview">
        <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="#">Link in level 2</a></li>
            <li><a href="#">Link in level 2</a></li>
        </ul>
    </li>
<?php elseif ($this->session->userdata("logged_in")["rol_idrol"] == 1): ?>
    <li class="header">MENU TELEMARKETING</li>
    <!-- Optionally, you can add icons to the links -->
    <li><a href="<?php echo base_url("index.php/admin"); ?>"><i class="fa fa-home"></i> <span>Inicio</span></a></li>
    <li><a href="<?php echo base_url("index.php/clientes"); ?>"><i class="fa fa-users"></i> <span>Clientes</span></a></li>
    <li><a href="<?php echo base_url("index.php/clientes/nuevos"); ?>"><i class="fa fa-users"></i> <span>Clientes Nuevos</span></a></li>
    <li><a href="<?php echo base_url("index.php/clientes/reasignar"); ?>"><i class="fa fa-users"></i> <span>Reasignar Vendedor</span></a></li>
    <li><a href="<?php echo base_url("index.php/vendedores"); ?>"><i class="fa fa-user-circle-o"></i> <span>Vendedores</span></a></li>
    <li><a href="<?php echo base_url("index.php/cotizaciones"); ?>"><i class="fa fa-usd"></i> <span>Cotizaciones</span></a></li>
    <p>&nbsp;</p>
    <li><a href="<?php echo str_replace('admin/','/',base_url());?>"  target="_blank"><i class="fa fa-shopping-cart"></i> <span>Herramienta de Ventas</span></a></li>
<?php elseif ($this->session->userdata("logged_in")["rol_idrol"] == 2): ?>
    <li class="header">MENU VENDEDOR</li>
    <!-- Optionally, you can add icons to the links -->
    <li><a href="<?php echo base_url("index.php/admin"); ?>"><i class="fa fa-home"></i> <span>Inicio</span></a></li>
    <li><a href="<?php echo base_url("index.php/clientes"); ?>"><i class="fa fa-users"></i> <span>Clientes</span></a></li>
    <li><a href="<?php echo base_url("index.php/cotizaciones"); ?>"><i class="fa fa-usd"></i> <span>Cotizaciones</span></a></li>
    <p>&nbsp;</p>
    <li><a href="<?php echo str_replace('admin/','/',base_url());?>"  target="_blank"><i class="fa fa-shopping-cart"></i> <span>Herramienta de Ventas</span></a></li>
<?php elseif ($this->session->userdata("logged_in")["rol_idrol"] == 3): ?>
    <li class="header">MENU ADMINISTRADOR</li>
    <!-- Optionally, you can add icons to the links -->
    <li><a href="<?php echo base_url("index.php/admin"); ?>"><i class="fa fa-home"></i> <span>Inicio</span></a></li>
    <li><a href="<?php echo base_url("index.php/usuarios"); ?>"><i class="fa fa-users"></i> <span>Usuarios</span></a></li>
    <li><a href="<?php echo base_url("index.php/cama"); ?>"><i class="fa fa-database"></i> <span>Cama de datos</span></a></li>
    <li><a href="<?php echo base_url("index.php/clientes/admin"); ?>"><i class="fa fa-user"></i> <span>Clientes</span></a></li>
    <li><a href="<?php echo base_url("index.php/cotizaciones/admin"); ?>"><i class="fa fa-usd"></i> <span>Cotizaciones</span></a></li>
    <li><a href="<?php echo base_url("index.php/ordenes/admin"); ?>"><i class="fa fa-archive"></i> <span>Pedidos</span></a></li>
<?php /*    <li><a href="#"><i class="fa fa-file-pdf-o"></i> <span>Lista Viajera</span></a></li> */ ?>
    <li><a href="<?php echo base_url("index.php/log"); ?>"><i class="fa fa-book"></i> <span>Bitácora</span></a></li>
    <li><a href="<?php echo base_url("index.php/estadisticas"); ?>"><i class="fa fa-pie-chart"></i> <span>Reportes</span></a></li>
    <li><a href="<?php echo base_url("index.php/configuracion"); ?>"><i class="fa fa-cogs"></i> <span>Configuración</span></a></li>
    <p>&nbsp;</p>
    <li><a href="<?php echo str_replace('admin/','/',base_url());?>"  target="_blank"><i class="fa fa-shopping-cart"></i> <span>Herramienta de Ventas</span></a></li>
<?php elseif ($this->session->userdata("logged_in")["rol_idrol"] == 4): ?>
    <li class="header">MENU GERENTE DE LA VENTA</li>
    <!-- Optionally, you can add icons to the links -->
        <li><a href="<?php echo base_url("index.php/admin"); ?>"><i class="fa fa-home"></i> <span>Inicio</span></a></li>
	<li><a href="<?php echo base_url("index.php/cotizaciones"); ?>"><i class="fa fa-home"></i> <span>Cotizaciones</span></a></li>
	    <li><a href="<?php echo base_url("index.php/clientes"); ?>"><i class="fa fa-users"></i> <span>Clientes</span></a></li>
        <li><a href="<?php echo base_url("index.php/ordenes/ordenesEmpresa"); ?>"><i class="fa fa-archive"></i> <span>Pedidos</span></a></li>

    <li><a href="<?php echo base_url("index.php/vendedores"); ?>"><i class="fa fa-users"></i> <span>Vendedores</span></a></li>
    <p>&nbsp;</p>
    <li><a href="<?php echo str_replace('admin/','/',base_url());?>"  target="_blank"><i class="fa fa-shopping-cart"></i> <span>Herramienta de Ventas</span></a></li>
<?php elseif ($this->session->userdata("logged_in")["rol_idrol"] == 5): ?>
    <li class="header">MENU ADMINISTRADOR DE LA VENTA</li>
    <!-- Optionally, you can add icons to the links -->
    <li><a href="<?php echo base_url("index.php/admin"); ?>"><i class="fa fa-home"></i> <span>Inicio</span></a></li>
    <li><a href="<?php echo base_url("index.php/ordenes"); ?>"><i class="fa fa-users"></i> <span>Órdenes</span></a></li>
<?php elseif ($this->session->userdata("logged_in")["rol_idrol"] == 6): ?>
    <li class="header">MENU DISTRIBUIDOR</li>
    <!-- Optionally, you can add icons to the links -->
    <li><a href="<?php echo base_url("index.php/admin"); ?>"><i class="fa fa-home"></i> <span>Inicio</span></a></li>
    <li><a href="<?php echo base_url("index.php/ordenes"); ?>"><i class="fa fa-users"></i> <span>Órdenes</span></a></li>

    <p>&nbsp;</p>
    <li><a href="<?php echo str_replace('admin/','/',base_url());?>"  target="_blank"><i class="fa fa-shopping-cart"></i> <span>Herramienta de Ventas</span></a></li>
<?php endif; ?>

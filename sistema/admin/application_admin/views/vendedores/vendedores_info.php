<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default">
				<div class="box-header">
					<h4><a href="<?php echo base_url("index.php/vendedores"); ?>"><i class="fa fa-backward" aria-hidden="true"></i></a>&nbsp; &nbsp; Información</h4>
				</div>
				<div class="box-body">
					<?php echo form_open(); ?>
					<div class="form-group">
						<label for="nombre">Nombre público</label>
						<input type="text" name="nombre" id="nombre" class="form-control" value="<?php echo $informacion["nombre"]; ?>">
					</div>
					<div class="form-group">
						<label for="datos">Información</label>
						<input type="text" name="datos" id="datos" class="form-control" value="<?php echo $informacion["datos"]; ?>">
					</div>					

					<div class="form-group">
						<label for="zona_idzona">Usuario de inicio de sesión:</label> &nbsp; &nbsp; &nbsp;
						<?php echo $login; ?>
					</div>
				</div>
				<div class="box-footer">
					<a href="<?php echo base_url('index.php/vendedores/clientes/' . $informacion["login_idlogin"]); ?>" class="btn btn-default">Ver clientes</a>
					<button type="submit" class="btn btn-info pull-right">Guardar datos</button>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</section>
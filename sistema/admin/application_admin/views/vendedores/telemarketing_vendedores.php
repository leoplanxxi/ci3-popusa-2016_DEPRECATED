<script>
	$(document).ready(function() {
		$('#tablaVendedores').DataTable();
	});
</script>

<section class="content">
<div class="row">
<div class="col-md-12">
    <div class="box box-primary">
        <div class="box-body">
            <h1>Vendedores</h1>
            <p>Usted tiene los siguientes vendedores registrados: </p>
            
            <table id="tablaVendedores" class="display responsive nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td>Nombre</td>
                        <td>Datos</td>
						<td>Login</td>
						<td>Asignados</td>
                        <td>&nbsp;</td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($asesores as $asesor): ?>
                        <tr>
                            <td><?php echo $asesor["nombre"]; ?></td>
                            <td><?php echo $asesor["datos"]; ?></td>
							<td><?php
								$activo = $this->db->query("SELECT login.nombre FROM login, asesor WHERE asesor.login_idlogin = login.idlogin AND asesor.idasesor = ?", array($asesor["idasesor"]))->result_array()[0]["nombre"];
								
							echo $activo;
							?></td>
							<td>
							<?php
								$cant_clientes = $this->db->query("SELECT * FROM cliente WHERE asesor_login = ?", array($asesor["login_idlogin"]));
								$cant_clientes = count($cant_clientes->result_array());
								
								echo $cant_clientes;
							?>
							</td>
                            <td><a href="<?php echo base_url('index.php/vendedores/informacion/' . $asesor["idasesor"]); ?>" title="Ver información"><i class="fa fa-info-circle" aria-hidden="true"></i> Información</a></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div> 
</div>
</div>
</div>
<script>
    $(document).ready(function () {
        $('#tablaVendedores').DataTable({
            responsive: true
        });
    });
</script>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">
                    <h1>Vendedores</h1>
                    <p>Los siguientes vendedores se encuentran registrados en el sistema: </p>

                    <table id="tablaVendedores" class="display responsive nowrap" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <td>Nombre</td>
                                <td>Login</td>
                                <td>Nuevas</td>
                                <td>En Proceso</td>
                                <td>Aceptadas</td>
                                <td>Rechazadas</td>
                                <td>Total</td>
                                <td>% Eficiencia</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($asesores as $asesor): ?>
                                <tr>
                                    <td><?php echo $asesor["nombre"]; ?></td>
									<td>
									<?php
									$activo = $this->db->query("SELECT login.nombre FROM login, asesor WHERE asesor.login_idlogin = login.idlogin AND asesor.idasesor = ?", array($asesor["idasesor"]))->result_array()[0]["nombre"];
								
							echo $activo;
									?>
									</td>
                                    <td><a href="<?php echo base_url("index.php/cotizaciones/nuevasVendedor/" . $asesor["login_idlogin"]); ?>">
                                        <?php
                                        $query = $this->db->query("SELECT * FROM cotizaciones WHERE asesor_login = ? AND (estado = 0)", array($asesor["login_idlogin"]));
                                        $query = $query->result_array();
										
										$n_ep = count($query);
                                        echo $n_ep;
                                        ?></a></td>
                                    <?php //<td><?php echo $asesor["datos"]; </td>?>
                                    <td><a href="<?php echo base_url("index.php/cotizaciones/procesoVendedor/" . $asesor["login_idlogin"]); ?>">
                                        <?php
                                        $query = $this->db->query("SELECT * FROM cotizaciones WHERE asesor_login = ? AND (estado = 1)", array($asesor["login_idlogin"]));
                                        $query = $query->result_array();
										
										$n_ep = count($query);
                                        echo $n_ep;
                                        ?></a>
                                    </td>
                                    <td><a href="<?php echo base_url("index.php/ordenes/ordenesVendedor/" . $asesor["login_idlogin"]); ?>">
                                        <?php
                                        $query = $this->db->query("SELECT * FROM cotizaciones WHERE asesor_login = ? AND (estado = 2)", array($asesor["login_idlogin"]));
                                        $query = $query->result_array();

                                        $ac = count($query);
										echo $ac;
                                        ?></a>
                                    </td>
                                    <td><a href="<?php echo base_url("index.php/cotizaciones/rechazadasVendedor/" . $asesor["login_idlogin"]); ?>">
                                        <?php
                                        $query = $this->db->query("SELECT * FROM cotizaciones WHERE asesor_login = ? AND (estado = 3)", array($asesor["login_idlogin"]));
                                        $query = $query->result_array();

                                        $rc = count($query);
										echo $rc;
                                        ?></a>
                                    </td>	
                                    <td>
                                        <?php
                                        $query = $this->db->query("SELECT * FROM cotizaciones WHERE asesor_login = ?", array($asesor["login_idlogin"]));
                                        $query = $query->result_array();

                                        $total = count($query);
										echo $total;
                                        ?>
                                    </td>									
                                    <td><?php
									if ($total != 0) {
										$eficiencia = ($ac * 100)/$total;
										echo round($eficiencia, 2) . " %";
									}
									else {
										echo "0%";
									}
										
									?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div> 
        </div>
    </div>
</div>
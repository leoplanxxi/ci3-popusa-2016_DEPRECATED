<script>
	$(document).ready(function() {
		$('#tablaClientes').DataTable({
        "ajax": {
            url : "<?php echo site_url("API/obtenerClientesAdmin") ?>",
            type : 'GET'
        },
				"order": [[ 1, "asc" ]]

    });
	});
</script>

<script>
function eliminar(id) {
    if (confirm('¿Confirma que desea eliminar el cliente seleccionado?. Ésta operación no puede revertirse.')) { location.href = '<?php echo base_url("index.php/clientes/eliminar_admin"); ?>/' + id; } 
    return false;
}
</script>
<section class="content">
<div class="row">
<div class="col-md-12">
    <div class="box box-primary">
        <div class="box-body">
		<?php if ($this->session->flashdata("mensaje") != NULL): ?>
		<div class="alert alert-success alert-dismissible">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<h4><i class="icon fa fa-check"></i> Éxito</h4>
			<?php echo $this->session->flashdata("mensaje"); ?>
		</div>
	<?php endif; ?>
            <h4>Clientes</h4>
            <p>Los siguientes clientes se encuentran registrados en el sistema: </p>

            <table id="tablaClientes" class="display responsive nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td>ID</td>
                        <td>Razón Social</td>
                        <td>RFC</td>
                        <td>Correo electrónico</td>
                        <td>Asesor asignado</td>
                        <td>Operaciones</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
</div>

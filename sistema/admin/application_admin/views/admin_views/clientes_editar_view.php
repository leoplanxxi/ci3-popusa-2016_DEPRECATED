<script>
    $(document).ready(function(){
        $("#metodo").val("<?php echo $cliente["metodo"]; ?>");
        $("#cfdi").val("<?php echo $cliente["cfdi"]; ?>");
        $("#forma").val("<?php echo $cliente["forma"]; ?>");

    });
</script>
<section class="content">
    <div class="row">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Información del Cliente</h3>
            </div>
            <div class="box-body">
                <?php echo form_open(); ?>
                <?php if (validation_errors()): ?>
                    <div class="alert alert-warning">
                        <?php echo validation_errors(); ?>
                    </div>  
                <?php endif; ?>
                <input type="hidden" name="idcliente" value="<?php echo $cliente["idcliente"]; ?>" />
                <div class="form-group">
                    <label for="contacto">Contacto:</label>
                    <input type="text" name="contacto" id="contacto" class="form-control" value="<?php echo $cliente["contacto"]; ?>"> 
                </div>
                <div class="form-group">
                    <label for="razon">Razón social:</label>
                    <input type="text" name="razon" id="razon" class="form-control" value="<?php echo $cliente["razon"]; ?>"> 
                </div>
                <div class="form-group">
                    <label for="rfc">RFC:</label>
                    <input type="text" name="rfc" id="rfc" class="form-control" value="<?php echo $cliente["rfc"]; ?>"> 
                </div>
                <div class="form-group">
                    <label for="tels">Teléfonos:</label>
                    <input type="text" name="tels" id="tels" class="form-control" value="<?php echo $cliente["tels"]; ?>"> 
                </div>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="text" name="email" id="email" class="form-control" value="<?php echo $cliente["email"]; ?>"> 
                </div>
				<div class="row">
                <div class="form-group col-md-6">
                    <label for="calle">Calle:</label>
                    <input type="text" name="calle" id="calle" class="form-control" value="<?php echo $cliente["calle"]; ?>"> 
                </div>
				 <div class="form-group col-md-3">
                    <label for="exterior">Número exterior:</label>
                    <input type="text" name="exterior" id="exterior" class="form-control" value="<?php echo $cliente["exterior"]; ?>"> 
                </div>
				 <div class="form-group col-md-3">
                    <label for="interior">Int.:</label>
                    <input type="text" name="interior" id="interior" class="form-control" value="<?php echo $cliente["interior"]; ?>"> 
                </div>
				</div>
				<div class="row">
				 <div class="form-group col-md-9">
                    <label for="direcion">Colonia:</label>
                    <input type="text" name="colonia" id="colonia" class="form-control" value="<?php echo $cliente["colonia"]; ?>"> 
                </div>
				 <div class="form-group col-md-3">
                    <label for="codigo_postal">Código Postal:</label>
                    <input type="text" name="codigo_postal" id="codigo_postal" class="form-control" value="<?php echo $cliente["codigo_postal"]; ?>"> 
                </div>
				</div>
				<div class="row">
				 <div class="form-group col-md-4">
                    <label for="ciudad">Ciudad:</label>
                    <input type="text" name="ciudad" id="ciudad" class="form-control" value="<?php echo $cliente["ciudad"]; ?>"> 
                </div>
				 <div class="form-group col-md-4">
                    <label for="estado">Estado:</label>
                    <input type="text" name="estado" id="estado" class="form-control" value="<?php echo $cliente["estado"]; ?>"> 
                </div>
				 <div class="form-group col-md-4">
                    <label for="pais">País:</label>
                    <input type="text" name="pais" id="pais" class="form-control" value="<?php echo $cliente["pais"]; ?>"> 
                </div>
                </div>
                <div class="row">
				 <div class="form-group col-md-4">
                    <label for="ciudad">Ciudad:</label>
                    <input type="text" name="ciudad" id="ciudad" class="form-control" value="<?php echo $cliente["ciudad"]; ?>"> 
                </div>
				 <div class="form-group col-md-4">
                    <label for="estado">Estado:</label>
                    <input type="text" name="estado" id="estado" class="form-control" value="<?php echo $cliente["estado"]; ?>"> 
                </div>
				 <div class="form-group col-md-4">
                    <label for="pais">País:</label>
                    <input type="text" name="pais" id="pais" class="form-control" value="<?php echo $cliente["pais"]; ?>"> 
                </div>
                </div>
                <div class="row">
                 
				 <div class="form-group col-md-4">
                    <label for="cfdi">CFDI:</label>
                    <select name="cfdi" id="cfdi" class="form-control">
                        <option value="G01 Adquisición de Mercancías">G01 Adquisición de Mercancías</option>
                        <option value="G02 Devoluciones, Descuentos o Bonificaciones">G02 Devoluciones, Descuentos o Bonificaciones</option>
                        <option value="G03 Gastos en General">G03 Gastos en General</option>
                    </select>
                </div>
				 <div class="form-group col-md-4"> 
                    <label for="metodo">Método:</label>
                    <select name="metodo" id="metodo" class="form-control">
                        <option value="01 Efectivo">01 Efectivo</option>
                        <option value="02 Cheque Nominativo">02 Cheque Nominativo</option>
                        <option value="03 Transferencia electrónica">03 Transferencia electrónica</option>
                        <option value="04 Tarjeta de Crédito">04 Tarjeta de Crédito</option>
                        <option value="28 Tarjeta de Débito">28 Tarjeta de Débito</option>
                        <option value="99 Por Definir">99 Por Definir</option>
                    </select>
                </div>
				 <div class="form-group col-md-4">
                    <label for="forma">Forma:</label>
                    <select name="forma" id="forma" class="form-control">
                        <option value="PUE- En una sola Exhibición">PUE- En una sola Exhibición</option>
                        <option value="PPD- En parcialidades o Diferido">PPD- En parcialidades o Diferido</option>
                    </select>
                </div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
					<label>Asesor asignado: </label> <?php echo $asesor; ?> &nbsp; &nbsp;  &nbsp; &nbsp; <a href="<?php echo base_url("/index.php/clientes/cambiar_asesor_admin/" . $cliente["idcliente"]); ?>" class="btn btn-default">Asignar otro asesor</a>
					</div>
				</div>

                <button type="submit" class="btn btn-primary">Guardar</button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
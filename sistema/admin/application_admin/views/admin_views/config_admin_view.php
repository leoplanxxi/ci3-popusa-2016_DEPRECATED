<section class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                <b class="text-center">Configuración de Administrador</b>
                </div>
                <div class="box-body">
                    <?php echo form_open(); ?>

                    <p>Desde esta interfaz usted podrá controlar diversos aspectos que modificar el comportamiento del sistema y del cotizador.</p>
                    <?php if (validation_errors()) : ?>
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Error</h4>
                            <?php echo validation_errors(); ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($this->session->flashdata("mensaje")) : ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Mensaje</h4>
                            <?php echo $this->session->flashdata("mensaje"); ?>
                        </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="factor_iva">Factor IVA:</label>
                                <input type="text" name="factor_iva" id="factor_iva" class="form-control" value="<?php echo $factor_iva; ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group">
                        <label for="fecha_caducidad_cotiz">Fecha de caducidad de cotizaciones (en días):</label>
                            <input type="text" name="fecha_caducidad_cotiz" id="fecha_caducidad_cotiz" class="form-control" value="<?php echo $fecha_caducidad_cotiz; ?>">
                        </div>
                        </div>
                    </div>

                    <hr>
                    <div class="row">
                        <div class="col-md-6" style="border-right: 1px solid #eee">
                            <h4>Alianza</h4>
                            <div class="form-group">
                            <label for="factor_p0_alianza">Factor P0:</label>
                                <input type="text" name="factor_p0_alianza" id="factor_p0_alianza" class="form-control" value="<?php echo $factor_p0_alianza; ?>">
                            </div>
                            <div class="form-group">
                            <label for="factor_p1_alianza">Factor P1:</label>
                                <input type="text" name="factor_p1_alianza" id="factor_p1_alianza" class="form-control" value="<?php echo $factor_p1_alianza; ?>">
                            </div>
                            <div class="form-group">
                            <label for="factor_p2_alianza">Factor P2:</label>
                                <input type="text" name="factor_p2_alianza" id="factor_p2_alianza" class="form-control" value="<?php echo $factor_p2_alianza; ?>">
                            </div>
                            <div class="form-group">
                            <label for="factor_p3_alianza">Factor PL:</label>
                                <input type="text" name="factor_p3_alianza" id="factor_p3_alianza" class="form-control" value="<?php echo $factor_p3_alianza; ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4>Paket</h4>
                            <div class="form-group">
                            <label for="factor_p0_paket">Factor P0:</label>
                                <input type="text" name="factor_p0_paket" id="factor_p0_paket" class="form-control" value="<?php echo $factor_p0_paket; ?>">
                            </div>
                            <div class="form-group">
                            <label for="factor_p1_paket">Factor P1:</label>
                                <input type="text" name="factor_p1_paket" id="factor_p1_paket" class="form-control" value="<?php echo $factor_p1_paket; ?>">
                            </div>
                            <div class="form-group">
                            <label for="factor_p2_paket">Factor P2:</label>
                                <input type="text" name="factor_p2_paket" id="factor_p2_paket" class="form-control" value="<?php echo $factor_p2_paket; ?>">
                            </div>
                            <div class="form-group">
                            <label for="factor_p3_paket">Factor PL:</label>
                                <input type="text" name="factor_p3_paket" id="factor_p3_paket" class="form-control" value="<?php echo $factor_p3_paket; ?>">
                            </div>
                        </div>
                    </div>
                   <hr>
                   <div class="row">
                       <div class="col-md-12">
                            <div class="form-group">
                            <label for="Mensaje_Cliente_Cotizador">Mensaje cotizaciones:</label>
                                <input type="text" name="Mensaje_Cliente_Cotizador" id="Mensaje_Cliente_Cotizador" class="form-control" value="<?php echo $mensaje_cliente_cotizador; ?>">
                                <small>Para mostrar el nombre del cliente, utilize la variable <i>nombre_cliente</i></small>
                            </div>
                       </div>
                    </div>
                    <label>Información de cuentas bancarias: (Nombre del banco, No. de Cuenta, CLABE y Nombre de la Cuenta)</label>

                    <div class="row">
                        <div class="col-md-3">
                          <input type="text" name="banco-1-banco" id="banco-1-banco" class="form-control" value="<?php echo $banco1["banco"]; ?>" placeholder="Nombre del Banco">
                        </div>
                        <div class="col-md-2">
                          <input type="text" name="banco-1-cuenta" id="banco-1-cuenta" class="form-control" value="<?php echo $banco1["cuenta"]; ?>" placeholder="Número de cuenta">
                        </div>
                        <div class="col-md-2">
                          <input type="text" name="banco-1-clabe" id="banco-1-clabe" class="form-control" value="<?php echo $banco1["clabe"]; ?>" placeholder="CLABE">
                        </div>
                        <div class="col-md-3">
                          <input type="text" name="banco-1-nombre" id="banco-1-nombre" class="form-control" value="<?php echo $banco1["nombre"]; ?>" placeholder="Nombre de la cuenta">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                          <input type="text" name="banco-2-banco" id="banco-2-banco" class="form-control" value="<?php echo $banco2["banco"]; ?>" placeholder="Nombre del Banco">
                        </div>
                        <div class="col-md-2">
                          <input type="text" name="banco-2-cuenta" id="banco-2-cuenta" class="form-control" value="<?php echo $banco2["cuenta"]; ?>" placeholder="Número de cuenta">
                        </div>
                        <div class="col-md-2">
                          <input type="text" name="banco-2-clabe" id="banco-2-clabe" class="form-control" value="<?php echo $banco2["clabe"]; ?>" placeholder="CLABE">
                        </div>
                        <div class="col-md-3">
                          <input type="text" name="banco-2-nombre" id="banco-2-nombre" class="form-control" value="<?php echo $banco1["nombre"]; ?>" placeholder="Nombre de la cuenta">
                        </div>
                    </div>
                    <p>&nbsp;</p>
                   <a href="javascript:history.back(1)" class="btn btn-default pull-left">Cancelar</a>
                   <input type="submit" value="Guardar" class="btn btn-primary pull-right">
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
</section>

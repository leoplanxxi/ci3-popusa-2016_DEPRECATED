<section class="container">
	<div class="box box-primary">
		<div class="box-header">
			<h3 class="box-title">Asignar nuevo asesor</h3>
		</div>
		<div class="box-body">
			El cliente <b><?php echo $cliente; ?></b> tiene asignado al asesor <b><?php echo $asesor; ?></b>. Usted puede asignar un nuevo asesor seleccionándolo de la siguiente lista:<br><br>
			<?php echo form_open(); ?>
			<input type="hidden" name="idcliente" value="<?php echo $idcliente; ?>">
			<select class="form form-control" name="asesor_idasesor">
				<?php foreach ($asesores as $asesor_): ?>
				<option value="<?php echo $asesor_["idasesor"]; ?>"><?php 
					$activo = $this->db->query("SELECT login.nombre FROM login, asesor WHERE asesor.login_idlogin = login.idlogin AND asesor.idasesor = ?", array($asesor_["idasesor"]))->result_array()[0]["nombre"];
								
							echo $activo;  		
					?> - <?php echo $asesor_["nombre"]; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<div class="box-footer">
			<button type="submit" class="btn btn-primary pull-right">
				Asignar
			</button>
			<a href="javascript:history.back();" class="btn btn-default pull-left">Cancelar</a>
		</div>
	</div>
</section>

<script>
	$(document).ready(function() {
        $('.input-daterange input').each(function() {
			$(this).datepicker('clearDates');
		});

		table = $('#tablaCotizaciones').DataTable({
        "ajax": {
            url : "<?php echo site_url("API/obtenerTodasCotizacionesAdmin") ?>",
            type : 'GET'
        },
		initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    });

    $.fn.dataTable.ext.search.push(
			function(settings, data, dataIndex) {
				var min = $('#min-date').val();
				var max = $('#max-date').val();
				var createdAt = data[1] || 0; // Our date column in the table

				if (
				(min == "" || max == "") ||
				(moment(createdAt).isSameOrAfter(min) && moment(createdAt).isSameOrBefore(max))
				) {
				return true;
				}
				return false;
			}
		);

			// Re-draw the table when the a date range filter changes
		$('.date-range-filter').change(function() {
			table.draw();
		});

		$('#my-table_filter').hide();
	});
</script>
<style>
.no-display > select {
	display: none !important;
}
</style>

<section class="content">
<div class="row">
<div class="col-md-12">
    <div class="box box-primary">
        <div class="box-body">
		<?php if ($this->session->flashdata("mensaje") != NULL || $this->session->flashdata("mensaje_positivo") != NULL) : ?>
		<div class="alert alert-success alert-dismissible">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<h4><i class="icon fa fa-check"></i> Éxito</h4>
			<?php echo $this->session->flashdata("mensaje"); ?>
		</div>
	<?php endif; ?>
            <h4>Cotizaciones</h4>
            <p>Los siguientes cotizaciones se encuentran registrados en el sistema: </p>
            <div class="row">
    <div class="col-md-4 pull-right">
    Buscar por rango de fechas:
        <div class="input-group input-daterange">

        <input type="text" id="min-date" class="form-control date-range-filter" data-date-format="yyyy-mm-dd" placeholder="De:">

        <div class="input-group-addon">a</div>

        <input type="text" id="max-date" class="form-control date-range-filter" data-date-format="yyyy-mm-dd" placeholder="A:">

        </div>
    </div>
</div>
            <table id="tablaCotizaciones" class="display responsive nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td>Folio</td>
                        <td>Fecha</td>
                        <td>Cliente</td>
                        <td>Asesor</td>
                        <td>Estado</td>
                        <td>Ver</td>
                    </tr>
                </thead>
				<tfoot>
					<tr>
                        <th class="no-display">Folio</th>
                        <th class="no-display">Fecha</th>
                        <th>Cliente</th>
                        <th>Asesor</th>
                        <th>Estado</th>
                        <th class="no-display">Ver</th>
					</tr>
				</tfoot>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
</div>

<section>
  <div class="row">
<div class="col-md-8">
  <div class="box box-warning">
    <div class="box-header">
      <b>Clientes Registrados</b>
    </div>
    <div class="box-body">
        <table class="table table-bordered" style="font-size:11px;" id="clientes">
          <thead>
          <tr>
            <th>Nombre</th>
            <th>E-mail</th>
            <th>Asesor</th>
            <th>Registrado</th>
          </tr>
          </thead>
          <tbody>
            <?php
            if($clientes):
              foreach($clientes as $c):
              ?>
              <tr>
                <td> <?php echo $c['contacto']; ?> </td>
                <td> <?php echo $c['email']; ?> </td>
                <td> <?php $asesor = $this->estadisticas_model->get_asesor($c['asesor_idasesor']);
                if($asesor){
                  echo $asesor[0]['nombre'];
                }else{
                  echo "Sin asesor";
                } ?> </td>
                <td> <?php echo $c['creado']; ?> </td>
              </tr>
            <?php
          endforeach;
            endif;
           ?>
            <tr>

            </tr>
          </tbody>
        </table>
    </div>
    <div class="box-footer">
    </div>
  </div>
</div>
<div class="col-md-4">
  <div class="box box-warning">
    <div class="box-header">
      <b>Recuperación de Contraseña</b>
    </div>
    <div class="box-body">
      <table class="table table-bordered" style="font-size:11px;">
        <thead>
          <tr>
            <th>Usuario</th>
            <th># Reset</th>
            <th>País</th>
            <th>Estado</th>
            <th>Cot. Prod.</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if($reset):
            foreach($reset as $r): ?>
            <tr>
              <?php $extinf = $this->estadisticas_model->get_info_by_email($r['usr']) ?>
              <th><?php echo $extinf[0]['contacto'] ?></th>
              <th><?php echo $r['reset'] ?></th>
              <th><?php echo $extinf[0]['pais'] ?></th>
              <th><?php echo $extinf[0]['estado'] ?></th>
              <th><?php $cot = $this->estadisticas_model->get_cot_prod($extinf[0]['idcliente']);
              if($cot){
                echo "Si";
              }else{
                echo "No";
              } ?></th>
            </tr>
          <?php
            endforeach;
          endif; ?>
        </tbody>
      </table>
    </div>
    <div class="box-footer">
    </div>
  </div>
</div>
</div>
</section>
<section>
<div class="row">
<div class="col-md-4">
  <div class="box box-warning">
    <div class="box-header">
      <b>Registro de ingresos</b>
    </div>
    <div class="box-box">
      <div class="table table-responsive">
        <table class="table table-bordered" style="font-size:11px;" id="entradas">
          <thead>
            <tr>
              <th>Usuario</th>
              <th>Fecha de entrada</th>
            </tr>
          </thaed>
          <tbody>
            <?php
            if($entradas):
              foreach($entradas as $ent):
               ?>
            <tr>
              <td><?php echo $ent['usr'] ?></td>
              <td><?php echo $ent['fecha'] ?></td>
            </tr>
            <?php
              endforeach;
            endif;
             ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="box-footer">
    </div>
  </div>
</div>


<div class="col-md-8">
  <div class="box box-warning">
    <div class="box-header">
      <b>Asesores Registrados</b>
    </div>
    <div class="box-body">
      <div class="table-responsive">
        <table class="table table-responsive" style="font-size:11px;" id="asesores">
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Datos</th>
              <th>Tipo</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if($asesores):
            foreach($asesores as $as):
               ?>
               <tr>
                 <td> <?php echo $as['nombre'] ?></td>
                 <td> <?php echo $as['datos'] ?></td>
                 <td> <?php echo $as['lv'] ?></td>
               </tr>
             <?php
             endforeach;
           endif; ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="box-footer">
    </div>
  </div>
</div>
</div>
</section>
<script>
	$(document).ready(function() {
		//$('#clientes').DataTable();
	});
</script>
<style>
	.no-display select {
		display: none !important;
	}
</style>

<section>
<div class="estadistica">
  <div class="box box-info">
    <div class="box-header">
      <b>Productos más Cotizados.</b>
    </div>
    <div class="box-body">
      <div class="table-responsive">
        <table class="table table-bordered" style="font-size:11px;">
          <thead>
            <tr>
              <th>Cotizaciones</th>
              <th>Familia</th>
              <th>Código</th>
              <th>Desc. Corta</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if($max_min):
              foreach($max_min as $mm):
               ?>
               <tr>
                 <td> <?php echo $mm['cant'] ?></td>
                 <td> <?php echo (json_decode($mm['json_carrito'])[0]->familia) ?></td>
                 <td> <?php echo $mm['codigo_producto'] ?></td>
                 <td> <?php echo $mm['descorta'] ?></td>
               </tr>
            <?php
          endforeach;
          endif;
          ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="box-footer">
    </div>
  </div>
</div>
</section>

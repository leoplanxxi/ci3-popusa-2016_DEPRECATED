<section>
<div class="estadistica">
  <div class="box box-success">
    <div class="box-header">
      <b>Cotizaciones</b>
    </div>
    <div class="box-body">
      <div class="table table-responsive">
        <table class="table table-bordered" style="font-size:11px;">
          <thead>
            <tr>
              <th>Asesor</th>
              <th>Aceptadas</th>
              <th>Rechazadas</th>
              <th>Negociadas</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if($cotizaciones):
              foreach($cotizaciones as $c):
               ?>
            <tr>
              <td><?php echo $c['nombre']; ?></td>
              <td>
                <b><?php echo count($this->estadisticas_model->getAllCotizacionesAceptadasVendedor($c['idasesor'])); ?></b>
              </td>
              <td>
                <b><?php echo count($this->estadisticas_model->getAllCotizacionesRechazadasVendedor($c['idasesor'])); ?>
                </b>
              </td>
              <td>
                <b>
                <?php echo count($this->estadisticas_model->getAllCotizacionesNegociacionVendedor($c['idasesor'])); ?>
                </b>
              </td>

            </tr>
          <?php
            endforeach;
          endif; ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="box-footer">
    </div>
  </div>
</div>
</section>

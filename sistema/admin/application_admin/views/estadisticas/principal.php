<section>
  <div class="row">

  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-primary"><i class="ion ion-gear-b"></i></span>

      <div class="info-box-content">
        <span class="info-box-text"><b>General</b></span>
        <ul>
          <a href="<?php echo base_url('index.php/estadisticas/mest/general/ordenes'); ?>">
          <li>Ordenes</li>
        </a>
        <a href="<?php echo base_url('index.php/estadisticas/mest/general/cotizaciones'); ?>">
          <li>Cotizaciones</li>
        </a>
        </ul>
        <!--span class="info-box-number">90<small>%</small></span-->
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <div class="col-md-3 col-sm-6 col-xs-12" >
    <a href="<?php echo base_url('index.php/estadisticas/mest/productos'); ?>">
    <div class="info-box">
      <span class="info-box-icon bg-aqua"><i class="ion ion-ios-cart-outline"></i></span>

      <div class="info-box-content">
        <span class="info-box-text"><b>Productos</b></span>
        <!--span class="info-box-number">90<small>%</small></span-->
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
    </a>
  </div>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <a href="<?php echo base_url('index.php/estadisticas/mest/clientes'); ?>">
    <div class="info-box">
      <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

      <div class="info-box-content">
        <span class="info-box-text"><b>Clientes</b></span>
        <!--span class="info-box-number">90<small>%</small></span-->
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
    </a>
  </div>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <a href="<?php echo base_url('index.php/estadisticas/mest/control'); ?>">
    <div class="info-box">
      <span class="info-box-icon bg-green"><i class="ion ion-stats-bars"></i></span>

      <div class="info-box-content">
        <span class="info-box-text"><b>Control</b></span>
        <!--span class="info-box-number">90<small>%</small></span-->
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
    </a>
  </div>

</section>

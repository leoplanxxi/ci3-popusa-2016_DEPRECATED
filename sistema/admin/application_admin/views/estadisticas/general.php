
<section>
<div class="estadistica">
  <div class="box box-success">
    <div class="box-header">
      <b><?php echo strtoupper($tip); ?></b>

      <?php if($general): ?>
      <div class="pull-right">
        <a href="<?php echo base_url('index.php/estadisticas/descargar/'.$tip); ?>" class="btn btn-sm btn-success" target="_blank">
          <i class="fas fa-file-excel"></i>
          Descargar
        </a>
      </div>
    <?php endif; ?>
    </div>
    <div class="box-body">

      <div class="table table-responsive">
        <table class="table table-bordered" style="font-size:11px;">
          <thead>
            <tr>
              <th>Día</th>
              <th>Hora</th>
              <th>Cantidad</th>
              <th>Código</th>
              <th>Descripción</th>
              <?php if($tip == 'ordenes'): ?>
                <th>Precio</th>
                <th>Monto</th>
            <?php endif; ?>
              <th>Estado</th>
              <th>C.P.</th>
              <th>Folio</th>
              <th>Vendedor</th>
              <?php if($tip == 'ordenes'): ?>
            <?php else: ?>
              <th>Status</th>
            <?php endif; ?>
              <th>Cliente</th>
              <th>CURP</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if($general):
              foreach($general as $g):
               ?>
            <tr>
              <td><?php echo explode(' ',$g["fecha"])[0]; ?></td>
              <td><?php echo explode(' ',$g["fecha"])[1]; ?></td>
              <td><?php echo $g["cantidad"]; ?></td>
              <td><?php echo $g["codigo_producto"]; ?></td>
              <td><?php echo $g["desc_producto"]; ?></td>

              <?php if($tip == 'ordenes'): ?>
              <td><?php echo $g["precio"]; ?></td>
              <td><?php echo "$".$g["monto"]; ?></td>

            <?php endif; ?>

              <td><?php echo $g["estado"]; ?></td>
              <td><?php echo $g["codigo_postal"]; ?></td>
              <td><?php echo $g["folio"]; ?></td>

              <td><?php echo $g["vendedor"]; ?></td>
              <?php if($tip == 'ordenes'): ?>
              <?php else: ?>
                <td><?php echo $g["status"]; ?></td>

            <?php endif; ?>

              <td><?php echo $g["cliente"]; ?></td>
              <td><?php echo $g["curp"]; ?></td>
            </tr>
          <?php
            endforeach;
          endif; ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="box-footer">
    </div>
  </div>
</div>
</section>

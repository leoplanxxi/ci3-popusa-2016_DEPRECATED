<script>
function deshabilitarBoton() {
      var enviar = document.getElementById("enviar");

      enviar.value = "Enviando correo, espere...";
      enviar.setAttribute("class", "disabled btn btn-primary col-md-4");

  }
</script>
<style>
	.disabled {
        opacity: 0.9;
        pointer-events: none;
        cursor: default;
        background-color: #9d9d9d !important; 
    }
</style>
<section class="content">
	<div class="row">
	<div class="col-md-6 col-md-offset-3">
	<div class="box box-success">
		<div class="box-header"><b class="box-title">Enviar cotización</b></div>
		<div class="box-body">
			<p>Usted va a enviar la cotización con folio <?php echo $idCotizacion; ?> al correo del cliente.<br> Si desea agregar algún mensaje adicional al cliente, puede escribirlo en el siguiente campo. Este campo es opcional: </p>
			<?php echo form_open(); ?>
				<input type="hidden" name="idCotizacion" value="<?php echo $idCotizacion; ?>" />
				<textarea class="form-control" name="msg_extra" rows=15></textarea>
		</div>
		<div class="box-footer">
			<input type="submit" class="btn btn-primary col-md-2 " value="Enviar" id="enviar" style="float: right"  onclick='deshabilitarBoton();'>
		</div>
			<?php echo form_close(); ?>

		</div>
	</div>
	</div>
</section>
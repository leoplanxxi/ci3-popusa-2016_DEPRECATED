<style>
	.content {
		height: 100%;
	}

	.content iframe {
		width: 100%;
		height: 85%;
	}
</style>

<?php
if ($this->session->userdata("logged_in")["rol_idrol"] == 5) {
	$link = base_url("index.php/ordenes");
}
else if ($this->session->userdata("logged_in")["rol_idrol"] == 3) {
	if ($estado == 2) {
		$link = base_url("index.php/ordenes/admin");
	}
	else {
		$link = base_url("index.php/cotizaciones");
	}
}
else if ($this->session->userdata("logged_in")["rol_idrol"] == 6) {
	$link = base_url("index.php/ordenes");
}
else {
	$link = base_url("index.php/cotizaciones");
}

if ($this->input->get("ref") != NULL) $link = base_url("index.php/" . $this->input->get("ref"));
?>

<section class="content">
<div class="row">
<?php if ($this->session->flashdata("mensaje_positivo") != NULL): ?>
	<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Éxito</h4>
		<?php echo $this->session->flashdata("mensaje_positivo"); ?>
	</div>
<?php endif; ?>
<div class="col-md-6">
	<!--<h4><a href="<?= $link; ?>"><i class="fa fa-backward"></i> Regresar</a> | Folio: <?php echo $idCotizacion; ?> | Caducidad: <?php echo $caducidad; ?></h4>-->
	<h4><!--<a href="<?php echo $link; ?>"><i class="fa fa-backward"></i> Regresar</a> |--> Folio: <?php echo $idCotizacion; ?> | <?php if ($estado == 0 || $estado == 1): ?>Caducidad: <?php echo $caducidad; ?> <?php endif; ?></h4>
</div>
<div class="col-md-6">
<div style="float: right;">
<?php if ($estado == 0 || $estado == 1): ?>
	<?php $asesor_idasesor = $this->db->query("SELECT asesor_idasesor FROM cotizaciones WHERE idcotizaciones = ?", array($idCotizacion))->result_array()[0]["asesor_idasesor"]; ?>
	<?php if ($asesor_idasesor != NULL) : ?>
    <a href="<?php echo HVENTAS; ?>index.php/principal/productos/<?php echo $idCotizacion; ?>" class="btn btn-primary" target="_blank">Recotizar</a> |
	<a href="<?php echo base_url("index.php/cotizaciones/enviarCotizacionCliente/" . $idCotizacion); ?>" class="btn btn-primary">Enviar a cliente</a> |
	<a href="<?php echo base_url("index.php/cotizaciones/aceptarCotizacion/" . $idCotizacion); ?>" class="btn btn-success">Realizar órden <br>de pedido</a> |
	<a href="<?php echo base_url("index.php/cotizaciones/rechazarCotizacion/" . $idCotizacion); ?>" class="btn btn-danger">Rechazar cotiz.</a>
<?php else: ?>
	<small>Debe asignar un vendedor a este cliente para poder ver, enviar o rechazar la cotización</small>
<?php endif; ?>
<?php endif; ?>
<?php if ($this->session->userdata("logged_in")["rol_idrol"] == 5 && $capturada != 1) : ?>
	| <a href="<?php echo base_url("index.php/cotizaciones/capturarCotizacion/" . $idCotizacion); ?>" class="btn btn-default"><i class="fa fa-hand-o-right"></i> Capturar en AdPro</a>
<?php endif; ?>
<?php if (($this->session->userdata("logged_in")["rol_idrol"] == 5 || $this->session->userdata("logged_in")["rol_idrol"] == 4) && $estado == 3) : ?>
	| <a href="<?php echo base_url("index.php/cotizaciones/reiniciarCotizacion/" . $idCotizacion); ?>" class="btn btn-default"><i class="fa fa-repeat" aria-hidden="true"></i> Reiniciar Cotización</a>
<?php elseif ($this->session->userdata("logged_in")["rol_idrol"] == 3 && $estado == 3): ?>
| <a href="<?php echo base_url("index.php/cotizaciones/reiniciarCotizacion/" . $idCotizacion); ?>" class="btn btn-default"><i class="fa fa-repeat" aria-hidden="true"></i> Reiniciar Cotización</a>
<?php endif; ?>
 |
 <?php if($this->session->userdata('logged_in')["rol_idrol"]==6): ?>
	 <a href="<?php echo base_url("index.php/cotizador/pdf_pedido/" . $idCotizacion); ?>" class="btn btn-default" target="_blank"><i class="fa fa-print"></i> Imprimir</a>

 <?php else: ?>
	 <?php $estado_cot = $this->cotizadorCore_model->get_estado_cotizacion($idCotizacion);
	 //var_dump($estado_cot);
	 if($estado_cot[0]['estado']==2):
	  ?>
		<a href="<?php echo base_url("index.php/cotizador/pdf_pedido/" . $idCotizacion); ?>" class="btn btn-default" target="_blank"><i class="fa fa-print"></i> Imprimir</a>

	<?php else: ?>
		<a href="<?php echo base_url("index.php/cotizador/pdf/" . $idCotizacion); ?>" class="btn btn-default" target="_blank"><i class="fa fa-print"></i> Imprimir</a>
	<?php endif; ?>


 <?php endif; ?>

	</div>
</div>
<p>&nbsp;</p>
	<iframe src="<?php echo base_url("index.php/cotizador/out/" . $idCotizacion); ?>" class="embed-responsive-item">
	</iframe>
</section>

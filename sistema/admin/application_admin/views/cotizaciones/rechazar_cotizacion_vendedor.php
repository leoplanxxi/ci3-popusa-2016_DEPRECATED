<section class="content">
	<div class="row">
	<div class="col-md-6 col-md-offset-3">
	<div class="box box-danger">
		<div class="box-header"><b class="box-title">Rechazar cotización</b></div>
		<div class="box-body">
			<p>Usted va a rechazar la cotización con folio <?php echo $idCotizacion; ?>.<br> Especifique la razón por la cual el cliente no aceptó la cotización: </p>
			<?php if (validation_errors()) : ?>
				<div class="alert alert-danger alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="icon fa fa-ban"></i> Error</h4>
					<?php echo validation_errors(); ?>
				</div>
			<?php endif; ?>
			<?php echo form_open(); ?>
				<input type="hidden" name="idCotizacion" value="<?php echo $idCotizacion; ?>" />
				<textarea class="form-control" name="razones" rows=15></textarea>
		</div>
		<div class="box-footer">
			<input type="submit" class="btn btn-danger col-md-2 " value="Enviar" style="float: right">
		</div>
			<?php echo form_close(); ?>

		</div>
	</div>
	</div>
</section>

<?php

if (isset($varAdm)) {
	$url_json = site_url("API/obtenerOrdenesVendedor/" . $idVendedor . "/1");
}
else {
	if ($this->session->userdata("logged_in")["rol_idrol"] == 5){

		$url_json = site_url("API/obtenerOrdenesAdminVenta");
	}else{
		$url_json = site_url("API/obtenerOrdenesVendedor/" . $idVendedor);
	}

}
?>

<script>
	$(document).ready(function(){
		$('.input-daterange input').each(function() {
			$(this).datepicker('clearDates');
		});
		table = $('#tableCotizaciones').DataTable({
			<?php if ($this->session->userdata("logged_in")["rol_idrol"] == 5): ?>
			"order": [[ 3, "asc" ]],
			<?php else: ?>
			"order": [[ 1, "desc" ]],
			<?php endif; ?>
			"ajax": {
            url : "<?php echo $url_json; ?>",
            type : 'GET'
        },
		initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
		});

		$.fn.dataTable.ext.search.push(
			function(settings, data, dataIndex) {
				var min = $('#min-date').val();
				var max = $('#max-date').val();
				var createdAt = data[1] || 0; // Our date column in the table

				if (
				(min == "" || max == "") ||
				(moment(createdAt).isSameOrAfter(min) && moment(createdAt).isSameOrBefore(max))
				) {
				return true;
				}
				return false;
			}
		);

			// Re-draw the table when the a date range filter changes
		$('.date-range-filter').change(function() {
			table.draw();
		});

		$('#my-table_filter').hide();

	});
</script>

<section class="content">

	<div class="col-md-12">
		<div class="nav-tabs-custom">
		<?php if (!isset($vistaClientes)): ?>
			<?php if ($this->session->userdata("logged_in")["rol_idrol"] != 5): ?>
            <ul class="nav nav-tabs" <?php if ($this->session->userdata("logged_in")["rol_idrol"] == 6) echo " style='display: none;'"; ?>>
             <li class=""><a href="<?php
                             if ($this->session->userdata("logged_in")["rol_idrol"] != 4) {
								if (isset($varAdm)) echo base_url("index.php/cotizaciones/nuevasVendedor/" . $idVendedor);
								else echo base_url("index.php/cotizaciones/");
                }
                else {
                    if (isset($var1)) {
						echo base_url("index.php/cotizaciones/");
					} else {
						echo base_url("index.php/cotizaciones/nuevasVendedor/" . $idVendedor);

					}
                }
             ?>"  aria-expanded="true">Nuevas</a></li>
              <li class=""><a href="<?php
                if ($this->session->userdata("logged_in")["rol_idrol"] != 4) {
					if (isset($varAdm)) echo base_url("index.php/cotizaciones/procesoVendedor/" . $idVendedor);
					else echo base_url("index.php/cotizaciones/proceso");
                }
                else {
                    if (isset($var1)){
						echo base_url("index.php/cotizaciones/proceso");
					}
					else {
						echo base_url("index.php/cotizaciones/procesoVendedor/" . $idVendedor);
					}
                }?>" aria-expanded="false">En proceso</a></li>
               <li class="active"><a href="<?php if ($this->session->userdata("logged_in")["rol_idrol"] != 4) {
					if (isset($varAdm)) echo base_url("index.php/ordenes/ordenesVendedor/" . $idVendedor);
					else echo base_url("index.php/ordenes");
                }
                else {
                    if (isset($var1)) {
						echo base_url("index.php/ordenes");
					}
					else {
						echo base_url("index.php/ordenes/ordenesVendedor/" . $idVendedor); //ACA NO SE QUE VA :(
					}
                } ?>" aria-expanded="false">Aceptadas</a></li>
			  <li class=""><a href="<?php if ($this->session->userdata("logged_in")["rol_idrol"] != 4) {
					if (isset($varAdm)) echo base_url("index.php/cotizaciones/rechazadasVendedor/" . $idVendedor);
					else echo base_url("index.php/cotizaciones/rechazadas");
                }
                else {
                    if (isset($var1)) {
						echo base_url("index.php/cotizaciones/rechazadas");
					}
					else {
	                    echo base_url("index.php/cotizaciones/rechazadasVendedor/" . $idVendedor);
					}
                } ?>" aria-expanded="false">Rechazadas</a></li>
            </ul>
			<?php endif; ?>
		</div>

			<?php else: ?>
			<ul class="nav nav-tabs">
             <li class=""><a href="<?php echo base_url("index.php/clientes/cotizacionesNuevas/" . $idCliente); ?>"  aria-expanded="true">Nuevas</a></li>
              <li class=""><a href="<?php echo base_url("index.php/clientes/cotizacionesProceso/" . $idCliente); ?>" aria-expanded="false">En proceso</a></li>
              <li class="active"><a href="<?php echo base_url("index.php/ordenes/ordenesCliente/" . $idCliente); ?>" aria-expanded="false">Aceptadas</a></li>
              <li class=""><a href="<?php echo base_url("index.php/clientes/cotizacionesRechazadas/" . $idCliente); ?>" aria-expanded="false">Rechazadas</a></li>
            </ul>
		</div>
		<?php endif; ?>
		<div class="tab-content">
                <div class="tab-pane active" id="tab_1">
				<?php if (isset($varAdm)) {
                        $nombre_vendedor = $this->db->query("SELECT nombre FROM asesor WHERE login_idlogin = ?", array($idVendedor))->result_array()[0]["nombre"];
                        echo "<h2 class='text-center'>" . $nombre_vendedor . "</h2>";
                    }  ?>
					<div class="row">
						<div class="col-md-4 pull-right">
						Buscar por rango de fechas:
							<div class="input-group input-daterange">

							<input type="text" id="min-date" class="form-control date-range-filter" data-date-format="yyyy-mm-dd" placeholder="De:">

							<div class="input-group-addon">a</div>

							<input type="text" id="max-date" class="form-control date-range-filter" data-date-format="yyyy-mm-dd" placeholder="A:">

							</div>
						</div>
					</div>
          	<div class="table-responsive">
					<table class="table table-hover" id="tableCotizaciones">
						<thead>
							<tr>
								<th>Folio</th>
								<th>Fecha</th>
								<th>Cliente</th>
								<th>Capturada</th>
								<th>Ver</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th class="no-display">Folio</th>
								<th class="no-display">Fecha</th>
								<th>Cliente</th>
								<th>Capturada en AdPro</th>
								<th class="no-display">Ver</th>
							</tr>
						</tfoot>
					</table>
				</div>
              </div>

</section>

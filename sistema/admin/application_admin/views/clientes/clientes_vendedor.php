<script>
	$(document).ready(function() {
		$('#tablaClientes').DataTable({
            "ajax": {
            url : "<?php echo site_url("API/obtenerClientesVendedor/" . $idVendedor); ?>",
            type : 'GET'
        },
				"order": [[ 0, "asc" ]]
        });
	});
</script>

<section class="content">
<div class="row">
<div class="col-md-12">
    <div class="box box-primary">
        <div class="box-body">
            <h4>Clientes</h4>
            <p>Usted tiene los siguientes clientes asignados: </p>

            <table id="tablaClientes" class="display responsive nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td>Razón Social</td>
                        <td>RFC</td>
                        <td>Datos</td>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
</div>
</div>
<?php
/*<a href="<?php echo base_url("index.php/clientes/cotizacionesNuevas/" . $cliente["idcliente"]); ?>" title="Ver cotizaciones"><?php if (!isset($var1)) :?><br><i class="fa fa-cart-plus" aria-hidden="true"></i> Ver cotizaciones</a><br><?php endif; ?> </td>*/
?>

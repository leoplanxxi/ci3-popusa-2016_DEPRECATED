<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script>
$(document).ready(function() {
    $('#clientes').select2();
    $('#asesores').select2();
});
</script>

<style>
.select2-container .select2-selection--single
{
    height: 35px;
}
</style>


<section class="content">
<div class="row">
<div class="col-md-8 col-md-offset-2">
    <div class="box box-primary">
        <div class="box-body">
            <h4>Reasignar vendedor</h4>

            <?php if (validation_errors()): ?>
            <div class="alert alert-danger">
                <?php echo validation_errors(); ?>
            </div>
            <?php endif; ?> 
            <?php if ($this->session->flashdata("mensaje")) : ?>
            <div class="alert alert-success">
                <?php echo $this->session->flashdata("mensaje"); ?>
            </div>
            <?php endif; ?>
            
            <?php echo form_open(); ?>
            <p>Seleccione un cliente al que desee reasignar su vendedor: </p>

            <?php
            $all_clientes = $this->db->query("SELECT * FROM cliente")->result_array(); 
            ?>

            <select name="clientes" id="clientes" class="form-control">

            <?php foreach ($all_clientes as $cliente): ?>
                <option value="<?php echo $cliente["idcliente"]; ?>"><?php echo $cliente["contacto"]; ?> (<?php echo $cliente["email"]; ?>)</option>
            <?php endforeach; ?>
           </select>
            <p>&nbsp;</p>
            <p>Seleccione el nuevo vendedor: </p>

            <?php
            $all_asesores = $this->db->query("SELECT login.activo, login.rol_idrol, asesor.datos, asesor.nombre, asesor.idasesor FROM login, asesor WHERE activo = 1 AND asesor.login_idlogin = login.idlogin AND (rol_idrol = 1 OR rol_idrol = 2 or rol_idrol = 4)")->result_array();
            ?>

            <select name="asesores" id="asesores" class="form-control">

            <?php foreach ($all_asesores as $asesor): ?>
                <option value="<?php echo $asesor["idasesor"]; ?>"><?php echo $asesor["nombre"]; ?></option>
            <?php endforeach; ?>
            </select>
            <p>&nbsp;</p>
            <input type="submit" value="Reasignar vendedor" class="btn btn-success">
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
</div>
</div>
</section>
<script>
	$(document).ready(function() {
		$('#tablaClientes').DataTable({
            "ajax": {
            url : "<?php echo site_url("API/obtenerNuevosClientesTM/"); ?>",
            type : 'GET'
        },
        });
	});
</script>

<section class="content">
<div class="row">
<div class="col-md-12">
    <div class="box box-primary">
        <div class="box-body">
            <h4>Clientes Nuevos</h4>
            <p>Los siguentes clientes se han registrado pero no tienen un vendedor asignado: </p>
            
            <table id="tablaClientes" class="display responsive nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td>Razón</td>
                        <td>RFC</td>
                        <td>Asignar</td>
                    </tr>
                </thead>

            </table>
        </div>
    </div>
</div>
</div>
</div>
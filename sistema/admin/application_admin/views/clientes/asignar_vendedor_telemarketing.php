<section class="content">
	<div class="row">
		<div class="box box-primary">
			<div class="box-header">
				<h4>Asignar vendedor a cliente</h4>
			</div>
			<div class="box-body">
				<?php echo form_open(); ?>
				<div class="form-group">
					<input type="hidden" name="idcliente" value="<?php echo $idcliente ?>" >
					<label for="asesor_idasesor">Vendedor: </label>
					<select class="form-control" name="asesor_idasesor" id="asesor_idasesor">
					<?php foreach ($asesores as $asesor): ?>
						<option value="<?php echo $asesor["idasesor"]; ?>"><?php 
							$activo = $this->db->query("SELECT login.nombre FROM login, asesor WHERE asesor.login_idlogin = login.idlogin AND asesor.idasesor = ?", array($asesor["idasesor"]))->result_array()[0]["nombre"];
								
							echo $activo; 
					?> - <?php echo $asesor["nombre"]; ?> </option>
					<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="box-footer">
				<button type="submit" class="btn btn-primary pull-right">
					Asignar
				</button>
				<?php echo form_close(); ?>
			</div>
		</div>
		
	</div>
</section>
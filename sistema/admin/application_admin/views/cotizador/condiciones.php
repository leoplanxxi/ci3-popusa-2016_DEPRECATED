<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<script>
$(document).ready(function() {
	$("select#condiciones").val('<?php echo $cotizacion["condiciones"]; ?>'); 
});
</script>
<?php echo form_open(); ?>
<input type="hidden" name="idcotizacion" value="<?php echo $idcotizacion; ?>" />
<div class="container">
<div class="col-6 offset-3">
	<div class="card">
		<div class="card-block">
			<h4 class="card-title">Editar condiciones</h4>
			<div class="card-text">
			
				<p>Especifique las condiciones de pago de la presente cotización: </p>
				<select name="condiciones" id="condiciones" class="form-control">
					<option value='Contado'>Contado</option>
					<option value='Dentro de los 5 dias'>Dentro de los 5 dias</option>
					<option value='Dentro de los 7 dias'>Dentro de los 7 dias</option>
					<option value='Dentro de los 8 dias'>Dentro de los 8 dias</option>
					<option value='Anticipado'>Anticipado</option>
					<option value='120 dias'>120 dias</option>
					<option value='Dentro de los 15 dias'>Dentro de los 15 dias</option>
					<option value='180 dias'>180 dias</option>
					<option value='Dentro de los 21 dias'>Dentro de los 21 dias</option>
					<option value='Pago autorizado con cheque'>Pago autorizado con cheque</option>
					<option value='25% anticipo y resto al embarcar'>25% anticipo y resto al embarcar</option>
					<option value='Dentro de los 30 dias'>Dentro de los 30 dias</option>
					<option value='35% anticipo y resto al embarcar'>35% anticipo y resto al embarcar</option>
					<option value='Dentro de los 45 dias'>Dentro de los 45 dias</option>
					<option value='50% anticipo y 50% al embarcar'>50% anticipo y 50% al embarcar</option>
					<option value='Dentro de los 60 dias'>Dentro de los 60 dias</option>
					<option value='75% anticipo y resto al embarcar'>75% anticipo y resto al embarcar</option>
					<option value='Dentro de  los 90 dias'>Dentro de  los 90 dias</option>
					<option value='Dentro de los 250 dias'>Dentro de los 250 dias</option>

				</select>
			<br>
			<input type="submit" name="enviar" class="btn btn-primary" value="Guardar">
			<a href="<?php echo base_url("index.php/cotizador/out/" . $idcotizacion); ?>" class="btn btn-default" style="float: right;">Cancelar</a>
			</div>
		</div>
	</div>
</div>
</div>
<?php echo form_close(); ?>
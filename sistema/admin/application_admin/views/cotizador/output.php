<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js" integrity="sha256-obZACiHd7gkOk9iIL/pimWMTJ4W/pBsKu+oZnSeBIek=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<script>
function formatNumber (num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}
</script>

<style>
	.container {
		max-width: 1260px;
	}

	@media print, screen {

		.col-alianza {
			background: #008000 !important;
			color: white;
			font-weight: bold;
            font-size: 15px;
            padding: 0.3rem !important;
		}

		.col-paket {
			background: #f00 !important;
			color: white;
			font-weight: bold;
            font-size: 15px;
            padding: 0.3rem !important;
		}
	}

</style>
<?php //$nombre_cliente = $cliente["contacto"]; ?>
<?php 
	$datos_cliente = json_decode($datos_cliente, TRUE);

$nombre_cliente = $datos_cliente["nombre"]; ?>
<script>
var tipo_cotizacion = "";
var articulos_alianza = 0;
var articulos_paket = 0;

var Cotizador = {};
 $(window).on('load', function() {
	var linksPDF = "<?php echo str_replace('admin/','/',base_url());?>public/pdfs/";
	var linkCotizador = "<?php echo base_url(); ?>index.php/cotizador/index/<?php echo $file; ?>";
	var linkCotizadorIn = "<?php echo base_url(); ?>index.php/cotizador/in/<?php echo $file; ?>";

	var linkFamilias = "<?php echo base_url(); ?>index.php/cotizador/obtenerFamiliasAlianza";
	var linkSubFamilias = "<?php echo base_url(); ?>index.php/cotizador/obtenerSubFamiliasPaket";

	Cotizador.familias_array = ""; //Para Alianza
    Cotizador.subfamilias_array = ""; //Para Paket


	$.getJSON(linkFamilias, function (data) {
		Cotizador.familias_array = data;

	});

    $.getJSON(linkSubFamilias, function (data) {
		Cotizador.subfamilias_array = data;
	});

	$.getJSON(linkCotizadorIn, function(data){

	});

	var ok = $.getJSON(linkCotizador, function (data) {

		subtotal = "<p class='text-right'>$MN " + formatNumber(data.subtotal) + "</p>";
		iva = "<p class='text-right'>$MN " + formatNumber(data.iva) + "</p>";
		total = "<p class='text-right'>$MN " + formatNumber(data.total) + "</p>";

		$(subtotal).appendTo("#subtotal");
		$(iva).appendTo("#iva");
		$(total).appendTo("#total");

        var count_m = _.countBy(data, function(d) { return d.marca; });

        if (count_m.Alianza != undefined) {
            articulos_alianza = count_m.Alianza;
        }

        if (count_m.Paket != undefined) {
            articulos_paket = count_m.Paket;
        }

        if (articulos_alianza > articulos_paket) {
            tipo_cotizacion = "Alianza";
        }
        else {
            tipo_cotizacion = "Paket";
        }

        if (tipo_cotizacion == "Alianza") {
            var groupd = _.groupBy(data, function(d) { return d.familia; });

            Object.keys(groupd).forEach(function(key) {
                familia = key;

                familia_array_eliminar = Cotizador.familias_array.indexOf(familia);
                if (familia_array_eliminar > -1) {
                    Cotizador.familias_array.splice(familia_array_eliminar,1);
                }

                marca = groupd[key][0]["marca"];

                var tb_class;
                if (marca == "Alianza") {
                    tb_class = "col-alianza";
                }
                else if (marca == "Paket") {
                    tb_class  = "col-paket";
                }
                if (familia != "undefined") {
                    familia_id = familia.replace(/\s/g, "-");

                    append = "<tbody id='familia-" + familia_id + "'><tr><td colspan=5 class='" + tb_class + "'>· " + familia + "</tr></td>";
                    $(append).appendTo("#productos");
                    var productos = groupd[key];
                    var productos_array = $.map(productos, function(el) { return el });
                    productos_array.forEach(function(item){

                        if (item["codigo"]) {
                            codigo = item["codigo"];
                            unidad = item["unidad"];
                            cantidad = item["cantidad"];
                            descripcion = item["descripcion"];
                            precio_neto = item["precio_unitario_siva"];
                            precio_con_iva = item["precio_con_iva"];
                            pdf = item["pdf"];
                            entrega = item["entrega"];
                            desespecial = item["descripcion_especial"];
                            precio_neto = precio_neto.toFixed(2);
                            precio_con_iva = precio_con_iva.toFixed(2);

							if (typeof item["parametrosExtra"] === 'undefined') {
								extra = "";
								extraP = "";
							}
							else {
								extra = "<br>";
								extraP = "<br>";

								if (typeof item["parametrosExtra"]["costura"] === 'undefined') {
								} else {
									if (item["parametrosExtra"]["costura"] == "SI") {
										extra = extra + "<hr><b>Costura:</b><br>" + item["parametrosExtra"]["codigo_costura"] + " - " + item["parametrosExtra"]["descripcion_costura"] + "<br>";
										extra = extra + "Ancho: " + item["parametrosExtra"]["ancho"] + "<br>";
										extra = extra + "Largo: " + item["parametrosExtra"]["largo"] + "<br>";
										extra = extra + "No. de uniones entre mallas: " + item["parametrosExtra"]["uniones"];

										precioCostura = item["parametrosExtra"]["precio_costura"];
										precioCostura = precioCostura.toFixed(2);
										extraP = extraP + "<b>Precio Costura:</b><br>$MN " + formatNumber(precioCostura);
									}
								}
								if (typeof item["parametrosExtra"]["ojillos"] === 'undefined') {
								} else {
									if (item["parametrosExtra"]["ojillos"] == "SI") {
										extra = extra + "<hr><b>Ojillos:</b><br>" + item["parametrosExtra"]["codigo_ojillos"] + " - " + item["parametrosExtra"]["descripcion_ojillos"] + "<br>";
										extra = extra + "No. de ojillos: " + item["parametrosExtra"]["numeroOjillos"];

										precioOjillos = item["parametrosExtra"]["precio_ojillos"];
										precioOjillos = precioOjillos.toFixed(2);
										extraP = extraP + "<br><b>Precio Ojillos:</b><br>$MN " + formatNumber(precioOjillos);
									}
								}

								if (typeof item["parametrosExtra"]["confeccion"] === 'undefined') {
								} else {
									if (item["parametrosExtra"]["confeccion"] == "SI") {
										extra = extra + "<hr><b>Confección:</b><br>" + item["parametrosExtra"]["codigo_confeccion"] + " - " + item["parametrosExtra"]["descripcion_confeccion"]+ "<br>";
										extra = extra + item["parametrosExtra"]["confeccion_m2"] + " metros cuadrados de confección.";
/*
										precioConfeccion = item["parametrosExtra"]["precio_ojillos"];
										precioOjillos = precioOjillos.toFixed(2);
										extraP = extraP + "<br><b>Precio Ojillos:</b><br>$MN " + formatNumber(precioOjillos);*/
									}
								}
							}
							/*if(!typeof item["parametrosExtra"] === 'undefined') {
								alert("existe");
								extra = "<br>";

								if (!typeof item["parametrosExtra"]["costura"] === 'undefined') {
									if (item["parametrosExtra"]["costura"] == "SI") {
										extra = extra + "<b>Costura:</b><br>" + item["parametrosExtra"]["codigo_costura"] + " - " + item["parametrosExtra"]["descripcion_costura"] + "<br>";
									}
								}
							}*/


                            append = "<tr><td>" + cantidad + "</td><td>" + unidad + "</td>"
                            + "<td><a href='" + linksPDF + pdf +"' target='_blank'>(" + codigo + ") " + descripcion + "</a>"
                                + "<br>" + desespecial
                                + "<br><b>Tiempo de entrega: </b>" + entrega + extra
                                +"</td>"
                            +"<td style='text-align: right;'> $MN " + formatNumber(precio_neto) + " <big><?php if ($cotizacion["estado"] == 0 || $cotizacion["estado"] == 1): ?><a href='<?php echo base_url("index.php/cotizador/cambiarPrecio/" . $file . "/"); ?>" + codigo + "/'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a><?php endif; ?></big><br>"
                                +"<b>IVA: </b>MN$" + item["valor_iva"].toFixed(2) + extraP
                                +"</td>"
                            +"<td  style='text-align: right;'> $MN " + formatNumber(precio_con_iva) + "</td></tr>";
                            $(append).appendTo("#familia-" + familia_id);
                        }

                    });
                }

            });

            Cotizador.familias_array.forEach(function(item) {
                tb_class = "col-alianza";

                if (item == "PAKET") {
                    tb_class = "col-paket";
                }
                append = "<tbody id='familia-" + item + "'><tr><td colspan=5 class='" + tb_class + "'> · " + item + "</tr></td>";
                //$(append).appendTo("#productos");
            });
        }
        else {
            var groupd = _.groupBy(data, function(d) { return d.subfamilia; });

            Object.keys(groupd).forEach(function(key) {
                familia = key;

                familia_array_eliminar = Cotizador.subfamilias_array.indexOf(familia);
                if (familia_array_eliminar > -1) {
                    Cotizador.subfamilias_array.splice(familia_array_eliminar,1);
                }

                marca = groupd[key][0]["marca"];

                var tb_class;
                if (marca == "Alianza") {
                    tb_class = "col-alianza";
                }
                else if (marca == "Paket") {
                    tb_class  = "col-paket";
                }
                if (familia != "undefined") {
                    familia_id = familia.replace(/\s/g, "-");

                    append = "<tbody id='familia-" + familia_id + "'><tr><td colspan=5 class='" + tb_class + "'>· " + familia + "</tr></td>";
                    $(append).appendTo("#productos");
                    var productos = groupd[key];
                    var productos_array = $.map(productos, function(el) { return el });
                    productos_array.forEach(function(item){

                        if (item["codigo"]) {
                            codigo = item["codigo"];
                            unidad = item["unidad"];
                            cantidad = item["cantidad"];
                            descripcion = item["descripcion"];
                            precio_neto = item["precio_neto"];
							//alert(precio_neto);
//                            precio_neto = item["precio_unitario_siva"];
                            precio_con_iva = item["precio_con_iva"];
                            pdf = item["pdf"];
                            entrega = item["entrega"];
                            desespecial = item["descripcion_especial"];

                            precio_neto = precio_neto.toFixed(2);
                            precio_con_iva = precio_con_iva.toFixed(2);

						//	alert(precio_neto);
							if (typeof item["parametrosExtra"] === 'undefined') {
									extra = "";
									extraP = "";
								}
								else {
									extra = "<br>";
									extraP = "<br>";

									if (typeof item["parametrosExtra"]["costura"] === 'undefined') {
									} else {
										if (item["parametrosExtra"]["costura"] == "SI") {
											extra = extra + "<hr><b>Costura:</b><br>" + item["parametrosExtra"]["codigo_costura"] + " - " + item["parametrosExtra"]["descripcion_costura"] + "<br>";
											extra = extra + "Ancho: " + item["parametrosExtra"]["ancho"] + "<br>";
											extra = extra + "Largo: " + item["parametrosExtra"]["largo"] + "<br>";
											extra = extra + "No. de uniones entre mallas: " + item["parametrosExtra"]["uniones"];

											precioCostura = item["parametrosExtra"]["precio_costura"];
											precioCostura = precioCostura.toFixed(2);
											extraP = extraP + "<b>Precio Costura:</b><br>$MN " + formatNumber(precioCostura);
										}
									}
									if (typeof item["parametrosExtra"]["ojillos"] === 'undefined') {
									} else {
										if (item["parametrosExtra"]["ojillos"] == "SI") {
											extra = extra + "<hr><b>Ojillos:</b><br>" + item["parametrosExtra"]["codigo_ojillos"] + " - " + item["parametrosExtra"]["descripcion_ojillos"] + "<br>";
											extra = extra + "No. de ojillos: " + item["parametrosExtra"]["numeroOjillos"];

											precioOjillos = item["parametrosExtra"]["precio_ojillos"];
											precioOjillos = precioOjillos.toFixed(2);
											extraP = extraP + "<br><b>Precio Ojillos:</b><br>$MN " + formatNumber(precioOjillos);
										}
									}

									if (typeof item["parametrosExtra"]["confeccion"] === 'undefined') {
									} else {
										if (item["parametrosExtra"]["confeccion"] == "SI") {
											extra = extra + "<hr><b>Confección:</b><br>" + item["parametrosExtra"]["codigo_confeccion"] + " - " + item["parametrosExtra"]["descripcion_confeccion"]+ "<br>";
											extra = extra + item["parametrosExtra"]["confeccion_m2"] + " metros cuadrados de confección.";

											
	/*
											precioConfeccion = item["parametrosExtra"]["precio_ojillos"];
											precioOjillos = precioOjillos.toFixed(2);
											extraP = extraP + "<br><b>Precio Ojillos:</b><br>$MN " + formatNumber(precioOjillos);*/
										}
									}
								}
						
                            append = "<tr><td>" + cantidad + "</td><td>" + unidad + "</td>"
                            + "<td><a href='" + linksPDF + pdf +"' target='_blank'>(" + codigo + ") " + descripcion + "</a>"
                                + "<br>" + desespecial
                                + "<br><b>Tiempo de entrega: </b>" + entrega + extra
                                +"</td>"
                            +"<td style='text-align: right;'> $MN " + formatNumber(precio_neto) + " <big><?php if ($cotizacion["estado"] == 0 || $cotizacion["estado"] == 1): ?> <a href='<?php echo base_url("index.php/cotizador/cambiarPrecio/" . $file . "/"); ?>" + codigo + "/'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a><?php endif; ?></big><br>"
                                +"<b>IVA: </b>MN$" + item["valor_iva"].toFixed(2) + extraP
                                +"</td>"
                            +"<td  style='text-align: right;'> $MN " + formatNumber(precio_con_iva) + "</td></tr>";
                            $(append).appendTo("#familia-" + familia_id);
                            if (marca == "Alianza") {
                                alianza_array_eliminar = Cotizador.subfamilias_array.indexOf("Alianza");

                                Cotizador.subfamilias_array.splice(alianza_array_eliminar,1);
                            }
                        }

                    });
                }

            });

            Cotizador.subfamilias_array.forEach(function(item) {
                tb_class = "col-paket";

                if (item == "ALIANZA") {
                    tb_class = "col-alianza";
                }
                append = "<tbody id='familia-" + item + "'><tr><td colspan=5 class='" + tb_class + "'> · " + item + "</tr></td>";
                //$(append).appendTo("#productos");
            });
        }


  });
	console.log(ok);
 });
</script>

<style>
.row-datos {
	border: 3px double green;
}

.row-productos table thead {
	background: black;
    color: white;
}


</style>

<div class="container">
	<div class="row">
		<div class="col-3 col-fecha">
			<?php echo $cotizacion["fecha"]; ?>
		</div>
		<div class="col-9">
			<h3 class="text-center"><?php
				if ($cotizacion["estado"] != 2) {
					echo "COTIZACION";
				}
				 else {

					echo "ORDEN DE PEDIDO (" . $cotizacion["folio02"] . ")";
				 }
			?></h3>
		</div>
	</div>
	<br>
	<div class="row row-datos">

		<div class="col-12">
			<div class="row row-datos-top">
				<div class="col-6">
					<b>Razón Social:</b> <?php if ($datos_cliente["razon"]) {
						echo $datos_cliente["razon"]; }
						else {
							echo $datos_cliente["nombre"];
						} ?>
				</div>
				<div class="col-6">
					<b>Tel(s):</b> <?php echo $datos_cliente["tels"]; ?>
				</div>
			</div>
			<div class="row row-datos-middle">
				<div class="col-6">
					<b>RFC:</b> <?php echo $datos_cliente["rfc"]; ?>
				</div>
				<div class="col-6">
					<b>E-mail:</b> <?php echo $datos_cliente["email"]; ?>
				</div>
			</div>
			<div class="row row-datos-bottom">
				<div class="col-12">
					<b>Dirección del cliente:</b> <?php echo $datos_cliente["direccion"] ; ?>
					<br>
					<b>Dirección de entrega: </b> <?php echo $cotizacion["dir_entrega"]; ?> <?php if ($cotizacion["capturada_admin_venta"] != 1): ?><a href='<?php echo base_url("index.php/cotizador/cambiarDirEntrega/" . $file . "/"); ?>'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a><?php endif; ?>
				</div>

			</div>
		</div>
	</div>
	<br>
	<?php if ($cotizacion["estado"] != 2): ?>
	<div class="row row-saludo">
		<div class="col-10 offset-1">
			<small><?php
			$mensaje_cliente = str_replace("nombre_cliente", $nombre_cliente, $mensaje_cliente);
			echo $mensaje_cliente; ?></small>

		</div>
	</div>
	<?php endif; ?>
	<br>
	<div class="row row-productos">
		<table class="table" id="productos">
			<thead>
				<tr>
					<th>Cant.</th>
					<th>Unidad</th>
					<th style="width: 57%;">Descripción</th>
					<th>Precio Unitario</th>
					<th>Importe</th>
				</tr>
			</thead>
<!--			<tbody id="productos">
			</tbody> -->
		</table>
	</div>
	<div class="row row-calculos">
		<div class="col-5 offset-7" style="padding-right: 0">
			<table class="table" style="padding-right: 0; font-weight: bold;">
				<tr>
					<td>Subtotal: </td>
					<td id="subtotal"></td>
				</tr>
				<tr>
					<td>IVA: </td>
					<td id="iva"></td>
				</tr>
				<tr>
					<td>Total: </td>
					<td id="total"></td>
				</tr>
			</table>
		</div>
	</div>
	<br>
	<div class="row row-precios-a-cambio">
		<div class="col-12">
			<small class="text-center" style="display: block;">LOS PRECIOS ESTÁN SUJETOS A CAMBIOS SIN PREVIO AVISO POR VARIACIONES EN LOS COSTOS DE NUESTRAS MATERIAS PRIMAS</small>

		</div>
	</div>
	<p>&nbsp;</p>
	<div class="row row-bancos">
	</div>

	<div class="row row-extra">
		<div class="col-8">
			<table class="table">
				<tr>
					<td style="width: 25%"><b>Condiciones: </b></td>
					<td style="width: 75%"><?php echo $cotizacion["condiciones"]; ?> <?php if ($cotizacion["capturada_admin_venta"] != 1): ?><a href='<?php echo base_url("index.php/cotizador/cambiarCondiciones/" . $file . "/"); ?>'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a><?php endif; ?></td>
				</tr>
				<tr>
					<td style="width: 25%"><b>L.A.B.: </b></td>
					<td style="width: 75%"><?php echo $cotizacion["lab"]; ?></td>
				</tr>
				<tr>
					<td style="width: 25%"><b>Observaciones: </b></td>
					<td style="width: 75%"><?php echo $cotizacion["observaciones"]; ?> <?php if ($cotizacion["capturada_admin_venta"] != 1): ?><a href='<?php echo base_url("index.php/cotizador/cambiarObservaciones/" . $file . "/"); ?>'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a><?php endif; ?></td>
				</tr>
			</table>

		</div>
		<div class="col-4">
			<small class="text-center" style="display: block;">ACEPTAMOS TARJETAS DE CRÉDITO</small><br>
			<div class="row">
				<div class="col-6">
					<img src="<?php echo base_url(); ?>/public/img_admin/visa.jpg" class="img img-fluid">
				</div>
				<div class="col-6">
					<img src="<?php echo base_url(); ?>/public/img_admin/master_card.jpg" class="img img-fluid">
				</div>
			</div>
		</div>
		</div>
		<p>&nbsp;</p>
		<div class="row row-asesor">
		<div class="col-8">
			<table class="table">
				<tr>
					<td style="width: 25%"><b>Asesor: </b></td>
					<td style="width: 75%"><?php echo $asesor; ?><?php // echo $asesor["nombre"]; ?><br><?php //echo $asesor["datos"]; ?></td>
				</tr>
			</table>

		</div>
		<div class="col-4">
			<table class="table table-sm" style="font-size: 11px;">
				<?php foreach ($bancos as $banco): ?>
				<tr>
					<td><strong>Banco</strong></td>
					<td><?php echo $banco["banco"]; ?></td>
				</tr>
				<tr>
					<td><strong>No. Cuenta</strong></td>
					<td><?php echo $banco["cuenta"]; ?></td>
				</tr>
								<tr>
					<td><strong>Clabe</strong></td>
					<td><?php echo $banco["clabe"]; ?></td>
				</tr>
				<tr style="line-height: 0px;">
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<?php endforeach; ?>
				<tr>
					<td><strong>A nombre de</strong></td>
					<td><?php echo $bancos[0]["nombre"]; ?></td>
				</tr>
			</table>
		</div>
	</div>

</div>

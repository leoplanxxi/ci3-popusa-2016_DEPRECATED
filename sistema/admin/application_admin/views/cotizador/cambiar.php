<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<?php echo form_open(); ?>
<input type="hidden" name="idcotizacion" value="<?php echo $idcotizacion; ?>" />
<input type="hidden" name="producto" value="<?php echo $producto; ?>" />
<?php if ($parametrosExtra["confeccion"] == "SI"): ?>
<input type="hidden" name="producto_aux" value="<?php echo $producto_aux; ?>" />
<?php endif; ?>
<script>
$(document).ready(function() {
	$("select#precio_alternativo").val('<?php echo $p_entrada; ?>');
});
</script>
<div class="container">
<div class="col-6 offset-3">
	<div class="card">
		<div class="card-block">
			<h4 class="card-title">Cambiar precio</h4>
			<div class="card-text">
			<p><strong><?php echo $descripcion; ?></strong></p>
			<p>El precio que está seleccionado actualmente para el producto es: <br>
			<?php if ($p_entrada == 3) $p_entrada = "L"; ?>
			P<?php echo $p_entrada . " ($" . $precio_calculado . ")"; ?></p>

			<p>Si desea cambiar el precio, seleccione uno: </p>
			<select name="precio_alternativo" id="precio_alternativo" class="form-control">
			 <?php
			 foreach ($precios_alternativos as $precio) {
				 if ($precio["valido"] == 1) {
					 if ($precio["p"] == 3) $precio["p_m"] = "L"; 
					 else $precio["p_m"]  = $precio["p"];
					 echo "<option value='" . $precio["p"] ."'>P" . $precio["p_m"] . " ($" . $precio["precio"] . ")</option>";
				 }
			 }
			 /*foreach ($precios_alternativos as $key => $value) {
				 echo "<option value='" . $key , "'>P" . $key . " ($" . $value . ")</option>";
			 } */?>
			</select>
			<br>
			<input type="submit" name="enviar" class="btn btn-primary" value="Guardar">
			<a href="<?php echo base_url("index.php/cotizador/out/" . $idcotizacion); ?>" class="btn btn-default" style="float: right;">Cancelar</a>
			</div>
		</div>
	</div>
</div>
</div>
<?php echo form_close(); ?>

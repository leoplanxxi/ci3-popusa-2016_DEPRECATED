<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<?php echo form_open(); ?>
<input type="hidden" name="idcotizacion" value="<?php echo $idcotizacion; ?>" />
<div class="container">
<div class="col-6 offset-3">
	<div class="card">
		<div class="card-block">
			<h4 class="card-title">Editar observaciones</h4>
			<div class="card-text">
			
				<p>Especifique las observaciones de la presente cotización: </p>
				<textarea name="observaciones" class="form-control" rows=5><?php echo $cotizacion["observaciones"]; ?></textarea>
			<br>
			<input type="submit" name="enviar" class="btn btn-primary" value="Guardar">
			<a href="<?php echo base_url("index.php/cotizador/out/" . $idcotizacion); ?>" class="btn btn-default" style="float: right;">Cancelar</a>
			</div>
		</div>
	</div>
</div>
</div>
<?php echo form_close(); ?>
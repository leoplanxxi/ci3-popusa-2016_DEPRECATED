<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.6/footable.min.js" integrity="sha256-R3NffQkOJUqmiutQHnxEURXUXZru/7GMdM6CdH673Qw=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.6/footable.bootstrap.min.css" integrity="sha256-HJmq7ZsJyNO0AZz/dTSiU984iDC5BSvR5It5SnGTyRg=" crossorigin="anonymous" />

<div class="container">
	<div class="col-12">
		<h4 class="text-center">Cotizaciones</h4>
		
     <!--   <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" href="#nuevas" role="tab" data-toggle="tab">Nuevas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#proceso" role="tab" data-toggle="tab">En proceso</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#aceptadas" role="tab" data-toggle="tab">Aceptadas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#rechazadas" role="tab" data-toggle="tab">Rechazadas</a>
            </li>
        </ul> 

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="nuevas"> -->
                <table class="table table-responsive"> 
                <thead>
                    <tr>
                        <th>Folio</th>
                        <th>Fecha</th>
                        <th>Cliente</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($cotizaciones_nuevas as $cotizacion): ?>
                    <tr>
                        <td><?php echo $cotizacion["idcotizaciones"]; ?></td>
                        <td><?php echo $cotizacion["fecha"]; ?></td>
                        <td>Alvaro Osvaldo Lopez-Garcia</td>
                        <td><a href="<?php echo base_url("index.php/cotizador/out/" . $cotizacion["idcotizaciones"]); ?>"><i class="fa fa-file-text-o" aria-hidden="true"></i> Ver Cotización</a></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <!--    </div>
            <div role="tabpanel" class="tab-pane fade" id="proceso">.p..</div>
            <div role="tabpanel" class="tab-pane fade" id="aceptadas">..a.</div>
            <div role="tabpanel" class="tab-pane fade" id="rechazadas">..r.</div>
        </div> -->
		
	</div>
</div>
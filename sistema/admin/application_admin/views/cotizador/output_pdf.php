<?php
tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "PDF Report";
$obj_pdf->SetTitle($title);
$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();
?>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js" integrity="sha256-obZACiHd7gkOk9iIL/pimWMTJ4W/pBsKu+oZnSeBIek=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<style>
	.container {
		max-width: 1260px;
	}
	
	@media print, screen {

		.col-alianza {
			background: #008000 !important;
			color: white;
			font-weight: bold;
            font-size: 15px;
            padding: 0.3rem !important;
		}
	
		.col-paket {
			background: #f00 !important;
			color: white;
			font-weight: bold;
            font-size: 15px;
            padding: 0.3rem !important;
		}
	}

</style>
<script>
var tipo_cotizacion = "";
var articulos_alianza = 0;
var articulos_paket = 0;

var Cotizador = {};
 $(window).on('load', function() {
	var linksPDF = "http://132.148.18.129/sistema-popusa/public/pdfs/";
	var linkCotizador = "<?php echo base_url(); ?>index.php/cotizador/index/<?php echo $file; ?>";
	var linkFamilias = "<?php echo base_url(); ?>index.php/cotizador/obtenerFamiliasAlianza";
	var linkSubFamilias = "<?php echo base_url(); ?>index.php/cotizador/obtenerSubFamiliasPaket";

	Cotizador.familias_array = ""; //Para Alianza
    Cotizador.subfamilias_array = ""; //Para Paket


	$.getJSON(linkFamilias, function (data) {
		Cotizador.familias_array = data;
	});
	
    $.getJSON(linkSubFamilias, function (data) {
		Cotizador.subfamilias_array = data;
	});

	$.getJSON(linkCotizador, function (data) {
		subtotal = "<p class='text-right'>$MN " + data.subtotal + "</p>";
		iva = "<p class='text-right'>$MN " + data.iva + "</p>";
		total = "<p class='text-right'>$MN " + data.total + "</p>";
		
		$(subtotal).appendTo("#subtotal");
		$(iva).appendTo("#iva");
		$(total).appendTo("#total");
		
        var count_m = _.countBy(data, function(d) { return d.marca; });

        if (count_m.Alianza != undefined) {
            articulos_alianza = count_m.Alianza;
        }

        if (count_m.Paket != undefined) {
            articulos_paket = count_m.Paket;
        }

        if (articulos_alianza > articulos_paket) {
            tipo_cotizacion = "Alianza";
        }
        else {
            tipo_cotizacion = "Paket";
        }

        if (tipo_cotizacion == "Alianza") {
            var groupd = _.groupBy(data, function(d) { return d.familia; });

            Object.keys(groupd).forEach(function(key) {
                familia = key;
                
                familia_array_eliminar = Cotizador.familias_array.indexOf(familia);
                if (familia_array_eliminar > -1) {
                    Cotizador.familias_array.splice(familia_array_eliminar,1);
                }
                
                marca = groupd[key][0]["marca"];
                
                var tb_class;
                if (marca == "Alianza") {
                    tb_class = "col-alianza";
                }
                else if (marca == "Paket") {
                    tb_class  = "col-paket";
                }
                if (familia != "undefined") {
                    familia_id = familia.replace(/\s/g, "-");

                    append = "<tbody id='familia-" + familia_id + "'><tr><td colspan=5 class='" + tb_class + "'>· " + familia + "</tr></td>";
                    $(append).appendTo("#productos");
                    var productos = groupd[key];
                    var productos_array = $.map(productos, function(el) { return el });
                    productos_array.forEach(function(item){
                        
                        if (item["codigo"]) {
                            codigo = item["codigo"];
                            unidad = item["unidad"];
                            cantidad = item["cantidad"];
                            descripcion = item["descripcion"];
                            precio_neto = item["precio_unitario_siva"];
                            precio_con_iva = item["precio_con_iva"];
                            pdf = item["pdf"];
                            entrega = item["entrega"];
                            desespecial = item["descripcion_especial"];
                            
                            precio_neto = precio_neto.toFixed(2);				
                            precio_con_iva = precio_con_iva.toFixed(2);				
                            append = "<tr><td>" + cantidad + "</td><td>" + unidad + "</td>"
                            + "<td><a href='" + linksPDF + pdf +"' target='_blank'>(" + codigo + ") " + descripcion + "</a>"
                                + "<br>" + desespecial
                                + "<br><b>Tiempo de entrega: </b>" + entrega 
                                +"</td>"
                            +"<td style='text-align: right;'> $MN " + precio_neto + " <big><a href='<?php echo base_url("index.php/cotizador/cambiarPrecio/" . $file . "/"); ?>" + codigo + "/'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a></big><br>"
                                +"<b>IVA: </b>MN$" + item["valor_iva"].toFixed(2)
                                +"</td>"
                            +"<td  style='text-align: right;'> $MN " + precio_con_iva + "</td></tr>";
                            $(append).appendTo("#familia-" + familia_id);
                        }
                        
                    });
                }

            });
            
            Cotizador.familias_array.forEach(function(item) {
                tb_class = "col-alianza";
                
                if (item == "PAKET") {
                    tb_class = "col-paket";
                }
                append = "<tbody id='familia-" + item + "'><tr><td colspan=5 class='" + tb_class + "'> · " + item + "</tr></td>";
                $(append).appendTo("#productos");
            });
        }
        else {
            var groupd = _.groupBy(data, function(d) { return d.subfamilia; });

            Object.keys(groupd).forEach(function(key) {
                familia = key;
                
                familia_array_eliminar = Cotizador.subfamilias_array.indexOf(familia);
                if (familia_array_eliminar > -1) {
                    Cotizador.subfamilias_array.splice(familia_array_eliminar,1);
                }
                
                marca = groupd[key][0]["marca"];
                
                var tb_class;
                if (marca == "Alianza") {
                    tb_class = "col-alianza";
                }
                else if (marca == "Paket") {
                    tb_class  = "col-paket";
                }
                if (familia != "undefined") {
                    familia_id = familia.replace(/\s/g, "-");

                    append = "<tbody id='familia-" + familia_id + "'><tr><td colspan=5 class='" + tb_class + "'>· " + familia + "</tr></td>";
                    $(append).appendTo("#productos");
                    var productos = groupd[key];
                    var productos_array = $.map(productos, function(el) { return el });
                    productos_array.forEach(function(item){
                        
                        if (item["codigo"]) {
                            codigo = item["codigo"];
                            unidad = item["unidad"];
                            cantidad = item["cantidad"];
                            descripcion = item["descripcion"];
                            precio_neto = item["precio_unitario_siva"];
                            precio_con_iva = item["precio_con_iva"];
                            pdf = item["pdf"];
                            entrega = item["entrega"];
                            desespecial = item["descripcion_especial"];
                            
                            precio_neto = precio_neto.toFixed(2);				
                            precio_con_iva = precio_con_iva.toFixed(2);				
                            append = "<tr><td>" + cantidad + "</td><td>" + unidad + "</td>"
                            + "<td><a href='" + linksPDF + pdf +"' target='_blank'>(" + codigo + ") " + descripcion + "</a>"
                                + "<br>" + desespecial
                                + "<br><b>Tiempo de entrega: </b>" + entrega 
                                +"</td>"
                            +"<td style='text-align: right;'> $MN " + precio_neto + " <big><a href='<?php echo base_url("index.php/cotizador/cambiarPrecio/" . $file . "/"); ?>" + codigo + "/'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a></big><br>"
                                +"<b>IVA: </b>MN$" + item["valor_iva"].toFixed(2)
                                +"</td>"
                            +"<td  style='text-align: right;'> $MN " + precio_con_iva + "</td></tr>";
                            $(append).appendTo("#familia-" + familia_id);
                            if (marca == "Alianza") {
                                alianza_array_eliminar = Cotizador.subfamilias_array.indexOf("Alianza");

                                Cotizador.subfamilias_array.splice(alianza_array_eliminar,1);
                            }
                        }
                        
                    });
                }

            });
            
            Cotizador.subfamilias_array.forEach(function(item) {
                tb_class = "col-paket";
                
                if (item == "ALIANZA") {
                    tb_class = "col-alianza";
                }
                append = "<tbody id='familia-" + item + "'><tr><td colspan=5 class='" + tb_class + "'> · " + item + "</tr></td>";
                $(append).appendTo("#productos");
            });
        }

            
  });
 });
</script>

<style>
.row-datos {
	border: 3px double green;
}

.row-productos table thead {
	background: black;
    color: white;
}


</style>

<div class="container">
	<div class="row">
		<div class="col-3 col-fecha">
			<?php echo $cotizacion["fecha"]; ?>
		</div>
		<div class="col-9">
			<h3 class="text-center">COTIZACIÓN</h3>
		</div>
	</div>
	<br>
	<div class="row row-datos">
		<div class="col-12">
			<div class="row row-datos-top">
				<div class="col-6">
					<b>Contacto:</b> <?php echo $cliente["contacto"] ?>
				</div>
				<div class="col-6">
					<b>Tel(s):</b> <?php echo $cliente["tels"]; ?>
				</div>
			</div>
			<div class="row row-datos-middle">
				<div class="col-6">
					<b>RFC:</b> <?php echo $cliente["rfc"]; ?>
				</div>
				<div class="col-6">
					<b>E-mail:</b> <?php echo $cliente["email"]; ?>
				</div>
			</div>
			<div class="row row-datos-bottom">
				<div class="col-12">
					<b>Dirección:</b> <?php echo $cliente["direccion"]; ?>
				</div>
				
			</div>
		</div>
	</div>
	<br>
	<div class="row row-saludo">
		<div class="col-10 offset-1">
			<small>Estimada, ALVARO OSVALDO LOPEZ-GARCIA, agradecemos de antemano la oportunidad que nos brinda y me permito enviarle, para su amable consideración, la cotización correspondiente a los productos de su interés.</small>

		</div>
	</div>
	<br>
	<div class="row row-productos">
		<table class="table" id="productos">
			<thead>
				<tr>
					<th>Cant.</th>
					<th>Unidad</th>
					<th style="width: 57%;">Descripción</th>
					<th>Precio Unitario</th>
					<th>Importe</th>
				</tr>
			</thead>
<!--			<tbody id="productos">
			</tbody> -->
		</table>
	</div>
	<div class="row row-calculos">
		<div class="col-5 offset-7" style="padding-right: 0">
			<table class="table" style="padding-right: 0; font-weight: bold;">
				<tr>
					<td>Subtotal: </td>
					<td id="subtotal"></td>
				</tr>
				<tr>
					<td>IVA: </td>
					<td id="iva"></td>
				</tr>
				<tr>
					<td>Total: </td>
					<td id="total"></td>
				</tr>
			</table>
		</div>		
	</div>
	<br>
	<div class="row row-precios-a-cambio">
		<div class="col-12">
			<small class="text-center" style="display: block;">LOS PRECIOS ESTÁN SUJETOS A CAMBIOS SIN PREVIO AVISO POR VARIACIONES EN LOS COSTOS DE NUESTRAS MATERIAS PRIMAS</small>

		</div>
	</div>
	<p>&nbsp;</p>
	<div class="row row-bancos">
	</div>
	
	<div class="row row-extra">
		<div class="col-8">
			<table class="table">
				<tr>
					<td style="width: 25%"><b>Condiciones: </b></td>
					<td style="width: 75%">Contado</td>
				</tr>
				<tr>
					<td style="width: 25%"><b>L.A.B.: </b></td>
					<td style="width: 75%">Puebla, Puebla</td>
				</tr>
				<tr>
					<td style="width: 25%"><b>Observaciones: </b></td>
					<td style="width: 75%">Contado</td>
				</tr>
			</table>
			
		</div>
		<div class="col-4">
			<small class="text-center" style="display: block;">ACEPTAMOS TARJETAS DE CRÉDITO</small><br>
			<div class="row">
				<div class="col-6">
					<img src="<?php echo base_url(); ?>/public/img_admin/visa.jpg" class="img img-fluid">
				</div>
				<div class="col-6">
					<img src="<?php echo base_url(); ?>/public/img_admin/master_card.jpg" class="img img-fluid">
				</div>			
			</div>
		</div>
		</div>
		<p>&nbsp;</p>
		<div class="row row-asesor">
		<div class="col-8">
			<table class="table">
				<tr>	
					<td style="width: 25%"><b>Asesor: </b></td>
					<td style="width: 75%">Ing. Roberto Anzures<br>División Agrícola<br>Tel. 222 240 99 99 / E-mail: paket003@popusa.com.mx</td>
				</tr>
			</table>

		</div>
		<div class="col-4">
			<table class="table table-sm" style="font-size: 11px;">
				<?php foreach ($bancos as $banco): ?>
				<tr>
					<td><strong>Banco</strong></td>
					<td><?php echo $banco["banco"]; ?></td>
				</tr>
				<tr>
					<td><strong>No. Cuenta</strong></td>
					<td><?php echo $banco["cuenta"]; ?></td>
				</tr>
								<tr>
					<td><strong>Clabe</strong></td>
					<td><?php echo $banco["clabe"]; ?></td>
				</tr>
				<tr style="line-height: 0px;">
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<?php endforeach; ?>
				<tr>
					<td><strong>A nombre de</strong></td>
					<td><?php echo $bancos[0]["nombre"]; ?></td>
				</tr>
			</table>
		</div>
	</div>
	
</div>
<?php
    // we can have any view part here like HTML, PHP etc
    $content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->Output('output.pdf', 'I');

?>
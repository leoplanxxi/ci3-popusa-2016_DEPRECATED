
<div class="container-fluid box">
  <div class="row">
    <div  style="width:30%"></div>
    <div  style="width:30%;">
    </div>
		<div  class="orden_footer">
      <table class="table table-bordered producto">
        <thead>
          <tr>
            <td style="background-color: #d3d3d342;"><b>SUBTOTAL: </b></td>
            <td><p class='text-right'><?php echo money_format("%nMN",$linkCotizador->subtotal); ?></p></td>
          </tr>
          <tr>
            <td style="background-color: #d3d3d342;"><b>IVA: </b></td>
            <td><p class='text-right'><?php echo money_format("%nMN",$linkCotizador->iva); ?></p></td>
          </tr>
          <tr>
            <td style="background-color: #d3d3d342;"><b>TOTAL: </b></td>
            <td><p class='text-right'><?php echo money_format("%nMN",$linkCotizador->total	); ?></p></td>
          </tr>
          <tr>
            <td style="height:20px;"></td>
          </tr>
          <tr>
          <td>
            <b>Condiciones:</b>
          </td>
          <td><?php echo $condiciones; ?></td>
          </tr>
          <tr>
          <td>
            <b>L.A.B.:</b>
          </td>
          <td><?php echo $lab; ?></td>

          </tr>
          <tr>
          <td>
            <b>Observaciones:</b>
            <td><?php echo $obervaciones ?></td>

          </td>
          </tr>
          <tr>
          <td>
            <b>Asesor:</b>
          </td>
          <td><?php echo $asesor2; ?></td>

          </tr>
        </thead>
      </table>
    </div>

    <div class="col-md-12">
      <table class="table table-bordered" >
        <tr>
          <td align="center" style="border:0px"><img src="<?php echo base_url('public/cotizador/1calidad_alianza.jpg') ?>" style="width:150px;" /></td>
          <td align="center" style="font-size:10px;">NOMBRE Y FIRMA DEL CLIENTE</td>
        </tr>
        <tr>
          <td align="center" style="font-size:10px; padding:5px;">
            Sinónimo de un producto de alta calidad que protegerá y aumentará la producción de tu cultivo.
          </td>
          <td align="justify" style="font-size:8px; padding:5px;">
            Los productos con Tasa IVA 0% son destinados para invernaderos hidropónicos, para producir temperaturas y humedad de forma controlada o/y para proteger a los cultivos de elementos naturales, de acuerdo al Art. 2-A Fracc I, inciso "g" de la ley del impuesto al valor agregado.
          </td>
        </tr>
      </table>
    </div>

    <div class="col-md-12" style="font-size:12px;" align="center">
      <strong>DATOS DE DEPOSITO BANCARIO A NOMBRE DE <?php echo $bancos[0]["nombre"]; ?> </strong><br>
      <table class="table" style="font-size:10px;" >
        <tr>
        <?php foreach ($bancos as $banco): ?>
          <td align="center">
          <strong><?php echo $banco["banco"]; ?></strong> &#160; &#160; No. Cuenta: <?php echo $banco["cuenta"]; ?><br>
          <strong>Clabe: </strong><?php echo $banco["clabe"]; ?>
          </td>
        <?php endforeach; ?>

        </tr>
      </table>


    </div>
  </div>
</div>

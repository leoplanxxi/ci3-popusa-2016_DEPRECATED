<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css">
<style>
	.col-alianza {
		background: #008000 !important;
		color: white;
		font-weight: bold;
					font-size: 10px;
	}

	.col-paket {
		background: #f00 !important;
		color: white;
		font-weight: bold;
					font-size: 10px;
	}
	.producto{
		font-size: 10px;
		text-align: center;

	}
</style>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<tr>
						<td style="background-color:green; width:60px; color:white; padding:8px;" align="center">

							<b><?php echo date('d/m/y',strtotime($cotizacion['fecha'])); ?></b></td>
						<td align="center" style="color:green;"><h3><b>COTIZACIÓN</b></h3></td>
						<td style="width:60px;"></td>
					</tr>
				</thead>
			</table>
			<table class="table" style="border: 1 solid #008000; text-align:center; font-size:12px;">
				<?php $datos_cliente = ((array)json_decode($cotizacion['info_cliente'])); ?>
				<tbody>
					<tr>
						<td style="padding:5px; color:green;"><b>Contacto: </b></td>
						<td style="padding:5px; color:green;"><?php
						echo $datos_cliente['nombre'];

						// if($cliente["contacto"]){
						// 	echo $cliente["contacto"];
						// }else{
						// 	echo $cliente["nombre"];
						// }
						 ?></td>
						<td style="padding:5px; color:green;"><b>Tel(s): </b></td>
						<td style="padding:5px; color:green;"><?php echo $datos_cliente["tels"]; ?></td>
					</tr>
					<tr>
						<td style="padding:5px; color:green;"><b>RFC: </b></td>
						<td style="padding:5px; color:green;"><?php echo $datos_cliente["rfc"];?></td>
						<td style="padding:5px; color:green;"><b>E-mail: </b></td>
						<td style="padding:5px; color:green;"><?php echo $datos_cliente["email"]; ?></td>
					</tr>
					<tr>
						<td style="padding:5px; color:green;"><b>Dirección: </b></td>
						<td style="padding:5px; color:green;"><?php
							echo $datos_cliente['direccion'];
						// if($cliente['calle']){
	          //   echo "Calle ".$cliente['calle'].", Ext. ".$cliente['exterior'].", Int. ".$cliente['interior'].", Col. ".$cliente['colonia'].", C.P. ".$cliente['codigo_postal'].", CD. ".$cliente['ciudad'].", Estado ".$cliente['estado'].", País ".$cliente['pais'];
	          // }
						 ?></td>
						<td style="padding:5px; color:green;"></td>
						<td style="padding:5px; color:green;"></td>
					</tr>
				</tbody>
			</table>
			<div class="col-md-12">
				<small style="font-size:10px;" align="justify">Estimad@, <?php
				echo $datos_cliente['nombre'];
				
				//  if($cliente["contacto"]){
				// 	echo $cliente["contacto"];
				// }else{
				// 	echo $cliente["nombre"];
				// }
				 ?>, agradecemos de antemano la oportunidad que nos brinda y me permito enviarle, para su amable consideración, la cotización correspondiente a los productos de su interés.</small>
			</div>
			<table class="table" style="margin-top:10px;">
				<thead>
					<tr>
						<th style="background-color:black;color:white;" class="producto">Cant.</th>
						<th style="background-color:black;color:white;" class="producto">Unidad</th>
						<th style="width: 57%;background-color:black;color:white;" class="producto">Descripción</th>
						<th style="background-color:black;color:white;" class="producto">Precio Unitario</th>
						<th style="background-color:black;color:white;" class="producto">Importe</th>
					</tr>
				</thead>
				<?php
				$productos = null;
				foreach($linkCotizador as $lc){
									$productos[] = $lc;
				}
				?>
				<?php foreach($linkFamilias as $fam):?>
					<tbody>
						<tr>
							<td colspan="5" <?php if($fam != "PAKET"){echo "style='background-color:#008000 !important;' class='col-alianza'" ;}else{ echo "class='col-paket' style='background: #f00 !important'";} ?>> - <?php echo $fam; ?></td>
						</tr>
						<?php foreach($productos as $prod): ?>

							<?php if($prod->familia == $fam): ?>
						<tr>
							<td class="producto"><?php echo $prod->cantidad;?></td>
							<td class="producto"><?php echo $prod->unidad;?></td>
							<td class="producto">
								<a href="<?php echo str_replace('/admin','',base_url('public/pdfs/'.$prod->pdf));?>" target="_blank">
									<?php echo "(".$prod->codigo.") ".$prod->descripcion ?>

								</a>
								<br>
								<?php
								if(isset($prod->parametrosExtra)){

									$p =  (array)$prod->parametrosExtra;
								}else{
									$p = null;
								}

								if(isset($p['costura'])){
									$costura = $p['costura'];
								}else{
									$costura = null;
								}
								if(isset($p['ojillos'])){
									$ojillos = $p['ojillos'];
								}else{
									$ojillos = null;
								}

								if($costura=='SI'){
									echo "<br><b>COSTURA:</b><br>";
									echo $p['codigo_costura']." - ".$p['descripcion_costura']."<br>";
									echo 'Ancho: '.$p['ancho'].'<br>';
									echo 'Largo: '.$p['ancho'].'<br>';
									echo "No. de uniones entre mallas: ".$p['uniones']."<br>";
								}

								if($ojillos=='SI'){
									echo "<br><b>Ojillos:</b><br>";
									echo $p['codigo_ojillos']." - ".$p['descripcion_ojillos']."<br>";
									echo "No. de ojillos: ".$p['numeroOjillos']."<br>";
								}

								?>
								<br>
								<!-- insertar valores aquí  -->
								<br>
								<b>Tiempo de entrega: </b><?php echo $prod->entrega;?>

							</td>
							<td class="producto" style="text-align: right;">
								<?php echo money_format("%nMN",$prod->precio_unitario_siva);?>
								<br>
								<b>IVA: </b><?php echo money_format("%nMN",$prod->valor_iva);?>
								</td>
								<td class="producto" style="text-align: right;"> <?php echo money_format("%nMN",$prod->precio_con_iva);?></td>
							</tr>
						<?php endif;?>
						<?php endforeach;?>
					</tbody>
				<?php endforeach;?>
			</table>
			<table class="table">
				<thead>
					<tr>
						<td class="producto" style="background-color:#CCFFE5; color:#008000;">LOS PRECIOS ESTÁN SUJETOS A CAMBIOS SIN PREVIO AVISO POR VARIACIONES EN LOS COSTOS DE NUESTRAS MATERIAS PRIMAS</td>
						<td class="producto" style="color:#008000;">
							ACEPTAMOS TARJETAS DE CRÉDITO

							<table class="table" style="margin-top:10px;">
								<thead>
									<tr>
										<td>
											<img src="<?php echo base_url(); ?>/public/img_admin/visa.jpg" style="width:100px">
										</td>
										<td>
											<img src="<?php echo base_url(); ?>/public/img_admin/master_card.jpg" style="width:100px">

										</td>
									</tr>
								</thaed>
							</table>
						</td>
						<td>
						<table class="table">
							<thead>
								<tr>
									<td class="producto" style="color:#008000;"><b>Subtotal: </b></td>
									<td class="producto" style="color:#008000;"><p class='text-right'><?php echo money_format("%nMN",$linkCotizador->subtotal); ?></p></td>
								</tr>
								<tr>
									<td class="producto" style="color:#008000;"><b>IVA: </b></td>
									<td class="producto" style="color:#008000;"><p class='text-right'><?php echo money_format("%nMN",$linkCotizador->iva); ?></p></td>
								</tr>
								<tr>
									<td class="producto" style="color:#008000;"><b>Total: </b></td>
									<td class="producto" style="color:#008000;"><p class='text-right'><?php echo money_format("%nMN",$linkCotizador->total	); ?></p></td>
								</tr>
							</thead>
						</table>
						</td>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css">
<style>
	.col-alianza {
		background: #008000 !important;
		color: white;
		font-weight: bold;
					font-size: 10px;
	}

	.col-paket {
		background: #f00 !important;
		color: white;
		font-weight: bold;
					font-size: 10px;
	}
	.producto{
		font-size: 10px;
		text-align: center;
    margin-bottom: 0px;

	}
  .orden_encabezado{
    width:50%;
    float:right;
  }
  .orden_footer{
    width:40%;
    float:right;
  }
  .generales{
    font-size: 10px;
    padding-top: 0px;
    margin-bottom: 0px;

  }
  .gtd{
    width: 50%;
    padding: 5px;
  }
  .gtdf{
    width: 100%;
    padding: 5px;
  }
  .cabezera{
    background-color: #d3d3d342;
  }
</style>
<div class="container-fluid">

	<div class="row">
    <div  style="width:50%"></div>
		<div  class="orden_encabezado">
			<table class="table table-bordered producto">
        <thead >
          <tr>
            <td><b>ELABORADO POR</b></td>
            <td><b>FECHA</b></td>
            <td><b>ORDEN DE PEDIDO</b></td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td style="padding:5px;"><?php echo $asesor['nombre'] ?></td>
            <td >
              <table class="table">
                <tbody>
                  <tr>
                    <td>DIA<br><?php echo date('d',strtotime($cotizacion['fecha'])); ?></td>
                    <td>MES<br><?php echo date('m',strtotime($cotizacion['fecha'])); ?></td>
                    <td>AÑO<br><?php echo date('Y',strtotime($cotizacion['fecha'])); ?></td>
                  </tr>
                </tbody>
              </table>
            </td>
            <td><b>No. <?php echo $file; ?></b></td>
          </tr>
        </tbody>
      </table>
    </div>
    <div  style="width:100%">
			<?php $datos_cliente = ((array)json_decode($cotizacion['info_cliente'])); ?>
      <table class="table table-bordered generales">
        <tr>
          <td class="gtd">NOMBRE<br><?php
					echo $datos_cliente['nombre'];
					// var_dump($datos_cliente);
          // if(isset($cliente['nombre'])){
          //   echo $cliente['nombre'];
          // }else{
          //   echo $cliente['contacto'];
          // }
           ?></td>
          <td class="gtd">TEL<br><?php
          echo $datos_cliente['tels'];
           ?></td>
        </tr>
        <tr>
          <td class="gtd">CORREO ELECTRONICO<br><?php echo $datos_cliente['email'] ?></td>
          <td class="gtd">RFC<br><?php echo $datos_cliente['rfc'] ?></td>
        </tr>
      </table>
        <table class="table table-bordered generales">
        <tr>
          <td class="gtdf">DOMICILIO<br>
            <?php
						echo $datos_cliente['direccion'];
            // if($cliente['calle']){
            //   echo "Calle ".$cliente['calle'].", Ext. ".$cliente['exterior'].", Int. ".$cliente['interior'].", Col. ".$cliente['colonia'].", C.P. ".$cliente['codigo_postal'].", CD. ".$cliente['ciudad'].", Estado ".$cliente['estado'].", País ".$cliente['pais'];
            // }

             ?>
          </td>
        </tr>
        <tr><td class="gtdf">DIRECCION DE ENVIO DE LA MERCANCIA Y/O EMPRESA DE TRANSPORTES DE MERCANCIAS<br>
          <?php
          /*if($cliente['calle']){
            echo "Calle ".$cliente['calle'].", Ext. ".$cliente['exterior'].", Int. ".$cliente['interior'].", Col. ".$cliente['colonia'].", C.P. ".$cliente['codigo_postal'].", CD. ".$cliente['ciudad'].", Estado ".$cliente['estado'].", País ".$cliente['pais'];
          }*/
					echo $dir_entrega;
           ?>
        </td></tr>
				<tr><td class="gtdf">OBSERVACIONES<br><?php echo $observaciones; ?></td></tr>
        <tr><td class="gtdf">FECHA DE ENTREGA DE LA MERCANCIA</td></tr>
      </table>
      <table class="table table-bordered producto">
        <thead >
          <tr >
            <td style="background-color: #d3d3d342;"><b>CODIGO</b></td>
            <td style="background-color: #d3d3d342;"><b>DESCRIPCION</b></td>
            <td style="background-color: #d3d3d342;"><b>CANT</b></td>
            <td style="background-color: #d3d3d342;"><b>UNIDAD<br>MED</b></td>
            <td style="background-color: #d3d3d342;"><b>PRECIO UNIT<br>SIN IVA</b></td>
            <td style="background-color: #d3d3d342;"><b>INPORTE NETO</b></td>
          </tr>
        </thead>
        <?php
				//$productos = null;
				foreach($linkCotizador as $lc){
									$productos[] = $lc;
				}

				?>
        <tbody>

          <?php foreach($productos as $prod):
            if($prod->precio_con_iva!=0):
            ?>
          <tr>
            <td style="padding:5px;"><?php echo $prod->codigo;?></td>
            <td style="padding:5px; width:40%;"><?php echo $prod->descripcion;
						//insertar código aquí
						echo "<br>";
						$p =  (array)$prod->parametrosExtra;

						if($p['costura']=='SI'){
							echo "<br><b>COSTURA:</b><br>";
							echo $p['codigo_costura']." - ".$p['descripcion_costura']."<br>";
							echo 'Ancho: '.$p['ancho'].'<br>';
							echo 'Largo: '.$p['ancho'].'<br>';
							echo "No. de uniones entre mallas: ".$p['uniones']."<br>";
						}
						if($p['ojillos']=='SI'){
							echo "<br><b>Ojillos:</b><br>";
							echo $p['codigo_ojillos']." - ".$p['descripcion_ojillos']."<br>";
							echo "No. de ojillos: ".$p['numeroOjillos']."<br>";
						}
						//var_dump($p);
						?></td>
            <td style="padding:5px;"><?php echo $prod->cantidad; ?></td>
            <td style="padding:5px;"><?php echo $prod->unidad; ?></td>
            <td style="padding:5px;"><?php echo money_format("%nMN",$prod->precio_unitario_siva);  ?></td>
            <td style="padding:5px;"><?php echo money_format("%nMN",$prod->precio_con_iva);?></td>
          </tr>
        <?php
      endif;
      endforeach; ?>
        </tbody>
      </table>
    </div>
    <br>



  </div>

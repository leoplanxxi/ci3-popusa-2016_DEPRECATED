
<div class="container-fluid box">
  <div class="row">
    <div class="col-md-12">
      <table class="table" style="font-size:10px; position:relative;" >
        <thead>
          <tr>
            <td>
              <b>Condiciones:</b>
            </td>
            <td align="center">
              <br>
            &#160;<?php  echo $cotizacion['condiciones'];?>&#160;
            </td>
          </tr>

          <tr>
            <td>
              <b>L.A.B:</b>
            </td>
            <td align="center">
              <br>
              &#160;<?php  echo $cotizacion['lab'];?>&#160;
            </td>
          </tr>

          <tr>
            <td>
              <b>Observaciones:</b>
            </td>
            <td align="center">
              <br>
              &#160;<?php  echo $cotizacion['observaciones'];?>&#160;
            </td>
          </tr>

          <tr>
            <td>
              <b>Asesor: </b>
            </td>
            <td align="center">
              &#160;<?php echo $asesor["nombre"]; ?>&#160;<br><?php echo $asesor["datos"]; ?></td>
            <td>
              <table class="table">
                <thead>
                  <?php foreach ($bancos as $banco): ?>
                  <tr>
                    <td><strong>Banco</strong></td>
                    <td><?php echo $banco["banco"]; ?></td>
                  </tr>
                  <tr>
                    <td><strong>No. Cuenta</strong></td>
                    <td><?php echo $banco["cuenta"]; ?></td>
                  </tr>
                    <tr>
                    <td><strong>Clabe</strong></td>
                    <td><?php echo $banco["clabe"]; ?></td>
                  </tr>

                  <?php endforeach; ?>
                  <tr>
                    <td><strong>A nombre de</strong></td>
                    <td><?php echo $bancos[0]["nombre"]; ?></td>
                  </tr>
                </thead>
              </table>
            </td>
          </tr>
      </thead>
      </table>
      <img src="<?php echo base_url('public/cotizador/flecha alianza.jpg');?>" style="position:absolute;">

    </div>
  </div>
</div>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="/sistema-popusa/admin/favicon.ico" type="image/ico">
  <title>AdminLTE 2 | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
<!-- Bootstrap 3.3.6 -->
  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.11/js/app.min.js"></script>

  <script src="https://cdn.datatables.net/v/bs/dt-1.10.13/fc-3.2.2/r-2.1.1/datatables.min.js"></script>

  <link rel="stylesheet" href="<?php echo base_url("public/styles.css"); ?>" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.11/css/AdminLTE.min.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.11/css/skins/skin-blue.min.css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css" />
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/v/bs/dt-1.10.13/fc-3.2.2/r-2.1.1/datatables.min.css">
  <!-- Font Awesome -->


  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>Popusa</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Inicie sesión para continuar</p>
    <?php if ($this->session->flashdata("msg") != NULL) : ?>
    <div class="alert alert-danger">
        <?php echo $this->session->flashdata("msg"); ?>
    </div>
    <?php endif; ?>
    <?php if (validation_errors() != "") : ?>
    <div class="alert alert-warning">
        <?php echo validation_errors(); ?>
    </div>
    <?php endif; ?>
    <?php echo form_open() ?>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Usuario" name="nombre">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="passwd" id="contrasena">
        <span class="input-group-btn">
        <button class="btn btn-default pull-right" type="button" onclick="viewpass()"><span class="fa fa-eye"></span></button>
      </span>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-12">
        Resuelva la siguiente suma:</div>
      </div>
      <div class="row">
        <div class="col-xs-4" style="padding-top: 7px; text-align: right;"><?php echo $num[0] ?> + <?php echo $num[1] ?> = </div>
        <input type="hidden" name="a" value="<?php echo $num[0]; ?>">
        <input type="hidden" name="b" value="<?php echo $num[1]; ?>">
        <div class="col-xs-8"><input type="text" class="form-control" name="c"></div>
        </div>
        <p>&nbsp;</p>
      <div class="row">
        <div class="col-xs-8">

        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Iniciar</button>
        </div>
        <!-- /.col -->
      </div>
    <?php echo form_close(); ?>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->


<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });

  function viewpass(){
  if($('#contrasena').attr('type') == "password"){
    $('#contrasena').attr('type','text');
  }else{
    $('#contrasena').attr('type','password');

  }
}
</script>
</body>
</html>

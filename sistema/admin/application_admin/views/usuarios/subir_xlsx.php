<style>
  table {
    font-size: 13px;
  }
</style>
<section class="content">
  <div class="row">
  <div class="col-md-10 col-md-offset-1">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Subir archivo de usuarios</h3>
      </div>
      <div class="box-body table-responsive">
        <?php if (validation_errors()): ?>
          <div class="alert alert-danger">
            <?php echo validation_errors(); ?>
          </div>
        <?php endif; ?>
        <?php if ($this->session->flashdata("msg")) : ?>
          <div class="alert alert-danger">
            <?php echo $this->session->flashdata("msg"); ?>
          </div>
        <?php endif; ?>
        <p>Desde esta interfaz usted podrá subir un archivo en formato Excel (*.xls, *.xlsx) o LibreOffice Calc (*.ods) el cual contiene la información sobre los usuarios que utilizarán el sistema.</p>

        <p>Éste archivo deberá tener la siguiente estructura:</p>

        <table class="table table-bordered table-responsive">
          <thead>
            <tr>
              <th>Rol</th>
              <th>No. de identificación</th>
              <th>Nombre de Usuario</th>
              <th>Password</th>
              <th>Nombre</th>
              <th>Teléfono</th>
              <th>Correo electrónico</th>
              <th>Precios</th>
              <th>Imagen en correo</th>
              <th>Lista viajera</th>
            </tr>
          </thead>
		  <tbody>
			<tr>
				<td>VENDEDOR</td>
				<td>VEN1</td>
				<td>vendedor1</td>
				<td>V3ndedor01!</td>
				<td>Juan Pérez</td>
				<td>2444727</td>
				<td>vendedor1@popusa.com.mx</td>
        <td>P0, P1, P2, PL</td>
				<td>vendedor1 (sin .jpg al final)</td>
        <td>Principal</td>

			</tr>
		  </tbody>
          <?php /*<tbody>
            <tr>
              <td>Nombre del Rol<br>(VENDEDOR, ADMINISTRADOR, GERENTE DE VENTA, ADMINISTRACION DE LA VENTA, TELEMARKETING o DISTRIBUIDOR)</td>
              <td>ID del usuario, el cual se utilizará al generar folios para las órdenes de pedido</td>
              <td>Nombre de usuario con el cual iniciará sesión el usuario.</td>
              <td>Contraseña para iniciar sesión</td>
              <td>Nombre propio con el que se identificará al usuario, aparece en la información de contacto de vendedor en cotizaciones y órdenes de pedido</td>
              <td>Teléfono del usuario, aparece en la información de contacto de vendedor en cotizaciones y órdenes de pedido</td>
            </tr>
          </tbody>*/?>
        </table>
        <br>
        <p>Archivo a subir (en formato *.xls, *.xlsx o *.ods):</p>
        <?php echo form_open_multipart("usuarios/guardar"); ?>
        <input type="file" name="archivo" class="form-control">
        <br>
        <input type="submit" value="Subir" class="btn btn-primary pull-right">
        <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>
</section>

<script>
	$(document).ready(function() {
		$('#usuariosTable').DataTable({

		initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    });
	});
</script>
<style>
	.no-display select {
		display: none !important;
	}
</style>

<div class="container">
  <div class="col-md-12">
<h1 class="align-center">Usuarios</h1>
<a href="<?php echo base_url("index.php/usuarios/subirUsuarios"); ?>" class="btn btn-default"><i class="fa fa-user-plus"></i> Subir archivo de usuarios</a> <a href="<?php echo base_url("index.php/elfinder_lib/manager/2"); ?>" class="btn btn-default" target="_blank"><i class="fa fa-file"></i> Administrar archivos de firmas</a>
<br><br>
<?php if ($this->session->flashdata("msg_s")) : ?>
  <div class="alert alert-success">
    <?php echo $this->session->flashdata("msg_s"); ?>
  </div>
<?php endif; ?>
<table id="usuariosTable">
  <thead>
    <tr>
      <th>Nombre</th>
      <th>Rol</th>
      <th>Operaciones</th>
    </tr>
</thead>
<tfoot>
	<tr>
		<th>Nombre</th>
		<th>Rol</th>
		<th class="no-display">Operaciones</th>
	</tr>
</tfoot>
<tbody>
  <?php
    foreach ($usuarios as $usuario) {
		
		
      echo "<tr>";
        echo "<td>" . $usuario["nombre"] . "</td>";
        echo "<td>" . $usuario["rol"] . "</td>";
		if ($usuario["rol"] == "Vendedor" || $usuario["rol"] == "Telemarketing") {
			$login_asesor = $this->db->query("SELECT login_idlogin FROM asesor where login_idlogin = ?", array($usuario["idlogin"]))->result_array()[0]["login_idlogin"];
			echo "<td><a href='" . base_url(). "index.php/cotizaciones/nuevasVendedor/" . $login_asesor . "'><i class='fa fa-folder'></i> Ver carpetas</a></td>";
		}
		else {
			echo "<td>&nbsp;</td>";
		}
        
      echo "</tr>";
    }
 ?>
</tbody>
</table>
</div>
</div>

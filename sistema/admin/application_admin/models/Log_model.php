<?php
/* Información Relevante:
    1. La severidad puede ser
        a) mensaje
        b) error
    2. Tipos pueden ser:
        a) error
        b) login
        c) asignacion
        d) modificacion
        e) envio
        f) caducar
        g) registro
        h) cambio de estado
        i) capturar
        j) reinicio de caducidad
        k) logout

    Para cada uno de estos, los mensajes propuestos son:

    a) Se produjo el error en el sistema: ** Mensaje **
    b) El usuario ** nombre del usuario ** inició sesión
    c) El usuario  ** nombre telemarketing ** asignó al cliente ** nombre cliente ** al vendedor ** nombre del vendedor **
    d) El usuario ** nombre del vendedor ** modificó la cotización con folio ** folio de la cotización **
    e) El usuario ** nombre del vendedor ** envió la cotización con folio ** folio de la cotización ** al email ** email del cliente **
    f) La cotización ** folio de la cotización ** caducó debido a un periodo de inactividad
    g)
    h) El usuario ** nombre del vendedor ** cambió el estado de la cotización ** folio de la cotización ** de ** estado original ** a ** estado final **
    i) El usuario ** nombre del admin venta ** capturó el pedido con folio ** folio del pedido **
    j) El usuario ** nombre del gerente de venta ** reinició el estado de caducidad de la cotización con folio ** folio de la cotización **
    k) El usuario ** nombre del usuario ** cerró sesión.
    l) El usuario ** nombre del usuario ** modificó la configuración del sistema

    Insertar este snippet donde corresponde
    // Guardar en log
    $this->load->model("log_model");
    $args_log = array(
        variables
    );

    $this->log_model->insertMsgLog(tipo, $args_log);
*/
Class Log_model extends CI_Model {
    public function __construct() {
        //parent::__construct();
    }

    public function getAllLog() {
        return $this->db->get("log")->result_array();
    }

    public function getAllLog_alt() {
        return $this->db->get("log");
    }

    public function getAllLogType($type) {
        $this->db->where("tipo", $type);
        return $this->db->get("log")->result_array();
    }

    public function insertLog($severidad, $tipo, $mensaje) {
        $fecha = date('Y-m-d H:i:s');
        $data = array(
            'severo' => $this->secclean->limpiar($severidad),
            'tipo' => $this->secclean->limpiar($tipo),
            'mensaje' => $this->secclean->limpiar($mensaje),
            "fecha" => $fecha,
        );

        $this->db->insert("log", $data);
        return $this->db->insert_id();

    }

    public function insertMsgLog($tipo, $args) {
        $fecha = date('Y-m-d H:i:s');

        switch ($tipo) {
            case "b":
                $usr = $args["idusuario"];
                $nombre = $args["nombre"];
                $severo = "mensaje";
                $tipo = "login";

                $usr = $this->db->query("SELECT nombre FROM login WHERE idlogin = ?", array($usr))->result_array()[0]["nombre"];
                $mensaje = "El usuario " . $usr . " inició sesión";

                $data = array(
                    "severo" => $severo,
                    "tipo" => $tipo,
                    "fecha" => $fecha,
                    "mensaje" => $mensaje,
                    "usr" => $usr,
                );
                break;
            case "c":
                $usr_tm = $args["idusuario"];
                $nombre_tm = $args["nombre"];
                $usr_cl = $args["idusuario_cl"];
                $nombre_cl = $args["nombre_cl"];
                $usr_ven = $args["idusuario_ven"];
                $nombre_ven = $args["nombre_ven"];
                $severo = "mensaje";
                $tipo = "asignación";

                $usr_tm = $this->db->query("SELECT nombre FROM login WHERE idlogin = ?", array($usr_tm))->result_array()[0]["nombre"];

                $mensaje = "El usuario " .  $usr_tm . " asignó al cliente " . $nombre_cl . " al vendedor " .  $usr_ven;

                $data = array(
                    "severo" => $severo,
                    "tipo" => $tipo,
                    "fecha" => $fecha,
                    "mensaje" => $mensaje,
                    "usr" => $usr_tm,
                );
                break;
            case "d":
                $usr_ven = $args["idusuario"];
                $nombre_ven = $args["nombre"];
                $folio = $args["folio"];
                $severo = "mensaje";
                $tipo = "modificación";

                $usr_ven = $this->db->query("SELECT nombre FROM login WHERE idlogin = ?", array($usr_ven))->result_array()[0]["nombre"];

                $mensaje = "El usuario " . $usr_ven . " modificó la cotización con folio " . $folio;

                $data = array(
                    "severo" => $severo,
                    "tipo" => $tipo,
                    "fecha" => $fecha,
                    "mensaje" => $mensaje,
                    "usr" => $usr_ven,
                );
                break;
            case "e":
                $email_cl = $args["email_cl"];
                $usr_ven = $args["idusuario_ven"];
                $nombre_ven = $args["nombre_ven"];
                $folio = $args["folio"];
                $severo = "mensaje";
                $tipo = "envío";

                $usr_ven = $this->db->query("SELECT nombre FROM login WHERE idlogin = ?", array($usr_ven))->result_array()[0]["nombre"];

                $mensaje = "El usuario " . $usr_ven . " envió la cotización con folio " . $folio . " al email " . $email_cl;

                $data = array(
                    "severo" => $severo,
                    "tipo" => $tipo,
                    "fecha" => $fecha,
                    "mensaje" => $mensaje,
                    "usr" => $usr_ven,
                );
                break;
            case "f":
                $folio = $args["folio"];
                $severo = "mensaje";
                $tipo = "caducidad";
                $mensaje = "La cotización " .  $folio . " caducó debido a un periodo de inactividad";

                $data = array(
                    "severo" => $severo,
                    "tipo" => $tipo,
                    "fecha" => $fecha,
                    "mensaje" => $mensaje,
                    "usr" => "sistema",
                );
                break;
            case "g":
                break;
            case "h":
                $usr_ven = $args["idusuario_ven"];
                $nombre_ven = $args["nombre_ven"];
                $folio = $args["folio"];
                $estado_inicial = $args["edo_inicial"];
                $estado_final = $args["edo_final"];
                $severo = "mensaje";
                $tipo = "cambio de estado";

                $usr_ven = $this->db->query("SELECT nombre FROM login WHERE idlogin = ?", array($usr_ven))->result_array()[0]["nombre"];

                $mensaje = "El usuario " . $usr_ven . " cambió el estado de la cotización con folio " . $folio . " de " . $estado_inicial . " a " .  $estado_final;

                $data = array(
                    "severo" => $severo,
                    "tipo" => $tipo,
                    "fecha" => $fecha,
                    "mensaje" => $mensaje,
                    "usr" => $usr_ven,

                );
                break;
            case "i":
                $usr_adminven = $args["idusuario_adminven"];
                $nombre_adminven = $args["nombre_adminven"];
                $folio = $args["folio"];
                $severo = "mensaje";
                $tipo = "capturar";

                $usr_adminven = $this->db->query("SELECT nombre FROM login WHERE idlogin = ?", array($usr_adminven))->result_array()[0]["nombre"];

                $mensaje = "El usuario " . $usr_adminven . " capturó el pedido con folio " . $folio;

                $data = array(
                    "severo" => $severo,
                    "tipo" => $tipo,
                    "fecha" => $fecha,
                    "mensaje" => $mensaje,
                    "usr" => $usr_adminven,
                );
                break;
            case "j":
                $usr_gven = $args["idusuario_gven"];
                $nombre_gven = $args["nombre_gven"];
                $folio = $args["folio"];

                $severo = "mensaje";
                $tipo = "reinicio de caducidad";

                $usr_gven = $this->db->query("SELECT nombre FROM login WHERE idlogin = ?", array($usr_gven))->result_array()[0]["nombre"];

                $mensaje = "El usuario " . $usr_gven . " reinició el estado de caducidad de la cotización con folio " . $folio;

                $data = array(
                    "severo" => $severo,
                    "tipo" => $tipo,
                    "fecha" => $fecha,
                    "mensaje" => $mensaje,
                    "usr" => $usr_gven,
                );
                break;
            case "k":
                $usr = $args["idusuario"];
                $nombre = $args["nombre"];
                $severo = "mensaje";
                $tipo = "logout";

                $usr = $this->db->query("SELECT nombre FROM login WHERE idlogin = ?", array($usr))->result_array()[0]["nombre"];

                $mensaje = "El usuario " . $usr . " cerró sesión";

                $data = array(
                    "severo" => $severo,
                    "tipo" => $tipo,
                    "fecha" => $fecha,
                    "mensaje" => $mensaje,
                    "usr" => "sistema",
                );
                break;
            case "l":
                $usr = $args["idusuario"];
                $severo = "mensaje";
                $tipo = "configuracion";

                $usr = $this->db->query("SELECT nombre FROM login WHERE idlogin = ?", array($usr))->result_array()[0]["nombre"];


                $mensaje = "El usuario " . $usr .  " modificó la configuración del sistema";
                $data = array(
                    "severo" => $severo,
                    "tipo" => $tipo,
                    "fecha" => $fecha,
                    "mensaje" => $mensaje,
                    "usr" => $usr,
                );
                break;
            default:
                break;
        }

        $this->db->insert("log", $data);
        return $this->db->insert_id();
    }
}
?>

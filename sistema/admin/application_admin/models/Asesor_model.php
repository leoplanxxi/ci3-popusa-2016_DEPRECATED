<?php

Class Asesor_model extends CI_Model {

    public function __construct() {
        //parent::__construct();
    }

    public function getAllAsesores() {
        return $this->db->get("asesor")->result_array();
    }

	public function getAllAsesoresActivos() {
		return $this->db->query("SELECT login.activo, login.rol_idrol, asesor.datos, asesor.nombre, asesor.idasesor, asesor.login_idlogin FROM login, asesor WHERE activo = 1 AND asesor.login_idlogin = login.idlogin AND (rol_idrol = 1 OR rol_idrol = 2 or rol_idrol = 4)")->result_array();
	}

	public function getAllCotizadores() {
		return $this->db->query("SELECT * FROM asesor LEFT JOIN login ON asesor.login_idlogin = login.idlogin UNION SELECT * FROM asesor RIGHT JOIN login ON asesor.login_idlogin = login.idlogin")->result_array();
	}

    public function getAsesorID($idAsesor) {
        $this->db->where("idasesor", $idAsesor);
        $resultado = $this->db->get("asesor")->result_array();
        if (count($resultado) > 0) {
            return $resultado[0];
        }
        else {

        }
    }

    public function getAsesorLogin($idLogin) {
        $this->db->where("login_idlogin", $idLogin);
        return $this->db->get("asesor")->result_array()[0];
    }

    public function addAsesor() {
        $data = array(
            'nombre' => $this->secclean->limpiar($this->input->post("nombre")),
            'datos' => $this->secclean->limpiar($this->input->post("datos")),
            'zona_idzona' => $this->secclean->limpiar($this->input->post("zona_idzona")),
        );
        $this->db->insert("asesor", $data);
        return $this->db->insert_id();
    }

    public function updateAsesor($idAsesor) {
        $data = array(
            'nombre' => $this->secclean->limpiar($this->input->post("nombre")),
            'datos' => $this->secclean->limpiar($this->input->post("datos")),
            'zona_idzona' => $this->secclean->limpiar($this->input->post("zona_idzona")),
        );
        $this->db->where("idasesor", $idAsesor);
        $this->db->update("asesor", $data);
    }

    public function removeAsesor($idAsesor) {
        $this->db->where("idasesor", $idAsesor);
        $this->db->delete("asesor");
    }

    public function assignLogin($idAsesor, $idLogin) {
        $data = array(
            'login_idlogin' => $idLogin
        );
        $this->db->where("idasesor", $idAsesor);
        $this->db->update("asesor", $data);
    }

	public function getFirma($idAsesor) {
		$this->db->select("firma");
		$this->db->where("idasesor", $idAsesor);

		return $this->db->get("asesor")->result_array()[0]["firma"];
	}

}

?>

<?php

/*
  INFORMACIÓN RELEVANTE
  ---------------------
  Las cotizaciones van a poseer los siguientes estados:
  0. Nueva
  1. En proceso
  2. Aceptada
  3. Rechazada
 */

Class CotizadorCore_model extends CI_Model {

    public function __construct() {
        //parent::__construct();
    }

    public function getProductoInfoCod($codigo) {
        $db_h_ventas = $this->load->database("db_h_ventas", TRUE);
        $db_h_ventas->where("codigo", $codigo);
        return $db_h_ventas->get("productos")->result_array()[0];
    }

    public function getProductoPrecioCod($codigo) {
        $db_h_ventas = $this->load->database("db_h_ventas", TRUE);

        $db_h_ventas->select("pxub");
        $db_h_ventas->where("codigo", $codigo);
        return $db_h_ventas->get("productos")->result_array()[0];
    }

    public function getProductoPrecioCodArray($codigo) {
        $db_h_ventas = $this->load->database("db_h_ventas", TRUE);

        $db_h_ventas->select("u_base2, pxub, u_aux2, pxua");
        $db_h_ventas->where("codigo", $codigo);

        $result = $db_h_ventas->get("productos")->result_array()[0];

        $precio[$result["u_base2"]] = $result["pxub"];
        $precio[$result["u_aux2"]] = $result["pxua"];
        return $precio;
    }

    public function getFechaCaducidadCotiz() {
        $this->db->select("Valor");
        $this->db->where("Nombre", "Dias_caducidad_cotiz");
        return $this->db->get("meta")->result_array()[0]["Valor"];
    }

    public function getFactor($factor) {
        $this->db->select("Valor");
        $this->db->where("Nombre", $factor);
        return $this->db->get("meta")->result_array()[0]["Valor"];
    }

    public function getFactorIVA() {
        $this->db->select("Valor");
        $this->db->where("Nombre", "Factor_IVA");
        return $this->db->get("meta")->result_array()[0]["Valor"];
    }

    public function getIva($codigo) {
        $db_h_ventas = $this->load->database("db_h_ventas", TRUE);

        $db_h_ventas->select("iva");
        $db_h_ventas->where("codigo", $codigo);
        return $db_h_ventas->get("productos")->result_array()[0]["iva"];
    }

    public function getCosturaCodigo($codigo) {
        $db_h_ventas = $this->load->database("db_h_ventas", TRUE);

        $db_h_ventas->select("costura");
        $db_h_ventas->where("codigo", $codigo);
        return $db_h_ventas->get("productos")->result_array()[0]["costura"];
    }

    public function getConfeccionCodigo($codigo) {
        $db_h_ventas = $this->load->database("db_h_ventas", TRUE);

        $db_h_ventas->select("confeccion");
        $db_h_ventas->where("codigo", $codigo);
        return $db_h_ventas->get("productos")->result_array()[0]["confeccion"];
    }

    public function getOjillosCodigo($codigo) {
        $db_h_ventas = $this->load->database("db_h_ventas", TRUE);

        $db_h_ventas->select("ojillos");
        $db_h_ventas->where("codigo", $codigo);
        return $db_h_ventas->get("productos")->result_array()[0]["ojillos"];
    }

    public function getTiempoEntrega($codigo) {
        $db_h_ventas = $this->load->database("db_h_ventas", TRUE);

        $db_h_ventas->select("tient");
        $db_h_ventas->where("codigo", $codigo);
        return $db_h_ventas->get("productos")->result_array()[0]["tient"];
    }

    public function getDescripcionesPDF($codigo) {
        $db_h_ventas = $this->load->database("db_h_ventas", TRUE);

        $db_h_ventas->select("descotizador, desespecial, pdf");
        $db_h_ventas->where("codigo", $codigo);
        return $db_h_ventas->get("productos")->result_array()[0];
    }

    ///////////////////////////

    public function getCotizacion($idCotizacion) {
        $this->db->where("idcotizaciones", $idCotizacion);
        $query = $this->db->get("cotizaciones");
        return $query->result_array();
    }

    public function updateCotizacionPrecio($idCotizacion, $json) {
        $json = $json;
        $data = array(
            'json_in' => $json,
			'json_out' => "upd"
        );

        $this->db->where("idcotizaciones", $idCotizacion);
        $this->db->update("cotizaciones", $data);
    }

    public function updateCotizacionCalculada($idCotizacion, $json) {
        $json = $json;
        $data = array(
            'json_out' => $json
        );

        $this->db->where("idcotizaciones", $idCotizacion);
        $this->db->update("cotizaciones", $data);
    }

    public function updateCotizacionContadorModif($idCotizacion, $num) {
        $data = array(
            'modificaciones' => $num
        );

        $this->db->where("idcotizaciones", $idCotizacion);
        $this->db->update("cotizaciones", $data);
    }

    public function updateCotizacionCaducidad($idcotizacion, $fecha) {
        $last_caducidad_changed = $this->db->query("SELECT last_caducidad_changed FROM cotizaciones WHERE idcotizaciones = ?", array($idcotizacion))->result_array()[0]["last_caducidad_changed"];
        $today = date("Y-m-d");
        if ($last_caducidad_changed != $today) {
            $data = array(
                "caducidad" => $fecha,
                "last_caducidad_changed" => date('Y-m-d'),
            );

            $this->db->where("idcotizaciones", $idcotizacion);
            $this->db->update("cotizaciones", $data);
        }


    }

    public function updateCotizacionAsesor($idcotizacion, $idasesor) {

        if($idasesor){
          $data = array(
              'asesor_idasesor' => $idasesor
          );
        }else{
          $asesor = $this->verificar_asesor($idcotizacion);
          $data = array(
              'asesor_idasesor' => $asesor[0]['asesor_idasesor']
          );
        }


        $this->db->where("idcotizaciones", $idcotizacion);
        $this->db->update("cotizaciones", $data);
    }

    public function imprintCotizacionAsesor($idcotizacion, $idasesor=null) {

      if($idasesor!=null){
        $this->db->where("idasesor", $idasesor);
        $asesor = $this->db->get("asesor")->result_array()[0];
        $nombre_asesor = $asesor["nombre"];
        $datos_asesor = $asesor["datos"];

        $imprint_asesor = $nombre_asesor . "<br>" . $datos_asesor;
      }else{

        $asesor = $this->cotizadorCore_model->verificar_asesor($idcotizacion);
        $imprint_asesor = $asesor[0]['asesor'];
      }
      $this->db->reset_query();



      $data = array(
        'asesor' => $imprint_asesor,
      );

      $this->db->where("idcotizaciones", $idcotizacion);
      $this->db->update("cotizaciones", $data);
    }

	public function imprintDataCliente($idcotizacion, $idcliente) {
		$this->load->model("cliente_model");

		$datos_cliente = $this->cliente_model->getClienteID($idcliente);

		$array_datos["nombre"] = $datos_cliente["contacto"];
		$array_datos["tels"] = $datos_cliente["tels"];
		$array_datos["rfc"] = $datos_cliente["rfc"];
		$array_datos["email"] = $datos_cliente["email"];
		$array_datos["direccion"] = $datos_cliente["calle"] . " " . $datos_cliente["exterior"] . " " . $datos_cliente["interior"] . " Col. " . $datos_cliente["colonia"] . " CP. " . $datos_cliente["codigo_postal"] . ". " . $datos_cliente["ciudad"] . ", " . $datos_cliente["estado"] . ", " . $datos_cliente["pais"];

		$json_datos = json_encode($array_datos);

		$data = array(
			'info_cliente' => $json_datos,
		);

		$this->db->where("idcotizaciones", $idcotizacion);
		$this->db->update("cotizaciones", $data);

	}

    public function getAllCotizaciones() {
        return $this->db->get("cotizaciones")->result_array();
    }

	public function getAllCotizacionesActivas()  {
		$query = $this->db->query("SELECT * FROM cotizaciones WHERE estado != 2 AND estado != 3");
		return $query->result_array();
	}

    public function getAllCotizacionesCliente($idCliente) {
        $this->db->where("cliente_idcliente", $idCliente);
        return $this->db->get("cotizaciones")->result_array();
    }


    public function getAllCotizacionesVendedor_old($idVendedor) {
        $this->db->where("asesor_idasesor", $idVendedor);
        return $this->db->get("cotizaciones")->result_array();
    }
    
    public function getAllCotizacionesVendedor($vendedor) {
        $this->db->where("asesor_login", $vendedor);
        return $this->db->get("cotizaciones")->result_array();
    }

	public function getAllCotizacionesNuevasCliente($idCliente) {
        $this->db->where("estado", 0);
        $this->db->where("cliente_idcliente", $idCliente);
        $this->db->order_by("fecha", "DESC");
        return $this->db->get("cotizaciones")->result_array();
    }

	public function getAllCotizacionesProcesoCliente($idCliente) {
        $this->db->where("estado", 1);
        $this->db->where("cliente_idcliente", $idCliente);
        $this->db->order_by("fecha", "DESC");
        return $this->db->get("cotizaciones")->result_array();
    }

	public function getAllCotizacionesRechazadasCliente($idCliente) {
        $this->db->where("estado", 3);
        $this->db->where("cliente_idcliente", $idCliente);
        $this->db->order_by("fecha", "DESC");
        return $this->db->get("cotizaciones")->result_array();
    }

	public function getAllOrdenesCliente($idCliente) {
        $this->db->where("estado", 2);
        $this->db->where("cliente_idcliente", $idCliente);
        $this->db->order_by("fecha", "DESC");
        return $this->db->get("cotizaciones")->result_array();
    }

  public function getAllOrdenesDistribuidor_old($idAsesor) {
        //$this->db->where("estado", 2);
        $this->db->where("asesor_idasesor", $idAsesor);
        $this->db->order_by("fecha", "DESC");
        return $this->db->get("cotizaciones")->result_array();
    }

    public function getAllOrdenesDistribuidor($idusuario) {
        return $this->db->query("SELECT * FROM cotizaciones WHERE folio02 LIKE '%" . $idusuario . "%'")->result_array();
    }
  //public function
    public function getAllCotizacionesNuevoAsesor_old($idAsesor) {
        $this->db->where("estado", 0);
        $this->db->where("asesor_idasesor", $idAsesor);
        $this->db->order_by("fecha", "DESC");
        return $this->db->get("cotizaciones")->result_array();
    }

    public function getAllCotizacionesNuevoAsesor($asesor) {
        $this->db->where("estado", 0);
        $this->db->where("asesor_login", $asesor);
        $this->db->order_by("fecha", "DESC");
        return $this->db->get("cotizaciones")->result_array();
    }

    public function getAllCotizacionesProcesoAsesor_old($idAsesor) {
        $this->db->where("estado", 1);
        $this->db->where("asesor_idasesor", $idAsesor);
        $this->db->order_by("fecha", "DESC");
        return $this->db->get("cotizaciones")->result_array();
    }

    public function getAllCotizacionesProcesoAsesor($asesor) {
        $this->db->where("estado", 1);
        $this->db->where("asesor_login", $asesor);
        $this->db->order_by("fecha", "DESC");
        return $this->db->get("cotizaciones")->result_array();
    }

    public function getAllCotizacionesRechazadasAsesor_old($idAsesor) {
        $this->db->where("estado", 3);
        $this->db->where("asesor_idasesor", $idAsesor);
        $this->db->order_by("fecha", "DESC");
        return $this->db->get("cotizaciones")->result_array();
    }

    public function getAllCotizacionesRechazadasAsesor($asesor) {
        $this->db->where("estado", 3);
        $this->db->where("asesor_login", $asesor);
        $this->db->order_by("fecha", "DESC");
        return $this->db->get("cotizaciones")->result_array();
    }

	 public function getAllCotizacionesIDLogin($idlogin) {
        $this->db->where("usuario_atiende", $idlogin);
        return $this->db->get("cotizaciones")->result_array();
    }

    public function getAllCotizacionesProcesoIDLogin($idlogin) {
        $this->db->where("estado", 1);
        $this->db->where("usuario_atiende", $idlogin);
        $this->db->order_by("fecha", "DESC");
        return $this->db->get("cotizaciones")->result_array();
    }

    public function getAllCotizacionesRechazadasIDLogin($idlogin) {
        $this->db->where("estado", 3);
        $this->db->where("usuario_atiende", $idlogin);
        $this->db->order_by("fecha", "DESC");
        return $this->db->get("cotizaciones")->result_array();
	}

	public function getAllCotizacionesAceptadasIDLogin($idlogin) {
        $this->db->where("estado", 2);
        $this->db->where("usuario_atiende", $idlogin);
        $this->db->order_by("fecha", "DESC");
        return $this->db->get("cotizaciones")->result_array();
	}

    public function getAllCotizacionesNuevo() {
        $this->db->where("estado", 0);
        $this->db->order_by("fecha", "DESC");
        return $this->db->get("cotizaciones")->result_array();
    }

    public function getAllCotizacionesProceso() {
        $this->db->where("estado", 1);
        $this->db->order_by("fecha", "DESC");
        return $this->db->get("cotizaciones")->result_array();
    }

    public function getAllCotizacionesRechazadas() {
        $this->db->where("estado", 3);
        $this->db->order_by("fecha", "DESC");
        return $this->db->get("cotizaciones")->result_array();
    }


    public function getAllOrdenes() {
        $this->db->where("estado", 2);
        $this->db->order_by("idcotizaciones", "DESC");
        return $this->db->get("cotizaciones")->result_array();
    }

    public function getAllOrdenesVendedor_old($idVendedor) {

        $this->db->where("estado", 2);
        $this->db->where("asesor_idasesor", $idVendedor);
        return $this->db->get("cotizaciones")->result_array();
    }

    public function getAllOrdenesVendedor($vendedor) {

        $this->db->where("estado", 2);
        $this->db->where("asesor_login", $vendedor);
        return $this->db->get("cotizaciones")->result_array();
    }

    public function getAllOrdenesNuevas_adminVenta() {
        $this->db->where("estado", 2);
        $this->db->where("visto_admin_venta", 0);

        return $this->db->get("cotizaciones")->result_array();
    }

    public function resetCotizacion($idCotizacion) {
        $this->db->set("json_out", NULL);
        $this->db->where("idcotizaciones", $idCotizacion);
        $this->db->update("cotizaciones");
    }


    ////////////////////////////////

    public function getAllFamiliasAlianza() {
        $db_h_ventas = $this->load->database("db_h_ventas", TRUE);
        $resultado = $db_h_ventas->query('SELECT DISTINCT familia FROM productos WHERE familia != ""')->result_array();

        $array_salida = array();

        foreach ($resultado as $res) {
            array_push($array_salida, $res["familia"]);
        }
        return $array_salida;
    }

    public function getAllSubFamiliasPaket() {
        $db_h_ventas = $this->load->database("db_h_ventas", TRUE);
        $resultado = $db_h_ventas->query('SELECT DISTINCT subfamilia FROM productos where marca = "Paket"')->result_array();

        $array_salida = array();

        foreach ($resultado as $res) {
            array_push($array_salida, $res["subfamilia"]);
        }

        array_push($array_salida, "ALIANZA");
        return $array_salida;
    }

    public function getBancosCotizador() {
        return $this->db->get("banco")->result_array();
    }

    //////////////////////////////

	public function cambiarAsesorLive($idCotizacion) {
		$idlogin = $this->session->userdata("logged_in")["idusuario"];

		$query = $this->db->query("SELECT * FROM asesor WHERE login_idlogin = ?", array($idlogin));
        $asesor_idasesor = $query->result_array()[0]["idasesor"];
        $asesor_login = $query->result_array()[0]["login_idlogin"];

		$data = array(
            "asesor_idasesor" => $asesor_idasesor,
            "asesor_login" => $asesor_login,
		);

		$this->db->where("idcotizaciones", $idCotizacion);
		$this->db->update("cotizaciones", $data);
	}

	public function setFolioDos($idCotizacion) {
		$asesor_idasesor = $this->db->query("SELECT asesor_idasesor FROM cotizaciones WHERE idcotizaciones = ?", array($idCotizacion))->result_array()[0]["asesor_idasesor"];
		$codigo = $this->db->query("SELECT login_idlogin FROM asesor WHERE idasesor = ?", array($asesor_idasesor))->result_array()[0]["login_idlogin"];

		$folio = $codigo . "-" . $idCotizacion;

		$data = array(
			"folio02" => $folio,
 		);

		$this->db->where("idcotizaciones", $idCotizacion);
		$this->db->update("cotizaciones", $data);
	}

    public function cambiarEstadoCotizacion($idCotizacion, $estado) {
        // 0. Nueva, 1. En proceso, 2. Aceptada (se vuelve Orden de Pedido), 3. Rechazada
        $data = array(
            "estado" => $estado,
        );

        $this->db->where("idcotizaciones", $idCotizacion);
        $this->db->update("cotizaciones", $data);
    }

    public function putObservacionesCotizacion($idCotizacion, $obs) {
        $data = array(
            "observaciones" => $obs,
        );

        $this->db->where("idcotizaciones", $idCotizacion);
        $this->db->update("cotizaciones", $data);
    }

    public function putCondiciones($idCotizacion, $condiciones) {
        $data = array(
            "condiciones" => $condiciones,
        );
        $this->db->where("idcotizaciones", $idCotizacion);
        $this->db->update("cotizaciones", $data);
    }

    public function putRazonesCotizacion($idCotizacion, $razon) {
        $data = array(
            "razones" => $razon,
        );

        $this->db->where("idcotizaciones", $idCotizacion);
        $this->db->update("cotizaciones", $data);
    }

    public function cambiarVistoAdminVenta($idCotizacion, $valor) {
        $data = array(
            "visto_admin_venta" => $valor,
        );

        $this->db->where("idcotizaciones", $idCotizacion);
        $this->db->update("cotizaciones", $data);
    }

    public function cambiarCapturadaAdminVenta($idCotizacion, $valor) {
        $data = array(
            "capturada_admin_venta" => $valor,
			"fecha_capturada_admin_venta" => date('Y-m-d'),
        );
        $this->db->where("idcotizaciones", $idCotizacion);
        $this->db->update("cotizaciones", $data);
    }

    public function setUsuarioAtiende($idCotizacion, $idUsuario) {
        $data = array(
            "usuario_atiende" => $idUsuario,
        );
        $this->db->where("idcotizaciones", $idCotizacion);
        $this->db->update("cotizaciones", $data);
    }

	public function reiniciarObservaciones($idCotizacion) {
		$data = array(
			"razones" => " ",
		);

		$this->db->where("idcotizaciones", $idCotizacion);
		$this->db->update("cotizaciones", $data);
	}

	public function deleteCotizacion($idCotizacion) {
		$this->db->where("idcotizaciones", $idCotizacion);
		$this->db->delete("cotizaciones");
	}

  public function verificar_asesor($idC){
    $this->db->select('asesor_idasesor,asesor');
    $this->db->from('cotizaciones');
    $this->db->where('idcotizaciones',$idC);
    $this->db->where('cliente_idcliente',0);
    return $this->db->get()->result_array();
  }

public function set_asesor($asesor,$idC){
  $data = array(
    'asesor_idasesor' => $asesor[0]['asesor_idasesor'],
    'asesor' => $asesor[0]['asesor'],
    'asesor_login' => $asesor[0]["asesor_login"],
  );
  $this->db->where('idcotizaciones',$idC);
  $this->db->where('cliente_idcliente',0);
  $this->db->update('cotizaciones',$data);
}

public function get_estado_cotizacion($id){

  return $this->db->select('estado')
          ->from('cotizaciones')
          ->where('idcotizaciones',$id)
          ->get()
          ->result_array();

}

public function setDireccionEntrega($idCotizacion, $dirEntrega) {
    $data = array(
        'dir_entrega' => $dirEntrega,
    );

    $this->db->where("idcotizaciones", $idCotizacion);
    $this->db->update("cotizaciones", $data);
}

public function getDireccionEntrega($idCotizacion) {
    return $this->db->select("dir_entrega")
    ->from("cotizaciones")
    ->where("idcotizaciones", $idCotizacion)
    ->get()
    ->result_array();
}
}

?>

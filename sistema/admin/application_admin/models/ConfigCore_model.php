<?php
	Class ConfigCore_model extends CI_Model {
		public function __construct() {
			//parent::__construct();
		}

		public function getFactorIVA() {
			$this->db->select("Valor");
			$this->db->where("Nombre", "Factor_IVA");
			return $this->db->get("meta")->result_array()[0]["Valor"];
		}

		public function setFactorIVA() {
			$data = array(
				"Valor" => $this->secclean->limpiar($this->input->post("factor_iva")),
			);

			$this->db->where("nombre", "Factor_IVA");
			$this->db->update("meta", $data);
		}

		public function getFechaCaducidadCotiz() {
			$this->db->select("Valor");
			$this->db->where("Nombre", "Dias_caducidad_cotiz");
			return $this->db->get("meta")->result_array()[0]["Valor"];
		}

		public function setFechaCaducidadCotiz() {
			$data = array(
				"Valor" => $this->secclean->limpiar($this->input->post("fecha_caducidad_cotiz")),
			);

			$this->db->where("nombre", "Dias_caducidad_cotiz");
			$this->db->update("meta", $data);
		}

		public function getFactor($factor) {
			$this->db->select("Valor");
			$this->db->where("Nombre", $factor);
			return $this->db->get("meta")->result_array()[0]["Valor"];
		}

		public function setFactor($factor, $marcadb, $marcaform) {
			$data = array(
				"Valor" => $this->secclean->limpiar($this->input->post("factor_p" . $factor . "_" . $marcaform)),
			);

			$this->db->where("nombre", "Factor_P" . $factor . "_" . $marcadb);
			$this->db->update("meta", $data);
		}

		public function insertarUsuarios($data) {
			$data_login = array(
				'idlogin' => $data["id"],
				'activo' => 1,
				'nombre' => $data["nombre_usuario"],
				'passwd' => password_hash($data["passwd"], PASSWORD_BCRYPT),
				'rol_idrol' => $data["rol"],
			);

			$this->db->insert("login", $data_login);
			$login_id = $data["id"];

			$data_asesor = array(
				'nombre' => $data["nombre"],
				'datos' => $data["datos"],
				'precios' => $data["permisos"],
				'firma' => $data["firma"],
				'lv' => $data["lv"],
				'login_idlogin' => $login_id,
				'pdf' => $data["pdf"],
			);

			$this->db->insert("asesor", $data_asesor);
		}

		public function actualizarUsuario($data, $idlogin) {
			$data_login = array(
				'idlogin' => $data["id"],
				'activo' => 1,
				'nombre' => $data["nombre_usuario"],
				'passwd' => password_hash($data["passwd"], PASSWORD_BCRYPT),
				'rol_idrol' => $data["rol"],
			);

			$this->db->where("idlogin", $idlogin);
			$this->db->update("login", $data_login);
			$this->db->reset_query();

			$data_asesor = array(
				'nombre' => $data["nombre"],
				'datos' => $data["datos"],
				'precios' => $data["permisos"],
				'login_idlogin' => $data["id"],
			);

			$this->db->where("login_idlogin", $idlogin);
			$this->db->update("asesor", $data_asesor);
			$this->db->reset_query();
		}

		public function vaciarUsuarios() {
			$this->db->truncate("asesor");
			$this->db->truncate("login");
		}

		public function checkUsrExists($idlogin) {
			$this->db->where("idlogin", $idlogin);
			$res = $this->db->get("login")->result_array();
			if (count($res) > 0) {
				return true;
			}
			else {
				return false;
			}
		}

		public function checkInfoSame($idlogin, $data) {
			$data_login = $this->db->query("SELECT * FROM login WHERE idlogin = ?", array($idlogin));
			$data_login = $data_login->result_array()[0];

			$data_asesor = $this->db->query("SELECT * FROM asesor WHERE login_idlogin = ?", array($idlogin));
			$data_asesor = $data_asesor->result_array()[0];

			if ($data_login["idlogin"] != $data["id"]) {
				return false;
			}
			else if ($data_login["nombre"] != $data["nombre_usuario"]) {
				return false;
			}
//Falta para revisar Passwd
			else if ($data_login["rol_idrol"] != $data["rol"]) {
				return false;
			}
			else if ($data_login["nombre"] != $data["nombre_usuario"]) {
				return false;
			}
			else if ($data_asesor["nombre"] != $data["nombre"]) {
				return false;
			}
			else if ($data_asesor["datos"] != $data["datos"]) {
				return false;
			}
			else if ($data_asesor["precios"] != $data["permisos"]) {
				return false;
			}
			else {
				return true;
			}
		}

		public function getMensajeClienteCotizacion() {
			$this->db->select("Valor");
			$this->db->where("Nombre", "Mensaje_Cliente_Cotizador");
			return $this->db->get("meta")->result_array()[0]["Valor"];
		}

		public function setMensajeClienteCotizacion() {
			$data = array(
				"Valor" => $this->secclean->limpiar($this->input->post("Mensaje_Cliente_Cotizador")),
			);

			$this->db->where("nombre", "Mensaje_Cliente_Cotizador");
			$this->db->update("meta", $data);
		}

		public function getBanco($idBanco) {
			$this->db->where("idbanco", $idBanco);
			return $this->db->get("banco")->result_array()[0];

		}

		public function updateBanco($idBanco) {
			$data = array(
				'banco' => $this->secclean->limpiar($this->input->post("banco-" . $idBanco . "-banco")),
				'cuenta' => $this->secclean->limpiar($this->input->post("banco-" . $idBanco . "-cuenta")),
				'clabe' => $this->secclean->limpiar($this->input->post("banco-" . $idBanco . "-clabe")),
				'nombre' => $this->secclean->limpiar($this->input->post("banco-" . $idBanco . "-nombre")),
			);

			$this->db->where("idbanco", $idBanco);
			$this->db->update("banco", $data);
		}

	}
?>

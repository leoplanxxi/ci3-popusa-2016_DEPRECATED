<?php
Class Estadisticas_model extends CI_Model {
  public function __construct() {
    //parent::__construct();
  }

  public function insert($idproductos, $precio, $idcotizacion, $idcliente) {
    $data = array(
      'idproductos' => $idproductos,
      'precio' => $precio,
      'idcotizacion' => $idcotizacion,
      'idcliente' => $idcliente,
    );

    $this->db->insert("estadisticas", $data);
    return $this->db->insert_id();
  }

  public function update($idproductos, $precio, $idcotizacion, $idcliente) {
    $data = array(
      'idproductos' => $idproductos,
      'precio' => $precio,
      'idcotizacion' => $idcotizacion,
      'idcliente' => $idcliente,
    );

    $this->db->where("idcotizacion", $idcotizacion);

    $this->db->update("estadisticas", $data);

  }

  public function updateID($idestadistica, $idproductos, $precio, $idcotizacion, $idcliente) {
    $data = array(
      'idproductos' => $idproductos,
      'precio' => $precio,
      'idcotizacion' => $idcotizacion,
      'idcliente' => $idcliente,
    );

    $this->db->where("idestadisticas", $idestadistica);

    $this->db->update("estadisticas", $data);
  }

  public function delete($idcotizacion) {
    $this->db->where("idcotizacion", $idcotizacion);
    $this->db->delete("estadisticas");
  }

  // DATOS COMPLEMENTARIOS

  public function getInfoCliente($idCliente) {
    $this->db->where("idcliente", $idCliente);
    return $this->db->get("cliente")->result_array()[0];

  }

  public function getInfoCotiz($idCotizacion) {
    $this->db->where("idcotizaciones", $idCotizacion);
    return $this->db->get("cotizaciones")->result_array()[0];
  }

  public function getInfoAsesor($idAsesor) {
    $this->db->where("idasesor", $idAsesor);
    return $this->db->get("asesor")->result_array()[0];
  }

  public function getAllCotizacionesAceptadasVendedor($idAsesor) {
    $this->db->where("estado", 2);
    $this->db->where("asesor_idasesor", $idAsesor);
    $this->db->order_by("fecha", "DESC");
    return $this->db->get("cotizaciones")->result_array();
  }

  public function getAllCotizacionesRechazadasVendedor($idAsesor) {
    $this->db->where("estado", 3);
    $this->db->where("asesor_idasesor", $idAsesor);
    $this->db->order_by("fecha", "DESC");
    return $this->db->get("cotizaciones")->result_array();
  }

  public function getAllCotizacionesNegociacionVendedor($idAsesor) {
    $this->db->where("estado", 1);
    $this->db->where("asesor_idasesor", $idAsesor);
    $this->db->order_by("fecha", "DESC");
    return $this->db->get("cotizaciones")->result_array();
  }

  public function get_maxmin_prod(){
    $ventas = $this->load->database('db_mh_ventas', TRUE);
    $ventas->select('count(*) as cant, codigo_producto,descorta, json_carrito');
    $ventas->from('carrito');
    $ventas->group_by('codigo_producto');
    $ventas->order_by('cant desc');
    return  $ventas->get()->result_array();
  }

  public function get_all_clientes(){
    $this->db->select('contacto, email, asesor_idasesor, creado');
    $this->db->from('cliente');
    $this->db->order_by('contacto asc');
    return $this->db->get()->result_array();

  }

  public function get_asesor($id){
    $this->db->select('*');
    $this->db->from('asesor');
    $this->db->where('idasesor',$id);
    return $this->db->get()->result_array();
  }

  public function get_all_asesores(){
    $this->db->select('*');
    $this->db->from('asesor');
    $this->db->order_by('nombre asc');
    return $this->db->get()->result_array();
  }

  public function get_all_vendedores(){
    $this->db->select('idasesor,nombre');
    $this->db->from('asesor');
    //$this->db->where("lv != 'Principal'");
    $this->db->order_by('nombre asc');
    return $this->db->get()->result_array();
  }

  public function get_entradas(){
    $this->db->select('*');
    $this->db->from('log');
    $this->db->where('tipo','login');
    $this->db->order_by('fecha desc');
    return $this->db->get()->result_array();
  }

  public function get_reset(){
    $ventas = $this->load->database('db_mh_ventas', TRUE);
    $ventas->select('l.usr,l.reset');
    $ventas->from('login l');
    $ventas->where('l.reset > 0');
    $ventas->order_by('request_token asc');
    return $ventas->get()->result_array();
  }
  public function get_info_by_email($email){
    $this->db->select('*');
    $this->db->from('cliente');
    $this->db->where('email',$email);
    return $this->db->get()->result_array();
  }
  public function get_cot_prod($usuario){
    /*$ventas = $this->load->database('db_mh_ventas', TRUE);
    $this->db->select('usuario');
    $this->db->where('usuario',$usuario);
    return $this->db->get()->result_array();*/
    $this->db->select('cliente_idcliente');
    $this->db->from('cotizaciones');
    $this->db->where('cliente_idcliente',$usuario);
    return $this->db->get()->result_array();
  }

  public function get_ordenes(){
    $this->load->model("cotizadorCore_model");
    //Para órdenes de pedido
    // Fecha Codigo_producto Desc_producto Vendedor Precio Cantidad Estado CP Folio Cliente CURP

          $cotiz = $this->db->query("SELECT * FROM estadisticasTablaOrdenes")->result_array();

          $array_salida = array();
          $i = 0;

          foreach ($cotiz as $c) {

            $datos_cotiz = $this->db->query("SELECT * FROM cotizaciones WHERE idcotizaciones = ?", array($c["idcotizacion"]))->result_array()[0];
            if ($c["idcliente"] != 0) {
              $datos_cliente = $this->db->query("SELECT * FROM cliente WHERE idcliente = ?", array($c["idcliente"]))->result_array()[0];
            }
            else {
              $datos_cliente["contacto"] = substr($datos_cotiz["asesor"], 0, strpos($datos_cotiz["asesor"], "<br>"));
              $datos_cliente["estado"] = " ";
              $datos_cliente["codigo_postal"] = " ";
              $datos_cliente["rfc"] = " ";
            }



            $array_salida[$i]["fecha"] = $datos_cotiz["fecha"];
            $array_salida[$i]["codigo_producto"] = $c["idproductos"];
            $array_salida[$i]["desc_producto"] = $this->cotizadorCore_model->getDescripcionesPDF($c["idproductos"])["descotizador"];

            $json_in = $datos_cotiz["json_in"];
            $json_in = json_decode($json_in, true);

            $json_out = $datos_cotiz["json_out"];
            $json_out = json_decode($json_out, true);

            $vendedor =  substr($datos_cotiz["asesor"], 0, strpos($datos_cotiz["asesor"], "<br>"));

            $array_salida[$i]["vendedor"] = $vendedor;

            if ($c["precio"] == "3") { $c["precio"] = "L"; }
            $array_salida[$i]["precio"] = "P" . $c["precio"];
            $array_salida[$i]["monto"] = $json_out[$c["idproductos"]]["precio_con_iva"];
            $array_salida[$i]["cantidad"] = $json_in[$c["idproductos"]]["cantidad"];
            $array_salida[$i]["estado"] = $datos_cliente["estado"];
            $array_salida[$i]["codigo_postal"] = $datos_cliente["codigo_postal"];
            $array_salida[$i]["folio"] = $datos_cotiz["folio02"];
            $array_salida[$i]["cliente"] = $datos_cliente["contacto"];
            $array_salida[$i]["curp"] = $datos_cliente["rfc"];

            $i++;

          }
          return $array_salida;
  }

  public function get_cotizaciones(){
    $this->load->model("cotizadorCore_model");
    // Para cotizaciones
    // Fecha Codigo_producto Desc_producto Cantidad Estado CP Folio Cliente CURP

    $cotiz = $this->db->query("SELECT * FROM estadisticasTablaCotiz")->result_array();

    $array_salida = array();
    $i = 0;

    foreach ($cotiz as $c) {

      $datos_cotiz = $this->db->query("SELECT * FROM cotizaciones WHERE idcotizaciones = ?", array($c["idcotizacion"]))->result_array()[0];
      $datos_cliente = $this->db->query("SELECT * FROM cliente WHERE idcliente = ?", array($c["idcliente"]))->result_array();
      if($datos_cliente!=null){
        $datos_cliente = $datos_cliente[0];
        $estado = $datos_cliente["estado"];
        $cp = $datos_cliente["codigo_postal"];
        $contacto = $datos_cliente["contacto"];
        $rfc = $datos_cliente["rfc"];
      }else{
        $estado = null;
        $cp = null;
        $contacto = null;
        $rfc = null;
      }
      $array_salida[$i]["fecha"] = $datos_cotiz["fecha"];
      $array_salida[$i]["codigo_producto"] = $c["idproductos"];
      $array_salida[$i]["desc_producto"] = $this->cotizadorCore_model->getDescripcionesPDF($c["idproductos"])["descotizador"];

      $json_in = $datos_cotiz["json_in"];
      $json_in = json_decode($json_in, true);

      $vendedor =  substr($datos_cotiz["asesor"], 0, strpos($datos_cotiz["asesor"], "<br>"));
      $array_salida[$i]["vendedor"] = $vendedor;

      $array_salida[$i]["cantidad"] = $json_in[$c["idproductos"]]["cantidad"];
      $array_salida[$i]["estado"] =
      $array_salida[$i]["codigo_postal"] = $cp;
      $array_salida[$i]["folio"] = $datos_cotiz["idcotizaciones"];
      $array_salida[$i]["cliente"] = $contacto;
      $array_salida[$i]["curp"] = $rfc;

      $status = $datos_cotiz["estado"];

      switch ($status) {
        case 0:
          $status = "Nueva";
          break;
        case 1:
          $status = "En proceso";
          break;
        case 3:
          $status = "Rechazada";
          break;
        default:
          $status = "No válido";
          break;
      }

      $array_salida[$i]["status"] = $status;
      $i++;

    }
    return $array_salida;
  }

}
 ?>

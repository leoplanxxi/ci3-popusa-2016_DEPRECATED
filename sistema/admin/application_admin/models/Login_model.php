<?php
  Class Login_model extends CI_Model {
    public function __construct() {
      //parent::__construct();
    }

    public function checkLogin($nombre, $pass) {
       $passwd_bcrypt = $this->db->query("SELECT passwd FROM login WHERE nombre = ?", array($nombre))->result_array()[0]["passwd"];

       $this->db->where("nombre", $nombre);

       $resultado = $this->db->get("login")->result_array();

       if (count($resultado) > 0) {
         if (password_verify($pass, $passwd_bcrypt)) {
			 if ($this->getActivo($resultado[0]["idlogin"]) == "1") {
				return true;
			}
			else {
				return false;
			}
         }
         else {
           return false;
         }
       }
       else {
         return false;
       }
    }

    public function get_user_hv($nombre,$pass){
      $this->load->library('bcrypt');
      if($this->bcrypt->check_password($pass,$this->get_passv($nombre))){
      $this->db->select('*');
      $this->db->from('login');
      $this->db->where('nombre',$nombre);
      $user = $this->db->get()->result_array();
      if(($user[0]['rol_idrol'] == 1 || $user[0]['rol_idrol'] == 2  || $user[0]['rol_idrol'] == 4 || $user[0]['rol_idrol'] == 6 ) && $user[0]['activo'] == 1 ){
        $this->session->set_userdata('popusa',$user);
        return true;
      }
    }
    }

    public function get_passv($nombre){
      $this->db->select('passwd');
      $this->db->from('login');
      $this->db->where('nombre',$nombre);
      $pass = $this->db->get()->result_array();

      if($pass!=null){
        return $pass[0]['passwd'];
      }else{
        return false;
      }

    }


    public function getLoginInfo($nombre) {
      $this->db->where("nombre", $nombre);
      $resultado = $this->db->get("login");

      return $resultado->result_array()[0];
    }

    public function getUsuariosView() {
      $resultado = $this->db->get("usuariosAdmin");
      return $resultado->result_array();
    }

	public function getActivo($idLogin) {
		$this->db->select("activo");
		return $this->db->get_where("login", array("idlogin" => $idLogin))->result_array()[0]["activo"];
	}
  }
 ?>

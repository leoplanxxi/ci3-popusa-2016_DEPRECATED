<?php
	Class Cliente_model extends CI_Model {
		public function __construct() {
			//parent::__construct();
		}

		public function getAllClientes() {
			return $this->db->get("cliente")->result_array();
		}

		public function getClienteID($idCliente) {
			if($idCliente){
				$this->db->where("idcliente", $idCliente);
				return $this->db->get("cliente")->result_array()[0];
			}

		}

		public function addCliente() {
			$data = array(
				'contacto' => $this->secclean->limpiar($this->input->post("contacto")),
				'rfc' => $this->secclean->limpiar($this->input->post("rfc")),
				'tels' => $this->secclean->limpiar($this->input->post("tels")),
				'email' => $this->secclean->limpiar($this->input->post("email")),
				'calle' => $this->secclean->limpiar($this->input->post("calle")),
				'exterior' => $this->secclean->limpiar($this->input->post("exterior")),
				'interior' => $this->secclean->limpiar($this->input->post("interior")),
				'colonia' => $this->secclean->limpiar($this->input->post("colonia")),
				'codigo_postal' => $this->secclean->limpiar($this->input->post("codigo_postal")),
				'ciudad' => $this->secclean->limpiar($this->input->post("ciudad")),
				'estado' => $this->secclean->limpiar($this->input->post("estado")),
				'pais' => $this->secclean->limpiar($this->input->post("pais")),
			);
			$this->db->insert("cliente", $data);
			return $this->db->insert_id();
		}

		public function addCliente_legacy() {
			$data = array(
				'contacto' => $this->secclean->limpiar($this->input->post("contacto")),
				'rfc' => $this->secclean->limpiar($this->input->post("rfc")),
				'tels' => $this->secclean->limpiar($this->input->post("tels")),
				'email' => $this->secclean->limpiar($this->input->post("email")),
				'direccion' => $this->secclean->limpiar($this->input->post("direccion")),
			);
			$this->db->insert("cliente", $data);
			return $this->db->insert_id();
		}

		public function updateCliente_legacy($idCliente) {
			$data = array(
				'contacto' => $this->secclean->limpiar($this->input->post("contacto")),
				'rfc' => $this->secclean->limpiar($this->input->post("rfc")),
				'tels' => $this->secclean->limpiar($this->input->post("tels")),
				'email' => $this->secclean->limpiar($this->input->post("email")),
				'direccion' => $this->secclean->limpiar($this->input->post("direccion")),
			);
			$this->db->where("idcliente", $idCliente);
			$this->db->update("cliente", $data);
		}

		public function updateCliente($idCliente) {
			$data = array(
				'contacto' => $this->secclean->limpiar($this->input->post("contacto")),
				'razon' => $this->secclean->limpiar($this->input->post("razon")),
				'rfc' => $this->secclean->limpiar($this->input->post("rfc")),
				'tels' => $this->secclean->limpiar($this->input->post("tels")),
				'email' => $this->secclean->limpiar($this->input->post("email")),
				'calle' => $this->secclean->limpiar($this->input->post("calle")),
				'exterior' => $this->secclean->limpiar($this->input->post("exterior")),
				'interior' => $this->secclean->limpiar($this->input->post("interior")),
				'colonia' => $this->secclean->limpiar($this->input->post("colonia")),
				'codigo_postal' => $this->secclean->limpiar($this->input->post("codigo_postal")),
				'ciudad' => $this->secclean->limpiar($this->input->post("ciudad")),
				'estado' => $this->secclean->limpiar($this->input->post("estado")),
				'pais' => $this->secclean->limpiar($this->input->post("pais")),
				'cfdi' => $this->secclean->limpiar($this->input->post("cfdi")),
				'metodo' => $this->secclean->limpiar($this->input->post("metodo")),
				'forma' => $this->secclean->limpiar($this->input->post("forma")),
			);
			$this->db->where("idcliente", $idCliente);
			$this->db->update("cliente", $data);
		}

		public function removeCliente($idCliente) {
			$this->db->where("idcliente", $idCliente);
			$this->db->delete("cliente");
		}

		public function getAllClientesAsesor_old($idAsesor) {
			$this->db->where("asesor_idasesor", $idAsesor);
			return $this->db->get("cliente")->result_array();
		}

		public function getAllClientesAsesor($asesor) {
			$this->db->where("asesor_login", $asesor);
			return $this->db->get("cliente")->result_array();
		}

        public function getClientesNuevos() { // Posiblemente haya que cambiar esto
            $this->db->where(array("asesor_idasesor" => NULL));
            return $this->db->get("cliente")->result_array();
        }

		public function assignAsesor($idCliente) {
			$asesor_login = $this->db->query("SELECT login_idlogin FROM asesor WHERE idasesor = ?", array($this->input->post("asesor_idasesor")))->result_array()[0]["login_idlogin"];

			$data = array(
				'asesor_idasesor' => $this->secclean->limpiar($this->input->post("asesor_idasesor")),
				'asesor_login' => $asesor_login,
			);
			$this->db->where("idcliente", $idCliente);
			$this->db->update("cliente", $data);
		}

		public function reassignAsesor($idCliente, $idAsesor) {
			$asesor_login = $this->db->query("SELECT login_idlogin FROM asesor WHERE idasesor = ?", array($idAsesor))->result_array()[0]["login_idlogin"];

			$data = array(
				'asesor_idasesor' => $this->secclean->limpiar($idAsesor),
				'asesor_login' => $asesor_login,
			);
			$this->db->where("idcliente", $idCliente);
			$this->db->update("cliente", $data);

		}

        public function getAllClientes_alt() {
            return $this->db->get("cliente");
		}

		public function updateClienteHVEmail($email_old, $email_new) {
			$hv = $this->load->database('db_mh_ventas', TRUE);

			$idusr = $hv->query("SELECT idlogin FROM login WHERE usr = ?", array($email_old))->result_array()[0]["idlogin"];

			$data = array(
				'usr' => $email_new,
			);

			$hv->where("idlogin", $idusr);
			$hv->update("login", $data);
		}

		public function datos_cliente_json($idcliente) {

			$datos_cliente = $this->db->query("SELECT * FROM cliente WHERE idcliente = ?", array($idcliente))->result_array()[0];

			$array_datos["nombre"] = $datos_cliente["razon"];
			$array_datos["tels"] = $datos_cliente["tels"];
			$array_datos["rfc"] = $datos_cliente["rfc"];
			$array_datos["email"] = $datos_cliente["email"];
			$array_datos["direccion"] = $datos_cliente["calle"] . " " . $datos_cliente["exterior"] . " " . $datos_cliente["interior"] . " Col. " . $datos_cliente["colonia"] . " CP. " . $datos_cliente["codigo_postal"] . ". " . $datos_cliente["ciudad"] . ", " . $datos_cliente["estado"] . ", " . $datos_cliente["pais"];

			$json_datos = json_encode($array_datos);
			return $json_datos;

		}

		public function updateDatosClienteJSON($idCliente, $json) {
			$data = array(
				'info_cliente' => $json,
			);
			$this->db->where("cliente_idcliente", $idCliente);
			$this->db->where("estado !=", 2);
			$this->db->update("cotizaciones", $data);
		}
	}
?>

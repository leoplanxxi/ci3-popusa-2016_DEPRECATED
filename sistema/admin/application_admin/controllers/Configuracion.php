<?php
	Class Configuracion extends MY_Controller {
		public function construct() {
			parent::__construct();
			if (!$this->session->userdata("logged_in")) {
				redirect("login");
			}
		}

		public function index() {
			$this->load->model("configCore_model");
			$this->load->model("log_model");
			$this->load->library("form_validation");

			// REVISAR ESTO
			$this->form_validation->set_rules("factor_iva", "Factor IVA", "required");
			$this->form_validation->set_rules("factor_p0_alianza", "Factor P0 Alianza", "required");
			$this->form_validation->set_rules("factor_p1_alianza", "Factor P1 Alianza", "required");
			$this->form_validation->set_rules("factor_p2_alianza", "Factor P2 Alianza", "required");
			$this->form_validation->set_rules("factor_p3_alianza", "Factor PL Alianza", "required");
			$this->form_validation->set_rules("factor_p0_paket", "Factor P0 Paket", "required");
			$this->form_validation->set_rules("factor_p1_paket", "Factor P1 Paket", "required");
			$this->form_validation->set_rules("factor_p2_paket", "Factor P2 Paket", "required");
			$this->form_validation->set_rules("factor_p3_paket", "Factor PL Paket", "required");
			$this->form_validation->set_rules("fecha_caducidad_cotiz", "Fecha de Caducidad", "required");
			$this->form_validation->set_rules("Mensaje_Cliente_Cotizador", "Mensaje Cotizador", "required");
			$this->form_validation->set_rules("banco-1-banco", "Nombre del Banco 1", "required");
			$this->form_validation->set_rules("banco-1-cuenta", "Número de cuenta 1", "required");
			$this->form_validation->set_rules("banco-1-clabe", "CLABE 1", "required");
			$this->form_validation->set_rules("banco-1-nombre", "Nombre de la cuenta 1", "required");
			$this->form_validation->set_rules("banco-2-banco", "Nombre del Banco 2", "required");
			$this->form_validation->set_rules("banco-2-cuenta", "Número de cuenta 2", "required");
			$this->form_validation->set_rules("banco-2-clabe", "CLABE 2", "required");
			$this->form_validation->set_rules("banco-2-nombre", "Nombre de la cuenta 2", "required");

			if ($this->form_validation->run() === FALSE) {
				$data["factor_iva"]  = $this->configCore_model->getFactorIVA();
				$data["factor_p0_alianza"] = $this->configCore_model->getFactor("Factor_P0_Alianza");
				$data["factor_p1_alianza"] = $this->configCore_model->getFactor("Factor_P1_Alianza");
				$data["factor_p2_alianza"] = $this->configCore_model->getFactor("Factor_P2_Alianza");
				$data["factor_p3_alianza"] = $this->configCore_model->getFactor("Factor_P3_Alianza");
				$data["factor_p0_paket"] = $this->configCore_model->getFactor("Factor_P0_Paket");
				$data["factor_p1_paket"] = $this->configCore_model->getFactor("Factor_P1_Paket");
				$data["factor_p2_paket"] = $this->configCore_model->getFactor("Factor_P2_Paket");
				$data["factor_p3_paket"] = $this->configCore_model->getFactor("Factor_P3_Paket");
				$data["fecha_caducidad_cotiz"] = $this->configCore_model->getFechaCaducidadCotiz();
				$data["mensaje_cliente_cotizador"] = $this->configCore_model->getMensajeClienteCotizacion();

				$data["banco1"] = $this->configCore_model->getBanco(1);
				$data["banco2"] = $this->configCore_model->getBanco(2);

				$this->load->view("admin_views/config_admin_view", $data);
			}
			else {
				$this->configCore_model->setFactorIVA();
				$this->configCore_model->setFechaCaducidadCotiz();
				$this->configCore_model->setFactor(0, "Alianza", "alianza");
				$this->configCore_model->setFactor(1, "Alianza", "alianza");
				$this->configCore_model->setFactor(2, "Alianza", "alianza");
				$this->configCore_model->setFactor(3, "Alianza", "alianza");
				$this->configCore_model->setFactor(0, "Paket", "paket");
				$this->configCore_model->setFactor(1, "Paket", "paket");
				$this->configCore_model->setFactor(2, "Paket", "paket");
				$this->configCore_model->setFactor(3, "Paket", "paket");
				$this->configCore_model->setMensajeClienteCotizacion();

				$this->configCore_model->updateBanco(1);
				$this->configCore_model->updateBanco(2);

				$args_log = array(
					'idusuario' => $this->session->userdata("logged_in")["idusuario"],
				);

				$this->log_model->insertMsgLog("l", $args_log);

				$this->session->set_flashdata("mensaje", "Se guardó con éxito su configuración");
				redirect("configuracion");
			}

		}
	}
?>

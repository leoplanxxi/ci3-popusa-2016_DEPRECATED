<?php

Class Ordenes extends MY_Controller {
	public  function __construct() {
		parent::__construct();
		if (!$this->session->userdata("logged_in")) {
            redirect("login");
        }
	}

	public function index() {
		$this->load->model("cotizadorCore_model");
		$this->load->model("asesor_model");

		//
		if ($this->session->userdata("logged_in")["rol_idrol"] == 5) {
			$data["ordenes"] = $this->cotizadorCore_model->getAllOrdenes();
		} else {
			$idusuario = $this->session->userdata("logged_in")["idusuario"];
			$idAsesor = $this->asesor_model->getAsesorLogin($idusuario)["idasesor"];
			$data["ordenes"] = $this->cotizadorCore_model->getAllOrdenesVendedor($idAsesor);
			$data["idVendedor"] = $idusuario;
		}

		if ($this->session->userdata("logged_in")["rol_idrol"] == 4) {
			$data["var1"] = true;
		}
		//Apartado para el Distribuidor
		if($this->session->userdata('logged_in')['rol_idrol'] == 6){
			$idusuario = $this->session->userdata("logged_in")["idusuario"];
			$idAsesor = $this->asesor_model->getAsesorLogin($idusuario)["idasesor"];
			$idAsesor = $this->db->query("SELECT login_idlogin FROM asesor WHERE idasesor = ?", array($idAsesor))->result_array()[0]["login_idlogin"]; //REVISAR ESTO
			$data["ordenes"] = $this->cotizadorCore_model->getAllOrdenesDistribuidor($idAsesor);
			$data["query"] = $this->db->last_query();
			$data["idVendedor"] = $idAsesor;
		}

		if ($this->session->userdata("logged_in")["rol_idrol"] == 6) $this->load->view("ordenes/ordenes_distribuidor", $data);
		else $this->load->view("ordenes/ordenes_vendedor", $data);

    }

    public function ordenesVendedor($idVendedor) {
		$this->load->model("cotizadorCore_model");

		if ($this->session->userdata("logged_in")["rol_idrol"] == 3) $data["varAdm"] = true;

        $data["ordenes"] = $this->cotizadorCore_model->getAllOrdenesVendedor($idVendedor);
        $data["idVendedor"] = $idVendedor;

        $this->load->view("ordenes/ordenes_vendedor", $data);
    }

	public function ordenesCliente($idCliente) {
        $this->load->model("cotizadorCore_model");

        $data["ordenes"] = $this->cotizadorCore_model->getAllOrdenesCliente($idCliente);
        $data["idCliente"] = $idCliente;
		$data["vistaClientes"] = true;


        $this->load->view("ordenes/ordenes_vendedor", $data);
	}
	
	public function ordenesEmpresa() {
		$this->load->view("admin_views/ordenes_view");

	}

	public function admin() {
		$this->load->view("admin_views/ordenes_view");
	}

	public function eliminar($idCotizacion) {
		$this->load->model("cotizadorCore_model");

		$this->cotizadorCore_model->deleteCotizacion($idCotizacion);

		$this->session->set_flashdata("mensaje", "Se eliminó con éxito el pedido con folio " . $idCotizacion);
		if ($this->session->userdata("logged_in")["rol_idrol"] == 3) {

			redirect("ordenes/admin");
		}
	}
}

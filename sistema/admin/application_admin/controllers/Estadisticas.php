<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estadisticas extends MY_Controller {

 public function __construct() {
   parent::__construct();
   if (!$this->session->userdata("logged_in")) {
     redirect("login");
   }
   error_reporting(0);
 }


 public function index(){
   $this->mest();
 }

 public function mest($info = "general", $tip="ordenes"){
   $this->load->model('estadisticas_model');
   $this->load->view('estadisticas/principal');
   switch ($info) {
     case 'productos':
     $data = array(
       'max_min' => $this->estadisticas_model->get_maxmin_prod()
     );
     $this->load->view('estadisticas/productos',$data);
      break;

    case 'clientes':
      $data = array(
        'clientes' => $this->estadisticas_model->get_all_clientes(),
        'asesores' => $this->estadisticas_model->get_all_asesores(),
        'entradas' => $this->estadisticas_model->get_entradas(),
        'reset' => $this->estadisticas_model->get_reset()
      );
      $this->load->view('estadisticas/clientes',$data);
      break;

    case 'control':
    $data = array(
      'cotizaciones' => $this->estadisticas_model->get_all_vendedores()
    );
    $this->load->view('estadisticas/control',$data);
      break;

    case 'general':
    switch($tip){
      case 'ordenes':
        $general = $this->estadisticas_model->get_ordenes();
      break;

      case 'cotizaciones':
      $general = $this->estadisticas_model->get_cotizaciones();
      break;

      default:
      $general = $this->estadisticas_model->get_ordenes();
      break;
    }

    $data = array(
      'general' => $general,
      'tip' => $tip
    );
    $this->load->view('estadisticas/general',$data);
    break;

    default:
      /*$data = array(
        'general' => $this->estadisticas_model->get_general()
      );
      $this->load->view('estadisticas/general',$data);*/
    break;
   }
 }

 public function descargar($tipo='ordenes'){
   $this->load->model('estadisticas_model');

   switch($tipo){
     case 'ordenes':
     $resultado = $this->estadisticas_model->get_ordenes();
     $this->down_orden($resultado,$tipo);
     break;

     case 'cotizaciones':
     $resultado = $this->estadisticas_model->get_cotizaciones();

     $this->down_cotizacion($resultado,$tipo);

     break;

     default:
     $resultado = $this->estadisticas_model->get_ordenes();
     $this->down_orden($resultado,$tipo);

     break;
   }

}
public function down_orden($resultado,$tipo){
  $registros = count ($resultado);
  if ($registros > 0) {
 $this->load->library('excel');
 $objPHPExcel = new PHPExcel();
 //Informacion del excel
 $objPHPExcel->
  getProperties()
      ->setCreator("Popusa")
      ->setLastModifiedBy("Popusa")
      ->setTitle($tipo)
      ->setSubject($tipo)
      ->setDescription($tipo)
      ->setKeywords($tipo)
      ->setCategory($tipo);
     $i = 2;

     $objPHPExcel->setActiveSheetIndex(0)
           ->setCellValue("A1", "Día")
           ->setCellValue("B1", "Hora")
           ->setCellValue("C1", "Cantidad")
           ->setCellValue("D1", "Código")
           ->setCellValue("E1", "Descripción")
           ->setCellValue("F1", "Precio")
           ->setCellValue("G1", "Estado")
           ->setCellValue("H1", "C.P.")
           ->setCellValue("I1", "Folio")
           ->setCellValue("J1",'Vendedor')
           ->setCellValue("K1", "Cliente")
           ->setCellValue("L1", "Curp");

     foreach($resultado as $res){
       if($tipo=='ordenes'){
         $precio = $res['precio'];
         $vendedor = $res['vendedor'];
       }else{
         $precio = "N/A";
         $vendedor = "N/A";
       }
         $objPHPExcel->setActiveSheetIndex(0)
               ->setCellValue("A".$i, explode(' ',$res['fecha'])[0])
               ->setCellValue("B".$i, explode(' ',$res['fecha'])[1])
               ->setCellValue("C".$i, $res['cantidad'])
               ->setCellValue("D".$i, $res['codigo_producto'])
               ->setCellValue("E".$i, $res['desc_producto'])
               ->setCellValue("F".$i, $precio)
               ->setCellValue("G".$i, $res['estado'])
               ->setCellValue("H".$i, $res['codigo_postal'])
               ->setCellValue("I".$i, $res['folio'])
               ->setCellValue("J".$i, $vendedor)
               ->setCellValue("K".$i, $res['cliente'])
               ->setCellValue("L".$i, $res['curp']);
        $i++;

     }
     header('Content-Type: application/vnd.ms-excel');
     header('Content-Disposition: attachment;filename="Ordenes_'.date("F j, Y, g:i a").'.xlsx"');
     header('Cache-Control: max-age=0');
     $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
     $objWriter->save('php://output');
     exit;
}
}
public function down_cotizacion($resultado,$tipo){
  $registros = count ($resultado);
  if ($registros > 0) {
 $this->load->library('excel');
 $objPHPExcel = new PHPExcel();
 //Informacion del excel
 $objPHPExcel->
  getProperties()
      ->setCreator("Popusa")
      ->setLastModifiedBy("Popusa")
      ->setTitle($tipo)
      ->setSubject($tipo)
      ->setDescription($tipo)
      ->setKeywords($tipo)
      ->setCategory($tipo);
     $i = 2;

     $objPHPExcel->setActiveSheetIndex(0)
           ->setCellValue("A1", "Día")
           ->setCellValue("B1", "Hora")
           ->setCellValue("C1", "Cantidad")
           ->setCellValue("D1", "Código")
           ->setCellValue("E1", "Descripción")
           //->setCellValue("E1", "Precio")
           ->setCellValue("F1", "Estado")
           ->setCellValue("G1", "C.P.")
           ->setCellValue("H1", "Folio")
           ->setCellValue("I1",'Vendedor')
           ->setCellValue("J1", "Cliente")
           ->setCellValue("K1", "Curp");

     foreach($resultado as $res){
       if($tipo=='ordenes'){
         //$precio = $res['precio'];
         $vendedor = $res['vendedor'];
       }else{
         $precio = "N/A";
         $vendedor = "N/A";
       }
         $objPHPExcel->setActiveSheetIndex(0)
               ->setCellValue("A".$i, explode(' ',$res['fecha'])[0])
               ->setCellValue("B".$i, explode(' ',$res['fecha'])[1])
               ->setCellValue("C".$i, $res['cantidad'])
               ->setCellValue("D".$i, $res['codigo_producto'])
               ->setCellValue("E".$i, $res['desc_producto'])
               //->setCellValue("E".$i, $precio)
               ->setCellValue("F".$i, $res['estado'])
               ->setCellValue("G".$i, $res['codigo_postal'])
               ->setCellValue("H".$i, $res['folio'])
               ->setCellValue("I".$i, $vendedor)
               ->setCellValue("J".$i, $res['cliente'])
               ->setCellValue("K".$i, $res['curp']);
        $i++;

     }
     header('Content-Type: application/vnd.ms-excel');
     header('Content-Disposition: attachment;filename="Cotizaciones_'.date("F j, Y, g:i a").'.xlsx"');
     header('Cache-Control: max-age=0');
     $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
     $objWriter->save('php://output');
     exit;
}
}

}

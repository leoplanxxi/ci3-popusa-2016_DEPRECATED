<?php

Class Admin extends MY_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata("logged_in")) {
            redirect("login");
        }
    }

    public function index() {
		/* 1. Telemarketing, 2. Vendedor, 3. Administrador, 4. Gerente Venta, 5. Admin Venta, 6. Distribuidor */
    //var_dump($this->session->userdata('popusa'));
        if (!$this->session->userdata("logged_in")) {
            redirect("login");
        }

		$this->jobCotizacionesVencidas();

        $idrol = $this->session->userdata("logged_in")["rol_idrol"];

        if ($idrol == 1) {
            $this->load->model("asesor_model");
            $this->load->model("cliente_model");
			$this->load->model("cotizadorCore_model");

            $data["asesores"] = $this->asesor_model->getAllAsesoresActivos();
            $data["clientes"] = $this->cliente_model->getClientesNuevos();
            $data["totales"] = $this->cliente_model->getAllClientes();
			$idusuario = $this->session->userdata("logged_in")["idusuario"];
            $idAsesor = $this->asesor_model->getAsesorLogin($idusuario)["idasesor"];
            $cotizaciones_nuevas = $this->cotizadorCore_model->getAllCotizacionesNuevoAsesor($idusuario);
			$data["cotizaciones_nuevas"] = count($cotizaciones_nuevas);

            $this->load->view("admin/index_telemarketing", $data);
        } else if ($idrol == 2) {
            $this->load->model("cliente_model");
            $this->load->model("asesor_model");
            $this->load->model("cotizadorCore_model");
            $idusuario = $this->session->userdata("logged_in")["idusuario"];
            $idAsesor = $this->asesor_model->getAsesorLogin($idusuario)["idasesor"];
            $cotizaciones_nuevas = $this->cotizadorCore_model->getAllCotizacionesNuevoAsesor($idusuario);
            $data["clientes"] = $this->cliente_model->getAllClientesAsesor($idusuario);
            $data["cotizaciones_nuevas"] = count($cotizaciones_nuevas);
            $this->load->view("admin/index_vendedor", $data);
        } else if ($idrol == 3) {
          $this->load->view("admin/index_admin");
        } else if ($idrol == 4) {
            $this->load->model("cotizadorCore_model");
            $this->load->model("asesor_model");
			$idusuario = $this->session->userdata("logged_in")["idusuario"];
			$idAsesor = $this->asesor_model->getAsesorLogin($idusuario)["idasesor"];
            $ordenes = $this->cotizadorCore_model->getAllCotizacionesNuevoAsesor($idusuario);
            $asesores = $this->asesor_model->getAllAsesoresActivos();
            $data["cotizaciones_nuevas"] = $ordenes;
            $data["asesores"] = $asesores;
            $this->load->view("admin/index_gerente_venta", $data);
        } else if ($idrol == 5) {
            $this->load->model("cotizadorCore_model");

            $ordenes = $this->cotizadorCore_model->getAllOrdenesNuevas_adminVenta();
            $data["ordenes"] = $ordenes;
            $this->load->view("admin/index_admin_venta", $data);
        }
        else if ($idrol == 6) {
            $this->load->view("admin/index_distr");
        }
    }

	    public function jobCotizacionesVencidas() {
			$this->load->model("log_model");

		   // $query = "SELECT * FROM cotizaciones WHERE caducidad <= NOW() AND (estado != 2  OR estado != 3)";
			$query = "SELECT * FROM cotizaciones WHERE caducidad <= NOW() AND (estado != 2  AND estado != 3)";

			$resultado = $this->db->query($query);
			$resultado = $resultado->result_array();

			foreach ($resultado as $cotizacion) {
				$query = $this->db->query("UPDATE cotizaciones SET estado = 3 WHERE idcotizaciones = ?", array($cotizacion["idcotizaciones"]));
				$query = $this->db->query("UPDATE cotizaciones SET razones = 'Cotización vencida el día " . $cotizacion["caducidad"] . "' WHERE idcotizaciones = ?", array($cotizacion["idcotizaciones"]));

				 //Guardar en log
				 $args_log = array(
					'folio' => $cotizacion["idcotizaciones"],
				);

				$this->log_model->insertMsgLog("f", $args_log);
			}
    }

}

?>

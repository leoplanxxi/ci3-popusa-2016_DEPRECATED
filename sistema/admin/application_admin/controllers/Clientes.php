<?php
	Class Clientes extends MY_Controller {
		public function __construct() {
			parent::__construct();
			if (!$this->session->userdata("logged_in")) {
				redirect("login");
			}
		}
		
		public function index() {
            $rol = $this->session->userdata("logged_in")["rol_idrol"];
			$this->load->model("cliente_model");
			$this->load->model("asesor_model");
			$idusuario = $this->session->userdata("logged_in")["idusuario"];

			$idAsesor = $this->asesor_model->getAsesorLogin($idusuario)["idasesor"];

			$data["idVendedor"] = $idusuario;
			
			$data["clientes"] = $this->cliente_model->getAllClientesAsesor($idAsesor);

			$this->load->view("clientes/clientes_vendedor", $data);
		}

        public function info($idCliente) {
            $this->load->model("cliente_model");
            $this->load->model("asesor_model");
            $this->load->library("form_validation");

            $data["cliente"] = $this->cliente_model->getClienteID($idCliente);

            $this->form_validation->set_rules("contacto", "Contacto", "required");
           // $this->form_validation->set_rules("rfc", "RFC", "regex_match[/^[A-Z]{4}([0-9]{2})(1[0-2]|0[1-9])([0-3][0-9])([ -]?)([A-Z0-9]{4})$/]");
			//$this->form_validation->set_rules("rfc", "RFC", "exact_length[13]");
			$this->form_validation->set_rules("rfc", "RFC", "required");
            $this->form_validation->set_rules("tels", "Teléfonos", "required");
            $this->form_validation->set_rules("email", "Email", "required|valid_email");
            if ($this->form_validation->run() === FALSE) {
                $this->load->view("clientes/clientes_detalles", $data);
            }
            else {
                $idcliente = $this->input->post("idcliente");  
                $this->cliente_model->updateCliente($idcliente);
				
				$idVendedor = $this->cliente_model->getClienteID($idcliente)["asesor_idasesor"];

				$json_infocliente = $this->cliente_model->datos_cliente_json($idcliente);
				$this->cliente_model->updateDatosClienteJSON($idcliente, $json_infocliente);
                redirect("vendedores/clientes/" . $idVendedor); 
            }
            
            
        }
		
		public function nuevos() {
			$this->load->model("cliente_model");
			
			$data["clientes"] = $this->cliente_model->getClientesNuevos();
			
			$this->load->view("clientes/clientes_nuevos_telemarketing", $data);
		}
		
		public function asignar_vendedor($idCliente) {
			$this->load->library("form_validation");
			$this->load->library("helper");
			$this->load->model("asesor_model");
			$this->load->model("cliente_model");
			$this->load->model("log_model");
			
			$data["asesores"] = $this->asesor_model->getAllAsesoresActivos();
			$data["idcliente"] = $idCliente;
			
			$this->form_validation->set_rules("idcliente", "ID Cliente", "required");
			$this->form_validation->set_rules("asesor_idasesor", "Asesor", "required");
			
			if ($this->form_validation->run() === FALSE) {
				$this->load->view("clientes/asignar_vendedor_telemarketing", $data);
			}
			else {
				$idCliente = $this->input->post("idcliente");
				$asesor_idAsesor = $this->input->post("asesor_idasesor");
				$asesor_login = $this->db->query("SELECT login_idlogin FROM asesor WHERE idasesor = ?", array($asesor_idAsesor))->result_array()[0]["login_idlogin"];
				$this->cliente_model->assignAsesor($idCliente);
				
				$this->db->query("UPDATE cotizaciones SET asesor_idasesor = ?, asesor_login = ? WHERE cliente_idcliente = ?", array($asesor_idAsesor, $asesor_login, $idCliente)); // REVISAR ESTO
				
				//Guardar en log
				$datos_cliente = $this->cliente_model->getClienteID($idCliente);
				$datos_asesor = $this->asesor_model->getAsesorID($asesor_idAsesor);

				$args_log = array(
					'idusuario' => $this->session->userdata("logged_in")["idusuario"],
					'nombre' => $this->session->userdata("logged_in")["nombre"],
					'idusuario_cl' => $idCliente,
					'nombre_cl' => $datos_cliente["contacto"],
					'idusuario_ven' => $asesor_idAsesor,
					'nombre_ven' => $datos_asesor["nombre"],
				);
			
				$this->log_model->insertMsgLog("c", $args_log);

				//Notificar a vendedor
				$correo_asesor = $this->db->query("SELECT datos FROM asesor WHERE idasesor = ?", array($asesor_idAsesor))->result_array()[0]["datos"];
				$correo_asesor = explode("/",$correo_asesor);
				$correo_asesor = $correo_asesor[1];
				$correo_asesor = str_replace(" E-mail: ", "", $correo_asesor);
				$this->helper->enviarCorreoAdministrativo($correo_asesor, "a");
				redirect("clientes/nuevos");
			}
			
		}

		public function reasignar() {
			$this->load->library("form_validation");
			$this->load->model("cliente_model");
			$this->load->model("asesor_model");
			$this->load->model("log_model");
			$this->load->library("helper");

			$this->form_validation->set_rules("clientes", "Cliente", "required");
			$this->form_validation->set_rules("asesores", "Asesores", "required");

			if ($this->form_validation->run() === FALSE) {
				$this->load->view("clientes/reasignar_vendedor");
			}
			else {
				$cliente_idcliente = $this->secclean->limpiar($this->input->post("clientes"));
				$asesor_idasesor = $this->secclean->limpiar($this->input->post("asesores"));
				
				$this->cliente_model->reassignAsesor($cliente_idcliente, $asesor_idasesor);
				
				// Guardar en log

				$idCliente = $cliente_idcliente;
				$asesor_idAsesor = $asesor_idasesor;

				$datos_cliente = $this->cliente_model->getClienteID($idCliente);
				$datos_asesor = $this->asesor_model->getAsesorID($asesor_idAsesor);

				$args_log = array(
					'idusuario' => $this->session->userdata("logged_in")["idusuario"],
					'nombre' => $this->session->userdata("logged_in")["nombre"],
					'idusuario_cl' => $idCliente,
					'nombre_cl' => $datos_cliente["contacto"],
					'idusuario_ven' => $asesor_idAsesor,
					'nombre_ven' => $datos_asesor["nombre"],
				);
			
				$this->log_model->insertMsgLog("c", $args_log);
				$this->session->set_flashdata("mensaje", "Se asignó el vendedor " . $args_log["nombre_ven"] . " al cliente " . $args_log["nombre_cl"]);

				//Notificar a vendedor
				$correo_asesor = $this->db->query("SELECT datos FROM asesor WHERE idasesor = ?", array($asesor_idAsesor))->result_array()[0]["datos"];
				$correo_asesor = explode("/",$correo_asesor);
				$correo_asesor = $correo_asesor[1];
				$correo_asesor = str_replace(" E-mail: ", "", $correo_asesor);
				$this->helper->enviarCorreoAdministrativo($correo_asesor, "a");
				redirect("clientes/reasignar");
			}
		}
		
		public function cotizacionesNuevas($idCliente) {
			$this->load->model("cotizadorCore_model");
			
			$data["idCliente"] = $idCliente;
			$data["vistaClientes"] = true;
			$data["cotizaciones"] = $this->cotizadorCore_model->getAllCotizacionesNuevasCliente($idCliente);

			$this->load->view("cotizaciones/cotizaciones_nuevas_vendedor", $data);			
		}
		
		public function cotizacionesProceso($idCliente) {
			$this->load->model("cotizadorCore_model");
			
			$data["idCliente"] = $idCliente;
			$data["vistaClientes"] = true;
			$data["cotizaciones"] = $this->cotizadorCore_model->getAllCotizacionesProcesoCliente($idCliente);

			$this->load->view("cotizaciones/cotizaciones_proceso_vendedor", $data);
		}
		
		public function cotizacionesRechazadas($idCliente) {
			$this->load->model("cotizadorCore_model");
			
			$data["idCliente"] = $idCliente;
			$data["vistaClientes"] = true;
			$data["cotizaciones"] = $this->cotizadorCore_model->getAllCotizacionesRechazadasCliente($idCliente);

			$this->load->view("cotizaciones/cotizaciones_rechazadas_vendedor", $data);
		}
		
		public function ordenes($idCliente) {
			$this->load->model("cotizadorCore_model");
			
			$data["idCliente"] = $idCliente;
			$data["vistaClientes"] = true;
			$data["cotizaciones"] = $this->cotizadorCore_model->getAllOrdenesCliente($idCliente);

			$this->load->view("cotizaciones/cotizaciones_proceso_vendedor", $data);
        }
        
        // Vistas administrador

        public function admin() {
            $this->load->model("cliente_model");

            $this->load->view("admin_views/clientes_view.php");
        }

        public function editar_admin($idCliente) {
			error_reporting(0); // REVISAR ESTO !!!
			
            $this->load->model("cliente_model");
            $this->load->model("asesor_model");
            $this->load->library("form_validation");
 
            $data["cliente"] = $this->cliente_model->getClienteID($idCliente);
			
			$asesor_idasesor = $data["cliente"]["asesor_idasesor"];
			$nombre_asesor = $this->asesor_model->getAsesorID($asesor_idasesor);
			if ($nombre_asesor["nombre"] == NULL) {
				$nombre_asesor = "No tiene asignado vendedor";
			}
			else {
				$nombre_asesor = $nombre_asesor["nombre"];
			}
			$data["asesor"] = $nombre_asesor;

            $this->form_validation->set_rules("contacto", "Contacto", "required");
            $this->form_validation->set_rules("rfc", "RFC", "required");
            $this->form_validation->set_rules("tels", "Teléfonos", "required");
            $this->form_validation->set_rules("email", "Email", "required|valid_email");
            if ($this->form_validation->run() === FALSE) {
                $this->load->view("admin_views/clientes_editar_view", $data);
            }
            else {
				$idcliente = $this->input->post("idcliente");  
				
				$email_old = $this->db->query("SELECT email FROM cliente WHERE idcliente = ?", array($idcliente))->result_array()[0]["email"];
				$email_new = $this->input->post("email");

                $this->cliente_model->updateCliente($idcliente);
				
				$this->cliente_model->updateClienteHVEmail($email_old, $email_new);
				$json_infocliente = $this->cliente_model->datos_cliente_json($idcliente);
				$this->cliente_model->updateDatosClienteJSON($idcliente, $json_infocliente);

				$idVendedor = $this->cliente_model->getClienteID($idcliente)["asesor_idasesor"];


                redirect("clientes/admin/"); 
            }
            
            
        }
		
		public function cambiar_asesor_admin($idCliente) {
			error_reporting(0); // REVISAR ESTO !!!
			$this->load->model("cliente_model");
			$this->load->model("asesor_model");
			$this->load->model("log_model");
			
			$this->load->library("form_validation");
			
			$nombre_asesor = $this->asesor_model->getAsesorID($this->cliente_model->getClienteID($idCliente)["asesor_idasesor"]);
			
			if ($nombre_asesor == NULL) {
				$nombre_asesor = "ninguno";
			}
			else {
				$nombre_asesor = $nombre_asesor["nombre"];
			}
			
			$this->form_validation->set_rules("idcliente", "ID Cliente", "required");
			$this->form_validation->set_rules("asesor_idasesor", "Asesor", "required");
			
			if ($this->form_validation->run()===FALSE) {
				$data["cliente"] = $this->cliente_model->getClienteID($idCliente)["contacto"];
				$data["idcliente"] = $idCliente;			
				$data["asesor"] = $nombre_asesor;
				$data["asesores"] = $this->asesor_model->getAllAsesoresActivos();
				$this->load->view("admin_views/clientes_asignar_asesor", $data);			
			}
			else {
				$idCliente = $this->input->post("idcliente");
				$asesor_idAsesor = $this->input->post("asesor_idasesor");
				$this->cliente_model->assignAsesor($idCliente);
				
				$this->db->query("UPDATE cotizaciones SET asesor_idasesor = ? WHERE cliente_idcliente = ?", array($asesor_idAsesor, $idCliente)); // REVISAR ESTO
				
				//Guardar en log
				$datos_cliente = $this->cliente_model->getClienteID($idCliente);
				$datos_asesor = $this->asesor_model->getAsesorID($asesor_idAsesor);

				$args_log = array(
					'idusuario' => $this->session->userdata("logged_in")["idusuario"],
					'nombre' => $this->session->userdata("logged_in")["nombre"],
					'idusuario_cl' => $idCliente,
					'nombre_cl' => $datos_cliente["contacto"],
					'idusuario_ven' => $asesor_idAsesor,
					'nombre_ven' => $datos_asesor["nombre"],
				);
			
				$this->log_model->insertMsgLog("c", $args_log);
				
				$this->session->set_flashdata("mensaje", "Se asignó el vendedor " . $args_log["nombre_ven"] . " al cliente " . $args_log["nombre_cl"]);
				
				redirect("clientes/admin");

			}

		}

		public function eliminar_admin($id) {
			$this->load->model("cliente_model");
			$this->cliente_model->removeCliente($id);

			$this->session->set_flashdata("Mensaje", "Se eliminó correctamente el usuario con identificador " + $id);

			redirect("clientes/admin");

		}
	}
?>
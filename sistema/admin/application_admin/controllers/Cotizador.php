<?php

Class Cotizador extends CI_Controller {

    public function __construct() {
        parent::__construct();
        header('Content-Type: text/html; charset=utf-8');

        /*if (!$this->session->userdata("logged_in")) {
            redirect("login");
        }*/
    }

    public function index($idCotizacion) {

        $this->load->model("cotizadorCore_model");
        $this->load->model("cliente_model");
        $this->load->helper("file");

        $json_in = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0]["json_in"];
        $json_out = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0]["json_out"];

        $cliente = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0]["cliente_idcliente"];
		//??$this->cotizadorCore_model->imprintDataCliente($idCotizacion, $cliente);

        $caducidad = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0]["caducidad"];
       // $imprintAsesor = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0]["asesor"];

		$this->cotizadorCore_model->setFolioDos($idCotizacion);
        if ($caducidad == NULL) {
            $this->_cotizadorCore_caducidad($idCotizacion, 0);
        }

        if ($json_out == NULL || $json_out == "upd" || $json_out == "rec") {

            // Paso 1. Convertir JSON en arreglo utilizable por PHP
            $json = json_decode($json_in, true);

            // Paso 2. Recorrer los primeros niveles del arreglo para obtener sus datos

            $array_prods = array();
            $array_salida = array();

            foreach ($json as $key => $value) {
                $temp_key = $key;
                array_push($array_prods, $temp_key);
                unset($temp_key);
            }

            // Paso 3. Obtener las (SUB) familias y marcas de los productos

            foreach ($array_prods as $prod_cod) {
                $array_prod_marcas[$prod_cod]["codigo"] = $prod_cod;
                $array_prod_marcas[$prod_cod]["marca"] = $this->cotizadorCore_model->getProductoInfoCod($prod_cod)["marca"];
                $array_prod_marcas[$prod_cod]["familia"] = $this->cotizadorCore_model->getProductoInfoCod($prod_cod)["familia"];
                $array_prod_marcas[$prod_cod]["subfamilia"] = $this->cotizadorCore_model->getProductoInfoCod($prod_cod)["subfamilia"];
            }

            // Paso 4. Obtener precios de los productos (solo precios), depende del rol del usuario para tomar P0, P1, P2 o P3

            foreach ($array_prod_marcas as $producto) {
                $codigo_prod = $producto["codigo"];
                $unidad = $json[$codigo_prod]["unidad"];
                $precio_base = $json[$producto["codigo"]]["precio_cama"];
                $array_prod_marcas[$codigo_prod]["unidad"] = $unidad;
                $array_prod_marcas[$producto["codigo"]]["precio_cama"] = $precio_base;
                $p0 = $this->_cotizadorCore_precioUnidad($producto["codigo"], $unidad);

                $multiplicacion = $this->_cotizadorCore_multiplicacionFactor($precio_base, $producto["marca"]);

                $precio = round($multiplicacion * $p0, 2);
                $array_prod_marcas[$producto["codigo"]]["precio_unitario_siva"] = $precio; //ACA HAY QUE CONSIDERAR U_AUX
            }

            // Paso 6. Cotizar (Aquí se consideran las variables que podrían afectar el precio final, también se considera IVA)

            foreach ($array_prod_marcas as $producto) {
                $codigo_prod = $producto["codigo"];
                if (isset($json[$codigo_prod]["cantidad"])) {
                    $cantidad = $json[$codigo_prod]["cantidad"];
                } else if (isset($json[$codigo_prod]["rollos"])) {
                    $cantidad = $json[$codigo_prod]["rollos"];
                } else {
                    $cantidad = false;
                }
                $array_prod_marcas[$codigo_prod]["cantidad"] = $cantidad;
                $array_prod_marcas[$codigo_prod]["precio_neto"] = $this->_cotizadorCore_precioNeto($producto["precio_unitario_siva"], $cantidad);
                $array_prod_marcas[$codigo_prod]["aplica_iva"] = $this->cotizadorCore_model->getIva($codigo_prod);
                $array_prod_marcas[$codigo_prod]["precio_con_iva"] = $this->_cotizadorCore_calcularIva($array_prod_marcas[$codigo_prod]["precio_neto"], $codigo_prod);
                $array_prod_marcas[$codigo_prod]["valor_iva"] = $array_prod_marcas[$codigo_prod]["precio_con_iva"] - $array_prod_marcas[$codigo_prod]["precio_neto"];
                $array_prod_marcas[$codigo_prod]["entrega"] = $this->cotizadorCore_model->getTiempoEntrega($codigo_prod);
                $array_prod_marcas[$codigo_prod]["descripcion"] = $this->cotizadorCore_model->getDescripcionesPDF($codigo_prod)["descotizador"];
                $array_prod_marcas[$codigo_prod]["descripcion_especial"] = $this->cotizadorCore_model->getDescripcionesPDF($codigo_prod)["desespecial"];
                $array_prod_marcas[$codigo_prod]["pdf"] = $this->cotizadorCore_model->getDescripcionesPDF($codigo_prod)["pdf"];
            }

            foreach ($json as $key => $value) {
                $codigo_prod = $key;

                if (isset($value["parametrosExtra"])) {
                    $array_prod_marcas[$codigo_prod]["parametrosExtra"] = $json[$codigo_prod]["parametrosExtra"];
                } else {
                    //No hacer nada
                }
            }

            foreach ($array_prod_marcas as $producto) {
                $codigo_prod = $producto["codigo"];

                // Para calcular precio costura la formula es Pc = (Largo*No.Uniones)*P(x)
                // Para calcular precio ojillos la formula es Po = (No.Ojiloos * P(x))
                if (isset($producto["parametrosExtra"])) {
                    $pars_extra = $producto["parametrosExtra"];

                    if (isset($pars_extra["costura"])) {
                        if ($pars_extra["costura"] == "SI") {
                            $codigo_costura = $this->cotizadorCore_model->getCosturaCodigo($codigo_prod);
                            $precio_base_costura = $pars_extra["costura_p"];
                            $largo = $pars_extra["largo"];
                            $uniones = $pars_extra["uniones"];
                            $ancho = $pars_extra["ancho"]; // ¿Incluir rollos?
                            $p0_costura = $this->cotizadorCore_model->getProductoPrecioCod($codigo_costura)["pxub"];
                            $factor_costura = $this->_cotizadorCore_multiplicacionFactor($precio_base_costura, $producto["marca"]);
                            $precio_neto_costura = round($p0_costura * $factor_costura, 2);

                            $precio_total_costura = $largo * $uniones * $precio_neto_costura;
                            $array_prod_marcas[$codigo_prod]["parametrosExtra"]["codigo_costura"] = $codigo_costura;
                            $array_prod_marcas[$codigo_prod]["parametrosExtra"]["precio_costura"] = $precio_total_costura;
                            $array_prod_marcas[$codigo_prod]["parametrosExtra"]["largo"] = $largo;
                            $array_prod_marcas[$codigo_prod]["parametrosExtra"]["uniones"] = $uniones;
                            $array_prod_marcas[$codigo_prod]["parametrosExtra"]["ancho"] = $ancho;
                            $array_prod_marcas[$codigo_prod]["parametrosExtra"]["descripcion_costura"] = $this->cotizadorCore_model->getDescripcionesPDF($codigo_costura)["descotizador"];

                        }

                    }

                    if (isset($pars_extra["ojillos"])) {
                        if ($pars_extra["ojillos"] == "SI") {
                            $numeroOjillos = $pars_extra["numeroOjillos"];
                            $codigo_ojillos = $this->cotizadorCore_model->getOjillosCodigo($codigo_prod);
                            $precio_base_ojillos = $pars_extra["ojillos_p"];
                            $p0_ojillos = $this->cotizadorCore_model->getProductoPrecioCod($codigo_ojillos)["pxub"];
                            $factor_ojillos = $this->_cotizadorCore_multiplicacionFactor($precio_base_ojillos, $producto["marca"]);
                            $precio_neto_ojillos = round($p0_ojillos * $factor_ojillos, 2);

                            $precio_total_ojillos = $numeroOjillos * $precio_neto_ojillos;
                            $array_prod_marcas[$codigo_prod]["parametrosExtra"]["codigo_ojillos"] = $codigo_ojillos;
                            $array_prod_marcas[$codigo_prod]["parametrosExtra"]["precio_ojillos"] = $precio_total_ojillos;
                            $array_prod_marcas[$codigo_prod]["parametrosExtra"]["numeroOjillos"] = $numeroOjillos;
                            $array_prod_marcas[$codigo_prod]["parametrosExtra"]["descripcion_ojillos"] = $this->cotizadorCore_model->getDescripcionesPDF($codigo_ojillos)["descotizador"];
                        }
                    }

                    if (isset($pars_extra["confeccion"])) {
                         if ($pars_extra["confeccion"] == "SI") {
                            // Se anula precio costura
                            $precio_total_costura = 0;
                            $array_prod_marcas[$codigo_prod]["parametrosExtra"]["precio_costura"] = $precio_total_costura;
                            $codigo_confeccion = $pars_extra["codigo_confeccion"];
                            $precio_base_confeccion = $pars_extra["confeccion_p"];

                            $p0_confeccion = $this->cotizadorCore_model->getProductoPrecioCod($codigo_confeccion)["pxub"];
                            $factor_confeccion = $this->_cotizadorCore_multiplicacionFactor($precio_base_confeccion, $producto["marca"]);
                            $precio_neto_confeccion = round($p0_confeccion * $factor_confeccion, 2);

                            //Sustituye precios de producto por el de confección
                            $array_prod_marcas[$codigo_prod]["precio_unitario_siva"] = $precio_neto_confeccion;

                            //Realiza cálculo de nuevo con los nuevos valores

                            $cantidad = $pars_extra["confeccion_m2"];

                            $array_prod_marcas[$codigo_prod]["parametrosExtra"]["descripcion_confeccion"] = $this->cotizadorCore_model->getDescripcionesPDF($codigo_confeccion)["descotizador"];


                            $array_prod_marcas[$codigo_prod]["cantidad"] = $cantidad;
                            $array_prod_marcas[$codigo_prod]["precio_neto"] = $this->_cotizadorCore_precioNeto($precio_neto_confeccion, $cantidad);
                            $array_prod_marcas[$codigo_prod]["aplica_iva"] = $this->cotizadorCore_model->getIva($codigo_confeccion);
                            $array_prod_marcas[$codigo_prod]["precio_con_iva"] = $this->_cotizadorCore_calcularIva($array_prod_marcas[$codigo_prod]["precio_neto"], $codigo_prod);
                            $array_prod_marcas[$codigo_prod]["valor_iva"] = $array_prod_marcas[$codigo_prod]["precio_con_iva"] - $array_prod_marcas[$codigo_prod]["precio_neto"];
                            $array_prod_marcas[$codigo_prod]["entrega"] = $this->cotizadorCore_model->getTiempoEntrega($codigo_confeccion);
                            $array_prod_marcas[$codigo_prod]["descripcion"] = $this->cotizadorCore_model->getDescripcionesPDF($codigo_prod)["descotizador"];
                            $array_prod_marcas[$codigo_prod]["descripcion_especial"] = $this->cotizadorCore_model->getDescripcionesPDF($codigo_prod)["desespecial"];
                            $array_prod_marcas[$codigo_prod]["pdf"] = $this->cotizadorCore_model->getDescripcionesPDF($codigo_prod)["pdf"];
                        }
                    }

                }

              /*  if (isset($producto["parametrosExtra"])) {

                    $pars = $producto["parametrosExtra"];
                    if (isset($pars["costura"])) {
                        if ($pars["costura"] == "SI") {
                            $codigo_costura = $this->cotizadorCore_model->getCosturaCodigo($codigo_prod);
                            $precio_base_costura = $pars["costura_p"];
                            $p0_costura = $this->cotizadorCore_model->getProductoPrecioCod($codigo_costura)["precio_siva"];
                            $factor_costura = $this->_cotizadorCore_multiplicacionFactor($precio_base_costura, $producto["marca"]);
                            $precio_neto_costura = round($p0_costura * $factor_costura, 2);

                            $precio_total_costura = $pars["largo"] * $pars["uniones"] * $precio_neto_costura;
                            $array_prod_marcas[$codigo_prod]["parametrosExtra"]["precio_costura"] = $precio_total_costura;
                            $array_prod_marcas[$codigo_prod]["parametrosExtra"]["descripcion_costura"] = $this->cotizadorCore_model->getDescripcionesPDF($codigo_costura)["descotizador"];
                        }
                    }
                    if (isset($pars["ojillos"])) {
                        if ($pars["ojillos"] == "SI") {
                            $codigo_ojillos = $this->cotizadorCore_model->getOjillosCodigo($codigo_prod);
                            $precio_base_ojillos = $pars["ojillos_p"];
                            $p0_ojillos = $this->cotizadorCore_model->getProductoPrecioCod($codigo_ojillos)["precio_siva"];
                            $factor_ojillos = $this->_cotizadorCore_multiplicacionFactor($precio_base_ojillos, $producto["marca"]);
                            $precio_neto_ojillos = round($p0_ojillos * $factor_ojillos, 2);

                            $precio_total_ojillos = $pars["numeroOjillos"] * $precio_neto_ojillos;
                            $array_prod_marcas[$codigo_prod]["parametrosExtra"]["precio_ojillos"] = $precio_total_ojillos;
                            $array_prod_marcas[$codigo_prod]["parametrosExtra"]["descripcion_ojillos"] = $this->cotizadorCore_model->getDescripcionesPDF($codigo_ojillos)["descotizador"];
                        }
                    }

                    // Falta confección
                    if (isset($pars["confeccion"])) {
                        if ($pars["confeccion"] == "SI") {
                            // Se anula precio costura
                            $precio_total_costura = 0;
                            $array_prod_marcas[$codigo_prod]["parametrosExtra"]["precio_costura"] = $precio_total_costura;
                            $codigo_confeccion = $this->cotizadorCore_model->getConfeccionCodigo($codigo_prod);
                            $precio_base_confeccion = $pars["confeccion_p"];
                            $p0_confeccion = $this->cotizadorCore_model->getProductoPrecioCod($codigo_confeccion)["precio_siva"];
                            $factor_confeccion = $this->_cotizadorCore_multiplicacionFactor($precio_base_confeccion, $producto["marca"]);
                            $precio_neto_confeccion = round($p0_confeccion * $factor_confeccion, 2);

                            //Sustituye precios de producto por el de confección
                            $array_prod_marcas[$codigo_prod]["precio_unitario_siva"] = $precio_neto_confeccion;

                            //Realiza cálculo de nuevo con los nuevos valores

                            $cantidad = $pars["confeccion_m2"];

                            $array_prod_marcas[$codigo_prod]["cantidad"] = $cantidad;
                            $array_prod_marcas[$codigo_prod]["precio_neto"] = $this->_cotizadorCore_precioNeto($producto["precio_unitario_siva"], $cantidad);
                            $array_prod_marcas[$codigo_prod]["aplica_iva"] = $this->cotizadorCore_model->getIva($codigo_prod);
                            $array_prod_marcas[$codigo_prod]["precio_con_iva"] = $this->_cotizadorCore_calcularIva($array_prod_marcas[$codigo_prod]["precio_neto"], $codigo_prod);
                            $array_prod_marcas[$codigo_prod]["valor_iva"] = $array_prod_marcas[$codigo_prod]["precio_con_iva"] - $array_prod_marcas[$codigo_prod]["precio_neto"];
                            $array_prod_marcas[$codigo_prod]["entrega"] = $this->cotizadorCore_model->getTiempoEntrega($codigo_prod);
                            $array_prod_marcas[$codigo_prod]["descripcion"] = $this->cotizadorCore_model->getDescripcionesPDF($codigo_prod)["descotizador"];
                            $array_prod_marcas[$codigo_prod]["descripcion_especial"] = $this->cotizadorCore_model->getDescripcionesPDF($codigo_prod)["desespecial"];
                            $array_prod_marcas[$codigo_prod]["pdf"] = $this->cotizadorCore_model->getDescripcionesPDF($codigo_prod)["pdf"];
                        }
                    }
                }*/
            }

            // Para estadísticas

            if ($json_out == NULL) {
              $this->load->model("estadisticas_model");

              $est_idcotizacion = $idCotizacion;
              $est_idcliente = $this->db->query("SELECT cliente_idcliente FROM cotizaciones WHERE idcotizaciones = ?", array($idCotizacion))->result_array()[0]["cliente_idcliente"];
              foreach ($array_prod_marcas as $prod) {
                $est_prodcod = $prod["codigo"];
                $est_preciocama = $prod["precio_cama"];
                $this->estadisticas_model->insert($est_prodcod, $est_preciocama, $est_idcotizacion, $est_idcliente);
              }
            }
            else if ($json_out == "upd") {
              $this->load->model("estadisticas_model");

              $est_idcotizacion = $idCotizacion;
              $est_idcliente = $this->db->query("SELECT cliente_idcliente FROM cotizaciones WHERE idcotizaciones = ?", array($idCotizacion))->result_array()[0]["cliente_idcliente"];
              foreach ($array_prod_marcas as $prod) {
                $est_prodcod = $prod["codigo"];
                $est_preciocama = $prod["precio_cama"];
                $est_idestadistica = $this->db->query("SELECT idestadisticas FROM estadisticas WHERE idproductos = ? AND idcotizacion = ?", array($est_prodcod, $est_idcotizacion))->result_array()[0]["idestadisticas"];
                $this->estadisticas_model->updateID($est_idestadistica,$est_prodcod, $est_preciocama, $est_idcotizacion, $est_idcliente);
            }
          }
            else if ($json_out == "rec") {
              $this->load->model("estadisticas_model");

              $this->estadisticas_model->delete($idCotizacion);

              $est_idcotizacion = $idCotizacion;
              $est_idcliente = $this->db->query("SELECT cliente_idcliente FROM cotizaciones WHERE idcotizaciones = ?", array($idCotizacion))->result_array()[0]["cliente_idcliente"];
              foreach ($array_prod_marcas as $prod) {
                $est_prodcod = $prod["codigo"];
                $est_preciocama = $prod["precio_cama"];
                $this->estadisticas_model->insert($est_prodcod, $est_preciocama, $est_idcotizacion, $est_idcliente);
            }
          }


            // Paso 7. Hacer cuentas para dar subtotal y total

            $array_salida = $array_prod_marcas;

            $subtotal = 0;
            $iva = 0;

            foreach ($array_prod_marcas as $producto) {
                $subtotal = $subtotal + $producto["precio_neto"];

                if (isset($producto["parametrosExtra"]["precio_costura"])) {
                    $subtotal = $subtotal + $producto["parametrosExtra"]["precio_costura"];
                }
                if (isset($producto["parametrosExtra"]["precio_ojillos"])) {
                    $subtotal = $subtotal + $producto["parametrosExtra"]["precio_ojillos"];
                }

                $iva = $iva + $producto["valor_iva"];
            }

            $array_salida["subtotal"] = round($subtotal, 2);
            $array_salida["iva"] = round($iva, 2);
            $array_salida["total"] = round($subtotal + $iva, 2);

            $json_out = json_encode($array_salida);
            $this->cotizadorCore_model->updateCotizacionCalculada($idCotizacion, $json_out);

            //Agregar datos extra
            $asesor = $this->cliente_model->getClienteID($cliente)["asesor_idasesor"];
            if($asesor){
              $asesor = $asesor;
            }else{
              $asesor = null;
            }
            $this->cotizadorCore_model->updateCotizacionAsesor($idCotizacion, $asesor);

            $this->cotizadorCore_model->imprintCotizacionAsesor($idCotizacion, $asesor);

            $this->output->set_content_type('application/json');
            $this->output->set_output($json_out);
            return $json_out;
        }
        else {
            $cotizacion = $this->cotizadorCore_model->getCotizacion($idCotizacion);
            $json_out = $cotizacion[0]["json_out"];
            $this->output->set_content_type('application/json');
            $this->output->set_output($json_out);
            return $json_out;

        }

    }

    public function reset($idCotizacion) {
        $this->load->model("cotizadorCore_model");

        $this->cotizadorCore_model->resetCotizacion($idCotizacion);

        echo "Se reinició la cotización con folio: " . $idCotizacion;
    }

    public function in($idCotizacion) {
        $this->load->model("cotizadorCore_model");
        $cotizacion_in = $this->cotizadorCore_model->getCotizacion($idCotizacion);
        $json_in = $cotizacion_in[0]["json_in"];

        $this->output->set_content_type("application/json");
        $this->output->set_output($json_in);
        return $json_in;
    }

    public function out($idCotizacion) {
        error_reporting(0); // Disable all errors. REVISAR ESTO

        $this->load->model("cotizadorCore_model");
        $this->load->model("asesor_model");
        $this->load->model("cliente_model");
        $this->load->model("configCore_model");

        $data["cotizacion"] = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0];
        $data["out"] = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0]["json_out"];
        $data["file"] = $idCotizacion;
        $data["bancos"] = $this->cotizadorCore_model->getBancosCotizador();
        $data["cliente"] = $this->cliente_model->getClienteID($data["cotizacion"]["cliente_idcliente"]);
        $data["asesor_idasesor"] = $this->asesor_model->getAsesorID($data["cliente"]["asesor_idasesor"]);
        $data["asesor"] = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0]["asesor"];
        $data["datos_cliente"] = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0]["info_cliente"];
        $data["mensaje_cliente"] = $this->configCore_model->getMensajeClienteCotizacion();

        //Algunas correcciones
        if ($data["asesor"] == NULL) {
            $asesor = $data["cliente"]["asesor_idasesor"];
            $this->cotizadorCore_model->updateCotizacionAsesor($idCotizacion, $asesor);
            $this->cotizadorCore_model->imprintCotizacionAsesor($idCotizacion, $asesor);
            $data["asesor_idasesor"] = $this->asesor_model->getAsesorID($data["cliente"]["asesor_idasesor"]);
            $data["asesor"] = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0]["asesor"];
        }
        $this->load->view("cotizador/output.php", $data);
    }

    public function lista() {
        $this->load->model("cotizadorCore_model");
        $data["cotizaciones_nuevas"] = $this->cotizadorCore_model->getAllCotizacionesNuevo();

        $this->load->view("cotizador/list", $data);
    }

    public function cambiarObservaciones($idCotizacion) {
        $this->load->model("cotizadorCore_model");
        $this->load->library("form_validation");

        $data["idcotizacion"] = $idCotizacion;

        $this->form_validation->set_rules("idcotizacion", "ID Cotizacion", "required");
        $this->form_validation->set_rules("observaciones", "Observaciones", "required");

        if ($this->form_validation->run() === FALSE) {
            $data["idcotizacion"] = $idCotizacion;
            $data["cotizacion"] = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0];

            $this->load->view("cotizador/observaciones", $data);
        } else {
            $idcotizacion = $this->secclean->limpiar($this->input->post("idcotizacion"));
            $obs = $this->secclean->limpiar($this->input->post("observaciones"));

            $this->cotizadorCore_model->putObservacionesCotizacion($idcotizacion, $obs);

            redirect("cotizador/out/" . $idcotizacion);
        }
    }

    public function cambiarDirEntrega($idCotizacion) {
        $this->load->model("cotizadorCore_model");
        $this->load->library("form_validation");

        $data["idcotizacion"] = $idCotizacion;

        $this->form_validation->set_rules("idcotizacion", "ID Cotizacion", "required");
        $this->form_validation->set_rules("dir_entrega", "Dirección Entrega", "required");

        if ($this->form_validation->run() === FALSE) {
            $data["idcotizacion"] = $idCotizacion;
            $data["cotizacion"] = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0];

            $this->load->view("cotizador/dirección_entrega", $data);
        } else {
            $idcotizacion = $this->secclean->limpiar($this->input->post("idcotizacion"));
            $dir_entrega = $this->secclean->limpiar($this->input->post("dir_entrega"));

            $this->cotizadorCore_model->setDireccionEntrega($idcotizacion, $dir_entrega);

            redirect("cotizador/out/" . $idcotizacion);
        }
    }

    public function cambiarCondiciones($idCotizacion) {
        $this->load->model("cotizadorCore_model");
        $this->load->library("form_validation");

        $data["idcotizacion"] = $idCotizacion;

        $this->form_validation->set_rules("idcotizacion", "ID Cotizacion", "required");
        $this->form_validation->set_rules("condiciones", "Condiciones", "required");

        if ($this->form_validation->run() === FALSE) {
            $data["idcotizacion"] = $idCotizacion;
            $data["cotizacion"] = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0];

            $this->load->view("cotizador/condiciones", $data);
        } else {
            $idcotizacion = $this->secclean->limpiar($this->input->post("idcotizacion"));
            $cond = $this->secclean->limpiar($this->input->post("condiciones"));

            $this->cotizadorCore_model->putCondiciones($idcotizacion, $cond);

            redirect("cotizador/out/" . $idcotizacion);
        }
    }

    public function cambiarPrecio($idcotizacion, $producto) {
        // Faltan varios casos especiales
        $this->load->model("cotizadorCore_model");
        $this->load->library("form_validation");
        $this->load->model("log_model");

        error_reporting(0);

        $json_in = $this->cotizadorCore_model->getCotizacion($idcotizacion)[0]["json_in"];
        $json_out = $this->cotizadorCore_model->getCotizacion($idcotizacion)[0]["json_out"];

        $json = json_decode($json_out, true);

        $pars_extra = $json[$producto]["parametrosExtra"];

        if ($pars_extra["confeccion"] == "SI") {
            //Se toman valores confección
            $p_entrada = $json[$producto]["parametrosExtra"]["confeccion_p"];
            $unidad = "M2";
            $descripcion = $json[$producto]["parametrosExtra"]["descripcion_confeccion"];
        }
        else {
            $p_entrada = $json[$producto]["precio_cama"];
            $unidad = $json[$producto]["unidad"];
            $descripcion = $json[$producto]["descripcion"];

        }
        $marca = $json[$producto]["marca"];

        if ($pars_extra["confeccion"] == "SI") {
            $prod_aux = $producto;
            $producto = $json[$producto]["parametrosExtra"]["codigo_confeccion"];
        }
        $factor = $this->_cotizadorCore_multiplicacionFactor($p_entrada, $marca);
        $precio = $this->cotizadorCore_model->getProductoPrecioCodArray($producto);
        $precio = $precio[$unidad];
        $precio_calculado_ = round($factor * $precio, 2);


        $precios_alternativos = array();

       // $idasesor = $this->db->query("SELECT idasesor FROM asesor WHERE login_idlogin = ?", array($this->session->userdata("logged_in")["idusuario"]))->result_array()[0]["idasesor"]; // REVISAR ESTO si es del asesor o del usuario logueado

        $idasesor = $this->session->userdata("logged_in")["idusuario"];
        $precios_validos_db = $this->db->query("SELECT precios FROM asesor WHERE login_idlogin = ?", array($idasesor))->result_array()[0]["precios"];
        $precios_validos = explode(",",$precios_validos_db);

        for ($i = 0; $i <= 3; $i++) {
            $factor = $this->_cotizadorCore_multiplicacionFactor($i, $marca);
            $precio_calculado = round($factor * $precio, 2);
            $precios_alternativos[$i]["p"] = $i;
            $precios_alternativos[$i]["precio"] = $precio_calculado;

            if (in_array("P" . $i, $precios_validos)) {
              $precios_alternativos[$i]["valido"] = 1;
            }
            else {
              $precios_alternativos[$i]["valido"] = 0;
            }
        }

        if (in_array("PL", $precios_validos)) {
            $factor = $this->_cotizadorCore_multiplicacionFactor(3, $marca);
            $precio_calculado = round($factor * $precio, 2);
            $precios_alternativos[3]["p"] = 3;
            $precios_alternativos[3]["precio"] = $precio_calculado;
            $precios_alternativos[3]["valido"] = 1;
        }

        #var_dump($precios_alternativos);

        $data["idcotizacion"] = $idcotizacion;
        $data["producto"] = $producto;

        $data["precios_alternativos"] = $precios_alternativos;
        $data["p_entrada"] = $p_entrada;
        $data["precio_calculado"] = $precio_calculado_;
        $data["descripcion"] = $descripcion;
        $data["parametrosExtra"] = $pars_extra;

        if ($pars_extra["confeccion"] == "SI") {
            $data["producto_aux"] = $prod_aux;
        }

        $this->form_validation->set_rules("producto", "Producto", "required");
        $this->form_validation->set_rules("precio_alternativo", "Precio alternativo", "required");
        if ($this->form_validation->run() === FALSE) {
            $this->load->view("cotizador/cambiar", $data);
        } else {
            $idcotizacion = $this->input->post("idcotizacion");
            $p_out = $this->input->post("precio_alternativo");
            $producto = $this->input->post("producto");
            $prod_aux = $this->input->post("producto_aux");
            $json_in = $this->cotizadorCore_model->getCotizacion($idcotizacion)[0]["json_in"];
            //$contador_modificaciones = $this->cotizadorCore_model->getCotizacion($idcotizacion)[0]["modificaciones"]; // HAY QUE MODIFICAR ESTA PARTE

            //$this->cotizadorCore_model->resetCotizacion($idcotizacion);

            $json_ = json_decode($json_in, true);

            if ($prod_aux != NULL) {
                if ($json_[$prod_aux]["parametrosExtra"]["confeccion"] == "SI") {
                    $json_[$prod_aux]["parametrosExtra"]["confeccion_p"] = $p_out;
                }
            }
            else {
                $json_[$producto]["precio_cama"] = $p_out;
            }


            $json_out = json_encode($json_);

            $this->cotizadorCore_model->updateCotizacionPrecio($idcotizacion, $json_out);

            //Guardar en log
            $args_log = array(
                'idusuario' => $this->session->userdata("logged_in")["idusuario"],
                'nombre' => $this->session->userdata("logged_in")["nombre"],
                'folio' => $idcotizacion,
            );

            $this->log_model->insertMsgLog("d", $args_log);
        //  $this->cotizadorCore_model->updateCotizacionContadorModif($idcotizacion, $contador_modificaciones + 1);
          //  $this->_cotizadorCore_caducidad($idcotizacion, 1);
            redirect("cotizador/out/" . $idcotizacion);
        }
    }

	// Falta hacer Cambiar Precio para Ojillos y Costura.

    public function obtenerFamiliasAlianza() {
        $this->load->model("cotizadorCore_model");
        $resultado = $this->cotizadorCore_model->getAllFamiliasAlianza();
        $json_out = json_encode($resultado);
        $this->output->set_content_type('application/json');
        $this->output->set_output($json_out);
        return $resultado;
    }

    public function obtenerSubFamiliasPaket() {
        $this->load->model("cotizadorCore_model");
        $resultado = $this->cotizadorCore_model->getAllSubFamiliasPaket();
        $json_out = json_encode($resultado);
        $this->output->set_content_type('application/json');
        $this->output->set_output($json_out);
        return $resultado;
    }

    public function ordenDistribuidor() {
        $this->load->model("cotizadorCore_model");

        $folio = $this->secclean->limpiar($this->input->post("folio"));
        $idDistribuidor = $this->secclean->limpiar($this->input->post("idDistribuidor"));
        $md5Distribuidor = $this->secclean->limpiar($this->input->post("md5Distribuidor"));

        if (md5($idDistribuidor) == $md5Distribuidor) {

            $this->db->where("login_idlogin", $idDistribuidor);
            $asesor = $this->db->get("asesor")->result_array()[0];
            $this->db->reset_query();

            $nombre_asesor = $asesor["nombre"];
            $datos_asesor = $asesor["datos"];
            $folio_prefix = $asesor["login_idlogin"];
            $idasesor = $asesor["idasesor"];

            $imprint_asesor = $nombre_asesor . "<br>" . $datos_asesor;

            $data = array(
                "asesor_idasesor" => $idasesor,
                "asesor" => $imprint_asesor,
                "folio02" => $folio_prefix . "-" . $folio,
            );

            $this->db->where("idcotizaciones", $folio);
            $this->db->update("cotizaciones", $data);
            $this->cotizadorCore_model->cambiarEstadoCotizacion($folio, 2);

            $response = array(
                "respuesta" => "ok",
            );

        }
        else {
            $response = array(
                "respuesta" => "no coincide",
            );
        }
        header('Content-Type: application/json');
        echo json_encode( $response );
    }

    public function _cotizadorCore_precioUnidad($codigo, $unidad) {
        $this->load->model("cotizadorCore_model");
        $arreglo_precios = $this->cotizadorCore_model->getProductoPrecioCodArray($codigo);

        return $arreglo_precios[$unidad];
    }

    public function _cotizadorCore_multiplicacionFactor($n, $marca) {
        $this->load->model("cotizadorCore_model");

        if ($n == 0) {
            return 1;
        }
        if ($marca == "Alianza") {
            if ($n == 1) {
                $factor = "Factor_P1_Alianza";
                $factor_ = $this->cotizadorCore_model->getFactor($factor);
                return 1 * $factor_;
            } elseif ($n == 2) {
                $factor_p1 = "Factor_P1_Alianza";
                $factor_p1_ = $this->cotizadorCore_model->getFactor($factor_p1);
                $factor_p2 = "Factor_P2_Alianza";
                $factor_p2_ = $this->cotizadorCore_model->getFactor($factor_p2);
                return 1 * $factor_p1_ * $factor_p2_;
            } elseif ($n == 3) {
                $factor_p1 = "Factor_P1_Alianza";
                $factor_p1_ = $this->cotizadorCore_model->getFactor($factor_p1);
                $factor_p2 = "Factor_P2_Alianza";
                $factor_p2_ = $this->cotizadorCore_model->getFactor($factor_p2);
                $factor_p3 = "Factor_P3_Alianza";
                $factor_p3_ = $this->cotizadorCore_model->getFactor($factor_p3);
                return 1 * $factor_p1_ * $factor_p2_ * $factor_p3_;
            } else {
                return false;
            }
        } else if ($marca == "Paket") {
            if ($n == 1) {
                $factor = "Factor_P1_Paket";
                $factor_ = $this->cotizadorCore_model->getFactor($factor);
                return 1 * $factor_;
            } elseif ($n == 2) {
                $factor_p1 = "Factor_P1_Paket";
                $factor_p1_ = $this->cotizadorCore_model->getFactor($factor_p1);
                $factor_p2 = "Factor_P2_Paket";
                $factor_p2_ = $this->cotizadorCore_model->getFactor($factor_p2);
                return 1 * $factor_p1_ * $factor_p2_;
            } elseif ($n == 3) {
                $factor_p1 = "Factor_P1_Paket";
                $factor_p1_ = $this->cotizadorCore_model->getFactor($factor_p1);
                $factor_p2 = "Factor_P2_Paket";
                $factor_p2_ = $this->cotizadorCore_model->getFactor($factor_p2);
                $factor_p3 = "Factor_P3_Paket";
                $factor_p3_ = $this->cotizadorCore_model->getFactor($factor_p3);
                return 1 * $factor_p1_ * $factor_p2_ * $factor_p3_;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function _cotizadorCore_precioNeto($precioUnidad, $cantidad) {
        return $precioUnidad * $cantidad;
    }

    function _cotizadorCore_calcularIva($precioNeto, $codigo) {
        $this->load->model("cotizadorCore_model");

        $factor_iva = $this->cotizadorCore_model->getFactorIVA();

        if ($this->cotizadorCore_model->getIva($codigo) == "Si") {
            return $precioNeto * $factor_iva;
        } else {
            return $precioNeto;
        }
    }

    function _cotizadorCore_caducidad($idcotizacion, $renovacion) {
        $this->load->model("cotizadorCore_model");

        $dias = $this->cotizadorCore_model->getFechaCaducidadCotiz();
        $cotizacion = $this->cotizadorCore_model->getCotizacion($idcotizacion)[0];

        if ($renovacion == "0") {
            $fecha = $cotizacion["fecha"];
            $fecha_nueva = date("Y-m-d", strtotime($fecha . ' + ' . $dias . ' days'));
            $this->cotizadorCore_model->updateCotizacionCaducidad($idcotizacion, $fecha_nueva);
        } else if ($renovacion == "1") {
            $fecha = $cotizacion["caducidad"];
            $fecha_nueva = date("Y-m-d", strtotime($fecha . ' + ' . $dias . ' days'));
            $this->cotizadorCore_model->updateCotizacionCaducidad($idcotizacion, $fecha_nueva);
        } else {
            return false;
        }
    }

    public function pdf($idCotizacion) {
        error_reporting(0);
        $this->load->library('M_pdf');
        setlocale(LC_ALL, "es_MX");
        ini_set('memory_limit', '512M');
        set_time_limit(0);
        $this->load->model("cotizadorCore_model");
        $this->load->model("asesor_model");
        $this->load->model("cliente_model");
        $data['linkFamilias'] = $this->obtenerFamiliasAlianza();
        $data['linkSubFamilias'] = $this->obtenerSubFamiliasPaket();
        $data['linkCotizador'] = json_decode($this->index($idCotizacion));
         $cotizacion = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0];
         $data["cotizacion"] = $cotizacion;
        $data["out"] = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0]["json_out"];
        $data["file"] = $idCotizacion;
        $data["bancos"] = $this->cotizadorCore_model->getBancosCotizador();
        if($data["cotizacion"]["cliente_idcliente"]){
          $data["cliente"] = $this->cliente_model->getClienteID($data["cotizacion"]["cliente_idcliente"]);
        }else{
          $data["cliente"] = (array) json_decode($cotizacion["info_cliente"]);
        }
        $data["asesor"] = $this->asesor_model->getAsesorID($data["cotizacion"]["asesor_idasesor"]);

        $html = $this->load->view("cotizador/pdf/pdf", $data, true);
        $footer = $this->load->view("cotizador/pdf/footer", $data, true);
        $this->m_pdf->pdf->SetHTMLHeader('<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<table class="table" style="font-size:12px;">
								<thead>
									<tr style="margin:0 auto;">
										<td align="center">
											<img src="' . base_url('public/cotizador/Logo_Popusa.jpg') . '" style="width:150px;" />
										</td>
										<td align="center">
											<b>POPUSA S.A. de C.V.</b><br>
											23 Poniente No. 918 Col. Chulavista<br>
											C.P. 72420, Puebla, Puebla.<br>
											Tel: (222) 240 99 99 / www.popusa.com.mx
										</td>
										<td align="center">
											<img src="' . base_url('public/cotizador/Logo_Alianza.jpg') . '" style="width:150px;" />
										</td>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>');

        $this->m_pdf->pdf->SetHTMLFooter($footer, 1);
        $this->m_pdf->pdf->AddPage('P', '', '', '', '', 5, 5, 30, 5, 5, 0);
        $this->m_pdf->pdf->SetWatermarkImage(base_url('public/cotizador/1calidad_alianza.jpg'));
        $this->m_pdf->pdf->showWatermarkImage = true;
        $this->m_pdf->pdf->WriteHTML($html); // write the HTML into the PDF
        $this->m_pdf->pdf->showImageErrors = true;
        $this->m_pdf->pdf->Output('cotizacion.pdf', 'I');
    }


    public function pdf_pedido($idCotizacion){
      error_reporting(0);
      $this->load->library('M_pdf');
      setlocale(LC_ALL, "es_MX");
      ini_set('memory_limit', '512M');
      set_time_limit(0);
      $this->load->model("cotizadorCore_model");
      $this->load->model("asesor_model");
      $this->load->model("cliente_model");


      $data['linkFamilias'] = $this->obtenerFamiliasAlianza();
      $data['linkSubFamilias'] = $this->obtenerSubFamiliasPaket();
      $data['linkCotizador'] = json_decode($this->index($idCotizacion));
      
       $cotizacion = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0];
       $data["cotizacion"] = $cotizacion;
      $data["out"] = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0]["json_out"];
      $data['dir_entrega'] = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0]['dir_entrega'];


      $data['condiciones'] = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0]['condiciones'];

      $data['obervaciones'] = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0]['observaciones'];

      $data['lab'] = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0]['lab'];
      $data['asesor2'] = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0]['asesor'];

      $data["file"] = $idCotizacion;
      $data["bancos"] = $this->cotizadorCore_model->getBancosCotizador();

      if($data["cotizacion"]["cliente_idcliente"]){
        $data["cliente"] = $this->cliente_model->getClienteID($data["cotizacion"]["cliente_idcliente"]);
      }else{
        $data["cliente"] = (array) json_decode($cotizacion["info_cliente"]);
      }
      $data["asesor"] = $this->asesor_model->getAsesorID($data["cotizacion"]["asesor_idasesor"]);

      $html = $this->load->view("cotizador/pdf/pedido", $data, true);
      $footer = $this->load->view("cotizador/pdf/footer_pedido", $data, true);
      $this->m_pdf->pdf->SetHTMLHeader('<div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <table class="table" style="font-size:12px;">
              <thead>
                <tr style="margin:0 auto;">
                  <td align="center">
                    <img src="' . base_url('public/cotizador/Logo_Popusa.jpg') . '" style="width:150px;" /><br>
                    <b>www.popusa.com.mx</b>
                  </td>
                  <td align="center">
                    <b style="font-size:14px;">POPUSA S.A. de C.V.</b><br>
                    23 Poniente No. 918 Col. Chulavista<br>
                    C.P. 72420, Puebla, Puebla.<br>
                    Tel: (222) 240 99 99<br>
                    RFC: POP 031010US3
                  </td>
                  <td align="center">
                    <img src="' . base_url('public/cotizador/Logo_Alianza.jpg') . '" style="width:150px;" />
                  </td>
                  <td align="center">
                    <img src="' . base_url('public/cotizador/paket.jpg') . '" style="width:150px;" />
                  </td>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>');
      $this->m_pdf->pdf->SetHTMLFooter($footer, 1);
      $this->m_pdf->pdf->AddPage('P', '', '', '', '', 5, 5, 35, 5, 5, 0);
      $this->m_pdf->pdf->SetWatermarkImage(base_url('public/cotizador/Logo_Popusa.jpg'));
      $this->m_pdf->pdf->showWatermarkImage = true;
      $this->m_pdf->pdf->WriteHTML($html); // write the HTML into the PDF
      $this->m_pdf->pdf->showImageErrors = true;
      $this->m_pdf->pdf->Output('cotizacion.pdf', 'I');
    }

    public function p(){
     $this->load->view("cotizador/pdf/pedido");

    }

    public function debug_precio() {
        $this->load->library("form_validation");

        $this->form_validation->set_rules("codigo", "Codigo", "required");

        if ($this->form_validation->run() === FALSE) {
            $this->load->view("dev/debug_precio");
        }
        else {
            $codigo = $this->input->post("codigo");
            $unidad = $this->input->post("unidad");

            $precio = $this->_cotizadorCore_precioUnidad($codigo, $unidad);

            echo $precio;
        }

    }
}

?>

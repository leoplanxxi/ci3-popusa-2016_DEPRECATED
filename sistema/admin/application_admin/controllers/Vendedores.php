<?php

Class Vendedores extends MY_Controller {
    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata("logged_in")) {
            redirect("login");
        }
    }

    public function index() {
        $this->load->model("asesor_model"); 
        
        if ($this->session->userdata("logged_in")["rol_idrol"] == 1) {
            $data["asesores"] = $this->asesor_model->getAllAsesoresActivos();
            $this->load->view("vendedores/telemarketing_vendedores", $data);
        }
        else if ($this->session->userdata("logged_in")["rol_idrol"] == 4) {
            $data["asesores"] = $this->asesor_model->getAllAsesoresActivos();
            $this->load->view("vendedores/gerente_venta_vendedores", $data);
        }
        
    }
	
	public function informacion($idVendedor) {
		$this->load->model("asesor_model");
		$this->load->library("form_validation");
		
		$data["informacion"] = $this->asesor_model->getAsesorID($idVendedor);
		$data["login"] = $this->db->query("SELECT nombre FROM login WHERE idlogin = ?", array($data["informacion"]["login_idlogin"]))->result_array()[0]["nombre"];
		
		$this->load->view("vendedores/vendedores_info", $data);
		
	}
	
	public function clientes($idVendedor) {
		$this->load->model("asesor_model");
		$this->load->model("cliente_model");
		
		$data["clientes"] = $this->cliente_model->getAllClientesAsesor($idVendedor);
        $data["var1"] = true;
        
        $data["idVendedor"] = $idVendedor;
		$this->load->view("clientes/clientes_vendedor", $data);
	}
}
<?php
Class Log extends MY_Controller {
    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata("logged_in")) {
            redirect("login");
        }
    }

    public function index() {
        $this->load->model("log_model");

        $data["logs"] = $this->log_model->getAllLog();

        $this->load->view("logs/log_view", $data);
    }

    public function ajax() {
        $this->load->model("log_model");

        $logs = $this->log_model->getAllLog_alt();

        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $data = array();

        foreach ($logs->result() as $log) {
            $data[] = array(
                $log->idlog,
                $log->fecha,
                $log->severo,
                $log->tipo,
                $log->usr,
                $log->mensaje
            );
        }

        $output = array(
            "draw" => $draw,
              "recordsTotal" => $logs->num_rows(),
              "recordsFiltered" => $logs->num_rows(),
              "data" => $data
         );

         echo json_encode($output);
         exit();

    }
}
?>
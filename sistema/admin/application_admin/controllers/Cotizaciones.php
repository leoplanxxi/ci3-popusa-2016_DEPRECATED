<?php

Class Cotizaciones extends MY_Controller {

    public function __construct() {
        parent::__construct();
        /*if (!$this->session->userdata("logged_in")) {
            redirect("login");
        }*/
    }

    public function index() {
		//if ($this->session->userdata("logged_in")["rol_idrol"] == 3) redirect("cotizaciones/admin");

        $this->load->model("cotizadorCore_model");
        $this->load->model("asesor_model");


		$idusuario = $this->session->userdata("logged_in")["idusuario"];
		$idAsesor = $this->asesor_model->getAsesorLogin($idusuario)["idasesor"];

		if ($this->session->userdata("logged_in")["rol_idrol"] == 4) {
			$data["var1"] = true;
		}
		else if ($this->session->userdata("logged_in")["rol_idrol"] == 3) {
			$data["varAdm"] = true;
		}
		$data["cotizaciones"] = $this->cotizadorCore_model->getAllCotizacionesNuevoAsesor($idusuario);

        $data["idVendedor"] = $idusuario;
        $this->load->view("cotizaciones/cotizaciones_nuevas_vendedor", $data);
    }

    public function nuevasVendedor($idVendedor = NULL) {
        $this->load->model("cotizadorCore_model");
        $this->load->model("asesor_model");

        $data["idVendedor"] = $idVendedor;
        if ($this->session->userdata("logged_in")["rol_idrol"] == 3) {
			$data["varAdm"] = true;
		}
		if ($idVendedor != NULL) {
			$data["cotizaciones"] = $this->cotizadorCore_model->getAllCotizacionesNuevoAsesor($idVendedor);
		}
		else {
			$data["cotizaciones"] = $this->cotizadorCore_model->getAllCotizacionesNuevo();
		}

        $this->load->view("cotizaciones/cotizaciones_nuevas_vendedor", $data);
    }

    public function procesoVendedor($idVendedor = NULL) {
        $this->load->model("cotizadorCore_model");
        $this->load->model("asesor_model");

        $data["idVendedor"] = $idVendedor;
        if ($this->session->userdata("logged_in")["rol_idrol"] == 3) {
			$data["varAdm"] = true;
		}
		if ($idVendedor != NULL) {
			$data["cotizaciones"] = $this->cotizadorCore_model->getAllCotizacionesProcesoAsesor($idVendedor);
		}
		else {
			$data["cotizaciones"] = $this->cotizadorCore_model->getAllCotizacionesProceso();
		}


        $this->load->view("cotizaciones/cotizaciones_proceso_vendedor", $data);
    }

    public function rechazadasVendedor($idVendedor = NULL) {
        $this->load->model("cotizadorCore_model");
        $this->load->model("asesor_model");

        $data["idVendedor"] = $idVendedor;
        if ($this->session->userdata("logged_in")["rol_idrol"] == 3) {
			$data["varAdm"] = true;
		}
		if ($idVendedor != NULL) {
			$data["cotizaciones"] = $this->cotizadorCore_model->getAllCotizacionesRechazadasAsesor($idVendedor);
		}
		else {
			$data["cotizaciones"] = $this->cotizadorCore_model->getAllCotizacionesRechazadas();
		}

        $this->load->view("cotizaciones/cotizaciones_rechazadas_vendedor", $data);
    }


    public function proceso() {
        $this->load->model("cotizadorCore_model");
        $this->load->model("asesor_model");

		$idusuario = $this->session->userdata("logged_in")["idusuario"];
		$idAsesor = $this->asesor_model->getAsesorLogin($idusuario)["idasesor"];

		if ($this->session->userdata("logged_in")["rol_idrol"] == 4) {
			$data["var1"] = true;
		}
		$data["cotizaciones"] = $this->cotizadorCore_model->getAllCotizacionesProcesoAsesor($idusuario);

       $data["idVendedor"] = $idusuario;
        $this->load->view("cotizaciones/cotizaciones_proceso_vendedor", $data);
    }

    public function rechazadas() {
        $this->load->model("cotizadorCore_model");
        $this->load->model("asesor_model");

        $idusuario = $this->session->userdata("logged_in")["idusuario"];
		$idAsesor = $this->asesor_model->getAsesorLogin($idusuario)["idasesor"];
		$data["cotizaciones"] = $this->cotizadorCore_model->getAllCotizacionesRechazadasAsesor($idusuario);
		if ($this->session->userdata("logged_in")["rol_idrol"] == 4) {
			$data["var1"] = true;
        }

        $data["idVendedor"] = $idusuario;

        $this->load->view("cotizaciones/cotizaciones_rechazadas_vendedor", $data);
    }

    public function cotizacion($idCotizacion) {

        $this->load->model("cotizadorCore_model");
				//echo "hola";
        $this->session->unset_userdata('folio');
        $data["estado"] = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0]["estado"];
        $data["capturada"] = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0]["capturada_admin_venta"];
        $data["caducidad"] = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0]["caducidad"];
        $data["estado"] = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0]["estado"];
        $data["idCotizacion"] = $idCotizacion;

        $data["cotizacion"]  = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0];



        $idrol = $this->session->userdata("logged_in")["rol_idrol"];
        if ($idrol == 5) {
            $this->cotizadorCore_model->cambiarVistoAdminVenta($idCotizacion, 1);
        }

        /*if ($data["cotizacion"]["usuario_atiende"] == NULL) {
            $idusuario_login = $this->session->userdata("logged_in")["idusuario"];

            $this->cotizadorCore_model->setUsuarioAtiende($idCotizacion, $idusuario_login);
        }*/


        $this->load->view("cotizaciones/visor_cotizaciones_vendedor", $data);
    }

    public function aceptarCotizacion($idCotizacion) {
        $this->load->model("cotizadorCore_model");
        $this->load->model("log_model");
        $this->load->library("helper");
        //Guardar en log

        $datos_cotizacion = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0];
        $estado = $datos_cotizacion["estado"];

        switch ($estado) {
        case "0":
            $edo_inicial = "nueva";
            break;
        case "1":
            $edo_inicial = "en proceso";
            break;
        case "2":
            $edo_inicial = "aceptada";
            break;
        case "3":
            $edo_inicial = "rechazada";
            break;
        default:
            $edo_inicial = "no válido";
            break;
        }
        $args_log = array(
            'idusuario_ven' => $this->session->userdata("logged_in")["idusuario"],
            'nombre_ven' => $this->session->userdata("logged_in")["nombre"],
            'folio' => $idCotizacion,
            'edo_inicial' => $edo_inicial,
            'edo_final' => "aceptada",
        );

        $this->log_model->insertMsgLog("h", $args_log);
        $this->cotizadorCore_model->cambiarEstadoCotizacion($idCotizacion, 2);
        $this->cotizadorCore_model->cambiarVistoAdminVenta($idCotizacion, 0);

        //Notificar a Admins Venta
        $asesores_id = $this->db->query("SELECT idlogin FROM login WHERE rol_idrol = ?", array("5"))->result_array();

        foreach ($asesores_id as $asesor_id) {
            $asesor_idAsesor = $this->db->query("SELECT idasesor FROM asesor WHERE login_idlogin = ?", array($asesor_id))->result_array()[0]["idasesor"];
            $correo_asesor = $this->db->query("SELECT datos FROM asesor WHERE idasesor = ?", array($asesor_idAsesor))->result_array()[0]["datos"];
            $correo_asesor = explode("/",$correo_asesor);
            $correo_asesor = $correo_asesor[1];
            $correo_asesor = str_replace(" E-mail: ", "", $correo_asesor);
            $this->helper->enviarCorreoAdministrativo($correo_asesor, "c");
        }

		//$this->cotizadorCore_model->cambiarAsesorLive($idCotizacion);
        $this->session->set_flashdata("mensaje_positivo", "Se aceptó la cotización con folio: " . $idCotizacion);
        redirect("cotizaciones/cotizacion/" . $idCotizacion);
    }

    public function rechazarCotizacion($idCotizacion) {
        $this->load->model("cotizadorCore_model");
        $this->load->model("log_model");
        $this->load->library("form_validation");

        $this->form_validation->set_rules("idCotizacion", "ID Cotizacion", "required");
        $this->form_validation->set_rules("razones", "Observaciones", "required");

        if ($this->form_validation->run() == FALSE) {
            $data["idCotizacion"] = $idCotizacion;
            $this->load->view("cotizaciones/rechazar_cotizacion_vendedor", $data);
        } else {
            $razones = $this->secclean->limpiar($this->input->post("razones"));

            //Guardar en log

            $datos_cotizacion = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0];
            $estado = $datos_cotizacion["estado"];

            switch ($estado) {
                case "0":
                    $edo_inicial = "nueva";
                    break;
                case "1":
                    $edo_inicial = "en proceso";
                    break;
                case "2":
                    $edo_inicial = "aceptada";
                    break;
                case "3":
                    $edo_inicial = "rechazada";
                    break;
                default:
                    $edo_inicial = "no válido";
                    break;
            }
            $args_log = array(
                'idusuario_ven' => $this->session->userdata("logged_in")["idusuario"],
                'nombre_ven' => $this->session->userdata("logged_in")["nombre"],
                'folio' => $idCotizacion,
                'edo_inicial' => $edo_inicial,
                'edo_final' => "rechazada",
            );

            $this->log_model->insertMsgLog("h", $args_log);
            $this->cotizadorCore_model->cambiarEstadoCotizacion($idCotizacion, 3);
			//$this->cotizadorCore_model->cambiarAsesorLive($idCotizacion);
            $this->cotizadorCore_model->putRazonesCotizacion($idCotizacion, $razones);

            $this->session->set_flashdata("mensaje_positivo", "Se rechazó la cotización con folio: " . $idCotizacion);

            redirect("cotizaciones/cotizacion/" . $idCotizacion);
            redirect("cotizaciones/cotizacion/" . $idCotizacion);
        }
    }

	public function reiniciarCotizacion($idCotizacion)  {
        $this->load->model("cotizadorCore_model");
        $this->load->model("log_model");

         //Guardar en log

         $datos_cotizacion = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0];
         $estado = $datos_cotizacion["estado"];

         switch ($estado) {
             case "0":
                 $edo_inicial = "nueva";
                 break;
             case "1":
                 $edo_inicial = "en proceso";
                 break;
             case "2":
                 $edo_inicial = "aceptada";
                 break;
             case "3":
                 $edo_inicial = "rechazada";
                 break;
             default:
                 $edo_inicial = "no válido";
                 break;
         }
         $args_log = array(
             'idusuario_ven' => $this->session->userdata("logged_in")["idusuario"],
             'nombre_ven' => $this->session->userdata("logged_in")["nombre"],
             'folio' => $idCotizacion,
             'edo_inicial' => $edo_inicial,
             'edo_final' => "en proceso",
         );

         $this->log_model->insertMsgLog("h", $args_log);

		$this->_cotizadorCore_caducidad($idCotizacion, 1);
		$this->_cotizadorCore_estado($idCotizacion, 0);
		$this->cotizadorCore_model->reiniciarObservaciones($idCotizacion);
		$msg = "Se reinició el estado de la cotización con folio: " . $idCotizacion;
        $this->session->set_flashdata("mensaje_positivo", $msg);

        redirect("cotizaciones/cotizacion/" . $idCotizacion);
	}

    public function enviarCotizacionCliente($idCotizacion) {
		$this->load->model("cotizadorCore_model");
		$this->load->library("form_validation");

		$this->form_validation->set_rules("idCotizacion", "ID Cotizacion", "required");

		if ($this->form_validation->run() === FALSE) {
			$data["idCotizacion"] = $idCotizacion;

			$this->load->view("cotizaciones/mensaje_cotizacion_vendedor", $data);
		}
		else {
			$idCotizacion = $this->secclean->limpiar($this->input->post("idCotizacion"));
			$extra = $this->secclean->limpiar($this->input->post("msg_extra"));

			$this->session->set_flashdata("msg_extra", $extra);
			redirect("cotizaciones/sendCotizacion/" . $idCotizacion);
		}

    }

	public function sendCotizacion($idCotizacion) {
        $this->load->model("cotizadorCore_model");
        $this->load->model("log_model");

		$msg = $this->session->flashdata("msg_extra");

        $datos = $this->cotizadorCore_model->getCotizacion($idCotizacion);
		$idasesor = $datos[0]["asesor_idasesor"];
        $datos = $datos[0]["cliente_idcliente"];

        $correo = $this->db->query("SELECT email FROM cliente WHERE idcliente = ?", array($datos))->result_array()[0]["email"];
        $firma = $this->db->query("SELECT firma FROM asesor WHERE idasesor = ?", array($idasesor))->result_array()[0]["firma"];
        $correo_asesor = $this->db->query("SELECT datos FROM asesor WHERE idasesor = ?", array($idasesor))->result_array()[0]["datos"];
		$correo_asesor = explode('/', $correo_asesor);
		$correo_asesor = $correo_asesor[1];
		$correo_asesor = str_replace(" E-mail: ", "", $correo_asesor);

		if ($firma == NULL || $firma == "") {
			$img_firma = "#";
		}
		else {
			$img_firma = base_url() . "/public/usuarios/firmas/" . $firma . ".jpg";
		}

        // Envío de correo
		if ($msg != NULL) {
			/*<table style="width: 500px; margin: 0 auto; display: block; font-family: arial; font-size: 14px; line-height: 1.5;"><tr><td style="text-align: justify">Acá iría el mensaje que le pondría el vendedor</td></tr><tr><td>&nbsp;</td></tr><tr><td style="text-align: justify">Agradecemos de antemano la oportunidad que nos brinda y me permito enviarle, para su amable consideración, la siguiente cotización correspondiente a los productos de su interés.</td></tr><tr><td style="text-align: justify">Cualquier duda o comentario estoy para servirle.</td></tr><tr><td>&nbsp;</td></tr><tr><td style="text-align: center"><a href="#" style="font-size: 20px; font-weight: bold;" >Ver cotización</a></td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr><tr><td><img src="usuarios/firmas/test.jpg" style="width: 100%"></td></tr></table>*/

            $mensaje = '<table style="width: 500px; margin: 0 auto; display: block; font-family: arial; font-size: 14px; line-height: 1.5;"><tr><td>&nbsp;</td></tr><tr><td style="text-align: justify">Agradecemos de antemano la oportunidad que nos brinda y me permito enviarle, para su amable consideración, la siguiente cotización correspondiente a los productos de su interés.</td></tr><tr><td style="text-align: center">Si desea ponerse en contacto haga click en el siguiente enlace</td></tr><tr><td style="text-align: center; font-size: 26px"><a href="mailto:' . $correo_asesor .'">' . $correo_asesor . '</a></td></tr><tr><td style="text-align: justify">Cualquier duda o comentario estoy para servirle.</td></tr><tr><td>&nbsp;</td></tr><tr><td style="text-align: justify"><b>Nota del vendedor:</b> ' . $msg . '</td></tr><tr><td style="text-align: center"><a href="'. base_url("index.php/cotizador/pdf/" . $idCotizacion) .  '" style="font-size: 20px; font-weight: bold;" >Ver cotización</a></td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr><tr><td><img src="' . $img_firma .  '" style="width: 100%"></td></tr></table>';
		}
		else {
			$mensaje =  '<table style="width: 500px; margin: 0 auto; display: block; font-family: arial; font-size: 14px; line-height: 1.5;"><tr><td style="text-align: justify">&nbsp;</td></tr><tr><td>&nbsp;</td></tr><tr><td style="text-align: justify">Agradecemos de antemano la oportunidad que nos brinda y me permito enviarle, para su amable consideración, la siguiente cotización correspondiente a los productos de su interés.</td></tr><tr><td style="text-align: center">Si desea ponerse en contacto haga click en el siguiente enlace</td></tr><tr><td style="text-align: center; font-size: 26px"><a href="mailto:' . $correo_asesor .'">' . $correo_asesor . '</a></td></tr><tr><td style="text-align: justify">Cualquier duda o comentario estoy para servirle.</td></tr><tr><td>&nbsp;</td></tr><tr><td style="text-align: center"><a href="'. base_url("index.php/cotizador/pdf/" . $idCotizacion) .  '" style="font-size: 20px; font-weight: bold;" >Ver cotización</a></td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr><tr><td><img src="' . $img_firma . '" style="width: 100%"></td></tr></table>';

        }

      /*  $this->load->library('email');
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'mail.popusa.com.mx';
        $config['smtp_port'] = '587';
        $config['smtp_crypto'] = 'tls';
        $config['smtp_timeout'] = '5';
        $config['newline'] = "\r\n";
        $config['smtp_user'] = 'no-reply@popusa.com.mx';
        $config['smtp_pass'] = '00002018';
        $config['charset'] = 'utf-8';

        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not

        $this->email->initialize($config);
        $this->email->set_crlf("\r\n");

        $message = $mensaje;
        $this->email->clear();
        $this->email->from('no-reply@popusa.com.mx');
        $this->email->to($correo);
        $this->email->subject('Envío de cotización Popusa');
        $this->email->message($message);*/
    /*   $this->load->library('email');
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'smtp.office365.com';
        $config['smtp_port'] = '587';
        $config['smtp_crypto'] = 'tls';
        $config['smtp_timeout'] = '5';
        $config['newline'] = "\r\n";
        $config['smtp_user'] = 'alvaro.lopez@correo.buap.mx';
        $config['smtp_pass'] = 'MBSprinter91';
        $config['charset'] = 'utf-8';
*/
        $this->load->library('email');

        $config['protocol']    = 'mail';
        $config['charset']    = 'utf-8';

        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not

        $this->email->initialize($config);
        $this->email->set_crlf("\r\n");

        $message = $mensaje;
        $this->email->clear();
        $this->email->from('no-reply@popusa.com.mx');
        $this->email->to($correo);
        $this->email->subject('Envío de cotización Popusa');
        $this->email->message($message);
        //	$this->email->attach(base_url($var_correo[3]["valor"]));
//
        if ($this->email->send() === TRUE) {
			// Modificar contador y caducidad
            $contador_modificaciones = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0]["modificaciones"];
            $this->cotizadorCore_model->updateCotizacionContadorModif($idCotizacion, $contador_modificaciones + 1);
			$this->_cotizadorCore_caducidad($idCotizacion, 1);

            //Guardar en log  (cambio de estado)

            $datos_cotizacion = $this->cotizadorCore_model->getCotizacion($idCotizacion)[0];
            $estado = $datos_cotizacion["estado"];

            switch ($estado) {
                case "0":
                    $edo_inicial = "nueva";
                    break;
                case "1":
                    $edo_inicial = "en proceso";
                    break;
                case "2":
                    $edo_inicial = "aceptada";
                    break;
                case "3":
                    $edo_inicial = "rechazada";
                    break;
                default:
                    $edo_inicial = "no válido";
                    break;
            }
            $args_log = array(
                'idusuario_ven' => $this->session->userdata("logged_in")["idusuario"],
                'nombre_ven' => $this->session->userdata("logged_in")["nombre"],
                'folio' => $idCotizacion,
                'edo_inicial' => $edo_inicial,
                'edo_final' => "en proceso",
            );

            $this->log_model->insertMsgLog("h", $args_log);
			$this->cotizadorCore_model->resetCotizacion($idCotizacion);
            $this->cotizadorCore_model->cambiarEstadoCotizacion($idCotizacion, 1);
            //$this->cotizadorCore_model->cambiarAsesorLive($idCotizacion);

             //Guardar en log
             $args_log = array(
                'idusuario_ven' => $this->session->userdata("logged_in")["idusuario"],
                'nombre_ven' => $this->session->userdata("logged_in")["nombre"],
                'folio' => $idCotizacion,
                'email_cl' => $correo,
            );

            $this->log_model->insertMsgLog("e", $args_log);

            $this->session->set_flashdata("mensaje_positivo", "Se envío correctamente la cotización con folio " . $idCotizacion . " al correo " . $correo . ".");
        } else {
            $this->session->set_flashdata("mensaje_positivo", "Error al enviar la cotización con folio " . $idCotizacion . " al correo " . $correo . ".");
            $this->email->print_debugger();
        }

        redirect("cotizaciones/cotizacion/" . $idCotizacion);
	}

    public function capturarCotizacion($idCotizacion) {
        $this->load->model("cotizadorCore_model");

        $this->cotizadorCore_model->cambiarCapturadaAdminVenta($idCotizacion, 1);

        $this->session->set_flashdata("mensaje_positivo", "Se capturó la órden de venta con folio " . $idCotizacion);

        $this->load->model("log_model");
        $args_log = array(
            'folio' => $idCotizacion,
            'idusuario_adminven' => $this->session->userdata("logged_in")["idusuario"],
            'nombre_adminven' => $this->session->userdata("logged_in")["nombre"]
        );

        $this->log_model->insertMsgLog("i", $args_log);
        redirect("cotizaciones/cotizacion/" . $idCotizacion);
    }

	public function admin() {
		$this->load->view("admin_views/cotizaciones_view");
	}

	public function eliminar($idCotizacion) {

		$this->load->model("cotizadorCore_model");
    $this->load->model("estadisticas_model");

		$this->cotizadorCore_model->deleteCotizacion($idCotizacion);
    $this->estadisticas_model->delete($idCotizacion);

		$this->session->set_flashdata("mensaje", "Se eliminó con éxito la cotización con folio " . $idCotizacion);
		if ($this->session->userdata("logged_in")["rol_idrol"] == 3) {
            $q = $this->input->get("q");
            if (isset($q)) {

                $idVendedor = $this->input->get("idV");

                switch ($q) {
                    case "nuevas":
                        redirect("cotizaciones/nuevasVendedor/" . $idVendedor);
                        break;
                    case "proceso":
                        redirect("cotizaciones/procesoVendedor/" . $idVendedor);
                        break;
                    case "ordenes":
                        redirect("ordenes/ordenesVendedor/" . $idVendedor);
                        break;
                    case "rechazadas":
                        redirect("cotizaciones/rechazadasVendedor/" . $idVendedor);
                        break;
                    default:
                        break;
                }
            }
			else {
                redirect("cotizaciones/admin");
            }
		}
	}

    public function jobCotizacionesVencidas() {
        $this->load->model("log_model");

       // $query = "SELECT * FROM cotizaciones WHERE caducidad <= NOW() AND (estado != 2  OR estado != 3)";
        $query = "SELECT * FROM cotizaciones WHERE caducidad <= NOW() AND (estado != 2  AND estado != 3)";

        $resultado = $this->db->query($query);
        $resultado = $resultado->result_array();

        foreach ($resultado as $cotizacion) {
            $query = $this->db->query("UPDATE cotizaciones SET estado = 3 WHERE idcotizaciones = ?", array($cotizacion["idcotizaciones"]));
            $query = $this->db->query("UPDATE cotizaciones SET razones = 'Cotización vencida el día " . $cotizacion["caducidad"] . "' WHERE idcotizaciones = ?", array($cotizacion["idcotizaciones"]));

             //Guardar en log
             $args_log = array(
                'folio' => $cotizacion["idcotizaciones"],
            );

            $this->log_model->insertMsgLog("f", $args_log);
        }
    }

    function _cotizadorCore_caducidad($idcotizacion, $renovacion) {
        $this->load->model("cotizadorCore_model");

        $dias = $this->cotizadorCore_model->getFechaCaducidadCotiz();
        $cotizacion = $this->cotizadorCore_model->getCotizacion($idcotizacion)[0];

        if ($renovacion == "0") {
            $fecha = $cotizacion["fecha"];
            $fecha_nueva = date("Y-m-d", strtotime($fecha . ' + ' . $dias . ' days'));
            $this->cotizadorCore_model->updateCotizacionCaducidad($idcotizacion, $fecha_nueva);
        } else if ($renovacion == "1") {
            $fecha = $cotizacion["caducidad"];
            $fecha_nueva = date("Y-m-d", strtotime($fecha . ' + ' . $dias . ' days'));
            $this->cotizadorCore_model->updateCotizacionCaducidad($idcotizacion, $fecha_nueva);
        } else {
            return false;
        }
    }

	function _cotizadorCore_estado($idcotizacion, $estado) {
		$this->load->model("cotizadorCore_model");

		$this->cotizadorCore_model->cambiarEstadoCotizacion($idcotizacion, $estado);
	}

}

<?php
  Class Usuarios extends MY_Controller {
    public function __construct() {
	  parent::__construct();
	  if (!$this->session->userdata("logged_in")) {
		redirect("login");
	}
    }

    public function index() {
      $this->load->model("login_model");

      $data["usuarios"] = $this->login_model->getUsuariosView();
      $this->load->view("usuarios/main_admin.php", $data);
    }

    public function subirUsuarios() {

		  $this->load->view("usuarios/subir_xlsx.php");
    }

    public function guardar() {
      $this->load->model("configCore_model");

      $config['upload_path']   = config_item("archivos_usuarios");
  		$config['allowed_types'] = 'xls|xlsx|ods';
  		$this->load->library('upload', $config);
  		if(!$this->upload->do_upload('archivo')){
        $this->session->set_flashdata("msg", "Error al cargar archivo.<br>" . $this->upload->display_errors());
        redirect("usuarios/subirUsuarios");
  		}else{
        $nombre = $this->upload->data()['file_name'];
        $this->load->library('excel');
        $file = './public/usuarios/' . $nombre;

  			//read file from path
  			$objPHPExcel = PHPExcel_IOFactory::load($file);

  			//get only the Cell Collection
  			$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

  			//extract to a PHP readable array format
  			foreach ($cell_collection as $cell) {
  			    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
  			    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
  			    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

  			    //The header will/should be in row 1 only. of course, this can be modified to suit your need.
  			    if ($row == 1) {
  			        $header[$row][$column] = $data_value;
  			    } else {
  			        $arr_data[$row][$column] = $data_value;
  			    }
  			}

			  //send the data in an array format

			if (!isset($header)) {
				$data['header'] = $header;
				$this->session->set_flashdata("msg_s", "Error al analizar archivo. Compruebe que cumple con el formato establecido e intente nuevamente.");
        		redirect("usuarios");
			}
			else {
				if ($header[1]["A"] == "ROL" && $header[1]["B"] == "No. De identificación" && $header[1]["C"] == "NOMBRE DE USUARIO" && $header[1]["D"] == "PASSWOORD" && $header[1]["E"] == "NOMBRE" && $header[1]["F"] == "TELEFONO" && $header[1]["G"] == "CORREO ELECTRONICO" && $header[1]["H"] == "PRECIO QUE PUEDE VER" && $header[1]["I"] == "IMAGEN EN CORREO" && $header[1]["J"] == "LISTAS VIAJERA A VISUALIZAR" && $header[1]["K"] == "PDF CORRESPONDIENTES A CADA USUARIO") {
					//No hace nada, continua ejecucion
				}
				else {
					$this->session->set_flashdata("msg_s", "Error al analizar archivo. Compruebe que cumple con el formato establecido e intente nuevamente..");
					redirect("usuarios");
				}
			}
  			$data['values'] = $arr_data;
	
			$array_usuarios = $this->_parseSpreadsheetUsrs($arr_data);

  			if ($array_usuarios != false) {
				$this->load->model("configCore_model");

				$this->configCore_model->vaciarUsuarios();
				$i = 0;
				foreach ($array_usuarios as $usuario) {
					$this->configCore_model->insertarUsuarios($usuario);
					$i++;
				}
				$this->session->set_flashdata("msg_s", "Se cargaron correctamente " . $i . " usuarios.");
        		redirect("usuarios");
			}
			else {
				$this->session->set_flashdata("msg_s", "Error al analizar archivo. Compruebe que cumple con el formato establecido e intente nuevamente.");
        		redirect("usuarios");
			}



    }
  }


    public function file_check($str){
        $allowed_mime_type_arr = array('application/pdf','image/gif','image/jpeg','image/pjpeg','image/png','image/x-png');
        $mime = get_mime_by_extension($_FILES['file']['name']);
        if(isset($_FILES['file']['name']) && $_FILES['file']['name']!=""){
            if(in_array($mime, $allowed_mime_type_arr)){
                return true;
            }else{
                $this->form_validation->set_message('file_check', 'Please select only pdf/gif/jpg/png file.');
                return false;
            }
        }else{
            $this->form_validation->set_message('file_check', 'Please choose a file to upload.');
            return false;
        }
    }

    public function _parseSpreadsheetUsrs($arr_data) {
		$array_usuarios = array();
		$i = 0;

		foreach ($arr_data as $dato) {

			if ($dato["A"] == NULL) {
				continue;
			}

			// ROLES
			switch ($dato["A"]) {
				case 'ADMINISTRADOR':
					$rol_idrol = 3;
					# code...
					break;
				case 'VENDEDOR':
					$rol_idrol = 2;
					break;
				case 'GERENTE DE VENTAS':
					$rol_idrol = 4;
					break;
				case 'DISTRIBUIDOR':
					$rol_idrol = 6;
					break;
				case 'ADMINISTRACION DE VENTA CEDIS':
					$rol_idrol = 5;
					break;
				case 'ADMINISTRACION DE VENTA PLANTA':
  					$rol_idrol = 5;
  					break;
				case 'TELEMARKETING':
					$rol_idrol = 1;
					break;
				case "BONASA":
					$rol_idrol = 6; // FALTA AGREGAR ESTE ROL
					break;
				default:
					echo "mal<br>" . $i;
					die();
					# code...
					return false;
					$rol_idrol = NULL;
					break;
			}
			$array_usuarios[$i]["rol"] = $rol_idrol;
			if ($dato["B"] != NULL) $array_usuarios[$i]["id"] = $dato["B"];
			else return false;
			if ($dato["C"] != NULL) $array_usuarios[$i]["nombre_usuario"] = $dato["C"];
			else return false;
			if ($dato["D"] != NULL) $array_usuarios[$i]["passwd"] = $dato["D"];
			else return false;
			if ($dato["E"] != NULL) $array_usuarios[$i]["nombre"] = $dato["E"];
			else return false;
			if ($dato["F"] != NULL) $array_usuarios[$i]["telefono"] = $dato["F"];
			else return false;
			if ($dato["G"] != NULL) $array_usuarios[$i]["correo_electronico"] = $dato["G"];
			else return false;

			$array_usuarios[$i]["datos"] = "Tel. " . $dato["F"] . " / E-mail: " . $dato["G"];

			if ($dato["H"] != NULL) $array_usuarios[$i]["permisos"] = str_replace(" ", "", $dato["H"]);
			else return false;
			if ($dato["J"] != NULL) $array_usuarios[$i]["lv"] = $dato["J"];
			else return false;

			if (array_key_exists("I",$dato)) {
			  $array_usuarios[$i]["firma"] = $dato["I"];
			}
			else {
				$array_usuarios[$i]["firma"] = "";
			}
						
			if (array_key_exists("K",$dato)) {
			  $array_usuarios[$i]["pdf"] = $dato["K"];
			}
			else {
				$array_usuarios[$i]["pdf"] = "";
			}

			$i++;
		}
		return $array_usuarios;
    }
  }
 ?>

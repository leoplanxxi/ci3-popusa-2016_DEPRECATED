<?php
	Class Dev extends MY_Controller {
		public function __construct() {
			parent::__construct();
		}

		public function testPDF() {
			$this->load->library('Pdf');

			$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
			$pdf->SetTitle('My Title');
			$pdf->SetHeaderMargin(5);
			$pdf->SetTopMargin(5);
			$pdf->setFooterMargin(5);
			$pdf->SetAutoPageBreak(true);
			$pdf->SetAuthor('Author');
			$pdf->SetDisplayMode('real', 'default');

			$pdf->AddPage();

			$pdf->Write(5, 'Some sample text');
			$pdf->Output('My-File-Name.pdf', 'I');
		}


		public function php_info() {
			echo phpinfo();
		}

		public function testParseUsersXLS() {
			$file = './public/usuarios/usuarios.xlsx';
			//load the excel library
			$this->load->library('excel');

			//read file from path
			$objPHPExcel = PHPExcel_IOFactory::load($file);

			//get only the Cell Collection
			$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

			//extract to a PHP readable array format
			foreach ($cell_collection as $cell) {
			    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
			    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
			    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

			    //The header will/should be in row 1 only. of course, this can be modified to suit your need.
			    if ($row == 1) {
			        $header[$row][$column] = $data_value;
			    } else {
			        $arr_data[$row][$column] = $data_value;
			    }
			}

			//send the data in an array format
			$data['header'] = $header;
			$data['values'] = $arr_data;

			$array_usuarios = array();
			$i = 0;

			foreach ($arr_data as $dato) {

				// ROLES
				switch ($dato["A"]) {
					case 'ADMINISTRADOR':
						$rol_idrol = 3;
						# code...
						break;
					case 'VENDEDOR':
						$rol_idrol = 2;
						break;
					case 'GERENTE DE VENTA':
						$rol_idrol = 4;
						break;
					case 'DISTRIBUIDOR':
						$rol_idrol = 6;
						break;
					case 'ADMINISTRACION DE VENTA':
						$rol_idrol = 5;
						break;
					case 'TELEMARKETING':
						$rol_idrol = 1;
						break;
					default:
						# code...
						$rol_idrol = NULL;
						break;
				}
				$array_usuarios[$i]["rol"] = $rol_idrol;
				$array_usuarios[$i]["id"] = $dato["B"];
				$array_usuarios[$i]["nombre_usuario"] = $dato["C"];
				$array_usuarios[$i]["passwd"] = $dato["D"];
				$array_usuarios[$i]["nombre"] = $dato["E"];
				$array_usuarios[$i]["telefono"] = $dato["F"];
				$array_usuarios[$i]["correo_electronico"] = $dato["G"];
				$array_usuarios[$i]["datos"] = "Tel. " . $dato["F"] . " / E-mail: " . $dato["G"];
				$array_usuarios[$i]["permisos"] = str_replace(" ", "", $dato["H"]);
				$array_usuarios[$i]["firma"] = $dato["I"];
				$i++;
			}

			$this->load->model("configCore_model");

			$this->configCore_model->vaciarUsuarios();

			foreach ($array_usuarios as $usuario) {
				/*if ($this->configCore_model->checkUsrExists($usuario["id"])) {
					if (!$this->configCore_model->checkInfoSame($usuario["id"], $usuario)) {
						echo "se actualizo este registro: " . $usuario["nombre_usuario"] . "<br>";
						$this->configCore_model->actualizarUsuario($usuario, $usuario["id"]);
					}
					else {
						echo "este registro está al día: " . $usuario["nombre_usuario"] . "<br>";
					}
				}
				else {
					$this->configCore_model->insertarUsuarios($usuario);
					echo "se inserto el usuario: " . $usuario["nombre_usuario"] . "<br>";
				}*/
				$this->configCore_model->insertarUsuarios($usuario);

			}
		}

		public function fm() {
			$this->load->view("dev/fm");
		}

		function sendMail()
		{

            $this->load->library('email');

            $config['protocol']    = 'sendmail';

            $config['newline'] = "\r\n";
			$config['mailpath'] = "/usr/sbin/sendmail";
            $config['charset']    = 'utf-8';

            $config['mailtype'] = 'html'; // or html
            $config['validation'] = TRUE; // bool whether to validate email or not

            $this->email->initialize($config);
            $this->email->set_crlf( "\r\n" );

            $message = "hola";

            $this->email->clear();
            $this->email->from("noreply@popusa.com.mx");
            $this->email->to("alvaro.lopez@correo.buap.mx");
            $this->email->subject("Prueba");
            $this->email->message($message);

            if($this->email->send()===TRUE){
               echo "Enviado exitosamente<br />";
                return true;
            }
            else {
                echo "Error al enviar<br />";
                  $this->email->print_debugger();

                return false;
            }
		}

		function sendMailx()
		{

						$this->load->library('email');

						$config['protocol']    = 'mail';

						$config['charset']    = 'utf-8';

						$config['mailtype'] = 'html'; // or html
						$config['validation'] = TRUE; // bool whether to validate email or not

						$this->email->initialize($config);
						$this->email->set_crlf( "\r\n" );

						$message = "hola";

						$this->email->clear();
						$this->email->from("no-reply@popusa.com.mx");
						$this->email->to("alvaro.lopez@correo.buap.mx");
						$this->email->subject("Prueba");
						$this->email->message($message);

						if($this->email->send()===TRUE){
							 echo "Enviado exitosamente<br />";
								return true;
						}
						else {
								echo "Error al enviar<br />";
									$this->email->print_debugger();

								return false;
						}
		}

		function pruebai() {
			$i = 0;
			$j = 1;

			echo $i++;
			echo "<br>";
			echo $j;
			echo "<br>";
			echo ++$j;
			echo "<br>";
			echo $j;
			echo "<br>";
			echo $i;
		}

		public function updateDB_asesor() {
			$cotizaciones = $this->db->query("SELECT folio02 FROM cotizaciones")->result_array();

			foreach ($cotizaciones as $c) {
				$id = $c["folio02"];
				$aux = explode("-", $id);
				$login_idlogin = $aux[0];
				$this->db->query("UPDATE cotizaciones SET asesor_login = '" . $login_idlogin . "' WHERE folio02 = '" . $id . "';");
				
			}
		}
	}
?>

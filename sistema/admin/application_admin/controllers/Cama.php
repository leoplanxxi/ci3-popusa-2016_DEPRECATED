<?php
  Class Cama extends MY_Controller {
    public function __construct() {
      parent::__construct();
      if (!$this->session->userdata("logged_in")) {
        redirect("login");
      }
      $this->load->model("cargarlista_model");
      $this->load->helper('url');
      ini_set('memory_limit', '512M');
      set_time_limit(0);
    }

    public function index() {
      $this->load->view("cama/cargar");
    }

    public function guardar(){

    $config['upload_path'] = config_item('archivos');
    $config['allowed_types'] = 'xls|xlsx|ods|csv';
    //$config['allowed_types']        = 'gif|jpg|png|webp';
    $config['max_size'] = '10240';
    $this->load->library('upload',$config);
    $this->upload->initialize($config);
    if(!$this->upload->do_upload('productos')){
      //redirect('cargar/lista');
      //// echo "aqui estoy<br>".$this->upload->display_errors();
      $this->session->set_flashdata("msg", "Error al cargar archivo.<br>" . $this->upload->display_errors());
      redirect("cama");
      //var_dump($this->upload->data());
      //die();
    }else{
    $this->cargarlista_model->delete();
    $dataa = $this->upload->data();
    // echo "<pre>";
    var_dump($this->upload->data());
    // echo "</pre>";
    $nombre = $this->upload->data()['file_name'];
    // echo "nombre: ".$nombre."<br>";

    $this->load->library('excel');
		$archivo = config_item('archivos').$nombre;
		// echo $archivo;
		try{
			//// echo "despues de entrar al try";
			$tipoarchivo = PHPExcel_IOFactory::identify($archivo);
			// echo $tipoarchivo;

			$leerobjeto = PHPExcel_IOFactory::createReader($tipoarchivo);
      //$spreadsheetInfo = $leerobjeto->listWorksheetInfo($archivo);
			$objetoexcel = $leerobjeto->load($archivo);
		}catch(Exception $e){
			//die('<br>Hay un error leyendo el archivo "'.pathinfo($archivo,PATHINFO_BASENAME).'":<br>'.$e->getMessage());
      $this->session->set_flashdata("msg", 'Hay un error leyendo el archivo "'.pathinfo($archivo,PATHINFO_BASENAME).'":<br>'.$e->getMessage());
      redirect("cama");
		}

		$hoja = $objetoexcel->getSheet(0);//obtenemos la hoja 1.
		$filamaxima = $hoja->getHighestRow();//Obtenemos las filas
		$columnamaxima = $hoja->getHighestColumn() ;//Obtenemos las columnas.

    // echo "<br>filamaxima ".$filamaxima."<br>";
    // echo "columnamaxima ".$columnamaxima."<br>";
    //$filamaxima
    // echo "<pre>";
		for($fila = 2; $fila <= $filamaxima; $fila++){
      //

			$datosfila = $hoja->rangeToArray('A'.$fila.':'.$columnamaxima.$fila,NULL,TRUE,FALSE);
      $this->cargarlista_model->agregar($datosfila);
		}
    // echo "</pre>";

    // echo "se cargaron ".$fila." productos";
    $this->session->set_flashdata("msg", "Se cargaron " . $fila . " productos exitosamente");
    redirect("cama");
    }

  }
  }
 ?>

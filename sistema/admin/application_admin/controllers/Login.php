<?php

Class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        
        if ($this->session->userdata("logged_in")) {
            redirect("admin");
        }
        $this->load->model("login_model");
        $this->load->model("log_model");

        $this->form_validation->set_rules("nombre", "Nombre de Usuario", "required");
        $this->form_validation->set_rules("passwd", "Contraseña", "required");

        if ($this->form_validation->run() === FALSE) {
            $a = rand(1,20);
            $b = rand(1,20);

            $c = $a+$b;

            $data["num"] = array($a,$b,$c);
            $this->load->view("login/login", $data);
        } else {
            $nombre = $this->secclean->limpiar($this->input->post("nombre"));
            $passwd = $this->secclean->limpiar($this->input->post("passwd"));

            $a = $this->secclean->limpiar($this->input->post("a"));
            $b = $this->secclean->limpiar($this->input->post("b"));

            $c = $this->secclean->limpiar($this->input->post("c"));

            if ($a+$b != $c) {
                $msg = "CAPTCHA incorrecto. Vuelva a intentar.";
                $this->session->set_flashdata('msg', $msg);
                redirect("login");
            }

            $check = $this->login_model->checkLogin($nombre, $passwd);
            $this->login_model->get_user_hv($nombre, $passwd);

            if ($check) {
                $info = $this->login_model->getLoginInfo($nombre);

                $sess_data = array(
                    'idusuario' => $info["idlogin"],
                    'nombre' => $info["nombre"],
                    'rol_idrol' => $info["rol_idrol"]
                );

                $this->session->set_userdata("logged_in", $sess_data);

                // Guardar en log
                $args_log = array(
                    "idusuario" => $info["idlogin"],
                    "nombre" => $info["nombre"],
                );

                $this->log_model->insertMsgLog("b", $args_log);
                redirect("admin");
            } else {
                $msg = "El usuario o contraseña que proporcionó no es correcto. <br>Favor de verificarlo.<br> También verifique si su usuario se encuentra activo.";
                $this->session->set_flashdata('msg', $msg);
                redirect("login");
            }
        }
    }

    public function logout() {
        $this->load->model("log_model");

         // Guardar en log
         $args_log = array(
            "idusuario" => $this->session->userdata("logged_in")["idusuario"],
            "nombre" => $this->session->userdata("logged_in")["nombre"],
        );

        $this->log_model->insertMsgLog("k", $args_log);

        $this->session->set_userdata('logged_in', FALSE);
        $this->session->sess_destroy();
        redirect('login');
    }

    public function genPwd() {
        if (ENVIRONMENT == "development") {
            echo password_hash("Popusa2017!", PASSWORD_BCRYPT);
        } else {
            echo "No permitido";
        }
    }

}

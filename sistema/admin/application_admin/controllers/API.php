<?php

Class API extends CI_Controller {
    public function __construct() {
        parent::__construct();
        /*if (!$this->session->userdata("logged_in")) {
            redirect("login");
        }*/
    }

    public function notificarClienteNuevoTelemarketing() {
        $this->load->library("helper");

        $this->helper->enviarCorreoAdministrativo("telemarketing@popusa.com.mx", "b"); // REVISAR ESTO
    }

    public function notificarNuevaCotizacionCliente($idCotizacion) {
        $this->load->library("helper");

        $asesor_idAsesor = $this->db->query("SELECT asesor_idasesor FROM cotizaciones WHERE idcotizaciones = ?", array($idCotizacion))->result_array()[0]["asesor_idasesor"];
        $correo_asesor = $this->db->query("SELECT datos FROM asesor WHERE idasesor = ?", array($asesor_idAsesor))->result_array()[0]["datos"];
        $correo_asesor = explode("/",$correo_asesor);
        $correo_asesor = $correo_asesor[1];
        $correo_asesor = str_replace(" E-mail: ", "", $correo_asesor);

        $this->helper->enviarCorreoAdministrativo($correo_asesor, "d"); // REVISAR ESTO
    }

    public function notificarClienteCorreo($idCotizacion) {
        $this->load->library("helper");

        $cliente_idcliente = $this->db->query("SELECT cliente_idcliente FROM cotizaciones WHERE idcotizaciones = ?", array($idCotizacion))->result_array()[0]["cliente_idcliente"];
        $correo_cliente = $this->db->query("SELECT email FROM cliente WHERE idcliente = ?", array($cliente_idcliente))->result_array()[0]["email"];
      
        $this->helper->enviarCorreoConfirmacion($correo_cliente); // REVISAR ESTO
    }

    public function obtenerClientesVendedor($idVendedor) {
        $this->load->model("cliente_model");
        $this->load->model("asesor_model");

        $clientes = $this->cliente_model->getAllClientesAsesor($idVendedor);

        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $data = array();

        foreach ($clientes as $cliente) {

            $data[] = array(
                $cliente["razon"],
                $cliente["rfc"],
                '<a href="' . base_url('index.php/clientes/info/' . $cliente["idcliente"]). '" title="Ver datos de contacto"><i class="fa fa-address-book" aria-hidden="true"></i> Información</a>',
            );
        }

        $output = array(
            "draw" => $draw,
              "recordsTotal" => count($clientes),
              "recordsFiltered" => count($clientes),
              "data" => $data
         );

         echo json_encode($output);
         exit();
    }

    public function obtenerNuevosClientesTM() {
        $this->load->model("cliente_model");
        $this->load->model("asesor_model");

        $clientes = $this->cliente_model->getClientesNuevos();

        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $data = array();

        foreach ($clientes as $cliente) {

            $data[] = array(
                $cliente["razon"],
                $cliente["rfc"],
                '<a href="' . base_url('index.php/clientes/asignar_vendedor/' . $cliente["idcliente"]). '" title="Asignar un vendedor"><i class="fa fa-plus" aria-hidden="true"></i> Asignar vendedor</a><br>
                <a href="' . base_url('index.php/clientes/info/' . $cliente["idcliente"]) . '"><i class="fa fa-id-card-o"></i> Ver perfil de cliente</a>'
            );
        }

        $output = array(
            "draw" => $draw,
              "recordsTotal" => count($clientes),
              "recordsFiltered" => count($clientes),
              "data" => $data
         );

         echo json_encode($output);
         exit();
    }

    public function obtenerClientesAdmin() {
        $this->load->model("cliente_model");
        $this->load->model("asesor_model");

        $clientes = $this->cliente_model->getAllClientes_alt();

        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $data = array();

        foreach ($clientes->result() as $cliente) {
            $asesor_idasesor = $cliente->asesor_idasesor;
           /* echo "<pre>";
            var_dump($cliente);
            var_dump($asesor_idasesor);
            echo "</pre>"; */
            if ($asesor_idasesor != NULL) {
                $nombre_asesor = $this->asesor_model->getAsesorID($cliente->asesor_idasesor)["nombre"];
            } else {
                $nombre_asesor = "0 - No se ha asignado vendedor";
            }


            $data[] = array(
                $cliente->idcliente,
                $cliente->razon,
                $cliente->rfc,
                $cliente->email,
                $nombre_asesor,
                "<a href='" . base_url("index.php/clientes/editar_admin/" . $cliente->idcliente) . "'><i class='fa fa-id-card-o'></i> Editar cliente</a> <br> <a href='#' onclick='eliminar(" . $cliente->idcliente . ")'><i class='fa  fa-minus-circle'></i> Eliminar</a>",
            );
            // "<a href='" . base_url("index.php/clientes/editar_admin/" . $cliente->idcliente) . "'><i class='fa fa-id-card-o'></i> Editar cliente</a> <br> <a href='" . base_url("index.php/clientes/eliminar_admin/" . $cliente->idcliente) . "'><i class='fa  fa-minus-circle'></i> Eliminar</a>",
        }

        $output = array(
            "draw" => $draw,
              "recordsTotal" => $clientes->num_rows(),
              "recordsFiltered" => $clientes->num_rows(),
              "data" => $data
         );

         echo json_encode($output);
         exit();
    }

	public function obtenerTodasCotizacionesAdmin() {
        $this->load->model("cliente_model");
        $this->load->model("asesor_model");
		$this->load->model("cotizadorCore_model");

        $cotizaciones = $this->cotizadorCore_model->getAllCotizacionesActivas();

        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $data = array();

        foreach ($cotizaciones as $cotizacion) {
            $asesor_idasesor = $cotizacion["asesor_idasesor"];
			$cliente_idcliente = $cotizacion["cliente_idcliente"];

			$estado = $cotizacion["estado"];

			switch ($estado) {
				case "0":
					$estado = "Nueva";
					break;
				case "1":
					$estado = "En proceso";
					break;
				default:
					break;
			}

			//$nombre_cliente = $this->cliente_model->getClienteID($cliente_idcliente)["contacto"];
            //$nombre_asesor = $this->asesor_model->getAsesorID($asesor_idasesor)["nombre"];
            $nombre_cliente =  $cotizacion["info_cliente"];
            $nombre_cliente = json_decode($nombre_cliente);
            if (isset($nombre_cliente->nombre)) $nombre_cliente = $nombre_cliente->nombre;
            //$nombre_cliente = $nombre_cliente->nombre;

            $nombre_asesor = $cotizacion["asesor"];
            $nombre_asesor = explode("<br>", $nombre_asesor);
            $nombre_asesor = $nombre_asesor[0];

            $data[] = array(
                $cotizacion["idcotizaciones"],
                $cotizacion["fecha"],
                $nombre_cliente,
				$nombre_asesor,
                $estado,
                "<a href='" . base_url("index.php/cotizaciones/cotizacion/" . $cotizacion["idcotizaciones"] . "?ref=cotizaciones/admin") . "'><i class='fa fa-eye'></i> Ver cotización</a> <br> <a href='" . base_url("index.php/cotizaciones/eliminar/" . $cotizacion["idcotizaciones"]) . "' onClick='javascript:return confirm(\"¿Seguro que desea eliminar la cotización seleccionada?. Esta acción no puede deshacerse.\")'><i class='fa fa-minus-circle'></i> Eliminar cotización</a>",
            );
        }

        $output = array(
            "draw" => $draw,
              "recordsTotal" => count($cotizaciones),
              "recordsFiltered" => count($cotizaciones),
              "data" => $data
         );

         echo json_encode($output);
         exit();

    }

    public function obtenerOrdenesAdminVenta() {
        $this->load->model("cliente_model");
        $this->load->model("asesor_model");
		$this->load->model("cotizadorCore_model");

        $cotizaciones = $this->cotizadorCore_model->getAllOrdenes();

        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $data = array();

        foreach ($cotizaciones as $cotizacion) {
            $asesor_idasesor = $cotizacion["asesor_idasesor"];
			$cliente_idcliente = $cotizacion["cliente_idcliente"];

            $nombre_cliente = $cotizacion["info_cliente"];
            $nombre_cliente = json_decode($nombre_cliente);
            $nombre_cliente = $nombre_cliente->nombre;
		//	$nombre_asesor = $this->asesor_model->getAsesorID($asesor_idasesor)["nombre"];

            if ($cotizacion["capturada_admin_venta"] == 0) {
                $capturada = "No";
            }
            else {
                $capturada = "Si";
            }

            $url = '<a href="' . base_url("index.php/cotizaciones/cotizacion/" . $cotizacion["idcotizaciones"]) . '"><i class="fa fa-eye"></i> Ver pedido</a><br>';

            if ($cotizacion["visto_admin_venta"] != 1) {
                $folio = "<i class='fa fa-caret-right'></i> " . $cotizacion["folio02"];
            }
            else {
                $folio = $cotizacion["folio02"];
            }
            $data[] = array(
                $folio,
                $cotizacion["fecha"],
                $nombre_cliente,
                $capturada,
                $url,
            );
        }

        $output = array(
            "draw" => $draw,
              "recordsTotal" => count($cotizaciones),
              "recordsFiltered" => count($cotizaciones),
              "data" => $data
         );

         echo json_encode($output);
         exit();
    }

    public function obtenerOrdenesGerenteVenta() {
        $this->load->model("cliente_model");
        $this->load->model("asesor_model");
		$this->load->model("cotizadorCore_model");

        $cotizaciones = $this->cotizadorCore_model->getAllOrdenes();

        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $data = array();

        foreach ($cotizaciones as $cotizacion) {
            $asesor_idasesor = $cotizacion["asesor_idasesor"];
			$cliente_idcliente = $cotizacion["cliente_idcliente"];

            $nombre_cliente = $cotizacion["info_cliente"];
            $nombre_cliente = json_decode($nombre_cliente);
            $nombre_cliente = $nombre_cliente->nombre;
		//	$nombre_asesor = $this->asesor_model->getAsesorID($asesor_idasesor)["nombre"];

            if ($cotizacion["capturada_admin_venta"] == 0) {
                $capturada = "No";
            }
            else {
                $capturada = "Si";
            }

            $url = '<a href="' . base_url("index.php/cotizaciones/cotizacion/" . $cotizacion["idcotizaciones"]) . '"><i class="fa fa-eye"></i> Ver pedido</a><br>';

            if ($cotizacion["visto_admin_venta"] != 1) {
                $folio = "<i class='fa fa-caret-right'></i> " . $cotizacion["folio02"];
            }
            else {
                $folio = $cotizacion["folio02"];
            }
            $data[] = array(
                $folio,
                $cotizacion["fecha"],
                $nombre_cliente,
                $capturada,
                $url,
            );
        }

        $output = array(
            "draw" => $draw,
              "recordsTotal" => count($cotizaciones),
              "recordsFiltered" => count($cotizaciones),
              "data" => $data
         );

         echo json_encode($output);
         exit();
    }

    public function obtenerCotizacionesActivasVendedor($idVendedor, $tipo, $varAdm = NULL) {
        $this->load->model("cliente_model");
        $this->load->model("asesor_model");
        $this->load->model("cotizadorCore_model");

        if ($tipo == "Nuevas") {
            $cotizaciones = $this->cotizadorCore_model->getAllCotizacionesNuevoAsesor($idVendedor);
        }
        else if ($tipo == "Proceso") {
            $cotizaciones = $this->cotizadorCore_model->getAllCotizacionesProcesoAsesor($idVendedor);
        }

        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $data = array();

        foreach ($cotizaciones as $cotizacion) {
            $asesor_idasesor = $cotizacion["asesor_idasesor"];
			$cliente_idcliente = $cotizacion["cliente_idcliente"];

            $nombre_cliente = $cotizacion["info_cliente"];
            $nombre_cliente = json_decode($nombre_cliente);
            $nombre_cliente = $nombre_cliente->nombre;
		//	$nombre_asesor = $this->asesor_model->getAsesorID($asesor_idasesor)["nombre"];

            if ($tipo == "Nuevas") {
                if ($varAdm == "1") {
                    $url =  "<a href='" . base_url("index.php/cotizaciones/cotizacion/" . $cotizacion  ["idcotizaciones"]) . "?ref=cotizaciones/nuevasVendedor/" . $idVendedor . "'><i class='fa fa-eye'></i> Ver cotización</a><br>";
                }
                else {
                    $url = "<a href='" . base_url("index.php/cotizaciones/cotizacion/" . $cotizacion  ["idcotizaciones"]) . "'><i class='fa fa-eye'></i> Ver cotización</a><br>";
                }
                if ($this->session->userdata("logged_in")["rol_idrol"] == 3) {
                    $url = $url . "<a href='" .  base_url("index.php/cotizaciones/eliminar/" . $cotizacion["idcotizaciones"]) . "?q=ordenes&idV=3' onClick='javascript:return confirm(\"¿Seguro que desea eliminar la cotización seleccionada?. Esta acción no puede deshacerse.\")'><i class='fa fa-minus-circle'></i> Eliminar cotización</a>";
                }
            }
            else if ($tipo == "Proceso") {
                if ($varAdm == "1") {
                    $url =  "<a href='" . base_url("index.php/cotizaciones/cotizacion/" . $cotizacion  ["idcotizaciones"]) . "?ref=cotizaciones/procesoVendedor/" . $idVendedor . "'><i class='fa fa-eye'></i> Ver cotización</a><br>";
                }
                else {
                    $url = "<a href='" . base_url("index.php/cotizaciones/cotizacion/" . $cotizacion  ["idcotizaciones"]) . "'><i class='fa fa-eye'></i> Ver cotización</a><br>";
                }
                if ($this->session->userdata("logged_in")["rol_idrol"] == 3) {
                    $url = $url . "<a href='" .  base_url("index.php/cotizaciones/eliminar/" . $cotizacion["idcotizaciones"]) . "?q=ordenes&idV=3' onClick='javascript:return confirm(\"¿Seguro que desea eliminar la cotización seleccionada?. Esta acción no puede deshacerse.\")'><i class='fa fa-minus-circle'></i> Eliminar cotización</a>";
                }
            }


            if ($this->session->userdata("logged_in")["rol_idrol"] == 4) {
                $data[] = array(
                    $cotizacion["idcotizaciones"],
                    $cotizacion["fecha"],
                    $nombre_cliente,
                    $cotizacion["caducidad"],
                    $cotizacion["modificaciones"],
                    $url,
                );
            }
            else {
                $data[] = array(
                    $cotizacion["idcotizaciones"],
                    $cotizacion["fecha"],
                    $nombre_cliente,
                    $url,
                );
            }

        }

        $output = array(
            "draw" => $draw,
              "recordsTotal" => count($cotizaciones),
              "recordsFiltered" => count($cotizaciones),
              "data" => $data
         );

         echo json_encode($output);
         exit();
    }

    public function obtenerCotizacionesInactivasVendedor($idVendedor, $varAdm = NULL) {
        $this->load->model("cliente_model");
        $this->load->model("asesor_model");
		$this->load->model("cotizadorCore_model");

        $cotizaciones = $this->cotizadorCore_model->getAllCotizacionesRechazadasAsesor($idVendedor);

        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $data = array();

        foreach ($cotizaciones as $cotizacion) {
            $asesor_idasesor = $cotizacion["asesor_idasesor"];
			$cliente_idcliente = $cotizacion["cliente_idcliente"];

            $nombre_cliente = $cotizacion["info_cliente"];
            $nombre_cliente = json_decode($nombre_cliente);
            $nombre_cliente = $nombre_cliente->nombre;
		//	$nombre_asesor = $this->asesor_model->getAsesorID($asesor_idasesor)["nombre"];
            if ($varAdm == "1") {
                $url = '<a href="' . base_url("index.php/cotizaciones/cotizacion/" . $cotizacion["idcotizaciones"] . "?ref=ordenes/rechazadasVendedor/" . $idVendedor). '"><i class="fa fa-eye"></i> Ver cotización</a><br>';
            }
            else {
                $url = '<a href="' . base_url("index.php/cotizaciones/cotizacion/" . $cotizacion["idcotizaciones"]) . '"><i class="fa fa-eye"></i> Ver cotización</a><br>';
            }

            if ($this->session->userdata("logged_in")["rol_idrol"] == 3) {
                $url = $url . "<a href='" .  base_url("index.php/cotizaciones/eliminar/" . $cotizacion["idcotizaciones"]) . "?q=ordenes&idV=3' onClick='javascript:return confirm(\"¿Seguro que desea eliminar la cotización seleccionada?. Esta acción no puede deshacerse.\")'><i class='fa fa-minus-circle'></i> Eliminar cotización</a>";
            }
            $data[] = array(
                $cotizacion["idcotizaciones"],
                $cotizacion["fecha"],
                $nombre_cliente,
                $cotizacion["razones"],
                $url,
            );
        }

        $output = array(
            "draw" => $draw,
              "recordsTotal" => count($cotizaciones),
              "recordsFiltered" => count($cotizaciones),
              "data" => $data
         );

         echo json_encode($output);
         exit();
    }


    public function obtenerOrdenesVendedor($idVendedor, $varAdm = NULL) {
        $this->load->model("cliente_model");
        $this->load->model("asesor_model");
		$this->load->model("cotizadorCore_model");

        $cotizaciones = $this->cotizadorCore_model->getAllOrdenesVendedor($idVendedor);

        /*foreach($cotizaciones as $idc){

          shell_exec("curl http://132.148.18.129/sistema-popusa/admin/index.php/cotizador/index/" . $idc['idcotizaciones']);
        }*/

        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $data = array();

        foreach ($cotizaciones as $cotizacion) {
            $asesor_idasesor = $cotizacion["asesor_idasesor"];
			$cliente_idcliente = $cotizacion["cliente_idcliente"];

            $nombre_cliente = $cotizacion["info_cliente"];
            $nombre_cliente = json_decode($nombre_cliente);
            $nombre_cliente = $nombre_cliente->nombre;
		//	$nombre_asesor = $this->asesor_model->getAsesorID($asesor_idasesor)["nombre"];

            if ($cotizacion["capturada_admin_venta"] == 0) {
                $capturada = "No";
            }
            else {
                $capturada = "Si";
            }

            if ($varAdm == "1") {
                $url = '<a href="' . base_url("index.php/cotizaciones/cotizacion/" . $cotizacion["idcotizaciones"] . "?ref=ordenes/ordenesVendedor/" . $idVendedor). '"><i class="fa fa-eye"></i> Ver pedido</a><br>';
            }
            else {
                $url = '<a href="' . base_url("index.php/cotizaciones/cotizacion/" . $cotizacion["idcotizaciones"]) . '"><i class="fa fa-eye"></i> Ver pedido</a><br>';
            }

            if ($this->session->userdata("logged_in")["rol_idrol"] == 3) {
                $url = $url . "<a href='" .  base_url("index.php/cotizaciones/eliminar/" . $cotizacion["idcotizaciones"]) . "?q=ordenes&idV=3' onClick='javascript:return confirm(\"¿Seguro que desea eliminar la cotización seleccionada?. Esta acción no puede deshacerse.\")'><i class='fa fa-minus-circle'></i> Eliminar pedido</a>";
            }

            $data[] = array(
                $cotizacion["folio02"],
                $cotizacion["fecha"],
                $nombre_cliente,
                $capturada,
                $url,
            );
        }

        $output = array(
            "draw" => $draw,
              "recordsTotal" => count($cotizaciones),
              "recordsFiltered" => count($cotizaciones),
              "data" => $data
         );

         echo json_encode($output);
         exit();
    }

    public function obtenerOrdenesDistribuidor($login_idlogin) {
        $this->load->model("cliente_model");
        $this->load->model("asesor_model");
		$this->load->model("cotizadorCore_model");

        $cotizaciones = $this->cotizadorCore_model->getAllOrdenesDistribuidor($login_idlogin);

        /*foreach($cotizaciones as $idc){

          shell_exec("curl http://132.148.18.129/sistema-popusa/admin/index.php/cotizador/index/" . $idc['idcotizaciones']);
        }*/

        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $data = array();

        foreach ($cotizaciones as $cotizacion) {
            $asesor_idasesor = $cotizacion["asesor_idasesor"];
			$cliente_idcliente = $cotizacion["cliente_idcliente"];

            $nombre_cliente = $cotizacion["info_cliente"];
            $nombre_cliente = json_decode($nombre_cliente);
            $nombre_cliente = $nombre_cliente->nombre;
		//	$nombre_asesor = $this->asesor_model->getAsesorID($asesor_idasesor)["nombre"];

            if ($cotizacion["capturada_admin_venta"] == 0) {
                $capturada = "No";
            }
            else {
                $capturada = "Si";
            }

                $url = '<a href="' . base_url("index.php/cotizaciones/cotizacion/" . $cotizacion["idcotizaciones"]) . '"><i class="fa fa-eye"></i> Ver pedido</a><br>';

            $data[] = array(
                $cotizacion["folio02"],
                $cotizacion["fecha"],
                $nombre_cliente,
                $capturada,
                $url,
            );
        }

        $output = array(
            "draw" => $draw,
              "recordsTotal" => count($cotizaciones),
              "recordsFiltered" => count($cotizaciones),
              "data" => $data
         );

         echo json_encode($output);
         exit();
    }

	public function obtenerTodasOrdenesAdmin() {
        $this->load->model("cliente_model");
        $this->load->model("asesor_model");
		$this->load->model("cotizadorCore_model");

        $cotizaciones = $this->cotizadorCore_model->getAllOrdenes();

        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $data = array();

        foreach ($cotizaciones as $cotizacion) {
            $asesor_idasesor = $cotizacion["asesor_idasesor"];
			$cliente_idcliente = $cotizacion["cliente_idcliente"];

			//$nombre_cliente = $this->cliente_model->getClienteID($cliente_idcliente)["contacto"];
            //$nombre_asesor = $this->asesor_model->getAsesorID($asesor_idasesor)["nombre"];
            $nombre_cliente =  $cotizacion["info_cliente"];
            $nombre_cliente = json_decode($nombre_cliente);
            $nombre_cliente = $nombre_cliente->nombre;

            $nombre_asesor = $cotizacion["asesor"];
            $nombre_asesor = explode("<br>", $nombre_asesor);
            $nombre_asesor = $nombre_asesor[0];

            if ($cotizacion["capturada_admin_venta"] == 1)
            {
                $capturada = "Si";
                $link =  "<a href='" . base_url("index.php/cotizaciones/cotizacion/" . $cotizacion["idcotizaciones"]) . "'><i class='fa fa-eye'></i> Ver pedido</a>";
            } else {
                $capturada = "No";
                $link =  "<a href='" . base_url("index.php/cotizaciones/cotizacion/" . $cotizacion["idcotizaciones"]) . "'><i class='fa fa-eye'></i> Ver pedido</a> <br> <a href='" . base_url("index.php/ordenes/eliminar/" . $cotizacion["idcotizaciones"]) . "' onClick='javascript:return confirm(\"¿Seguro que desea eliminar el pedido seleccionado?. Esta acción no puede deshacerse.\")'><i class='fa fa-minus-circle'></i> Eliminar pedido</a>";
            }

            $data[] = array(
                $cotizacion["folio02"],
                $cotizacion["fecha"],
                $nombre_cliente,
				$nombre_asesor,
                $capturada,
                $link,
            );
        }

        $output = array(
            "draw" => $draw,
              "recordsTotal" => count($cotizaciones),
              "recordsFiltered" => count($cotizaciones),
              "data" => $data
         );

         echo json_encode($output);
         exit();

    }

    public function tablaCotizaciones() {
      $this->load->model("cotizadorCore_model");
      // Para cotizaciones
      // Fecha Codigo_producto Desc_producto Cantidad Estado CP Folio Cliente CURP

      $cotiz = $this->db->query("SELECT * FROM estadisticasTablaCotiz")->result_array();

      $array_salida = array();
      $i = 0;

      foreach ($cotiz as $c) {

        $datos_cotiz = $this->db->query("SELECT * FROM cotizaciones WHERE idcotizaciones = ?", array($c["idcotizacion"]))->result_array()[0];
        $datos_cliente = $this->db->query("SELECT * FROM cliente WHERE idcliente = ?", array($c["idcliente"]))->result_array()[0];

        $array_salida[$i]["fecha"] = $datos_cotiz["fecha"];
        $array_salida[$i]["codigo_producto"] = $c["idproductos"];
        $array_salida[$i]["desc_producto"] = $this->cotizadorCore_model->getDescripcionesPDF($c["idproductos"])["descotizador"];

        $json_in = $datos_cotiz["json_in"];
        $json_in = json_decode($json_in, true);

        $array_salida[$i]["cantidad"] = $json_in[$c["idproductos"]]["cantidad"];
        $array_salida[$i]["estado"] = $datos_cliente["estado"];
        $array_salida[$i]["codigo_postal"] = $datos_cliente["codigo_postal"];
        $array_salida[$i]["folio"] = $datos_cotiz["idcotizaciones"];
        $array_salida[$i]["cliente"] = $datos_cliente["contacto"];
        $array_salida[$i]["curp"] = $datos_cliente["rfc"];

        $i++;

      }

      header('Content-Type: application/json');

      echo json_encode($array_salida);
    }

    public function tablaOrdenes() {
      $this->load->model("cotizadorCore_model");
      //Para órdenes de pedido
      // Fecha Codigo_producto Desc_producto Vendedor Precio Cantidad Estado CP Folio Cliente CURP

            $cotiz = $this->db->query("SELECT * FROM estadisticasTablaOrdenes")->result_array();

            $array_salida = array();
            $i = 0;

            foreach ($cotiz as $c) {

              $datos_cotiz = $this->db->query("SELECT * FROM cotizaciones WHERE idcotizaciones = ?", array($c["idcotizacion"]))->result_array()[0];
              if ($c["idcliente"] != 0) {
                $datos_cliente = $this->db->query("SELECT * FROM cliente WHERE idcliente = ?", array($c["idcliente"]))->result_array()[0];
              }
              else {
                $datos_cliente["contacto"] = substr($datos_cotiz["asesor"], 0, strpos($datos_cotiz["asesor"], "<br>"));
                $datos_cliente["estado"] = " ";
                $datos_cliente["codigo_postal"] = " ";
                $datos_cliente["rfc"] = " ";
              }



              $array_salida[$i]["fecha"] = $datos_cotiz["fecha"];
              $array_salida[$i]["codigo_producto"] = $c["idproductos"];
              $array_salida[$i]["desc_producto"] = $this->cotizadorCore_model->getDescripcionesPDF($c["idproductos"])["descotizador"];

              $json_in = $datos_cotiz["json_in"];
              $json_in = json_decode($json_in, true);

              $vendedor =  substr($datos_cotiz["asesor"], 0, strpos($datos_cotiz["asesor"], "<br>"));

              $array_salida[$i]["vendedor"] = $vendedor;

              if ($c["precio"] == "3") { $c["precio"] = "L"; }
              $array_salida[$i]["precio"] = "P" . $c["precio"];
              $array_salida[$i]["cantidad"] = $json_in[$c["idproductos"]]["cantidad"];
              $array_salida[$i]["estado"] = $datos_cliente["estado"];
              $array_salida[$i]["codigo_postal"] = $datos_cliente["codigo_postal"];
              $array_salida[$i]["folio"] = $datos_cotiz["folio02"];
              $array_salida[$i]["cliente"] = $datos_cliente["contacto"];
              $array_salida[$i]["curp"] = $datos_cliente["rfc"];

              $i++;

            }

            header('Content-Type: application/json');

            echo json_encode($array_salida);

    }


}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Helper {

    public function __construct() {
        $this->CI =& get_instance();
    }

    public function enviarCorreoConfirmacion($correo) {

        $cc = "leoplanxxi@gmail.com"; // Asignar a correos reales

        $message = '<table style="width: 500px; margin: 0 auto; display: block; font-family: arial; font-size: 14px; line-height: 1.5;"><tr><td style="text-align: justify">&nbsp;</td></tr><tr><td>&nbsp;</td></tr><tr><td style="text-align: justify">Estimada/o cliente:</td></tr><tr><td>&nbsp;</td></tr><tr><td style="text-align: justify">Su cotización ha sido enviada y a la brevedad un vendedor se comunicará con usted.</td></tr><tr><td>&nbsp;</td></tr><tr><td style="text-align: justify">Grupo Industrial Popusa le agradece su preferencia.</td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr></table>';

        $this->CI->load->library('email');
        $config['protocol']    = 'mail';
        $config['charset']    = 'utf-8';

        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not

        $this->CI->email->initialize($config);
        $this->CI->email->set_crlf("\r\n");

        $this->CI->email->clear();
        $this->CI->email->from('no-reply@popusa.com.mx');
        $this->CI->email->to($correo);
		$this->CI->email->cc($cc);
        $this->CI->email->subject('Grupo Industrial Popusa le agradece su preferencia');
        $this->CI->email->message($message);

        if ($this->CI->email->send() === TRUE) {
			return true;
        } else {
            return false;
        }
    }
    
    public function enviarCorreoAdministrativo($correo, $dmsg) {
        /* Valores de dmsg
            a: Asignación de Telemarketing a Vendedor de nuevo cliente
            b: Registro de nuevo cliente
            c: Realización de una órden de pedido
            d: Nueva cotización de cliente
        */

        $nombre_usuario_admin = $this->CI->db->query("SELECT nombre FROM asesor WHERE datos LIKE '%" . $correo . "'")->result_array()[0]["nombre"];

        $cc = "leoplanxxi@gmail.com"; // Asignar a correos reales

        switch ($dmsg) {
            case "a":
                $msg = '<tr><td style="text-align: justify">Se le notifica que telemarketing le ha asignado un nuevo usuario.</td></tr><tr><td style="text-align: justify">Para más información acceda al sistema, a la sección Clientes:</td></tr>';
                break;
            case "b":
                $msg = '<tr><td>&nbsp;</td></tr><tr><td style="text-align: justify">Se le notifica que se ha registrado un nuevo cliente en la plataforma.</td></tr><tr><td style="text-align: justify">Para más información acceda al sistema, a la sección Clientes Nuevos:</td></tr>';
                break;
            case "c":
                $msg = '<tr><td>&nbsp;</td></tr><tr><td style="text-align: justify">Se le notifica que se ha registrado una nueva órden para ser capturada en AdPro.</td></tr>
                <tr><td style="text-align: justify">Para más información acceda al sistema:</td></tr>';
                break;
            case "d":
                $msg = '<tr><td>&nbsp;</td></tr><tr><td style="text-align: justify">Se le notifica que se ha registrado una nueva solicitud de cotización en la plataforma.</td></tr><tr><td style="text-align: justify">Para más información acceda al sistema, a la sección Cotizaciones:</td></tr>';
                break;
            case "e":
                $msg = '<tr><td>&nbsp;</td></tr><tr><td style="text-align: justify">Se le notifica que ha caducado una cotización en la plataforma.</td></tr><tr><td style="text-align: justify">Para más información acceda al sistema, a la sección Cotizaciones:</td></tr>';
                break;
            default:
                break;
        }

        $message = '<table style="width: 500px; margin: 0 auto; display: block; font-family: arial; font-size: 14px; line-height: 1.5;"><tr><td style="text-align: justify">&nbsp;</td>     </tr><tr><td>&nbsp;</td></tr><tr><td style="text-align: justify">Estimada/o ' . $nombre_usuario_admin . ':</td></tr>' . $msg . '<tr><td>&nbsp;</td></tr><tr><td style="text-align: center"><a href=" ' . base_url() . '" style="font-size: 20px; font-weight: bold;" >Abrir sistema administrador</a></td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr></table>';

        /*$this->CI->load->library('email');
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'smtp.office365.com';
        $config['smtp_port'] = '587';
        $config['smtp_crypto'] = 'tls';
        $config['smtp_timeout'] = '5';
        $config['newline'] = "\r\n";
        $config['smtp_user'] = 'alvaro.lopez@correo.buap.mx';
        $config['smtp_pass'] = 'MBSprinter91';
        $config['charset'] = 'utf-8';
*/
        $this->CI->load->library('email');
        $config['protocol']    = 'mail';
        $config['charset']    = 'utf-8';

        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not

        $this->CI->email->initialize($config);
        $this->CI->email->set_crlf("\r\n");

        $this->CI->email->clear();
        $this->CI->email->from('no-reply@popusa.com.mx');
        $this->CI->email->to($correo);
		$this->CI->email->cc($cc);
        $this->CI->email->subject('Mensaje administrativo Popusa');
        $this->CI->email->message($message);

        if ($this->CI->email->send() === TRUE) {
			return true;
        } else {
            return false;
        }
    }
}

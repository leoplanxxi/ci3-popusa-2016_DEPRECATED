<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Secclean {

    public function __construct() {
        $this->CI =& get_instance();
        $this->CI->load->helper("security");
        $this->CI->load->database();
    }

    public function limpiar($arg) {
        $aux = array("'",'"',"&&","||","=","[removed]",";","\\","onclick","$","<",">","php","PHP","?");

        $data = strip_tags(trim($this->CI->security->xss_clean($arg)));
        $data = $this->CI->db->escape_str($data);
        $data = str_replace($aux, "", $data);
        $data = $this->CI->db->escape_str($data);

        return $data;
    }

    public function limpiarwoHTML($arg) {
        $arg = $this->CI->security->xss_clean($arg);

        $arg = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $arg);
        $arg = str_replace("[removed]", "", $arg);

        return $arg;
    }

}
